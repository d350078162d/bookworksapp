//
//  ACPageViewController.h
//  ACReuseQueue
//
//  Created by Arnaud Coomans on 24/01/14.
//  Copyright (c) 2014 Arnaud Coomans. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ACPageViewController : UIPageViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>


- (void)setCurrentPageIndex:(NSInteger)bindex;
- (void)setBookAllPages:(NSInteger)all_page andCurReadPage:(NSInteger)read_page;
- (void)setCurrentPageInfoWithChapter:(NSInteger)chap andPage:(NSInteger)page;
- (void)setCurrentPage;

@end
