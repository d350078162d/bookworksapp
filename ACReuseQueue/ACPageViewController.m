//
//  ACPageViewController.m
//  ACReuseQueue
//
//  Created by Arnaud Coomans on 24/01/14.
//  Copyright (c) 2014 Arnaud Coomans. All rights reserved.
//

#import "ACPageViewController.h"

#import "BKPageContent.h"



@interface ACPageViewController () {
    
//    BKPageContent *contentDetail,*contentLeft,*contentRight;
    
    NSInteger bookIndex;
    
    NSInteger curChap,curPage;
    
    NSInteger allPage,readPage;
    
    NSMutableSet<BKPageContent *> *reusableViewControllers;
    
}

@end

@implementation ACPageViewController



#pragma mark  - SYS

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    self.dataSource = self;
    
}


/*
private func unusedViewController() -> MyViewController {
    let unusedViewControllers = reusableViewControllers.filter { $0.parentViewController == nil }
    if let someUnusedViewController = unusedViewControllers.last {
        return someUnusedViewController
    } else {
        let newViewController = MyViewController()
        reusableViewControllers.insert(newViewController)
        return newViewController
    }
}
*/

- (BKPageContent *)unusedViewController {
    
    BKPageContent *unusedViewController = nil;
    
    
    for (BKPageContent *content in reusableViewControllers) {
        if (content.parentViewController == nil) {
            unusedViewController = content;
            break;
        }
    }
    
    if (unusedViewController) {
        
        return unusedViewController;
    }
    
    unusedViewController = [[BKPageContent alloc] initWithBookIdex:bookIndex];
    [reusableViewControllers addObject:unusedViewController];
    return unusedViewController;
}

- (void)viewWillAppear:(BOOL)animated {
    
    if(!reusableViewControllers) {  //首次显示
        
        reusableViewControllers = [[NSMutableSet alloc] initWithCapacity:3];
        
    }
//    else { // 之前已经显示过 数据有保存 
//    }
    
    
    __weak ACPageViewController *weak = self;
    __weak BKPageContent *contentDetail = [weak unusedViewController];
    contentDetail.pageFlag = 0;
    [contentDetail updateCurrentPageInfoWithChapter:curChap andPage:curPage];
    [weak setViewControllers:@[contentDetail]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO
                  completion:nil];
}

#pragma mark - Override

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - Private

#pragma mark - Public 

- (void)setCurrentPage {
    
    __weak ACPageViewController *weak = self;
    __weak BKPageContent *contentDetail = [weak unusedViewController];
    contentDetail.pageFlag = 0;
    [contentDetail updateCurrentPageInfoWithChapter:curChap andPage:curPage];
    [weak setViewControllers:@[contentDetail]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO
                  completion:nil];
}

- (void)setCurrentPageInfoWithChapter:(NSInteger)chap andPage:(NSInteger)page {
    
    curChap = chap;
    curPage = page;
    
    NSDictionary *dic_page = @{@"page":@(curPage),@"chapter":@(curChap)};
    [[NSNotificationCenter defaultCenter] postNotificationName:kPAGE_CHANGE_NOTI
                                                        object:nil
                                                      userInfo:dic_page];
}

- (void)setBookAllPages:(NSInteger)all_page andCurReadPage:(NSInteger)read_page {
    
    if(all_page != -1) allPage = all_page;
    if(read_page != -1) readPage = read_page;
    
}

- (void)setCurrentPageIndex:(NSInteger)bindex {
    
    bookIndex = bindex;
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    
    
    //    NSLog(@"*******  finished %d  ****** complete %d",finished,completed);
    
    
    if (completed) {
        
        
        __weak ACPageViewController *weak = self;
        __weak BKPageContent *content = (BKPageContent *)pageViewController.viewControllers[0];
        
        
        NSIndexPath *index = [content getCurrentIndexPath];
        
        if (curChap < index.section) { // 下一页
            readPage ++;
        }
        else if (curChap == index.section) {
            
            if (curPage > index.item) { //上一页
                readPage --;
            }
            else if (curPage == index.item ) { //不可能
                
            }
            else { //下一页
                readPage ++;
            }
            
        }
        else {  //上一页
            readPage --;
        }
        
        curPage = index.item;
        curChap = index.section;
        
        NSDictionary *dic_page = @{@"page":@(curPage),@"chapter":@(curChap)};
        [[NSNotificationCenter defaultCenter] postNotificationName:kPAGE_CHANGE_NOTI
                                                            object:nil
                                                          userInfo:dic_page];
        
        
        
        BKPageContent *contentDetail = [weak unusedViewController];
        __weak BKPageContent *weak_detail = contentDetail;
        weak_detail.pageFlag = 0;
        [weak_detail updateBookAllPage:allPage andReadPage:readPage];
        
        [weak setViewControllers:@[weak_detail]
                       direction:UIPageViewControllerNavigationDirectionForward
                        animated:NO
                      completion:NULL];
        index = nil;
        dic_page = nil;
        content = nil;
        
        
    }
    
}

#pragma mark - UIPageViewControllerDataSource

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
//    if (!contentLeft) {
//        contentLeft = [[BKPageContent alloc] initWithBookIdex:bookIndex];
//        contentLeft.pageFlag = -1;
//    }
    
    
    BKPageContent *contentLeft = [self unusedViewController];
    contentLeft.pageFlag = -1;
    [contentLeft updateCurrentPageInfoWithChapter:curChap andPage:curPage];
    [contentLeft updateBookAllPage:allPage andReadPage:readPage-1];
    
//    NSIndexPath *ind = [contentLeft getPreviousPageIndexPath];
//    
//    if (!ind) {
//        return nil;
//    }
    
    return contentLeft;
    
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    
//    if (!contentRight) {
//        contentRight = [[BKPageContent alloc] initWithBookIdex:bookIndex];
//        contentRight.pageFlag = 1;
//    }
    
    
    BKPageContent *contentRight = [self unusedViewController];
    contentRight.pageFlag = 1;
    [contentRight updateCurrentPageInfoWithChapter:curChap andPage:curPage];
    [contentRight updateBookAllPage:allPage andReadPage:readPage+1];
    
//    NSIndexPath *ind = [contentRight getNextPageIndexPath];
//    
//    if (!ind) {
//        return nil;
//    }
    
    return contentRight;
    
    
}


@end
