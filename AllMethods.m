//
//  AllMethods.m
//  MyOwnApp
//
//  Created by diaozhiyuan on 11-11-2.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AllMethods.h"

#import <AFNetworking/AFNetworking.h>

#import "RJBookData.h"
#import "LSYReadConfig.h"

@implementation AllMethods


+ (void)clearDocumentTrash {
    
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *contentOfFolder = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    for (NSString *aPath in contentOfFolder) {
        
        NSString * fullPath = [documentsDirectory stringByAppendingPathComponent:aPath];
        BOOL isDir;
        if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath isDirectory:&isDir] && !isDir){
            
            if ([[fullPath pathExtension] isEqualToString:@"sqlite"]) {
                continue;
            }
            
            [[NSFileManager defaultManager] removeItemAtPath:fullPath
                                                       error:nil];
        }
    }

    
}


+ (BOOL)createReadProgressTable {
    
    BOOL bol = NO;
    FMDatabase *db = [AllMethods openDatabase];
    
    if ([db open]) {
        //        [db executeUpdate:@"DROP table read_progress"];
        if (![db tableExists:@"read_progress"]) {
            
            NSString *sql = @"CREATE table read_progress (id TEXT NOT NULL PRIMARY KEY UNIQUE ON CONFLICT REPLACE, name TEXT,progress TEXT)";
            bol = [db executeUpdate:sql];
        }
        [db close];
    }
    
    
    
    return bol;
    
}


+ (BOOL)saveReadProgressInfo:(NSDictionary *)dic {
    
    FMDatabase *db = [AllMethods openDatabase];
    BOOL bol = NO;
    if ([db open]) {
        
        bol = [db executeUpdate:@"INSERT INTO read_progress(id, name, progress) VALUES (?,?,?)",
               dic[@"id"],dic[@"name"],dic[@"progress"]];
        
        [db close];
    }
    
    return bol;
    
}
+ (NSString *)getReadProgressWithKey:(NSString *)key {
    
    
    FMDatabase *db = [AllMethods openDatabase];
    
    NSString *res = nil;
    
    if ([db open]) {
        
        NSString *sql = [NSString stringWithFormat:@"select progress from 'main'.'read_progress' where id='%@'",key];
        res = [db stringForQuery:sql];
        
        [db close];
    }
    
    return res;
    
}

+ (BOOL)deleteReadProgressWithKey:(NSString *)key {
    
    BOOL bol = NO;
    FMDatabase *db = [AllMethods openDatabase];
    
    if ([db open]) {
        NSString *sql = [NSString stringWithFormat:@"delete from 'main'.'read_progress' where id='%@'",key];
        bol = [db executeUpdate:sql];
        [db close];
    }
    
    return bol;
}

+ (void)clearOldReadInfoWithBookIndex:(NSInteger)bookIndex {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    [AllMethods deleteReadProgressWithKey:book.name];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: [NSString stringWithFormat:@"%@-progress",
                                                                book.name]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:book.name];
    
    NSString *path = [AllMethods applicationDocumentsDirectory];
    
    NSString *file_r = [NSString stringWithFormat:@"%@/%@.r",path,book.name];
    [AllMethods delRecordWith:book.name];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    file_r = [NSString stringWithFormat:@"%@/%@-%@.r",path,book.name,
              [[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    
}


+ (void)deleteBookWithIndex:(NSInteger)delIndex {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:delIndex];
    [AllMethods deleteReadProgressWithKey:book.name];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: [NSString stringWithFormat:@"%@-progress",
                                                                book.name]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:book.name];
    
    NSString *path = [AllMethods applicationDocumentsDirectory];
    
    NSString *file_r = [NSString stringWithFormat:@"%@/%@.r",path,book.name];
    NSString *file = [NSString stringWithFormat:@"%@/%@",path,book.name];
    
    [AllMethods delRecordWith:book.name];
    
    [[NSFileManager defaultManager] removeItemAtPath:file error:nil];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    file_r = [NSString stringWithFormat:@"%@/%@-%@.r",path,book.name,
              [[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    file_r = [NSString stringWithFormat:@"%@/%@-%.0f.chr",path,book.name,config.fontSize];
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    [[RJBookData sharedRJBookData].books removeObjectAtIndex:delIndex];
}


+ (BOOL)createDownloadListTable {
    
    BOOL bol = NO;
    FMDatabase *db = [AllMethods openDatabase];
    
    if ([db open]) {
//        [db executeUpdate:@"DROP table download_list"];
        if (![db tableExists:@"download_list"]) {
            
            NSString *sql = @"CREATE table download_list (id TEXT NOT NULL PRIMARY KEY UNIQUE ON CONFLICT REPLACE, name TEXT,href TEXT,aut_name TEXT,img TEXT)";
            bol = [db executeUpdate:sql];
        }
        [db close];
    }
    
    
    
    return bol;
}

+ (NSDictionary *)getBookDownloadList {
    
    FMDatabase *db = [AllMethods openDatabase];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:3];
    
    if ([db open]) {
        
        NSString *sql = [NSString stringWithFormat:@"select * from 'main'.'download_list'"];
        
        FMResultSet *rs = [db executeQuery:sql];
        
        while ([rs next]) {
            NSMutableDictionary *res = [NSMutableDictionary dictionaryWithCapacity:3];
            NSString *str_id = [rs stringForColumn:@"id"];
            res[@"id"] = str_id;
            res[@"name"] = [rs stringForColumn:@"name"];
//            res[@"chap_href"] = [rs stringForColumn:@"chap_href"];
//            res[@"chap_name"] = [rs stringForColumn:@"chap_name"];
//            res[@"chap_index"] = [rs stringForColumn:@"chap_index"];
            res[@"aut_name"] = [rs stringForColumn:@"aut_name"];
            res[@"href"] = [rs stringForColumn:@"href"];
            res[@"img"] = [rs stringForColumn:@"img"];
//            res[@"cata_name"] = [rs stringForColumn:@"cata_name"];
//            res[@"cata_href"] = [rs stringForColumn:@"cata_href"];
            
            dic[str_id] = res;
        }
        [rs close];
        [db close];
    }
    
    return dic;
    
}

+ (BOOL)saveDownloadInfo:(NSDictionary *)dic {
    
    FMDatabase *db = [AllMethods openDatabase];
    BOOL bol = NO;
    if ([db open]) {
        
        bol = [db executeUpdate:@"INSERT INTO download_list(id, name, aut_name, href, img) VALUES (?,?,?,?,?)",
               dic[@"id"],dic[@"name"],dic[@"aut_name"],
               dic[@"href"],dic[@"img"]];
        
        [db close];
    }
    
    return bol;
    
}

+ (BOOL)createNewUpdateTable {
    
    BOOL bol = NO;
    FMDatabase *db = [AllMethods openDatabase];
    
    if ([db open]) {
        //[db executeUpdate:@"DROP table new_update"];
        if (![db tableExists:@"new_update"]) {
            
            NSString *sql = @"CREATE table new_update (id TEXT NOT NULL PRIMARY KEY UNIQUE ON CONFLICT REPLACE, name TEXT,chap_href TEXT,chap_name TEXT,chap_index INTEGER,aut_name TEXT,detail_href TEXT,img TEXT,cata_name TEXT,cata_href TEXT)";
            bol = [db executeUpdate:sql];
        }
        [db close];
    }
    
    
    
    return bol;
}

+ (NSArray *)getBookListInNewUpdateTable {
    
    FMDatabase *db = [AllMethods openDatabase];
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:3];
    
    if ([db open]) {
        
        NSString *sql = [NSString stringWithFormat:@"select * from 'main'.'new_update'"];
        
        FMResultSet *rs = [db executeQuery:sql];
        
        while ([rs next]) {
            NSMutableDictionary *res = [NSMutableDictionary dictionaryWithCapacity:3];
            res[@"id"] = [rs stringForColumn:@"id"];
            res[@"name"] = [rs stringForColumn:@"name"];
            res[@"chap_href"] = [rs stringForColumn:@"chap_href"];
            res[@"chap_name"] = [rs stringForColumn:@"chap_name"];
            res[@"chap_index"] = [rs stringForColumn:@"chap_index"];
            res[@"aut_name"] = [rs stringForColumn:@"aut_name"];
            res[@"detail_href"] = [rs stringForColumn:@"detail_href"];
            res[@"img"] = [rs stringForColumn:@"img"];
            res[@"cata_name"] = [rs stringForColumn:@"cata_name"];
            res[@"cata_href"] = [rs stringForColumn:@"cata_href"];
            
            [arr addObject:res];
        }
        [rs close];
        [db close];
    }
    
    return arr;
    
}

+ (BOOL)saveNewUpdateInfo:(NSDictionary *)dic {
    
    
    FMDatabase *db = [AllMethods openDatabase];
    BOOL bol = NO;
    if ([db open]) {
        
        bol = [db executeUpdate:@"INSERT INTO new_update(id, name, chap_href, chap_name, chap_index, aut_name, detail_href, img,cata_name,cata_href) VALUES (?,?,?,?,?,?,?,?,?,?)",
               dic[@"id"],dic[@"name"],dic[@"chap_href"],
               dic[@"chap_name"],dic[@"chap_index"],dic[@"aut_name"],
               dic[@"detail_href"],dic[@"img"],dic[@"cata_name"],
               dic[@"cata_href"]];
        
        [db close];
    }
    
    return bol;
}

+ (BOOL)getCntOfBookIdInNewUpdateTable:(NSString *)href {
    
    FMDatabase *db = [AllMethods openDatabase];
    BOOL bol = NO;
    if ([db open]) {
        
        NSString *sql = [NSString stringWithFormat:@"select count(*) from 'main'.'new_update' where id='%@'",href];
        
        NSString *res = [db stringForQuery:sql];
        
        if ([res intValue] > 0) {
            bol = YES;
        }
        [db close];
    }

    return bol;
    
}

+ (BOOL)delBookInNewUpdateTable:(NSString *)href {
    
    
    FMDatabase *db = [AllMethods openDatabase];
    BOOL bol = NO;
    
    if ([db open]) {
        
        NSString *strSQL = [NSString stringWithFormat:@"DELETE FROM 'main'.'new_update' WHERE id='%@'",href];
        bol = [db executeUpdate:strSQL];
        [db close];
    }
    
    return bol;
}

+ (void)createTableBuddy {
    
    /*
     CREATE TABLE "main"."ddd" (
     "id" INTEGER NOT NULL,
     "uid" text NOT NULL,
     "head" TEXT,
     "nick" TEXT,
     "note" TEXT,
     PRIMARY KEY("id")
     );
     */
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"create_buddy"]) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"create_buddy"];
    
    FMDatabase *db = [AllMethods openDatabase];
    if ([db open]) {
        NSString *strSql = @"CREATE TABLE \"main\".\"buddy\" ("\
        "\"id\" INTEGER NOT NULL,"\
        "\"uid\" text NOT NULL,"\
        "\"head\" TEXT,"\
        "\"nick\" TEXT,"\
        "\"note\" TEXT,"\
        "PRIMARY KEY(\"id\")"\
        ");";
        BOOL bol = [db executeUpdate:strSql];
        [db close];
        
        if (bol) {
            NSLog(@"creae success");
        }
        else {
            NSLog(@"create failed .... ");
        }
    }
    
}

+ (void)saveUserInfoWithUID:(NSString *)uid nick:(NSString *)nick andHead:(NSString *)head {
    
    FMDatabase *db = [AllMethods openDatabase];
    if ([db open]) {
        
        NSString *sql = [NSString stringWithFormat:@"select count(*) from 'main'.'buddy' where uid='%@'",uid];
        
        NSString *res = [db stringForQuery:sql];
        
        NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'buddy' (id,'uid','head','nick') VALUES (NULL,'%@','%@','%@')",uid,head,nick];
        
        if ([res intValue] == 1) {
            strSQL = [NSString stringWithFormat:@"UPDATE 'main'.'buddy' SET nick='%@',head='%@' WHERE uid='%@'",nick,head,uid];
        }
        
        
        [db executeUpdate:strSQL];
//        if([db executeUpdate:strSQL])
//            NSLog(@"success");
//        else NSLog(@"failed");
        [db close];
    }
    
}

+ (NSDictionary *)getUserInfoByUID:(NSString *)uid {
    
    FMDatabase *db = [AllMethods openDatabase];
    NSMutableDictionary *res = [NSMutableDictionary dictionaryWithCapacity:3];
    
    if ([db open]) {
        
        NSString *sql = [NSString stringWithFormat:@"select * from 'main'.'buddy' where uid='%@'",uid];
        
        FMResultSet *rs = [db executeQuery:sql];
        
        while ([rs next]) {
            
            res[@"uid"] = [rs stringForColumn:@"uid"];
            res[@"head"] = [rs stringForColumn:@"head"];
            res[@"nick"] = [rs stringForColumn:@"nick"];
            
        }
        [rs close];
        [db close];
    }
    
    return res;
    
}

+ (NSDictionary *)getAllUserInfo {
    
    FMDatabase *db = [AllMethods openDatabase];
    NSMutableDictionary *res = [NSMutableDictionary dictionaryWithCapacity:3];
    
    if ([db open]) {
        
        NSString *sql = [NSString stringWithFormat:@"select * from 'main'.'buddy'"];
        
        FMResultSet *rs = [db executeQuery:sql];
        
        while ([rs next]) {
            
            NSMutableDictionary *dic_d = [NSMutableDictionary dictionaryWithCapacity:2];
            NSString *uid = [rs stringForColumn:@"uid"];
            dic_d[@"head"] = [rs stringForColumn:@"head"];
            dic_d[@"nick"] = [rs stringForColumn:@"nick"];
            
            res[uid] = dic_d;
            
        }
        [rs close];
        [db close];
    }
    
    return res;
    
}

+(FMDatabase *)openDatabase{
	BOOL success;
	NSError *error;
	NSFileManager *fm = [NSFileManager defaultManager];
	NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"novel.sqlite"];
	success = [fm fileExistsAtPath:writableDBPath];
	if(!success){
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"novel.sqlite"];
		success = [fm copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
		if(!success){
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
															message:@"无法打开数据库"
														   delegate:nil
												  cancelButtonTitle:@"确定" otherButtonTitles:nil];
			[alert show];
			return nil;
		}
	}
	
	FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
	return db;
}

+ (NSString *)applicationDocumentsDirectory {
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
	//初始化临时文件路径
	NSString *folderPath = [path stringByAppendingPathComponent:@"data"];
	//创建文件管理
	NSFileManager *fileManager = [NSFileManager defaultManager];
	//判断temp文件夹是否存在
	BOOL fileExists = [fileManager fileExistsAtPath:folderPath];
	if (!fileExists) {//如果不存在说创建,因为下载时,不会自动创建文件夹
		[fileManager createDirectoryAtPath:folderPath
			   withIntermediateDirectories:YES
								attributes:nil
									 error:nil];
	}
	
	return folderPath;
}

+ (NSString *)tempDirectory{
    NSString *path = NSTemporaryDirectory();
    return path;
}

+(CGSize)getNovelSizeWithTitle:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT content, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		NSString *str = [db stringForQuery:strSql];
		[db close];
		
		if (str==nil) {
			return CGSizeMake(0, 0);
		}
		NSArray *arr = [str componentsSeparatedByString:@","];
		CGSize size = CGSizeMake([[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue]);
		
		return size;
	}
	
	return CGSizeMake(0, 0);
}

+(NSString *)getEncodeWithTitle:(NSString *)title{
    FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT code, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		NSString *str = [db stringForQuery:strSql];
		[db close];
        
        return str;
	}
	
	return nil;
}

+(void)insertNovelSizeWithTitle:(NSString *)title andSize:(CGSize)size andCode:(NSString *)code{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *str = [NSString stringWithFormat:@"%f,%f",size.width,size.height];
		NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'record' ('title','content','code') VALUES ('%@','%@','%@')",title,str,code];
		[db executeUpdate:strSQL];
		[db close];
	}
}

+(void)delRecordWith:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"DELETE FROM 'main'.'record' WHERE title='%@'",title];
		[db executeUpdate:strSQL];
		[db close];
	}
}

+(void)dbWithSQL:(NSString *)sql{
    FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		[db executeUpdate:sql];
		[db close];
	}
}

+(void)delShuzhaiWithContent:(NSString *)con{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"DELETE FROM 'main'.'shuzhai' WHERE content='%@'",con];
		[db executeUpdate:strSQL];
		[db close];
	}
}

+ (void)delUpdateBookWithID:(NSDictionary *)bid{
    FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"DELETE FROM 'main'.'bookbag' WHERE book_id='%@'",bid];
		[db executeUpdate:strSQL];
		[db close];
	}
}

+(void)updatePages:(NSString *)json andTitle:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		int num = [db intForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) FROM 'record' WHERE title='%@'",title]];
		//NSLog(@"%d",num);
		NSString *strSQL = nil;
		if(num==0) strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'record' (title,content) VALUES ('%@','%@')",title,json];
		else strSQL = [NSString stringWithFormat:@"UPDATE 'main'.'record' SET content='%@' WHERE title='%@'",json,title];
		[db executeUpdate:strSQL];
		//if([db executeUpdate:strSQL])
//			NSLog(@"success");
//		else NSLog(@"failed");
		[db close];
	}
}

+(void)insertObjectInTitle:(NSString *)title withObject:(id)obj{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'record' ('title','content') VALUES ('%@','%@')",title,obj];
		[db executeUpdate:strSQL];
		//if([db executeUpdate:strSQL])
//			NSLog(@"success");
//		else NSLog(@"failed");
		[db close];
	}
}


+ (void)insertBookWithSQL:(NSString *)sql{
    FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		[db executeUpdate:sql];
		[db close];
	}
}

+(void)insertChaptersWithTitle:(NSString *)title andChapter:(int)chap{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'chapters' ('title','chapter') VALUES ('%@','%d')",title,chap];
		[db executeUpdate:strSQL];
		//if([db executeUpdate:strSQL])
//			NSLog(@"success");
//		else NSLog(@"failed");
		[db close];
	}
}

+(NSString *)getSaveRangeString:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT content, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		NSString *str = [db stringForQuery:strSql];
		
		[db close];
		
		return str;
	}
	
	return nil;
}

+(NSDictionary *)getRangeWithTitle:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSDictionary *dic = nil;
		NSString *strSql = [NSString stringWithFormat:@"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		FMResultSet *rs = [db executeQuery:strSql];
		while ([rs next]) {
			NSString *str = [rs objectForColumnName:@"content"];
			//NSLog(@"%@",str);
			str = [str stringByReplacingOccurrencesOfString:@"#" withString:@"\""];
			NSArray *arr = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding]
                                                           options:NSJSONReadingAllowFragments
                                                             error:nil];//[str JSONValue];
			dic = [arr objectAtIndex:0];
		}
		[rs close];
		[db close];
		
		return dic;
	}
	
	return nil;
}

+(int)getCountOfRecord:(NSString *)rec inTable:(NSString *)table{//Select Name,Count(*) From A Group By Name Having Count(*) > 1

	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*), _ROWID_ 'NAVICAT_ROWID' FROM '%@' WHERE title='%@'",table,rec];
		int count = [db intForQuery:sql];
		[db close];
		return count;
	}
	
	return 0;
}

+(NSMutableArray *)getShuzhaiTitle{
	NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = @"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'shuzhai' GROUP BY title";
		FMResultSet *rs = [db executeQuery:strSql];
		while ([rs next]) {
			//NSLog(@"%@",[rs stringForColumn:@"title"]);
			if([rs stringForColumn:@"title"]) [arr addObject:[rs stringForColumn:@"title"]];
		}
		[rs close];
		[db close];
	}
	
	return arr;
}

+(NSMutableArray *)getShuzhaiContent:(NSString *)key{
	NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'shuzhai' WHERE title='%@'",key];
		FMResultSet *rs = [db executeQuery:strSql];
		//NSLog(@"%d\n%@",[rs columnCount],strSql);
		while ([rs next]) {
			if([rs stringForColumn:@"content"])[arr addObject:[rs stringForColumn:@"content"]];
		}
		[rs close];
		[db close];
	}
	
	return arr;
}

+ (NSArray *)getBookBagIDList {
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
    FMDatabase *db = [AllMethods openDatabase];
    if ([db open]) {
        
        NSString *strSql = @"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'bookbag'";
        FMResultSet *rs = [db executeQuery:strSql];
        while ([rs next]) {
            [arr addObject:[rs stringForColumn:@"book_id"]];
        }
        [rs close];
        [db close];
    }
    
    return arr;
}


+ (NSDictionary *)getBookWithBookID:(NSString *)bid{
    FMDatabase *db = [AllMethods openDatabase];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:3];
    if ([db open]) {
        NSString *strSql = [NSString stringWithFormat:@"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'bookbag' where book_id='%@'",bid];
        FMResultSet *rs = [db executeQuery:strSql];
        while ([rs next]) {
            
            if([rs stringForColumn:@"book_name"])[dic setObject:[rs stringForColumn:@"book_name"] forKey:@"book_name"];
            if([rs stringForColumn:@"book_id"])[dic setObject:[rs stringForColumn:@"book_id"] forKey:@"book_id"];
            if([rs stringForColumn:@"book_author"])[dic setObject:[rs stringForColumn:@"book_author"]forKey:@"book_author"];
            if([rs stringForColumn:@"book_img"])[dic setObject:[rs stringForColumn:@"book_img"] forKey:@"book_img"];
            if([rs stringForColumn:@"book_other"])[dic setObject:[rs stringForColumn:@"book_other"] forKey:@"book_other"];
        }
        [rs close];
        [db close];
    }
    
    return dic;
}


+ (NSArray *)getBookBagList{
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
        
		NSString *strSql = @"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'bookbag'";
		FMResultSet *rs = [db executeQuery:strSql];
		while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:3];
			if([rs stringForColumn:@"book_name"])[dic setObject:[rs stringForColumn:@"book_name"] forKey:@"book_name"];
            if([rs stringForColumn:@"book_id"])[dic setObject:[rs stringForColumn:@"book_id"] forKey:@"book_id"];
            if([rs stringForColumn:@"book_author"])[dic setObject:[rs stringForColumn:@"book_author"]forKey:@"book_author"];
            if([rs stringForColumn:@"book_img"])[dic setObject:[rs stringForColumn:@"book_img"] forKey:@"book_img"];
            if([rs stringForColumn:@"book_other"])[dic setObject:[rs stringForColumn:@"book_other"] forKey:@"book_other"];
            [arr addObject:dic];
		}
		[rs close];
		[db close];
	}
	
	return arr;
}

+ (NSArray *)getLocalBookID{
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = @"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'bookbag'";
		FMResultSet *rs = [db executeQuery:strSql];
		while ([rs next]) {
            if([rs stringForColumn:@"book_id"])[arr addObject:[rs stringForColumn:@"book_id"]];
		}
		[rs close];
		[db close];
	}
	
	return arr;
}

+(NSString *)getUserDef:(NSString *)key{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	return [def valueForKey:key];
}

+(NSDictionary *)getShuzhai{
	NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *str = [path objectAtIndex:0];
	str = [str stringByAppendingPathComponent:Key_ShuZhai];
	if ([[NSFileManager defaultManager] fileExistsAtPath:str]) {
		NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:str];
		return dic;
	}
	
	return nil;
}

+(NSString *)getFilePath:(NSString *)fileName{
	NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *str = [path objectAtIndex:0];
	return [str stringByAppendingPathComponent:fileName];
}

+(UIColor*)getColorWithIndex:(NSInteger)index{
	switch (index) {
		case 0:
			return [UIColor blackColor];
		case 1:
			return [UIColor blueColor];
		case 2:
			return [UIColor brownColor];
		case 3:
			return [UIColor cyanColor];
		case 4:
			return [UIColor darkGrayColor];
		case 5:
			return [UIColor grayColor];
		case 6:
			return [UIColor greenColor];
		case 7:
			return [UIColor lightGrayColor];
		case 8:
			return [UIColor magentaColor];
		case 9:
			return [UIColor orangeColor];
		case 10:
			return [UIColor purpleColor];
		case 11:
			return [UIColor redColor];
		case 12:
			return [UIColor whiteColor];
		case 13:
			return [UIColor yellowColor];
		
		default:
			break;
	}
	return nil;
}


+(NSDate *)getPriousDateFromDate:(NSDate *)date withHour:(NSInteger)hour{//计算日期
    NSDateComponents *comps = [[NSDateComponents alloc] init];
	[comps setHour:hour];
    NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *mDate = [calender dateByAddingComponents:comps toDate:date options:0];
    return mDate;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

+ (void)showAltMsg:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles: nil];
    [alert show];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

+ (float)sysversion{
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}


+ (UIImage *)getImageWithColor:(UIColor *)color andRect:(CGRect)rect{
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (float)getEdge{
    if ([AllMethods sysversion] >= 7.0) {
        return 64.0;
    }
    
    return 44;
}

+ (float)getContentInset{
    if ([AllMethods sysversion] >= 7.0) {
        return 64.0;
    }
    
    return 0;
    
}

+ (UIFont *)getChapterFontWithSize:(float)size {
    return [UIFont systemFontOfSize:size];
}

+ (UIFont *)getFontWithSize:(float)size{
    
    return [UIFont fontWithName:@"FZLanTingHei-R-GBK" size:size];
}


+ (void)clearCacheWithExtention:(NSString *)ext{
    NSString *extension = ext;
    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [AllMethods applicationDocumentsDirectory];//[paths objectAtIndex:0];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        
        if ([[filename pathExtension] isEqualToString:extension]) {
            [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        }
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    
    contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    e = [contents objectEnumerator];
    while ((filename = [e nextObject])) {
        
        if ([[filename pathExtension] isEqualToString:extension]) {
            [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        }
    }
}

+ (UIBarButtonItem *)getButtonBarItemWithTitle:(NSString *)str andSelect:(SEL)select andTarget:(id)obj{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 40, 35);
    btn.titleLabel.font = [AllMethods getFontWithSize:16];
    [btn setTitle:str forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:obj action:select forControlEvents:UIControlEventTouchUpInside];
    [btn setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return item;
}

+ (UIBarButtonItem *)getButtonBarItemWithImageName:(NSString *)str andSelect:(SEL)select andTarget:(id)obj{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setImage:[UIImage imageNamed:str] forState:UIControlStateNormal];
    [btn addTarget:obj action:select forControlEvents:UIControlEventTouchUpInside];
    [btn setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return item;
}


+ (BOOL)isRarFile:(NSData *)data {
    
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0x52:
            return YES;
        default:
            return NO;
//        case 0xFF:
//            return @"image/jpeg";
//        case 0x89:
//            return @"image/png";
//        case 0x47:
//            return @"image/gif";
//        case 0x49:
//        case 0x4D:
//            return @"image/tiff";
//        case 0x52:
//            // R as RIFF for WEBP
//            if ([data length] < 12) {
//                return nil;
//            }
//            
//            NSString *testString = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(0, 12)] encoding:NSASCIIStringEncoding];
//            if ([testString hasPrefix:@"RIFF"] && [testString hasSuffix:@"WEBP"]) {
//                return @"image/webp";
//            }
//            
//            return nil;
    }
    
    return NO;
    
}


+ (NSString *)updateURL {
    //http://localhost:63342/Book/book/BookAPI.php?_ijt=1sbscbnpcv9qpfq5tm55kauuhp
    //http://localhost:63342/Book/book/BookAPI.php?_ijt=6p02dta03e687j3jec4j8c4nku
    
    return @"http://dydog.sinaapp.com/book/BookAPI.php?_ijt=dtgnr75jm2i58al0rcphqr0nts";
    
}


+ (void)requestWithType:(RequestType)type andPara:(id)obj andUrl:(NSString *)surl andGetDataBlock:(HandleRequestDataEvent)block {
    
    AFHTTPRequestOperationManager *man = [AFHTTPRequestOperationManager manager];
    man.requestSerializer = [AFHTTPRequestSerializer serializer];
    man.responseSerializer = [AFHTTPResponseSerializer serializer];
    man.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain",@"text/xml",@"text/html",@"application/json",nil];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy defaultPolicy];
    securityPolicy.allowInvalidCertificates = YES;
    man.securityPolicy = securityPolicy;
    
    
    surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if (type == REQUEST_GET) {
        
        [man GET:surl parameters:obj
         success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
             
             
             if (block) {
                 
                 if (operation.responseData) {
                     NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:nil];
                     if(dic) block(dic);
                     else {
                         block(operation.responseString);
                     }
                 }
                 else {
                     block(operation.responseString);
                 }
             }
             
         }
         failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
             
             if (operation.responseData) {
                 NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:nil];
                 if(dic) block(dic);
                 else {
                     block(operation.responseString);
                 }
             }
             else {
                 block(operation.responseString);
             }
             
         }];
        
    }
    else {
        
        [man POST:surl parameters:obj
          success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
              if (block) {
                  
                  if (operation.responseData) {
                      NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:nil];
                      if(dic) block(dic);
                      else {
                          block(operation.responseString);
                      }
                  }
                  else {
                      block(operation.responseString);
                  }
              }
          }
          failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
              if (block) {
                  
                  if (operation.responseData) {
                      NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:nil];
                      if(dic) block(dic);
                      else {
                          block(operation.responseString);
                      }
                  }
                  else {
                      block(operation.responseString);
                  }
              }
          }];
    }
    
}

@end









#import <objc/runtime.h>

static void *indexKey = (void *)@"index_path";
static void *subKey = (void *)@"submit_key";
static void *extKey = (void *)@"ext_key";
static void *txtKey = (void *)@"txt_key";


@implementation UIButton (IndexPath)

- (void)setIndexPath:(NSIndexPath *)indexPath{
    objc_setAssociatedObject(self, indexKey, indexPath, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSIndexPath *)indexPath{
    return objc_getAssociatedObject(self, indexKey);
}

- (void)setSubmitKey:(NSString *)submitKey{
    objc_setAssociatedObject(self, subKey, submitKey, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)submitKey{
    return objc_getAssociatedObject(self, subKey);
}

- (void)setExtFlag:(NSString *)extFlag{
    objc_setAssociatedObject(self, extKey, extFlag, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)extFlag{
    return objc_getAssociatedObject(self, extKey);
}

- (void)setTxtView:(UITextView *)txtView{
    objc_setAssociatedObject(self, txtKey, txtView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UITextView *)txtView{
    return objc_getAssociatedObject(self, txtKey);
}

@end



@implementation NSData (gb18030)

- (NSData *)dataByHealingGB18030Stream {
    NSUInteger length = [self length];
    if (length == 0) {
        return self;
    }
    
    static NSString * replacementCharacter = @"?";
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSData *replacementCharacterData = [replacementCharacter dataUsingEncoding:enc];
    
    NSMutableData *resultData = [NSMutableData dataWithCapacity:self.length];
    
    const Byte *bytes = [self bytes];
    
    static const NSUInteger bufferMaxSize = 1024;
    Byte buffer[bufferMaxSize];
    NSUInteger bufferIndex = 0;
    
    NSUInteger byteIndex = 0;
    BOOL invalidByte = NO;
    
    
#define FlushBuffer() if (bufferIndex > 0) { \
[resultData appendBytes:buffer length:bufferIndex]; \
bufferIndex = 0; \
}
#define CheckBuffer() if ((bufferIndex+5) >= bufferMaxSize) { \
[resultData appendBytes:buffer length:bufferIndex]; \
bufferIndex = 0; \
}
    
    
    while (byteIndex < length) {
        Byte byte = bytes[byteIndex];
        
        //检查第一位
        if (byte >= 0 && byte <= (Byte)0x7f) {
            //单字节文字
            CheckBuffer();
            buffer[bufferIndex++] = byte;
        } else if (byte >= (Byte)0x81 && byte <= (Byte)0xfe){
            //可能是双字节，可能是四字节
            if (byteIndex + 1 >= length) {
                //这是最后一个字节了，但是这个字节表明后面应该还有1或3个字节，那么这个字节一定是错误字节
                FlushBuffer();
                return resultData;
            }
            
            Byte byte2 = bytes[++byteIndex];
            if (byte2 >= (Byte)0x40 && byte <= (Byte)0xfe && byte != (Byte)0x7f) {
                //是双字节，并且可能合法
                Byte tuple[] = {byte, byte2};
                CFStringRef cfstr = CFStringCreateWithBytes(kCFAllocatorDefault, tuple, 2, kCFStringEncodingGB_18030_2000, false);
                if (cfstr) {
                    CFRelease(cfstr);
                    CheckBuffer();
                    buffer[bufferIndex++] = byte;
                    buffer[bufferIndex++] = byte2;
                } else {
                    //这个双字节字符不合法，但byte2可能是下一字符的第一字节
                    byteIndex -= 1;
                    invalidByte = YES;
                }
            } else if (byte2 >= (Byte)0x30 && byte2 <= (Byte)0x39) {
                //可能是四字节
                if (byteIndex + 2 >= length) {
                    FlushBuffer();
                    return resultData;
                }
                
                Byte byte3 = bytes[++byteIndex];
                
                if (byte3 >= (Byte)0x81 && byte3 <= (Byte)0xfe) {
                    // 第三位合法，判断第四位
                    
                    Byte byte4 = bytes[++byteIndex];
                    
                    if (byte4 >= (Byte)0x30 && byte4 <= (Byte)0x39) {
                        //第四位可能合法
                        Byte tuple[] = {byte, byte2, byte3, byte4};
                        CFStringRef cfstr = CFStringCreateWithBytes(kCFAllocatorDefault, tuple, 4, kCFStringEncodingGB_18030_2000, false);
                        if (cfstr) {
                            CFRelease(cfstr);
                            CheckBuffer();
                            buffer[bufferIndex++] = byte;
                            buffer[bufferIndex++] = byte2;
                            buffer[bufferIndex++] = byte3;
                            buffer[bufferIndex++] = byte4;
                        } else {
                            //这个四字节字符不合法，但是byte2可能是下一个合法字符的第一字节，回退3位
                            //并且将byte1,byte2用?替代
                            byteIndex -= 3;
                            invalidByte = YES;
                        }
                    } else {
                        //第四字节不合法
                        byteIndex -= 3;
                        invalidByte = YES;
                    }
                } else {
                    // 第三字节不合法
                    byteIndex -= 2;
                    invalidByte = YES;
                }
            } else {
                // 第二字节不是合法的第二位，但可能是下一个合法的第一位，所以回退一个byte
                invalidByte = YES;
                byteIndex -= 1;
            }
            
            if (invalidByte) {
                invalidByte = NO;
                FlushBuffer();
                [resultData appendData:replacementCharacterData];
            }
        }
        byteIndex++;
    }
    FlushBuffer();
    return resultData;
}

@end


@implementation UIImage (Tint)

- (UIImage *) imageWithTintColor:(UIColor *)tintColor
{
    return [self imageWithTintColor:tintColor blendMode:kCGBlendModeDestinationIn];
}

- (UIImage *) imageWithGradientTintColor:(UIColor *)tintColor
{
    return [self imageWithTintColor:tintColor blendMode:kCGBlendModeOverlay];
}

- (UIImage *) imageWithTintColor:(UIColor *)tintColor blendMode:(CGBlendMode)blendMode
{
    //We want to keep alpha, set opaque to NO; Use 0.0f for scale to use the scale factor of the device’s main screen.
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    [tintColor setFill];
    CGRect bounds = CGRectMake(0, 0, self.size.width, self.size.height);
    UIRectFill(bounds);
    
    //Draw the tinted image in context
    [self drawInRect:bounds blendMode:blendMode alpha:1.0f];
    
    if (blendMode != kCGBlendModeDestinationIn) {
        [self drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0f];
    }
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

@end



@implementation UIColor (UIColorToNSString)

+ (UIColor *)colorFromeString:(NSString *)str{
    
    
    NSArray *values = [str componentsSeparatedByString:@" "];
    CGFloat red = [[values objectAtIndex:1] floatValue];
    CGFloat green = [[values objectAtIndex:2] floatValue];
    CGFloat blue = [[values objectAtIndex:3] floatValue];
    CGFloat alpha = [[values objectAtIndex:4] floatValue];
    
    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    
    return color;
}

+ (UIColor *)colorFromeString:(NSString *)str withAlpha:(float) alpha{
    
    
    NSArray *values = [str componentsSeparatedByString:@" "];
    CGFloat red = [[values objectAtIndex:1] floatValue];
    CGFloat green = [[values objectAtIndex:2] floatValue];
    CGFloat blue = [[values objectAtIndex:3] floatValue];
    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    
    return color;
}

+ (CGFloat )colorSumFromString:(NSString *)str{
    
    NSArray *values = [str componentsSeparatedByString:@" "];
    CGFloat red = [[values objectAtIndex:1] floatValue];
    CGFloat green = [[values objectAtIndex:2] floatValue];
    CGFloat blue = [[values objectAtIndex:3] floatValue];
    
    return red+green+blue;
}

- (NSString *)colorToString {
    return [NSString stringWithFormat:@"%@", self];
}

- (CGFloat *) getRGB{
    UIColor * uiColor = self;
    
    CGColorRef cgColor = [uiColor CGColor];
    
    int numComponents = CGColorGetNumberOfComponents(cgColor);
    
    if (numComponents == 4){
        static CGFloat * components = Nil;
        components = (CGFloat *) CGColorGetComponents(cgColor);
        return (CGFloat *)components;
    } else { //否则默认返回黑色
        static CGFloat components[4] = {0};
        CGFloat f = 0;
        //非RGB空间的系统颜色单独处理
        if ([uiColor isEqual:[UIColor whiteColor]]) {
            f = 1.0;
        } else if ([uiColor isEqual:[UIColor lightGrayColor]]) {
            f = 0.8;
        } else if ([uiColor isEqual:[UIColor grayColor]]) {
            f = 0.5;
        }
        components[0] = f;
        components[1] = f;
        components[2] = f;
        components[3] = 1.0;
        return (CGFloat *)components;
    }
}

- (UIColor *) mix: (UIColor *) color{
    CGFloat * rgb1 = [self getRGB];
    CGFloat r1 = rgb1[0];
    CGFloat g1 = rgb1[1];
    CGFloat b1 = rgb1[2];
    CGFloat alpha1 = rgb1[3];
    
    CGFloat * rgb2 = [color getRGB];
    CGFloat r2 = rgb2[0];
    CGFloat g2 = rgb2[1];
    CGFloat b2 = rgb2[2];
    CGFloat alpha2 = rgb2[3];
    
    //mix them!!
    CGFloat r = (r1 + r2) / 2.0;
    CGFloat g = (g1 + g2) / 2.0;
    CGFloat b = (b1 + b2) / 2.0;
    CGFloat alpha = (alpha1 + alpha2) / 2.0;
    
    UIColor * uiColor = [UIColor colorWithRed:r green:g blue:b alpha:alpha];
    return [uiColor copy];
}

- (UIColor *) darken{ //变暗
    CGFloat *rgb = [self getRGB];
    CGFloat r = rgb[0];
    CGFloat g = rgb[1];
    CGFloat b = rgb[2];
    CGFloat alpha = rgb[3];
    
    r = r * 0.618;
    g = g * 0.618;
    b = b * 0.618;
    
    UIColor *uiColor = [UIColor colorWithRed:r green:g blue:b alpha:alpha];
    return [uiColor copy];
}

- (UIColor *) lighten {
    CGFloat *rgb = [self getRGB];
    CGFloat r = rgb[0];
    CGFloat g = rgb[1];
    CGFloat b = rgb[2];
    CGFloat alpha = rgb[3];
    
    r = r + (1 - r) / 6.18;
    g = g + (1 - g) / 6.18;
    b = b + (1 - b) / 6.18;
    
    UIColor * uiColor = [UIColor colorWithRed:r green:g blue:b alpha:alpha];
    return [uiColor copy];
}

@end


const static NSInteger cellCount = 17;

static NSMutableArray<CALayer *> *booms;

@implementation UIView (Boom)


+(void)load{
    booms = [NSMutableArray<CALayer *> array];
}

-(void)boom{
    [booms removeAllObjects];
    for(int i = 0 ; i < cellCount ; i++){
        for(int j = 0 ; j < cellCount ; j++){
            CGFloat pWidth = MIN(self.frame.size.width, self.frame.size.height)/cellCount;
            CALayer *boomCell = [CALayer layer];
            boomCell.backgroundColor = [self getPixelColorAtLocation:CGPointMake(i*2, j*2)].CGColor;
            boomCell.cornerRadius = pWidth/2;
            boomCell.frame = CGRectMake(i*pWidth, j*pWidth, pWidth, pWidth);
            [self.layer.superlayer addSublayer:boomCell];
            [booms addObject:boomCell];
        }
    }
    
    
    //粉碎动画
    [self cellAnimation];
    
    //缩放消失
    [self scaleOpacityAnimations];
}

-(void)cellAnimation{
    for(CALayer *cell in booms){
        
        CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
        
        CAKeyframeAnimation *keyframeAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        keyframeAnimation.path = [self makeRandomPath:cell].CGPath;
        keyframeAnimation.fillMode = kCAFillModeForwards;
        keyframeAnimation.timingFunction =  [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseOut];
        keyframeAnimation.duration = (random()%10) * 0.05 + 0.3;
        
        CABasicAnimation *scale = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        scale.toValue = @([self makeScaleValue]);
        scale.duration = keyframeAnimation.duration;
        scale.removedOnCompletion = NO;
        scale.fillMode = kCAFillModeForwards;
        
        
        CABasicAnimation *opacity = [CABasicAnimation animationWithKeyPath:@"opacity"];
        opacity.fromValue = @1;
        opacity.toValue = @0;
        opacity.duration = keyframeAnimation.duration;
        opacity.removedOnCompletion = NO;
        opacity.fillMode = kCAFillModeForwards;
        opacity.timingFunction =  [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseOut];
        
        
        animationGroup.duration = keyframeAnimation.duration;
        animationGroup.removedOnCompletion = NO;
        animationGroup.fillMode = kCAFillModeForwards;
        animationGroup.animations = @[keyframeAnimation,scale,opacity];
        animationGroup.delegate = self;
        
        [cell addAnimation:animationGroup forKey: @"moveAnimation"];
    }
}

/**
 *  绘制粉碎路径
 *
 *  @param alayer <#alayer description#>
 *
 *  @return <#return value description#>
 */
- (UIBezierPath *) makeRandomPath: (CALayer *) alayer {
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:self.layer.position];
    CGFloat left = -self.layer.frame.size.width * 1.3;
    CGFloat maxOffset = 2 * fabs(left);
    CGFloat randomNumber = random()%101;
    CGFloat endPointX = self.layer.position.x + left + (randomNumber/100) * maxOffset;
    CGFloat controlPointOffSetX = self.layer.position.x + (endPointX-self.layer.position.x)/2;
    CGFloat controlPointOffSetY = self.layer.position.y - 0.2 * self.layer.frame.size.height - random()%(int)(1.2 * self.layer.frame.size.height);
    CGFloat endPointY = self.layer.position.y + self.layer.frame.size.height/2 +random()%(int)(self.layer.frame.size.height/2);
    [path addQuadCurveToPoint:CGPointMake(endPointX, endPointY) controlPoint:CGPointMake(controlPointOffSetX, controlPointOffSetY)];
    return path;
}

+(void)separateChapter:(NSMutableArray *)chapters content:(NSString *)content andContentFrame:(CGRect) frame andFontSize:(float)font_size andLineSpace:(float)space
{
    [chapters removeAllObjects];
    NSString *parten = @"第[0-9两一二三四五六七八九十百千]*[章回节].*";
    NSError* error = NULL;
    NSRegularExpression *reg = [NSRegularExpression regularExpressionWithPattern:parten options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSArray* match = [reg matchesInString:content options:NSMatchingReportCompletion range:NSMakeRange(0, [content length])];
    
    if (match.count != 0)
    {
        
        __block NSRange lastRange = NSMakeRange(0, 0);
        
        [match enumerateObjectsUsingBlock:^(NSTextCheckingResult *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSRange range = [obj range];
            if (idx == 0) {
                [chapters addObject:@"开始"];
                
            }
            if (idx > 0 ) {
                [chapters addObject:[content substringWithRange:lastRange]];
            }
            if (idx == match.count-1) {
                [chapters addObject:[content substringWithRange:range]];
            }
            lastRange = range;
            
        }];
    }
    else{
        [chapters addObject:@"全部"];
    }
    
}

#pragma mark - 缩放透明度动画
- (void) scaleOpacityAnimations {
    // 缩放
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath: @"transform.scale"];
    scaleAnimation.toValue = @0.01;
    scaleAnimation.duration = 0.15;
    scaleAnimation.fillMode = kCAFillModeForwards;
    
    // 透明度
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath: @"opacity"];
    opacityAnimation.fromValue = @1;
    opacityAnimation.toValue = @0;
    opacityAnimation.duration = 0.15;
    opacityAnimation.fillMode = kCAFillModeForwards;
    
    [self.layer addAnimation: scaleAnimation forKey: @"lscale"];
    [self.layer addAnimation: opacityAnimation forKey: @"lopacity"];
    self.layer.opacity = 0;
}

-(CGFloat)makeScaleValue{
    return 1 - 0.7 * (random()%101 - 50)/50;
}


-(UIImage *)snapShot{
    
    //根据视图size开始图片上下文
    UIGraphicsBeginImageContext(self.frame.size);
    //提供视图当前图文
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    //获取视图当前图片
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //关闭图片上下文
    UIGraphicsEndImageContext();
    return image;
}




/*
 
 * data 指被渲染的内存区域 ，这个内存区域大小应该为(bytesPerRow * height)个字节，如果对绘制操作被渲染的内存区域并无特别的要求，那么刻意传NULL
 
 * width 指被渲染内存区域的宽度
 * height 指被渲染内存区域的高度
 
 * bitsPerComponent 指被渲染内存区域中组件在屏幕每个像素点上需要使用的bits位，举例来说，如果使用32-bit像素和RGB格式，那么RGBA颜色格式中每个组件在屏幕每一个像素点需要使用的bits位就是为 32/4 = 8
 
 * bytesPerRow 指被渲染区域中每行所使用的bytes位数
 
 * space 指被渲染内存区域的“位图上下文”
 
 * bitmapInfo 指被渲染内存区域的“视图”是否包含一个alpha（透视）通道以及每一个像素点对应的位置，除此之外还可以指定组件是浮点值还是整数值
 
 
 CGBitmapContextCreate(void * __nullable data,
 size_t width,
 size_t height,
 size_t bitsPerComponent,
 size_t bytesPerRow,
 CGColorSpaceRef __nullable space,
 uint32_t bitmapInfo)
 
 */
-(CGContextRef) createARGBBitmapContextFromImage:(CGImageRef) inImage {
    CGContextRef context = NULL;
    CGColorSpaceRef colorSpace;
    //内存空间的指针，该内存空间的大小等于图像使用RGB通道所占用的字节数
    void *bitmapData;
    unsigned long bitmapByteCount;
    unsigned long bitmapBytePerRow;
    
    size_t pixelsWide = CGImageGetWidth(inImage);//获取横向的像素点的个数
    size_t pixelsHeight = CGImageGetHeight(inImage);
    
    //每一行的像素点占用的字节数，每个像素点的ARGB四个通道各占8个bit(0-255)的空间
    bitmapBytePerRow = pixelsWide * 4;
    
    //计算整张图占用的字节数
    bitmapByteCount = bitmapBytePerRow * pixelsHeight;
    
    //创建依赖于设备的RGB通道
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    //分配足够容纳图片字节数的内存空间
    bitmapData = malloc(bitmapByteCount);
    
    //创建CoreGraphic的图形上下文，该上下文描述了bitmaData指向的内存空间需要绘制的图像的一些绘制参数
    context = CGBitmapContextCreate(bitmapData, pixelsWide, pixelsHeight, 8, bitmapBytePerRow, colorSpace, kCGImageAlphaPremultipliedFirst);
    
    //Core Foundation中通过含有Create、Alloc的方法名字创建的指针，需要使用CFRelease()函数释放
    CGColorSpaceRelease(colorSpace);
    return context;
}

/**
 *  根据点，取得对应颜色
 *
 *  @param point 坐标点
 *
 *  @return 颜色
 */
-(UIColor *)getPixelColorAtLocation:(CGPoint)point{
    
    //拿到放大后的图片
    CGImageRef inImage = [self scaleImageToSize:CGSizeMake(cellCount*2, cellCount*2)].CGImage;
    
    //使用上面的方法(createARGBBitmapContextFromImage:)创建上下文
    CGContextRef cgctx = [self createARGBBitmapContextFromImage:inImage];
    
    //图片的宽高
    size_t w = CGImageGetWidth(inImage);
    size_t h = CGImageGetHeight(inImage);
    //rect
    CGRect rect = CGRectMake(0, 0, w, h);
    //将目标图像绘制到指定的上下文，实际为上下文内的bitmapData。
    CGContextDrawImage(cgctx, rect, inImage);
    //取色
    unsigned char *bitmapData = CGBitmapContextGetData(cgctx);
    
    int pixelInfo = 4*((w*round(point.y))+round(point.x));
    CGFloat a = bitmapData[pixelInfo]/255.0;
    CGFloat r = bitmapData[pixelInfo+1]/255.0;
    CGFloat g = bitmapData[pixelInfo+2]/255.0;
    CGFloat b = bitmapData[pixelInfo+3]/255.0;
    
    //释放上面的函数创建的上下文
    CGContextRelease(cgctx);
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}


/**
 *  缩放图片
 *
 *  @param size 缩放大小
 *
 *  @return 图片
 */
-(UIImage *)scaleImageToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [[self snapShot] drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *res = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return res;
}



@end

@implementation UIViewController (StatusBar)

- (UIStatusBarStyle )preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

@end



