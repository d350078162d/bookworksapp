//
//  BKNewUpdateBookDetail.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/23.
//
//

#import "BKNewUpdateBookDetail.h"

#import "BKUpdateCell.h"
#import "Layout.h"

#import "BKUpdateHead.h"

#import "BKNewUpdateBookBacklist.h"


@interface BKNewUpdateBookDetail () <UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    
    NSDictionary *dicDesc, *dicDetail;
    
    UIView *headView;
}

@property (weak, nonatomic) UICollectionView *myCollect;

@end

@implementation BKNewUpdateBookDetail


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 2) {
        
        NSArray *arr = dicDetail[@"top"];
        NSDictionary *dic = arr[indexPath.item];
        
        BKNewUpdateBookDetail *detail = [[BKNewUpdateBookDetail alloc] initWithBookDesc:dic];
        
        [self.tabBarController setHidesBottomBarWhenPushed:YES];
        [detail setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BKUpdateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collect_cell"
                                                                   forIndexPath:indexPath];
    
    
    switch (indexPath.section) {
        case 0:{
            cell.scrCata.hidden = YES;
            cell.rtUpdate.hidden = YES;
            cell.viewContent.hidden = NO;
            
            
            break;
        }
        case 2:{//同类推荐
            cell.scrCata.hidden = YES;
            cell.rtUpdate.hidden = YES;
            cell.viewContent.hidden = NO;
            cell.imgNoRecord.hidden = YES;
            
            NSArray *arr = dicDetail[@"top"];
            NSDictionary *dic = arr[indexPath.item];
            
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"img"]]
                              placeholderImage:ImageNamed(@"TXT_80x107_")];
            cell.labTitle.text = dic[@"name"];
            
            
            break;
        }
        
        case 1:{//最近更新
            cell.scrCata.hidden = YES;
            cell.rtUpdate.hidden = NO;
            cell.viewContent.hidden = YES;
            cell.imgNoRecord.hidden = YES;
            
            CGRect frame = cell.rtUpdate.frame;
            frame.size.height = 25;
            frame.origin.y = 0;
            cell.rtUpdate.frame = frame;
            
            NSArray *arr = dicDetail[@"update"];
            NSDictionary *dic = arr[indexPath.item];
            
            //            NSString *html = [NSString stringWithFormat:@"<font size=13 color=#208DDB><a href=type%td>%@</a></font><font size=13 color=#9b9b9b> 更新 </font><font size=15 color=#208DDB><a href=book%td>%@</a></font><br><font size=13 color=#9b9b9b> 最新 </font><font size=15 color=#208DDB><a href=chapter%td>%@</a></font>",
            //                              indexPath.item,dic[@"cata_name"],
            //                              indexPath.item,dic[@"name"],
            //                              indexPath.item,dic[@"chap_name"]];
            
            NSString *html = [NSString stringWithFormat:@"<font size=15 color=#208DDB>%@</font>",
                              dic[@"name"]];
            [cell.rtUpdate setText:html];
            
            break;
        }
        default:
            break;
    }
    
    return cell;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    switch (section) {
        case 0:
            
            return 0;
            break;
        case 1:{//最新章节
            
            NSArray *arr = dicDetail[@"update"];
            
            return arr.count;
            
            break;
        }
            
        case 2:{//同类推荐
            NSArray *arr = dicDetail[@"top"];
            
            return arr.count;
            break;
        }
        default:
            break;
    }
    
    return 0;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *view = nil;
    
    
    if (kind == UICollectionElementKindSectionHeader) {
        view = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                  withReuseIdentifier:@"update_head"
                                                         forIndexPath:indexPath];
        
        
        CGSize size = [self collectionView:collectionView layout:collectionView.collectionViewLayout referenceSizeForHeaderInSection:indexPath.section];
        BKUpdateHead *head = (BKUpdateHead *)view;
        CGRect frame = head.titleLabel.frame;
        
        
        switch (indexPath.section) {
            case 0:{
                head.titleLabel.text = @"";
                frame.size = size;
                head.titleLabel.frame = frame;
                [head addSubview:headView];
                
                break;
            }
            case 1:{
                head.titleLabel.text = @"最新章节";
                frame.size = size;
                head.titleLabel.frame = frame;
                
                break;
            }
            case 2:{
                head.titleLabel.text = @"同类推荐";
                frame.size = size;
                head.titleLabel.frame = frame;
                break;
            }
            default:
                break;
        }
    }
    
    return view;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.section == 0) {
        
        
        return CGSizeMake(ScreenWidth, 0);
    }
    else if (indexPath.section == 2) {
        
        float width = 60*1.7;
        float height = 75*1.7+50;
        return CGSizeMake(width, height);
    }
    else if (indexPath.section == 1) {//最近更新
        
        return CGSizeMake(ScreenWidth, 25);
    }
    
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    int num_per_row = 3;
    float width = 60*1.7;
    
    //    if (section == 0) {
    //        return UIEdgeInsetsMake(20, (ScreenWidth-num_per_row*width)/(num_per_row+1),
    //                                0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    //    }
    
    if (isPad) {
        num_per_row = 5;
    }
    
    if (isPad) {
        return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+2),
                                0, (ScreenWidth-num_per_row*width)/(num_per_row+2));
    }
    
    
    return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+1),
                            0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    
    switch (section) {
        case 0://记录
            //            return CGSizeMake(ScreenWidth, 70);
            //            break;
            return CGSizeMake(ScreenWidth, headView.frame.size.height);
        case 1://最新章节
        case 2:{//推荐
            return CGSizeMake(ScreenWidth, 40);
            break;
        }
            
        default:
            break;
    }
    
    return CGSizeZero;
}



#pragma mark - Request Data

- (void)requestData {
    
    NSString *surl = [NSString stringWithFormat:@"%@&action=detail&href=%@",
                      [AllMethods updateURL],dicDesc[@"href"]];
    
    __weak BKNewUpdateBookDetail *weakSelf = self;
    [AllMethods requestWithType:REQUEST_GET
                        andPara:nil
                         andUrl:surl andGetDataBlock:^(id object) {
                             
                             NSDictionary *dic = object;
                             
                             if (![dic isKindOfClass:[NSDictionary class]]) {
                                 
                                 [AllMethods showAltMsg:[NSString stringWithFormat:@"%@",dic]];
                                 [weakSelf.myCollect.mj_header endRefreshing];
                                 return ;
                             }
                             
                             dicDetail = [NSDictionary dictionaryWithDictionary:dic];
                             
                             [weakSelf.myCollect reloadData];
                             
                             [weakSelf.myCollect.mj_header endRefreshing];
                             
                             [weakSelf updateHead];
                         }];
}

#pragma mark - Private

- (void)updateHead {
    
    NSString *html = [NSString stringWithFormat:@"<font size=13 color=#9b9b9b>作者 </font><font size=15 color=#313131>%@</font><br><font size=13 color=#9b9b9b>介绍  </font><font size=15 color=#313131>%@</font>",
                      dicDetail[@"aut_name"],
                      dicDetail[@"desc"]];
    
    RTLabel *rt = [headView viewWithTag:2];
    [rt setText:html];
    
    if (!dicDesc[@"img"]) {
        UIImageView *iv = [headView viewWithTag:1];
        [iv sd_setImageWithURL:[NSURL URLWithString:dicDetail[@"img"]]
              placeholderImage:ImageNamed(@"TXT_80x107_")];
    }
    
    NSString *href = [NSString stringWithFormat:@"%@",dicDetail[@"href"]];
    if ([AllMethods getCntOfBookIdInNewUpdateTable:href]) {
        UIButton *btn = [headView viewWithTag:4];
        [btn setTitle:@"已添加" forState:UIControlStateNormal];
    }
}

- (void)btnClicked:(UIButton *)btn {
    
    if (btn.tag == 3) {//目录
        
        BKNewUpdateBookBacklist *backlist = [[BKNewUpdateBookBacklist alloc] initWithBookInfo:dicDetail];
        [self.tabBarController setHidesBottomBarWhenPushed:YES];
        [backlist setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:backlist animated:YES];
        
    }
    else if (btn.tag == 4){//添加追书
        
        /*
         NSString *sql = @"CREATE table new_update (id TEXT NOT NULL PRIMARY KEY UNIQUE ON CONFLICT REPLACE, name TEXT,chap_href TEXT,chap_name TEXT,index TEXT,aut_name TEXT,detail_href TEXT,img TEXT,cata_name TEXT,cata_href TEXT)";
         */
        
        if (!dicDetail) {
            return;
        }
        
        NSDictionary *dic = @{@"id":dicDetail[@"href"],@"name":dicDetail[@"name"],
                              @"chap_href":@"",@"chap_name":@"未观看",
                              @"chap_index":@(0),@"aut_name":dicDetail[@"aut_name"],
                              @"detail_href":dicDetail[@"href"],
                              @"img":dicDetail[@"img"],@"cata_name":@"",
                              @"cata_href":@""};
        
        if ([AllMethods saveNewUpdateInfo:dic]) {
            [WSProgressHUD showSuccessWithStatus:@"添加成功"];
            [btn setTitle:@"已添加" forState:UIControlStateNormal];
        }
        
    }
}

#pragma mark - Build UI

- (void)buildHeadView {
    
    headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
    headView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 60*1.7, 75*1.7)];
    [headView addSubview:iv];
    
    [iv sd_setImageWithURL:[NSURL URLWithString:dicDesc[@"img"]]
          placeholderImage:ImageNamed(@"TXT_80x107_")];
    iv.tag = 1;
    
    RTLabel *rt = [[RTLabel alloc] initWithFrame:CGRectMake(ORIGINAL_X(iv)+5, iv.frame.origin.y,
                                                            SIZE_W(headView)-ORIGINAL_X(iv)-7.5,
                                                            SIZE_H(iv))];
    
    rt.tag = 2;
    [headView addSubview:rt];
    
    
    float width = (ScreenWidth-15*3)/2;
    
    UIButton *btn_cata = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_cata.frame = CGRectMake(0, ORIGINAL_Y(iv)+15, width, 30);
    [btn_cata setTitle:@"目录" forState:UIControlStateNormal];
    [btn_cata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_cata.titleLabel.font = [UIFont systemFontOfSize:16];
    btn_cata.tag = 3;
    [btn_cata addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn_cata.center = CGPointMake(SIZE_W(headView)/4.0, btn_cata.center.y);
    [btn_cata setBackgroundColor:NAV_COLOR];
    btn_cata.layer.cornerRadius = 4.6f;
    btn_cata.layer.borderColor = NAV_COLOR.CGColor;
    btn_cata.layer.borderWidth = 0.65f;
    btn_cata.clipsToBounds = YES;
    [headView addSubview:btn_cata];
    
    UIButton *btn_add = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_add.frame = CGRectMake(0, ORIGINAL_Y(iv)+15, width, 30);
    [btn_add setTitle:@"加入追书" forState:UIControlStateNormal];
    [btn_add setTitleColor:NAV_COLOR forState:UIControlStateNormal];
    btn_add.layer.cornerRadius = 4.6f;
    btn_add.layer.borderColor = NAV_COLOR.CGColor;
    btn_add.layer.borderWidth = 0.65f;
    btn_add.clipsToBounds = YES;
    btn_add.titleLabel.font = [UIFont systemFontOfSize:16];
    btn_add.tag = 4;
    [btn_add addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn_add.center = CGPointMake(3*SIZE_W(headView)/4.0, btn_cata.center.y);
    [headView addSubview:btn_add];
}

- (void)buildCollectView {
    
    float width = 60*1.7;
    
    Layout *lay = [[Layout alloc] initWithItemWidth:width andHeight:75*1.7+50];
    lay.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                                   collectionViewLayout:lay];
    [collect registerClass:[BKUpdateCell class] forCellWithReuseIdentifier:@"collect_cell"];
    [collect registerClass:[BKUpdateHead class]
forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
       withReuseIdentifier:@"update_head"];
    
    collect.delegate = self;
    collect.dataSource = self;
    collect.tag = 1;
    collect.backgroundColor = [UIColor whiteColor];
    collect.contentInset = UIEdgeInsetsMake(0, 0.0, 50.0, 0.0);
    
    
    self.myCollect = collect;
    [self.myCollect setAlwaysBounceVertical:YES];
    
    [self.view addSubview:self.myCollect];
}




#pragma mark - SYS

- (id)initWithBookDesc:(NSDictionary *)dic {
    
    self = [super init];
    if (self) {
        dicDesc = [NSDictionary dictionaryWithDictionary:dic];
    }
    
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (dicDetail) {
        [self updateHead];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self buildHeadView];
    [self buildCollectView];
    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"back_up"
                                                                            andSelect:@selector(popViewControllerAnimated:) andTarget:self.navigationController];
    
    
    self.navigationItem.title = dicDesc[@"name"];
    
    self.myCollect.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self
                                                                refreshingAction:@selector(requestData)];
    [self.myCollect.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
