//
//  BKMainBookCell.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/18.
//
//

#import <UIKit/UIKit.h>


@interface BKMainBookCell : UICollectionViewCell


@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UIImageView *imageMark;

@property (nonatomic, strong) IBOutlet UIView *viewProgress;
@property (nonatomic, strong) IBOutlet UILabel *labProgress;

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) UIView *bkView;
@property (nonatomic, strong) IBOutlet UIView *viewTitle;
@property (nonatomic, strong) IBOutlet UILabel *labAuthor;

@property (nonatomic, copy) void(^CellLongClickEvent)(UICollectionViewCell *);

- (void)setDeleteHidden:(BOOL)bol;


@end
