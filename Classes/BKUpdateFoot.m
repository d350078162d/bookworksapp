//
//  BKUpdateFoot.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/22.
//
//

#import "BKUpdateFoot.h"

@implementation BKUpdateFoot


- (void)initinalData {
    
    //    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, ScreenWidth-15, SIZE_H(self))];
    //    lab.textColor = RGBACOLOR(180, 180, 180, 1.0f);
    //    lab.font = [UIFont systemFontOfSize:16];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(10, 0, ScreenWidth-15, SIZE_H(self));
    [btn setTitleColor:RGBACOLOR(105, 105, 105, 1.0f) forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    self.btnTitle = btn;
    [self addSubview:btn];
    
    UILabel *lab_line = [[UILabel alloc] initWithFrame:CGRectMake(10, SIZE_H(self)-0.65, ScreenWidth-20, 0.65)];
    lab_line.backgroundColor = RGBACOLOR(230, 230, 230, 1.0f);
    self.labLine = lab_line;
    [self addSubview:lab_line];
    
//    self.backgroundColor = [UIColor purpleColor];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initinalData];
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initinalData];
    }
    return self;
    
}

@end
