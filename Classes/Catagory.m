//
//  Catagory.m
//  BookWorksApp
//
//  Created by 刁志远 on 12-3-31.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Catagory.h"

@implementation UINavigationBar (BackGround)

- (void)drawRect:(CGRect)rect {  
	UIImage *navBarImage = [UIImage imageNamed:@"navbar1.png"];  
	CGContextRef context = UIGraphicsGetCurrentContext();  
	CGContextTranslateCTM(context, 0.0, self.frame.size.height);  
	CGContextScaleCTM(context, 1.0, -1.0);         
	
	CGRect winSize = [[UIScreen mainScreen] bounds];
	
	CGImageRef cgImage= CGImageCreateWithImageInRect(navBarImage.CGImage, CGRectMake(0, 0, 1, 44));  
	CGContextDrawImage(context, CGRectMake(0, 0, winSize.size.height, self.frame.size.height), cgImage);  
	CGContextDrawImage(context, CGRectMake(0, 0, winSize.size.height, self.frame.size.height), navBarImage.CGImage);  
	CGContextDrawImage(context, CGRectMake(winSize.size.height, 0, winSize.size.height, self.frame.size.height), cgImage);
	CGImageRelease(cgImage);  
} 

@end


