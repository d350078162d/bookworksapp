//
//  BKUpdateCell.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/22.
//
//

#import <UIKit/UIKit.h>

#import "RTLabel.h"
#import "TMMuiLazyScrollView.h"

@interface BKUpdateCell : UICollectionViewCell


@property (nonatomic, strong) UIView *viewContent;

@property (nonatomic, strong) TMMuiLazyScrollView *scrCata;

@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) UIButton *imgNoRecord;
@property (nonatomic, strong) IBOutlet UIImageView *imageMark;

@property (nonatomic, strong) IBOutlet UIView *viewTitle;
@property (nonatomic, strong) IBOutlet UILabel *labTitle;
@property (nonatomic, strong) IBOutlet UILabel *labAuthor;

@property (nonatomic, strong) IBOutlet RTLabel *rtUpdate;

- (void)setDeleteHidden:(BOOL)bol;



@end
