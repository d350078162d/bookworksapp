//
//  HorizontalHead.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/25.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HorizontalHeadState) {
    HorizontalHeaderStateIdle = 0,    // 正常状态下的footer提示
    HorizontalHeaderStateTrigger,     // 被拖至触发点的footer提示
};

@interface HorizontalHead : UICollectionReusableView


@property (nonatomic, assign) HorizontalHeadState state;

@property (nonatomic, strong) UIImageView *arrowView;
@property (nonatomic, strong) UILabel *label;

@property (nonatomic, copy) NSString *idleTitle;
@property (nonatomic, copy) NSString *triggerTitle;

@end
