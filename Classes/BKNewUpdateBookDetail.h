//
//  BKNewUpdateBookDetail.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/23.
//
//

#import <UIKit/UIKit.h>

@interface BKNewUpdateBookDetail : UIViewController

- (id)initWithBookDesc:(NSDictionary *)dic;

@end
