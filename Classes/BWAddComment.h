//
//  BWAddComment.h
//  BookWorksApp
//
//  Created by 刁志远 on 13-11-29.
//
//

#import <UIKit/UIKit.h>

#import "DJQRateView.h"

@protocol BWAddCommentDelegate;

@interface BWAddComment : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *txtContent;
@property (weak, nonatomic) IBOutlet DJQRateView *rateView;
@property (retain,nonatomic) NSDictionary *dicBook;

@property (retain,nonatomic) id<BWAddCommentDelegate> delegate;

- (IBAction)dismissWithAnimation:(id)sender;
- (IBAction)btnClicked:(id)sender;
- (void)show;
- (void)hide;

@end


@protocol BWAddCommentDelegate <NSObject>

- (void)commentView:(BWAddComment *) comment didDismissWithClick:(int )index;

@end
