//
//  DownloadView.h
//  BookWorksApp
//
//  Created by 刁志远 on 13-5-27.
//
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "AllMethods.h"

#import "BookWorksAppAppDelegate.h"
#import "SearchView.h"

#import "PPiFlatSegmentedControl.h"
#import "MJRefresh.h"


#define EGO_FOOT_HEIGHT 44.0f

@interface DownloadView : UIViewController<UIScrollViewDelegate,MJRefreshBaseViewDelegate>{
    NSMutableArray *arrDownList;
    NSMutableArray *arrBooks;
    NSMutableArray *arrList;
    
    int curPage;
    
    BOOL _reloading,isDowning,isFirst;
    
    ASINetworkQueue *down_queue;
    
    PPiFlatSegmentedControl *segType;
    
    MJRefreshFooterView *mjFoot;
    MJRefreshHeaderView *mjHead;
}

@property (retain, nonatomic) IBOutlet UITableView *myTable;
@property (retain, nonatomic) PPiFlatSegmentedControl *segType;

- (IBAction)typeChange:(id)sender;
@end
