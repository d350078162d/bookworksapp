//
//  BKUpdateHead.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/22.
//
//

#import "BKUpdateHead.h"

@implementation BKUpdateHead


- (void)initinalData {
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, ScreenWidth-15, SIZE_H(self))];
    lab.textColor = RGBACOLOR(55, 55, 55, 1.0f);
    lab.font = [UIFont systemFontOfSize:16];
    
    self.titleLabel = lab;
    [self addSubview:lab];
    
//    self.backgroundColor = [UIColor brownColor];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initinalData];
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initinalData];
    }
    return self;
    
}

@end
