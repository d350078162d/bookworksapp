//
//  BKGroupList.m
//  BookWorksApp
//
//  Created by 刁志远 on 16/5/16.
//
//

#import "BKGroupList.h"

@interface BKGroupList ()<UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray *arrList;
    NSMutableArray *addList;
}

@property (nonatomic, weak) IBOutlet UITableView *myTable;

@end

@implementation BKGroupList



#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 58;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    EMGroup *group = arrList[indexPath.row];
//    [[EaseMob sharedInstance].chatManager asyncDestroyGroup:group.groupId completion:^(EMGroup *group, EMGroupLeaveReason reason, EMError *error) {
//        if (!error) {
//            NSLog(@"解散成功");
//        }
//    } onQueue:nil];
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    ECGroup *gp = arrList[indexPath.row];
    cell.textLabel.text = gp.name;
    
    
    return cell;
}


#pragma mark - Request Action


- (void)requestGroupList {
    
    
    [WSProgressHUD show];
    
    if (!arrList) {
        arrList = [NSMutableArray arrayWithCapacity:3];
    }
    [arrList removeAllObjects];
    
    ECGroupMatch *match = [[ECGroupMatch alloc] init];
    match.searchType = 2; //搜索类型 1: 群组ID  2：群组名称
    match.keywords = @"小说";
    
    
    __weak BKGroupList *weak = self;
    [[ECDevice sharedInstance].messageManager searchPublicGroups:
     match completion:^(ECError *error, NSArray *groups) {
         
         [WSProgressHUD dismiss];
         if (error.errorCode == ECErrorType_NoError) {
             /*if(groups.count==0){
                 NSLog(@"获取到的群组为空");
             }else{
                 for (ECGroup* group in groups) {
                     NSLog(@"群组ID:%@",group.groupId);
                     NSLog(@"群主:%@",group.owner);
                     NSLog(@"群名字:%@",group.name);
                     NSLog(@"群公告:%@",group.declared);
                 }
             }*/
             
             [arrList addObjectsFromArray:groups];
             [weak.myTable reloadData];
         }else {
             NSLog(@"errorCode:%d\rerrorDescription:%@",(int)error.errorCode,error.errorDescription);
         }
     }];
}

#pragma mark - Private Method

- (void)addGroup:(id )sender {
    
    [WSProgressHUD show];
    UIButton *btn = (UIButton *)sender;
    
}


#pragma mark - Build UI

- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
}

#pragma mark - SYS

- (id)initWithAlreadyAddGroupList:(NSArray *)arr{
    
    self = [super init];
    if (self) {
        
        addList = [NSMutableArray arrayWithArray:arr];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

}

- (void)dealloc {
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationItem.title = NSLocalizedString(@"Nav_Group", nil);
    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"back_up"
                                                                            andSelect:@selector(popViewControllerAnimated:)
                                                                            andTarget:self.navigationController];
    [self buildTableView];
    
    [self requestGroupList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
