//
//  LSYChapterModel.h
//  LSYReader
//
//  Created by Labanotation on 16/5/31.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreText/CoreText.h>

typedef void (^ChapterLoadPageEvent)(long page_cnt,id model);
typedef void (^ChapterLoading)(long);


@interface LSYChapterModel : NSObject<NSCopying,NSCoding>
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *title;
@property (nonatomic) NSUInteger pageCount;
@property (nonatomic) NSInteger chapterIndex;
@property (nonatomic,copy) ChapterLoadPageEvent loadPageBlock;
@property (nonatomic,copy) ChapterLoading loadingBlock;
@property (nonatomic) CGRect contentRect;
@property (nonatomic) CGSize fontSize;

-(NSString *)stringOfPage:(NSUInteger)index;
-(void)updateFont;
- (NSArray *)getPageList;
- (NSInteger)getIsFinishLoad;
- (void)addPagesFromeRecord:(NSArray *)arr;



@end

