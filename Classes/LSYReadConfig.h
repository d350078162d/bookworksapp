//
//  LSYReadConfig.h
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface LSYReadConfig : NSObject<NSCoding>

+(instancetype)shareInstance;
@property (nonatomic) CGFloat fontSize;   //字体大小
@property (nonatomic) CGFloat lineSpace;  //行间距
@property (nonatomic) CGFloat kernSpace;  //字间距
@property (nonatomic) CGFloat paraSpace;  //段间距
@property (nonatomic,strong) UIColor *fontColor;   //字体颜色
@property (nonatomic) NSUInteger theme;            //主题
@property (nonatomic) NSUInteger pageTurn;            //翻页方式

@property (nonatomic, strong) NSArray *arrThemes;
@property (nonatomic, strong) NSArray *arrPageTurn;

+(NSDictionary *)parserAttribute:(LSYReadConfig *)config;

@end
