//
//  BWWebView.h
//  BookWorksApp
//
//  Created by 刁志远 on 14-1-13.
//
//

#import <UIKit/UIKit.h>

@interface BWWebView : UIViewController{
    NSDictionary *dicBook;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;

- (void)setCurBook:(NSDictionary *)dic;
- (IBAction)btnClicked:(id)sender;

@end
