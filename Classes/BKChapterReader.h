//
//  BKChapterReader.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/25.
//
//

#import <UIKit/UIKit.h>

@interface BKChapterReader : UIViewController

- (id)initWithChapterList:(NSArray *)arr andChapterIndex:(NSInteger )index andBookInfo:(NSDictionary *)dic;

@end
