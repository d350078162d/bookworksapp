//
//  BKNewReaderMenu.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/20.
//
//

#import <UIKit/UIKit.h>


@protocol BKNewReaderMenuDelegate ;

@interface BKNewReaderMenu : UIView


@property (nonatomic, strong) id<BKNewReaderMenuDelegate> delegate;

@property (nonatomic, assign) BOOL isHidden;

- (void)showInController:(UIViewController *)ctrl;
- (void)dismissView;
- (void)setLabTitle:(NSString *)tit;

@end




@protocol BKNewReaderMenuDelegate <NSObject>

@optional

- (void)menuDidClickedBack;
- (void)menuFontSizeChanged:(float)size;
- (void)menuDidClickedCatagory;
- (void)menuDidChangeTheme;
- (void)menuWillShow;
- (void)menuDidDismiss;
- (void)menuPageTurnChanged:(NSInteger)type;

@end



