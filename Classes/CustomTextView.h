//
//  CustomTextView.h
//  BookWorksApp
//
//  Created by 刁志远 on 12-9-25.
//
//

#import <UIKit/UIKit.h>

#import "AllMethods.h"
#import "FMDatabase.h"
#import "FMResultSet.h"

@protocol TextViewTouchDelegate;

@interface CustomTextView : UITextView{
    id<TextViewTouchDelegate> tdelegate;
}

@property (nonatomic,assign) id<TextViewTouchDelegate> tdelegate;

@end

@protocol TextViewTouchDelegate <NSObject>

@optional
-(void)customTextView:(CustomTextView *)cusView touchBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)customTextView:(CustomTextView *)cusView touchMoved:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)customTextView:(CustomTextView *)cusView touchEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end
