//
//  RJBookData.m
//  txtReader
//
//  Created by Zeng Qingrong on 12-8-22.
//  Copyright (c) 2012年 Zeng Qingrong. All rights reserved.
//

#import "RJBookData.h"

@implementation RJSingleBook

@synthesize name,icon,pages,pageSize,bookFile;

@end

@implementation RJBookData
@synthesize books;


static RJBookData *shareBookData = nil;

+(RJBookData *)sharedRJBookData{
    @synchronized(self){
        if(shareBookData == nil){
            shareBookData = [[RJBookData alloc] init];
        }
    }
    return shareBookData;
}

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
	{
        if(shareBookData == nil)
		{
            shareBookData = [super allocWithZone:zone];
            return shareBookData;
        }
    }
    return nil;
}


-(id)init
{
	if (self = [super init])
	{
        self.books = [NSMutableArray arrayWithCapacity:1];
	}
	return self;
}

-(BOOL) loadBooks
{
    if (!books) {
        books = [NSMutableArray arrayWithCapacity:3];
    }
    [books removeAllObjects];
    
    
    NSString *documentsDirectory = [AllMethods applicationDocumentsDirectory];
//    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"data"];
    //[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/"];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:kHAD_LAUNCH]){
        NSString *help = [[NSBundle mainBundle] pathForResource:@"使用帮助" ofType:@"txt"];
        NSString *des = [documentsDirectory stringByAppendingPathComponent:@"使用帮助.txt"];
        [[NSFileManager defaultManager] copyItemAtPath:help toPath:des error:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kHAD_LAUNCH];
    }
    
//    NSArray *mua = (NSArray *)[[[NSFileManager defaultManager]
//                                contentsOfDirectoryAtPath:documentsDirectory
//                                error:nil]
//                               pathsMatchingExtensions:[NSArray arrayWithObjects:@"txt",@"TXT",@"",nil]];
    
    NSFileManager *man = [NSFileManager defaultManager];
    NSArray *mua = [man subpathsAtPath:documentsDirectory];
    NSArray *sortedPaths = [mua sortedArrayUsingComparator:^(NSString * firstPath, NSString* secondPath) {//
        NSString *firstUrl = [documentsDirectory stringByAppendingPathComponent:firstPath];//获取前一个文件完整路径
        NSString *secondUrl = [documentsDirectory stringByAppendingPathComponent:secondPath];//获取后一个文件完整路径
        NSDictionary *firstFileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:firstUrl error:nil];//获取前一个文件信息
        NSDictionary *secondFileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:secondUrl error:nil];//获取后一个文件信息
        id firstData = [firstFileInfo objectForKey:NSFileModificationDate];//获取前一个文件修改时间
        id secondData = [secondFileInfo objectForKey:NSFileModificationDate];//获取后一个文件修改时间
        //        return [firstData compare:secondData];//升序
        return [secondData compare:firstData];//降序
    }];
    
    for (int i=0; i<[sortedPaths count]; i++) {
        
        NSString *name = sortedPaths[i];
        if ([[[name pathExtension] uppercaseString] isEqualToString:@"R"] ||
            [[[name pathExtension] uppercaseString] isEqualToString:@"CHAPTER"] ||
            [[[name pathExtension] uppercaseString] isEqualToString:@"ENC"] ||
            [[[name pathExtension] uppercaseString] isEqualToString:@"CHR"] ||
            [[[name pathExtension] uppercaseString] isEqualToString:@"CONFIG"]) {
            continue;
        }
        
        RJSingleBook* singleBook = [[RJSingleBook alloc]init];
        singleBook.name =  name;
        singleBook.bookFile = [NSString stringWithFormat:@"%@/%@",
                               documentsDirectory,name];
        
        [books addObject:singleBook];
    }
    
    return YES;
}

@end
