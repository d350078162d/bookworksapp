//
//  BKNewUpdateTopBook.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/24.
//
//

#import "BKNewUpdateTopBook.h"

#import "BKUpdateCell.h"
#import "Layout.h"

#import "BKNewUpdateBookDetail.h"

#import "BKUpdateHead.h"

@interface BKNewUpdateTopBook ()<UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout> {
    
    NSMutableArray *arrList;
    
}

@property (weak, nonatomic) UICollectionView *myCollect;

@end

@implementation BKNewUpdateTopBook

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = arrList[indexPath.section];
    NSArray *arr = dic[@"book"];
    dic = arr[indexPath.item];
    
    BKNewUpdateBookDetail *detail = [[BKNewUpdateBookDetail alloc] initWithBookDesc:dic];
    
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [detail setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:detail animated:YES];
    [self.tabBarController setHidesBottomBarWhenPushed:NO];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return arrList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BKUpdateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collect_cell"
                                                                   forIndexPath:indexPath];
    
    
    cell.scrCata.hidden = YES;
    cell.rtUpdate.hidden = YES;
    cell.viewContent.hidden = NO;
    cell.imgNoRecord.hidden = YES;
    
    NSDictionary *dic = arrList[indexPath.section];
    NSArray *arr = dic[@"book"];
    dic = arr[indexPath.item];
    
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"img"]]
                      placeholderImage:ImageNamed(@"TXT_80x107_")];
    cell.labTitle.text = dic[@"name"];
    cell.labAuthor.text = dic[@"aut_name"];
    
    return cell;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    NSDictionary *dic = arrList[section];
    NSArray *arr = dic[@"book"];
    
    return arr.count;
    
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    float width = 60*1.7;
    float height = 75*1.7+50;
    return CGSizeMake(width, height);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    int num_per_row = 3;
    float width = 60*1.7;
    
    //    if (section == 0) {
    //        return UIEdgeInsetsMake(20, (ScreenWidth-num_per_row*width)/(num_per_row+1),
    //                                0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    //    }
    
    if (isPad) {
        num_per_row = 5;
    }
    
    if (isPad) {
        return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+2),
                                0, (ScreenWidth-num_per_row*width)/(num_per_row+2));
    }
    
    
    return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+1),
                            0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeMake(ScreenWidth, 50);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *view = nil;
    
    
    if (kind == UICollectionElementKindSectionHeader) {
        view = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                  withReuseIdentifier:@"update_head"
                                                         forIndexPath:indexPath];
        
        NSDictionary *dic = arrList[indexPath.section];
        
        
        CGSize size = [self collectionView:collectionView layout:collectionView.collectionViewLayout referenceSizeForHeaderInSection:indexPath.section];
        BKUpdateHead *head = (BKUpdateHead *)view;
        CGRect frame = head.titleLabel.frame;
        head.titleLabel.text = [NSString stringWithFormat:@"%@",
                                dic[@"name"]];
        frame.size = size;
        head.titleLabel.frame = frame;
        
    }
    
    return view;
}


#pragma mark - Build UI

- (void)buildCollectView {
    
    float width = 60*1.7;
    
    Layout *lay = [[Layout alloc] initWithItemWidth:width andHeight:75*1.7+50];
    lay.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                                   collectionViewLayout:lay];
    [collect registerClass:[BKUpdateCell class] forCellWithReuseIdentifier:@"collect_cell"];
    
    collect.delegate = self;
    collect.dataSource = self;
    collect.tag = 1;
    collect.backgroundColor = [UIColor whiteColor];
    collect.contentInset = UIEdgeInsetsMake(0, 0.0, 50.0, 0.0);
    
    [collect registerClass:[BKUpdateHead class]
forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
       withReuseIdentifier:@"update_head"];
    
    
    self.myCollect = collect;
    [self.myCollect setAlwaysBounceVertical:YES];
    
    [self.view addSubview:self.myCollect];
}


#pragma mark - SYS

- (id)initWithBookList:(NSArray *)arr {
    
    self = [super init];
    
    if (self) {
        arrList = [NSMutableArray arrayWithArray:arr];
        
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"推荐";
    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"back_up"
                                                                            andSelect:@selector(popViewControllerAnimated:) andTarget:self.navigationController];
    
    [self buildCollectView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
