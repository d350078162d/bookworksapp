//
//  BKNewUpdateAllRecord.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/25.
//
//

#import <UIKit/UIKit.h>

@interface BKNewUpdateAllRecord : UIViewController

- (id)initWithBookList:(NSArray *)arr;

@end
