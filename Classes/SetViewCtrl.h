//
//  SetViewCtrl.h
//  BookWorksApp
//
//  Created by 刁志远 on 12-3-30.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AllMethods.h"
#import "UIPopoverListView.h"

#import "PPiFlatSegmentedControl.h"
#import "QLLoginView.h"


@interface SetViewCtrl : UIViewController <UIAlertViewDelegate,UIPopoverListViewDataSource,UIPopoverListViewDelegate>{
	UITableView *myTable;
	NSArray *arrList;
    
    int curSel;
    NSString *curFont;
    PPiFlatSegmentedControl *segButton;
    
    BOOL isFirst,refresh;
    NSTimer *timerRefresh;
}
@property (nonatomic,retain) IBOutlet UITableView *myTable;
@property (strong, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;


@end
