//
//  About.m
//  BookWorksApp
//
//  Created by diaozhiyuan on 11-11-3.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "About.h"

#import <QuartzCore/QuartzCore.h>


@implementation About


-(void)setConLabContent:(NSString *)str{
	strContent = [[NSString alloc] initWithString:str];
}
- (IBAction)backToUpView:(id)sender{
	[self.navigationController popViewControllerAnimated:YES];
}

-(id)init{
	self = [super init];
	if (self) {
		self.navigationItem.title = @"关于";
		self.navigationController.navigationBar.hidden = NO;
		self.tabBarItem.image = [UIImage imageNamed:@"about.png"];
	}
	
	return self;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	UIScrollView *sv = (UIScrollView *)[self.view viewWithTag:10];
	
	UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScreenWidth-20, ScreenHeight-64-20)];
	lab.numberOfLines = 120;
	lab.backgroundColor = [UIColor clearColor];
    lab.textColor = [UIColor darkGrayColor];
    
    
//    UITextView *txt ;
//    txt.layoutManager.allowsNonContiguousLayout = NO;
    
	
	if (strContent) {
		lab.text = strContent;
        self.navigationItem.title = NSLocalizedString(@"Nav_Sumary", @"");
	}
	else {
        self.navigationItem.title = NSLocalizedString(@"Nav_About", @"");
		lab.text = @"1.软件资源收集于网络，如果喜欢请购买正版图书，软件中所有书籍版权归作者所有。\n\r"\
		"2.如有任何意见或者建议，请联系作者：diaozhiyuan95@gmail.com\n\r"\
        "3.小说阅读是集下载、阅读于一体的电子书阅读软件，软件中电子书全部收集于网络，供大家免费下载。\n\r";
	}

	
    CGSize size = [lab.text sizeWithFont:lab.font
                       constrainedToSize:CGSizeMake(lab.frame.size.width, CGFLOAT_MAX)];
    CGRect frame = lab.frame;
    frame.size.height = size.height;
    lab.frame = frame;
    
	lab.textAlignment = UITextAlignmentLeft;
	lab.contentMode = UIViewContentModeTopLeft;
	
	[sv addSubview:lab];
	[sv setContentSize:CGSizeMake(ScreenWidth, ScreenHeight)];
	
//    [sv setBackgroundColor:CELL_COLOR];
	
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btn setBackgroundColor:[UIColor blackColor]];
	[btn setFrame:CGRectMake(0, 8, 55, 35)];
	[btn setTitle:NSLocalizedString(@"Nav_Back", @"") forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
	[btn addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
    
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
	
	UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.leftBarButtonItem = item;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
