//
//  HorizontalHead.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/25.
//
//

#import "HorizontalHead.h"

#define ZY_ARROW_SIDE 15.f

@implementation HorizontalHead


@synthesize idleTitle = _idleTitle;
@synthesize triggerTitle = _triggerTitle;

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self addSubview:self.arrowView];
        [self addSubview:self.label];
        
        self.arrowView.image = [UIImage imageNamed:@"banner_arrow"];
        self.state = HorizontalHeaderStateIdle;
        
        self.frame = frame;
        [self layoutSubviews];
        self.label.text = self.idleTitle;
        self.arrowView.transform = CGAffineTransformMakeRotation(M_PI);
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat arrowX = self.bounds.size.width / 2 + 2;
    CGFloat arrowY = self.bounds.size.height / 2 - ZY_ARROW_SIDE / 2;
    CGFloat arrowW = ZY_ARROW_SIDE;
    CGFloat arrowH = ZY_ARROW_SIDE;
    self.arrowView.frame = CGRectMake(arrowX, arrowY, arrowW, arrowH);
    
    CGFloat labelX = self.bounds.size.width / 2 - ZY_ARROW_SIDE - 2;
    CGFloat labelY = 0;
    CGFloat labelW = ZY_ARROW_SIDE;
    CGFloat labelH = self.bounds.size.height;
    self.label.frame = CGRectMake(labelX, labelY, labelW, labelH);
}

#pragma mark - setters & getters

- (void)setState:(HorizontalHeadState)state
{
    
    if (_state == state) {
        return;
    }
    _state = state;
    
    switch (state) {
        case HorizontalHeaderStateIdle:
        {
            self.label.text = self.idleTitle;
            [UIView animateWithDuration:0.3 animations:^{
                self.arrowView.transform = CGAffineTransformMakeRotation(M_PI);
            }];
            
        }
            break;
        case HorizontalHeaderStateTrigger:
        {
            self.label.text = self.triggerTitle;
            [UIView animateWithDuration:0.3 animations:^{
                self.arrowView.transform = CGAffineTransformMakeRotation(0);
            }];
        }
            break;
            
        default:
            break;
    }
}

- (UIImageView *)arrowView
{
    if (!_arrowView) {
        _arrowView = [[UIImageView alloc] init];
    }
    return _arrowView;
}

- (UILabel *)label
{
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.font = [UIFont systemFontOfSize:13];
        _label.textColor = [UIColor darkGrayColor];
        _label.numberOfLines = 0;
    }
    return _label;
}

- (void)setIdleTitle:(NSString *)idleTitle
{
    _idleTitle = idleTitle;
    
    if (self.state == HorizontalHeaderStateIdle) {
        self.label.text = idleTitle;
    }
}

- (NSString *)idleTitle
{
    if (!_idleTitle) {
        _idleTitle = @"右滑加载上一章"; // default
    }
    return _idleTitle;
}

- (void)setTriggerTitle:(NSString *)triggerTitle
{
    _triggerTitle = triggerTitle;
    
    if (self.state == HorizontalHeaderStateTrigger) {
        self.label.text = triggerTitle;
    }
}

- (NSString *)triggerTitle
{
    if (!_triggerTitle) {
        _triggerTitle = @"释放加载上一章"; // default
    }
    return _triggerTitle;
}

@end
