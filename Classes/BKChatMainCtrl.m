//
//  BKChatMainCtrl.m
//  BookWorksApp
//
//  Created by 刁志远 on 16/5/16.
//
//

#import "BKChatMainCtrl.h"

#import "SWTableViewCell.h"

#import "ChatViewController.h"

#import "BKGroupList.h"
#import "BKUserList.h"

#import <PopoverView/PopoverView.h>

#import "ChatViewController.h"
#import "SessionViewCell.h"

//两次提示的默认间隔
static const CGFloat kDefaultPlaySoundInterval = 3.0;
static NSString *kMessageType = @"MessageType";
static NSString *kConversationChatter = @"ConversationChatter";
static NSString *kGroupName = @"GroupName";

@interface BKChatMainCtrl ()<UITableViewDelegate, UITableViewDataSource,SWTableViewCellDelegate,PopoverViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate> {
    
    NSMutableArray *arrList;
    NSMutableArray *arrBuddyList;
    
    NSString *curUserName;
    
    NSDictionary *dicAllUser;
    
    UITableViewCell * _memoryCell;
}

@property (nonatomic, weak) IBOutlet UITableView *myTable;

@property (strong, nonatomic) NSDate *lastPlaySoundDate;


@end

@implementation BKChatMainCtrl

#pragma mark - SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    
    
    
}


#pragma mark - UITableViewDelegate

- (nullable NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return @"退出群组";
    }
    return @"删除";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 30)];
    view.backgroundColor = RGBACOLOR(239, 239, 239, 1.0f);
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, view.frame.size.width-15, view.frame.size.height)];
    
    lab.font = [UIFont systemFontOfSize:13];
    lab.textColor = [UIColor blackColor];
    [view addSubview:lab];
    
    if (section == 0) {
        
        lab.text = @"我的群";
    }
    else {
        lab.text = @"消息";
    }
    
    return view;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (![userDefaults objectForKey:@"nick"] || ![userDefaults objectForKey:@"head"] ) {
        [AllMethods showAltMsg:@"亲，需要先在‘设置’中设置昵称和头像!"];
        return;
    }
    
    if (indexPath.section == 0) {
        ECGroup *g = arrList[indexPath.row];
        ChatViewController *chat = [[ChatViewController alloc] initWithSessionId:g.groupId];
        
        [self.tabBarController setHidesBottomBarWhenPushed:YES];
        [chat setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:chat animated:YES];
        [self.tabBarController setHidesBottomBarWhenPushed:NO];
    }
    else if (indexPath.section == 1) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        ECSession* session = [arrBuddyList objectAtIndex:indexPath.row];
        session.isAt = NO;
        [[IMMsgDBAccess sharedInstance] updateSession:session];
        session.unreadCount = 0;
        if (session.type == 100) {
            [[DeviceDBHelper sharedInstance] markGroupMessagesAsRead];
            UIViewController* viewController = [[NSClassFromString(@"GroupNoticeViewController") alloc] init];
            
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [viewController setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:viewController animated:YES];
            [self.tabBarController setHidesBottomBarWhenPushed:NO];
            
            
        } else {
            
            UIViewController* viewController = [[NSClassFromString(@"ChatViewController") alloc] init];
            SEL aSelector = NSSelectorFromString(@"ECDemo_setSessionId:");
            if ([viewController respondsToSelector:aSelector]) {
                IMP aIMP = [viewController methodForSelector:aSelector];
                void (*setter)(id, SEL, NSString*) = (void(*)(id, SEL, NSString*))aIMP;
                setter(viewController, aSelector,session.sessionId);
            }
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [viewController setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:viewController animated:YES];
            [self.tabBarController setHidesBottomBarWhenPushed:NO];
            
        }
        [tableView reloadData];
        
    }
    
     [[BookWorksAppAppDelegate shareInstance] setADHidden:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 65;
    }
    return 65.0f;
}



#pragma mark - UITableViewDataSource

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        [WSProgressHUD showWithStatus:@"退出中..."];
        
        ECGroup *g = arrList[indexPath.row];
        [[ECDevice sharedInstance].messageManager quitGroup:g.groupId
                                                 completion:^(ECError *error, NSString *groupId) {
                                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                     if (error.errorCode == ECErrorType_NoError) {
                                                         [WSProgressHUD showSuccessWithStatus:@"已退出"];
                                                         [self requestChatList];
                                                     }
                                                     else{
                                                         [WSProgressHUD showErrorWithStatus:error.errorDescription];
                                                     }
                                                 }];
        
    }
    else {
        
        
        ECSession *session = arrBuddyList[indexPath.row];
        
        
        [[DeviceDBHelper sharedInstance] clearGroupMessageTable];
        [[DeviceDBHelper sharedInstance] deleteAllMessageOfSession:session.sessionId];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [arrBuddyList removeObjectAtIndex:indexPath.row];
            _memoryCell = nil;
            [_myTable reloadData];
        });
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    if(section == 0)
        return arrList.count;
    
    return arrBuddyList.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        static NSString *sid = @"cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
        
        
        if (!cell) {
            cell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:sid];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(15, 9, 40, 40)];
            iv.layer.cornerRadius = iv.frame.size.height/2;
            iv.clipsToBounds = YES;
            iv.backgroundColor = RGBACOLOR(239, 239, 239, 1.0f);
            iv.tag = 1;
            [cell.contentView addSubview:iv];
            
            
            UILabel *lab_title = [[UILabel alloc] initWithFrame:CGRectMake(ORIGINAL_X(iv)+10, 0, ScreenWidth-ORIGINAL_X(iv)-15, 58)];
            lab_title.textColor = [UIColor blackColor];
            lab_title.tag = 2;
            [cell.contentView addSubview:lab_title];
        }
        
        UILabel *lab = (UILabel *)[cell.contentView viewWithTag:2];
        if (indexPath.section == 0) {//群组
            ECGroup *group = arrList[indexPath.row];
            lab.text = group.name;
        }
        UIImageView *iv = (UIImageView *)[cell.contentView viewWithTag:1];
        iv.image = [UIImage imageNamed:@"group_head"];
        
        
        return cell;
    }
    else {
        
        static NSString *sessioncellid = @"sessionCellidentifier";
        SessionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sessioncellid];
        
        if (cell == nil) {
            cell = [[SessionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sessioncellid];
            
            UILongPressGestureRecognizer  * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellLongPress:)];
            cell.nameLabel.tag =100;
            [cell.contentView addGestureRecognizer:longPress];
            cell.contentView.userInteractionEnabled = YES;
        }
        
        ECSession* session = [arrBuddyList objectAtIndex:indexPath.row];
        cell.session = session;
        [cell updateCellUI];
        
        return cell;
    }
    
    
}


#pragma mark - PopoverViewDelegate

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index {
    
    [popoverView dismiss];
    
    if (index == 0) {
        BKGroupList *group = [[BKGroupList alloc] initWithAlreadyAddGroupList:arrList];
        
        
        [self.tabBarController setHidesBottomBarWhenPushed:YES];
        [group setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:group animated:YES];
        [self.tabBarController setHidesBottomBarWhenPushed:NO];
    }
    else {
        BKUserList *user = [[BKUserList alloc] init];
        
        [self.tabBarController setHidesBottomBarWhenPushed:YES];
        [user setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:user animated:YES];
        [self.tabBarController setHidesBottomBarWhenPushed:NO];
    }
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
}


#pragma mark - Request Action 


- (void)requestChatList {
    
    __weak BKChatMainCtrl *weak = self;
    
    [[ECDevice sharedInstance].messageManager
     queryOwnGroupsWith: ECGroupType_Group/ECGroupType_Discuss
     completion:^(ECError *error, NSArray *groups) {
         
         if (error.errorCode == ECErrorType_NoError) {
             
             if (!arrList) {
                 arrList = [NSMutableArray arrayWithCapacity:3];
             }
             [arrList removeAllObjects];
             [arrList addObjectsFromArray:groups];
             [weak.myTable reloadData];
         } else {
             
         }
     }];
    
    
}

- (void)updateUserList {
    
    dicAllUser = [NSDictionary dictionaryWithDictionary:[AllMethods getAllUserInfo]];
    
    if (!arrBuddyList) {
        arrBuddyList = [NSMutableArray arrayWithCapacity:3];
    }
    
    

}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;{
    NSIndexPath * path = [_myTable indexPathForCell:_memoryCell];
    ECSession* session = [arrBuddyList objectAtIndex:path.row];
    if (buttonIndex == 0) {
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"删除该会话";
        hud.margin = 10.0f;
        hud.removeFromSuperViewOnHide = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            /*
            if (session.type == 100) {
                [[DeviceDBHelper sharedInstance] clearGroupMessageTable];
            } else {
                [[DeviceDBHelper sharedInstance] deleteAllMessageOfSession:session.sessionId];
            }
            */
            
            [[DeviceDBHelper sharedInstance] clearGroupMessageTable];
            [[DeviceDBHelper sharedInstance] deleteAllMessageOfSession:session.sessionId];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [arrBuddyList removeObjectAtIndex:path.row];
                _memoryCell = nil;
                [_myTable reloadData];
            });
        });
    } else if (buttonIndex==2 && session.type !=100) {
        NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
        BOOL isTop = [buttonTitle isEqualToString:@"置顶会话"];
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = buttonTitle;
        hud.margin = 10.0f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
        __weak  __typeof(self) weakSelf = self;
        [[ECDevice sharedInstance].messageManager setSession:session.sessionId IsTop:isTop completion:^(ECError *error, NSString *seesionId) {
            __strong  __typeof(weakSelf)strongSelf = weakSelf;
            if (error.errorCode == ECErrorType_NoError && isTop) {
                [[DeviceDBHelper sharedInstance].topContactLists addObject:session.sessionId];
                NSSet *set = [NSSet setWithArray:[DeviceDBHelper sharedInstance].topContactLists];
                [[DeviceDBHelper sharedInstance].topContactLists removeAllObjects];
                [[DeviceDBHelper sharedInstance].topContactLists addObjectsFromArray:set.allObjects];
                session.isTop = isTop;
                NSIndexPath *zeroIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                if (path.row!=0) {
                    [strongSelf.myTable moveRowAtIndexPath:[NSIndexPath indexPathForRow:path.row inSection:0] toIndexPath:zeroIndexPath];
                }
            }
            isTop==YES?:[[DeviceDBHelper sharedInstance].topContactLists removeObject:session.sessionId];
            [[IMMsgDBAccess sharedInstance] updateIsTopSessionId:session.sessionId isTop:isTop];
            [DeviceDBHelper sharedInstance].sessionDic = nil;
            [strongSelf prepareDisplay];
        }];
    }
}

#pragma mark - Private Method

-(void)cellLongPress:(UILongPressGestureRecognizer * )longPress{
    
    if (longPress.state == UIGestureRecognizerStateBegan) {
        
        CGPoint point = [longPress locationInView:self.myTable];
        NSIndexPath * indexPath = [self.myTable indexPathForRowAtPoint:point];
        if(indexPath == nil) return ;
        SessionViewCell  * cell = (SessionViewCell *)[self.myTable cellForRowAtIndexPath:indexPath];
        UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:cell.nameLabel.text delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"删除" otherButtonTitles:nil];
        NSString *str = cell.session.isTop?@"取消置顶":@"置顶会话";
        if (cell.session.type !=100) {
            [sheet addButtonWithTitle:str];
        }
        [sheet showInView:cell];
        _memoryCell = cell;
    }
}

-(void)prepareDisplay {
    dispatch_async(dispatch_get_main_queue(), ^{
        [arrBuddyList removeAllObjects];
        [arrBuddyList addObjectsFromArray:[[DeviceDBHelper sharedInstance] getMyCustomSession]];
        [self.myTable reloadData];
    });
}

- (void)rightItemClicked:(id)sender {
    
//    PopoverView *pop = [[PopoverView alloc] initWithFrame:[[sender customView] frame]];
//    pop.delegate = self;
//    [pop showAtPoint:[[sender customView] center] inView:self.view withStringArray:@[@"添加群聊",@"添加好友"]];
    
    BKGroupList *group = [[BKGroupList alloc] initWithAlreadyAddGroupList:arrList];
    
    
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [group setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:group animated:YES];
    [self.tabBarController setHidesBottomBarWhenPushed:NO];
    
}

- (void)leftItemClicked:(id)sender {
    
    
}


#pragma mark - Build UI

- (void)buildLeftItem {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.frame = CGRectMake(0, 0, 80, 40);
    btn.titleLabel.font = [AllMethods getFontWithSize:15];
    [btn setTitle:@"设为已读" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
}

- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
}


#pragma mark - SYS

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self requestChatList];
    [self updateUserList];
    [self prepareDisplay];
    
    [[BookWorksAppAppDelegate shareInstance] setADHidden:NO];
  }

- (id)init {
    
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationItem.title = NSLocalizedString(@"Tab_Chat", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                           target:self
                                                                                           action:@selector(rightItemClicked:)];
//    [self buildLeftItem];
    [self buildTableView];
    
    [[ECDevice sharedInstance].messageManager getTopSessionLists:^(ECError *error, NSArray *topContactLists) {
        if (error.errorCode == ECErrorType_NoError) {
            [[DeviceDBHelper sharedInstance].topContactLists removeAllObjects];
            [[DeviceDBHelper sharedInstance].topContactLists addObjectsFromArray:topContactLists];
            for (NSString *sessionId in topContactLists) {
                [[IMMsgDBAccess sharedInstance] updateIsTopSessionId:sessionId isTop:YES];
            }
            [DeviceDBHelper sharedInstance].sessionDic = nil;
        }
    }];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareDisplay) name:KNOTIFICATION_onMesssageChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareDisplay) name:KNOTIFICATION_onReceivedGroupNotice object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self.myTable
                                             selector:@selector(reloadData)
                                                 name:KNotice_ReloadSessionGroup object:nil];
    
    [self prepareDisplay];
    
    
    ECPersonInfo *person = [[ECPersonInfo alloc] init];
    BOOL bol = NO;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:@"head"]) {
        bol = YES;
        person.sign = [userDefaults valueForKey:@"head"];
    }
    if ([userDefaults valueForKey:@"nick"]) {
        bol = YES;
        person.nickName = [userDefaults valueForKey:@"nick"];
    }
    
    [[ECDevice sharedInstance] setPersonInfo:person completion:^(ECError *error, ECPersonInfo *person) {
        if (error.errorCode == ECErrorType_NoError) {
            [DemoGlobalClass sharedInstance].sign = person.sign;
            [DemoGlobalClass sharedInstance].dataVersion = person.version;
            [DemoGlobalClass sharedInstance].isNeedSetData = NO;
        }
        else {
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
