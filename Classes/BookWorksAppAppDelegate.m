//
//  BookWorksAppAppDelegate.m
//  BookWorksApp
//
//  Created by diaozhiyuan on 11-11-3.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BookWorksAppAppDelegate.h"

#import "About.h"
#import "BKSetViewMain.h"


#import "BookStore.h"
#import "BKQuestionList.h"

//#import "BWInUpdateView.h"
#import "BKNewUpdateCtrl.h"
#import "BKContent.h"
#import "BKNewContent.h"

#import "JPUSHService.h"

//#import "BKBookListCtrl.h"
#import "BKNewMainBookList.h"

#import "MLBlackTransition.h"

#import "BaseNavigationCtrl.h"


#import "MZDownloadManagerViewController.h"

#import <Harpy/Harpy.h>

//#import "GDTTrack.h"

#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

#define NAVBAR_COLOR [UIColor colorWithRed:212/255.0 green:183/255.0 blue:70/255.0 alpha:1.0f]

#if kFREE


@interface BookWorksAppAppDelegate ()<UIWebViewDelegate,GDTSplashAdDelegate,GDTMobBannerViewDelegate,UIAlertViewDelegate,GDTMobInterstitialDelegate> {
    
    GDTMobBannerView *adView;
    GDTSplashAd *splashView;
    GDTMobInterstitial *insAdView;
    
    UIImageView *ivADBK;
    
    
    BOOL isAdHide;

#else

@interface BookWorksAppAppDelegate ()<UIWebViewDelegate,UIAlertViewDelegate> {

#endif

}

@end

@implementation BookWorksAppAppDelegate

@synthesize window;


#if kFREE
    
#pragma mark - Ad method
    
    
    
    - (void)closeAD:(id)sender {
        
//        [self setADHidden:YES];
        
        [adView setShowCloseBtn:YES];
        UIButton *btn = (UIButton *)sender;
        btn.enabled = YES;
        [adView removeFromSuperview];
    }
    
#pragma mark - GDTMobInterstitialDelegate
    
    - (void)interstitialDidDismissScreen:(GDTMobInterstitial *)interstitial {
        
//        NSLog(@"---interstitialDidDismissScreen");
        [[Harpy sharedInstance] checkVersionDaily];
    }
    
    - (void)interstitialFailToLoadAd:(GDTMobInterstitial *)interstitial error:(NSError *)error
    {
        [insAdView loadAd];
    }
    - (void)interstitialSuccessToLoadAd:(GDTMobInterstitial *)interstitial
    {
//     ..   [insAdView presentFromRootViewController:self.window.rootViewController];
        [insAdView presentFromRootViewController:[self.window.rootViewController presentedViewController]];
    }
    
    
    - (void)interstitialAdDidDismissFullScreenModal:(GDTMobInterstitial *)interstitial {
        
//        [self removeAdView];
    }
    
    - (void)interstitialClicked:(GDTMobInterstitial *)interstitial {
        
    }
    
    
#pragma mark - GDTSplashAdDelegate
    
    
    -(void)splashAdSuccessPresentScreen:(GDTSplashAd *)splashAd {
        [self setADHidden:YES];
    }
    
    
    -(void)splashAdClosed:(GDTSplashAd *)splashAd
    {
        splashView = nil;
        
        [self setADHidden:NO];
//        NSLog(@"---splashAdClosed");
        [[Harpy sharedInstance] checkVersionDaily];
    }


#pragma mark - GDTMobBannerViewDelegate
    
//    - (void)bannerViewDidReceived {
//        adView.hidden = isAdHide;
//    }
//    
//    - (void)bannerViewWillExposure {
//        adView.hidden = isAdHide;
//    }
    
    -(void) bannerViewClicked {
        
        
//        if (self.canHideAd) {
//            
//            __weak BookWorksAppAppDelegate *weakSelf = self;
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                
//                if (weakSelf.canHideAd) {
////                    [WSProgressHUD showSuccessWithStatus:@"广告展示成功，广告已隐藏"];
//                    [weakSelf setADHidden:YES];
//                }
//                else {
////                    [AllMethods showAltMsg:@"广告展示时间不足10秒，展示10秒后，广告条会自动隐藏，本次看书不再显示"];
//                }
//            });
//        }
        
        UIButton *btn = (UIButton *)[adView viewWithTag:13579];
        if(!btn.enabled) {
            btn.enabled = YES;
        }
    }
    -(void) didDismissLandingPage {
        self.canHideAd = NO;
    }
    
#endif



#pragma mark - Custom Action
    
    - (void)buildInsAdView {
        
        //        adView = [[BaiduMobAdInterstitial alloc] init];
        //        adView.delegate = self;
        //        adView.AdUnitTag = @"2084519";
        //        adView.interstitialType = BaiduMobAdViewTypeInterstitialOther;
        
#if kFREE
        
        insAdView = [[GDTMobInterstitial alloc] initWithAppkey:kGDT_APP_ID
                                                   placementId:kGDT_INTER_ID];
        
        insAdView.delegate = self;
#endif
    }
    
    - (void)buildADView {
        
        
#if kFREE
        
        if (!adView.superview) {
            [self.tabCtrl.view addSubview:adView];
            [adView loadAdAndShow];
        }
        
//        NSLog(@"%@",[self.window.rootViewController presentedViewController]);
        if ([self.window.rootViewController presentedViewController]) {
            if (![[self.window.rootViewController presentedViewController] isKindOfClass:[UIAlertController class]]) {
                [insAdView loadAd];
            }
        }
        else {
            splashView = [[GDTSplashAd alloc] initWithAppkey:kGDT_APP_ID
                                                 placementId:kGDT_SPL_ID];
            splashView.delegate = self;
            splashView.fetchDelay = 5;
            
            [splashView loadAdAndShowInWindow:self.window];
        }
        
        
#endif
    }


- (BOOL)addSkipBackupAttributeToFileAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (void)applicationDocumentsDirectory {
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    //初始化临时文件路径
    NSString *folderPath = [path stringByAppendingPathComponent:@"data"];
    //创建文件管理
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //判断temp文件夹是否存在
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath];
    if (!fileExists) {//如果不存在说创建,因为下载时,不会自动创建文件夹
        [fileManager createDirectoryAtPath:folderPath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil];
    }
    
    NSURL *pathURL= [NSURL fileURLWithPath:folderPath];
    if([self addSkipBackupAttributeToItemAtURL:pathURL]) ;//NSLog(@"add directory to not backip");
}

- (void)showAllFonts{
    
    NSArray *familyNames =[[NSArray alloc]initWithArray:[UIFont familyNames]];
    
    NSArray *fontNames;
    
    NSInteger indFamily, indFont;
    
    for(indFamily=0;indFamily<[familyNames count];++indFamily)
    {
        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames =[[NSArray alloc]initWithArray:[UIFont fontNamesForFamilyName:[familyNames objectAtIndex:indFamily]]];
        for(indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"    Font name: %@",[fontNames objectAtIndex:indFont]);
        }
    }
    
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    const char* filePath = [[URL path] fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
    
}
    
    - (void)changeADCenter:(CGPoint)pot{
        
#if kFREE
        
        adView.center = pot;
#endif
    }
    
    - (void)setADHidden:(BOOL)hidden{
        
#if kFREE
        
        isAdHide = hidden;
        
        if(hidden) {
            [adView removeFromSuperview];
        }
        else {
            [self.tabCtrl.view addSubview:adView];
        }
        
        adView.hidden = hidden;
        if (hidden) {
            adView.alpha = 0.0f;
        }
        else {
            adView.alpha = 1.0f;
        }
#endif
        
    }
    

#pragma mark - Build Content UI
    

- (void)buildTabCtrlContent {
    
//    BKBookListCtrl *m_ctrl = [[BKBookListCtrl alloc] init];
    BKNewMainBookList *m_ctrl = [[BKNewMainBookList alloc] init];
    BaseNavigationCtrl *nav1 = [[BaseNavigationCtrl alloc] initWithRootViewController:m_ctrl];
    
    BookStore *nc = [[BookStore alloc] init];
    BaseNavigationCtrl *nav3 = [[BaseNavigationCtrl alloc] initWithRootViewController:nc];
    
    BKSetViewMain *sc = [[BKSetViewMain alloc] init];
    BaseNavigationCtrl *nav5 = [[BaseNavigationCtrl alloc] initWithRootViewController:sc];
    
    BKQuestionList *ques = [[BKQuestionList alloc] init];
    BaseNavigationCtrl *nav2 = [[BaseNavigationCtrl alloc] initWithRootViewController:ques];
    
//    BWInUpdateView *up = [[BWInUpdateView alloc] init];
    BKNewUpdateCtrl *up = [[BKNewUpdateCtrl alloc] init];
    BaseNavigationCtrl *nav4 = [[BaseNavigationCtrl alloc] initWithRootViewController:up];
    

    nav1.tabBarItem.image = [UIImage imageNamed:@"tab1"];
    nav1.title = @"书架";
    
    nav2.tabBarItem.image = [UIImage imageNamed:@"tab3"];
    nav2.title = @"问答";//NSLocalizedString(@"Tab_Chat", @"");
    
    nav3.tabBarItem.image = [UIImage imageNamed:@"tab2"];
    nav3.title = @"书城";
    
    nav4.tabBarItem.image = [UIImage imageNamed:@"tab5"];
    nav4.title = @"连载";
    
    nav5.tabBarItem.image = [UIImage imageNamed:@"tab4"];
    nav5.title = @"设置";
    
    _tabCtrl = [[UITabBarController alloc] init];
    [self.tabCtrl setViewControllers:[NSMutableArray arrayWithObjects:nav1,nav3,nav4,nav2,nav5,nil]];
}

- (void)customNavBar {
    
    
    [[UINavigationBar appearance] setTintColor: [UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:NAV_COLOR];
    UIColor *titleColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:titleColor,
                                                            NSFontAttributeName: [AllMethods getFontWithSize:18],}];
}

- (void)buildADContent {
    
#if kFREE
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        adView = [[GDTMobBannerView alloc] initWithFrame:CGRectMake(0,ScreenHeight-50-50,ScreenWidth,50)
                                                      appkey:kGDT_APP_ID
                                                    placementId:kGDT_BANNER_ID];
    } else {
        adView = [[GDTMobBannerView alloc] initWithFrame:CGRectMake(0,ScreenHeight-50-50,ScreenWidth,50)
                                                      appkey:kGDT_APP_ID placementId:kGDT_BANNER_ID];
    }
    
    
    
    adView.delegate = self;
    adView.currentViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    adView.isAnimationOn = YES;
    adView.showCloseBtn = NO;
    adView.isGpsOn = NO;
    [adView loadAdAndShow];
    
    
    
    adView.center = CGPointMake(ScreenWidth/2, adView.center.y);
    [self.tabCtrl.view addSubview:adView];
    
    UIView *view = [adView viewWithTag:13579];
    if (view) {
        [view removeFromSuperview];
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = RGBACOLOR(71, 71, 71, .5f);
    btn.frame = CGRectMake(0, 0, 18, 18);
    btn.clipsToBounds = YES;
    btn.layer.cornerRadius = btn.frame.size.width/2;
    [btn setBackgroundImage:[UIImage imageNamed:@"cl"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"cl"] forState:UIControlStateDisabled];
    [btn addTarget:self action:@selector(closeAD:) forControlEvents:UIControlEventTouchUpInside];
    [btn setEnabled:NO];
    btn.tag = 13579;
    [adView addSubview:btn];
    
#endif
    
}
    
    - (void)getNewVersionFeature {
        
        
        NSString *ver = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        if (![[NSUserDefaults standardUserDefaults] valueForKey:ver]) {
            
            dispatch_async(dispatch_queue_create("get_feature", nil), ^{
                
                NSString *surl = [NSString stringWithFormat:@"http://dydog.sinaapp.com/books.php?method=15&ver=%@",ver];
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:surl]];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    if (data) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:ver];
                        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:NSJSONReadingAllowFragments
                                                                              error:nil];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"更新提示"
                                                                        message:@""
                                                                       delegate:nil
                                                              cancelButtonTitle:@"我知道了"
                                                              otherButtonTitles: nil];
                        
                        UIView *view = [[UIView alloc] init];
                        
                        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, -10, 240, 100)];
                        textLabel.font = [UIFont systemFontOfSize:13];
                        textLabel.numberOfLines =0;
                        textLabel.textAlignment =NSTextAlignmentLeft;
                        textLabel.text = dic[@"feature"];
                        [view addSubview:textLabel];
                        [alert setValue:view forKey:@"accessoryView"];
                        [alert show];
                    }
                });
                
            });
            
            
            
        }
        
    }

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.
    
//    [self getNewVersionFeature];
    
    [AllMethods createNewUpdateTable];
    [AllMethods createDownloadListTable];
    [AllMethods createReadProgressTable];
    
#if kFREE
    isAdHide = NO;
#endif
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:kCHAT_NOTI_KEY]) {
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kCHAT_NOTI_KEY];
    }
    [AllMethods clearDocumentTrash];
    [AllMethods createTableBuddy];
    
    [WSProgressHUD setProgressHUDIndicatorStyle:WSProgressHUDIndicatorMMSpinner];
    [MLBlackTransition validatePanPackWithMLBlackTransitionGestureRecognizerType:MLBlackTransitionGestureRecognizerTypeScreenEdgePan];
    
    NSString *str = [[launchOptions valueForKey:@"UIApplicationLaunchOptionsURLKey"] absoluteString];
    if ([str length] > 0) {
        str = [str stringByReplacingOccurrencesOfString:@"readinganddownload://novel?font=" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"/" withString:@""];
        
        [[NSUserDefaults standardUserDefaults] setValue:str forKey:kFONT];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kFONT] length] <= 0) {
        [[NSUserDefaults standardUserDefaults] setValue:@"Arial" forKey:kFONT];
    }
    
    if( ![[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE]){
        [[NSUserDefaults standardUserDefaults] setValue:@(18.5) forKey:kFONT_SIZE];
    }
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE] floatValue] == 17.f){
        [[NSUserDefaults standardUserDefaults] setValue:@(18.5) forKey:kFONT_SIZE];
    }
    
    [self applicationDocumentsDirectory];
    
    
    [self buildTabCtrlContent];
    [self customNavBar];
    
    
    self.window.rootViewController = _tabCtrl;
    [self.window addSubview:_tabCtrl.view];
    [self.window makeKeyAndVisible];

    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                          UIUserNotificationTypeSound |
                                                          UIUserNotificationTypeAlert)
                                              categories:nil];
    } else {
        //categories 必须为nil
        [JPUSHService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                          UIRemoteNotificationTypeSound |
                                                          UIRemoteNotificationTypeAlert)
                                              categories:nil];
    }
    
    [JPUSHService setLogOFF];
    
    
#if (kFREE == 1)
    [JPUSHService setupWithOption:launchOptions
                           appKey:@"dc796f209a4dcbc35b59a769"
                          channel:@"Publish channel"
                 apsForProduction:YES
            advertisingIdentifier:nil];
    [[Harpy sharedInstance] setAppID:@"682215901"];
#else
    [JPUSHService setupWithOption:launchOptions
                           appKey:@"6718a2ba05062baa13bc31d4"
                          channel:@"Publish channel"
                 apsForProduction:YES
            advertisingIdentifier:nil];
    [[Harpy sharedInstance] setAppID:@"673879657"];
#endif
    
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [WSProgressHUD dismiss];
    
    [self buildADContent];
    [self buildInsAdView];
    
    
//    for (int i=0;i<6;i++) {
//        
//        NSString *file = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%d",i+1]
//                                                         ofType:@"rar"];
//        if (file) {
//            
//            if([AllMethods isRarFile:[NSData dataWithContentsOfFile:file]])
//                NSLog(@"yes");
//            else {
//                NSLog(@"No");
//            }
//        }
//        
//    }
    
//    [Helpshift installForApiKey:@"25739b735cdbfc489d916dc0fa23eb5b"
//                     domainName:@"dhcc.helpshift.com"
//                          appID:@"dhcc_platform_20151106065334402-d99705ed1950084"];
    
    MZDownloadManagerViewController *mzDownloadingViewObj = [MZDownloadManagerViewController sharedInstance];
    mzDownloadingViewObj.sessionManager = [mzDownloadingViewObj backgroundSession];
    [mzDownloadingViewObj populateOtherDownloadTasks];
    
    
    [[Harpy sharedInstance] setPresentingViewController:self.tabCtrl];
    [[Harpy sharedInstance] setAppName:@"阅读小说"];
    
    return YES;
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    [JPUSHService handleRemoteNotification:userInfo];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//    NSLog(@"Regist fail%@",error);
}
    
    - (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
        [JPUSHService registerDeviceToken:deviceToken];
    }

#ifdef __IPHONE_7_0
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNoData);
}
#endif
    
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#pragma mark- JPUSHRegisterDelegate
    - (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
        NSDictionary * userInfo = notification.request.content.userInfo;
        
        UNNotificationRequest *request = notification.request; // 收到推送的请求
        UNNotificationContent *content = request.content; // 收到推送的消息内容
        
        NSNumber *badge = content.badge;  // 推送消息的角标
        NSString *body = content.body;    // 推送消息体
        UNNotificationSound *sound = content.sound;  // 推送消息的声音
        NSString *subtitle = content.subtitle;  // 推送消息的副标题
        NSString *title = content.title;  // 推送消息的标题
        
        if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
            [JPUSHService handleRemoteNotification:userInfo];
            NSLog(@"iOS10 前台收到远程通知:%@", userInfo);
            
            
            
        }
        else {
            // 判断为本地通知
            NSLog(@"iOS10 前台收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
        }
        completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
    }
    
    - (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
        
        NSDictionary * userInfo = response.notification.request.content.userInfo;
        UNNotificationRequest *request = response.notification.request; // 收到推送的请求
        UNNotificationContent *content = request.content; // 收到推送的消息内容
        
        NSNumber *badge = content.badge;  // 推送消息的角标
        NSString *body = content.body;    // 推送消息体
        UNNotificationSound *sound = content.sound;  // 推送消息的声音
        NSString *subtitle = content.subtitle;  // 推送消息的副标题
        NSString *title = content.title;  // 推送消息的标题
        
        if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
            [JPUSHService handleRemoteNotification:userInfo];
            NSLog(@"iOS10 收到远程通知:%@", userInfo);
            
            
        }
        else {
            // 判断为本地通知
            NSLog(@"iOS10 收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
        }
        
        completionHandler();  // 系统要求执行这个方法
    }
#endif


//- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
//{
//    NSLog(@"%@",url);
//    NSString *str = [url parameterString];
//    NSLog(@"%@",str);
//    return YES;
//}
//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation
//{
//    
//    return [ShareSDK handleOpenURL:url
//                 sourceApplication:sourceApplication
//                        annotation:annotation
//                        wxDelegate:self];;
//}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    NSString *str = [url absoluteString];
    str = [str stringByReplacingOccurrencesOfString:@"readinganddownload://novel?font=" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    return YES;
}
    + (UIViewController *)getPresentedViewController
    {
        UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
        UIViewController *topVC = appRootVC;
        if (topVC.presentedViewController) {
            topVC = topVC.presentedViewController;
        }
        
        return topVC;
    }
- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    
    NSArray *arr = _tabCtrl.viewControllers;
    UINavigationController *nav = [arr objectAtIndex:0];
    NSArray *arr2 = nav.viewControllers;
    
    UIViewController *ctrl = [arr2[0] presentedViewController];
    if (ctrl) {
        if([ctrl respondsToSelector:@selector(saveProgress)]){
            [(BKContent *)ctrl saveProgress];
        }
        else if ([ctrl respondsToSelector:@selector(saveRecord)]) {
            [(BKNewContent *)ctrl saveRecord];
        }
        
    }
    
//    if ([arr2 count] > 1) {
//        UIViewController *ctrl = [arr2 objectAtIndex:1];
//        if([ctrl respondsToSelector:@selector(saveProgress)]){
//            [(BKContent *)ctrl saveProgress];
//        }
//    }
    
    NSInteger count = 0;
    application.applicationIconBadgeNumber = count;
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kEXIT]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kEXIT];
        exit(1);
    }
    
    [application setApplicationIconBadgeNumber:0];
    
    [[MZDownloadManagerViewController sharedInstance] suspendAllTask];
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    [application setApplicationIconBadgeNumber:0];
    [[MZDownloadManagerViewController sharedInstance] resumeAllTask];
    

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
//    [GDTTrack activateApp];
    
#if !kFREE
    [[Harpy sharedInstance] checkVersionDaily];
#endif

    [self buildADView];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark - Backgrounding Methods -
- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier
  completionHandler:(void (^)())completionHandler
{
    self.backgroundSessionCompletionHandler = completionHandler;
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

#pragma mark - Custom
    
    - (UIView *)getADView {
#if kFREE
        adView.hidden = NO;
        adView.alpha = 1.0f;
        return adView;
#endif
        
        return nil;
    }
    
    +(BookWorksAppAppDelegate *)shareInstance {
        return (BookWorksAppAppDelegate *)[[UIApplication sharedApplication] delegate];
    }


@end
