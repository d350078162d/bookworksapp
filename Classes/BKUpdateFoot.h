//
//  BKUpdateFoot.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/22.
//
//

#import <UIKit/UIKit.h>

@interface BKUpdateFoot : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIButton *btnTitle;
@property (weak, nonatomic) IBOutlet UILabel *labLine;


@end
