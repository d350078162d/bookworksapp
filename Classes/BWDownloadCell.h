//
//  BWDownloadCell.h
//  BookWorksApp
//
//  Created by 刁志远 on 13-11-29.
//
//

#import <UIKit/UIKit.h>

@interface BWDownloadCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labDown;
@property (weak, nonatomic) IBOutlet UIButton *btnRec;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *btnDown;
@end
