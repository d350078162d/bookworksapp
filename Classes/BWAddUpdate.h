//
//  BWAddUpdate.h
//  BookWorksApp
//
//  Created by 刁志远 on 14-1-13.
//
//

#import <UIKit/UIKit.h>

#import "PPiFlatSegmentedControl.h"
#import "MJRefresh.h"

@interface BWAddUpdate : UIViewController<MJRefreshBaseViewDelegate>{
    NSMutableArray *arrList;
    PPiFlatSegmentedControl *segType;
    NSMutableArray *arrType;
    NSDictionary *dicURL;
    int curPage;
    
    MJRefreshFooterView *mjFoot;
    MJRefreshHeaderView *mjHead;
    
    NSMutableArray *arrLocal;
}

@property (weak, nonatomic) IBOutlet UITableView *myTable;


@end
