//
//  HorizontalFlowLayout.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/26.
//
//

#import <UIKit/UIKit.h>

#import "LayoutAttributesAnimator.h"

@interface HorizontalFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, strong) LayoutAttributesAnimator *animator;

@end



@interface AnimatedCollectionViewLayoutAttributes : UICollectionViewLayoutAttributes


@property (nonatomic,strong) UIView *contentView;
@property (assign) UICollectionViewScrollDirection scrollDirection;
@property (assign) CGFloat startOffset;
@property (assign) CGFloat middleOffset;
@property (assign) CGFloat endOffset;

@end
