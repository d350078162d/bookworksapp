//
//  BKNewUpdateAllRecord.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/25.
//
//

#import "BKNewUpdateAllRecord.h"

#import "BKUpdateCell.h"
#import "Layout.h"

#import "BKNewUpdateBookBacklist.h"

#import "BKUpdateHead.h"

#import "SGActionSheet.h"

@interface BKNewUpdateAllRecord () <UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,SGActionSheetDelegate> {
    
    NSMutableArray *arrList;
    
    BOOL isEdit;
    
    NSInteger delIndex;
    
}

@property (weak, nonatomic) UICollectionView *myCollect;

@end

@implementation BKNewUpdateAllRecord


#pragma mark - SGActionSheetDelegate


- (void)SGActionSheet:(SGActionSheet *)actionSheet didSelectRowAtIndexPath:(NSInteger)indexPath {
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        NSIndexPath *inx = [NSIndexPath indexPathForItem:delIndex inSection:0];
    //        BKMainBookCell *cell = (BKMainBookCell *)[self.myCollect cellForItemAtIndexPath:inx];
    //        [cell.bkView boom];
    //    });
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        NSIndexPath *inx = [NSIndexPath indexPathForItem:delIndex inSection:0];
    //        BKMainBookCell *cell = (BKMainBookCell *)[self.myCollect cellForItemAtIndexPath:inx];
    //        [cell.bkView boom];
    //    });
    
    
    
    //    return;
    
    
    NSDictionary *dic = arrList[delIndex];
    
    if ([AllMethods delBookInNewUpdateTable:dic[@"id"]]) {
        [arrList removeObject:dic];
        [self.myCollect reloadData];
        
    }
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isEdit) {
        delIndex = indexPath.item;
        [self delConfirm];
        return;
    }
    
    NSDictionary *dic = arrList[indexPath.item];
    
    NSMutableDictionary *dic_detail = [NSMutableDictionary dictionaryWithDictionary:dic];
    dic_detail[@"href"] = dic[@"detail_href"];
    
    BKNewUpdateBookBacklist *backlist = [[BKNewUpdateBookBacklist alloc] initWithBookInfo:dic_detail];
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [backlist setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:backlist animated:YES];
}


#pragma mark - UICollectionViewDataSource


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BKUpdateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collect_cell"
                                                                   forIndexPath:indexPath];
    
    
    cell.scrCata.hidden = YES;
    cell.rtUpdate.hidden = YES;
    cell.viewContent.hidden = NO;
    cell.imgNoRecord.hidden = YES;
    
    NSDictionary *dic = arrList[indexPath.item];
    
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"img"]]
                      placeholderImage:ImageNamed(@"TXT_80x107_")];
    cell.labTitle.text = dic[@"name"];
    cell.labAuthor.text = dic[@"aut_name"];
    
    [cell setDeleteHidden:!isEdit];
    
    return cell;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    return arrList.count;
    
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    float width = 60*1.7;
    float height = 75*1.7+50;
    return CGSizeMake(width, height);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    int num_per_row = 3;
    float width = 60*1.7;
    
    //    if (section == 0) {
    //        return UIEdgeInsetsMake(20, (ScreenWidth-num_per_row*width)/(num_per_row+1),
    //                                0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    //    }
    
    if (isPad) {
        num_per_row = 5;
    }
    
    if (isPad) {
        return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+2),
                                0, (ScreenWidth-num_per_row*width)/(num_per_row+2));
    }
    
    return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+1),
                            0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeMake(ScreenWidth, 30);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *view = nil;
    
    
    if (kind == UICollectionElementKindSectionHeader) {
        view = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                  withReuseIdentifier:@"update_head"
                                                         forIndexPath:indexPath];
        /*
        NSDictionary *dic = arrList[indexPath.section];
        
        
        CGSize size = [self collectionView:collectionView layout:collectionView.collectionViewLayout referenceSizeForHeaderInSection:indexPath.section];
        BKUpdateHead *head = (BKUpdateHead *)view;
        CGRect frame = head.titleLabel.frame;
        head.titleLabel.text = [NSString stringWithFormat:@"%@",
                                dic[@"name"]];
        frame.size = size;
        head.titleLabel.frame = frame;
         */
        
    }
    
    return view;
}

#pragma mark - Private

- (void)delConfirm {
    
    
    
    SGActionSheet *actSheet = [[SGActionSheet alloc] initWithTitle:@"确定要删除吗？"
                                                          delegate:self
                                                 cancelButtonTitle:@"取消"
                                             otherButtonTitleArray:@[@"确定删除"]
                                                          showType:ActTypeDelete];
    actSheet.cancelButtonTitleColor = [UIColor redColor];
    [actSheet show];
    
}

- (void)editTable:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    if ([[btn titleForState:UIControlStateNormal] isEqualToString:@"编辑"]) {
        isEdit = YES;
        //        [btn setTitle:@"取消" forState:UIControlStateNormal];
    }
    else {
        isEdit = NO;
        //        [btn setTitle:@"编辑" forState:UIControlStateNormal];
    }
    
    [self.myCollect reloadData];
    [self updateEditButoon:sender];
}
- (void)updateEditButoon:(UIButton *)btn {
    
    if (isEdit) {
        [btn setTitle:@"取消" forState:UIControlStateNormal];
    }
    else {
        [btn setTitle:@"编辑" forState:UIControlStateNormal];
    }
    
    [self.myCollect reloadData];
    
}

#pragma mark - Build UI

- (void)buildEditButton {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 55, 44)];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn setTitle:@"编辑" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
    [btn addTarget:self action:@selector(editTable:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)buildCollectView {
    
    float width = 60*1.7;
    
    Layout *lay = [[Layout alloc] initWithItemWidth:width andHeight:75*1.7+50];
    lay.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                                   collectionViewLayout:lay];
    [collect registerClass:[BKUpdateCell class] forCellWithReuseIdentifier:@"collect_cell"];
    
    collect.delegate = self;
    collect.dataSource = self;
    collect.tag = 1;
    collect.backgroundColor = [UIColor whiteColor];
    collect.contentInset = UIEdgeInsetsMake(0, 0.0, 50.0, 0.0);
    
    [collect registerClass:[BKUpdateHead class]
forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
       withReuseIdentifier:@"update_head"];
    
    
    self.myCollect = collect;
    [self.myCollect setAlwaysBounceVertical:YES];
    
    [self.view addSubview:self.myCollect];
}


#pragma mark - SYS

- (id)initWithBookList:(NSArray *)arr {
    
    self = [super init];
    
    if (self) {
        arrList = [NSMutableArray arrayWithArray:arr];
        
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"追书记录";
    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"back_up"
                                                                            andSelect:@selector(popViewControllerAnimated:) andTarget:self.navigationController];
    
    [self buildCollectView];
    [self buildEditButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
