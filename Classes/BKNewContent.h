//
//  BKNewContent.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/15.
//
//

#import <UIKit/UIKit.h>

@class BKPageContent;

@interface BKNewContent : UIViewController

- (id)initWithBookIndex:(int)index;

- (void)setOldRecord:(NSString *)str;
- (void)saveRecord;

@end
