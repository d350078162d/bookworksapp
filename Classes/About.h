//
//  About.h
//  BookWorksApp
//
//  Created by diaozhiyuan on 11-11-3.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AllMethods.h"


@interface About : UIViewController {
	NSString *strContent;
}

-(void)setConLabContent:(NSString *)str;

@end
