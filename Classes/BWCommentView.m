//
//  BWCommentView.m
//  BookWorksApp
//
//  Created by 刁志远 on 13-11-29.
//
//

#import "BWCommentView.h"
#import "BWAddComment.h"

#import "DJQRateView.h"

@interface BWCommentView ()<BWAddCommentDelegate>{
    BWAddComment *addComent;
}

@end

@implementation BWCommentView

#pragma mark - BWAddCommentDelegate

- (void)commentView:(BWAddComment *) comment didDismissWithClick:(int )index{
    if (index) {
        [self getComment];
    }
}

#pragma mark - UITableViewDelegate UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str_id = [NSString stringWithFormat:@"cell_comment_id_%d",indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str_id];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str_id];
        UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, ScreenWidth-10, 20)];
        lab1.font = [AllMethods getFontWithSize:16];//[UIFont fontWithName:@"LEXUS-HeiS-Light-U" size:16];
        lab1.textColor = [UIColor whiteColor];
        lab1.backgroundColor = [UIColor clearColor];
        lab1.tag = 1;
        
        UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, ScreenWidth-10, 40)];
        lab2.font = [AllMethods getFontWithSize:16];;
        lab2.textColor = [UIColor whiteColor];
        lab2.backgroundColor = [UIColor clearColor];
        lab2.minimumFontSize = 12;
        [lab2 setAdjustsFontSizeToFitWidth:YES];
        lab2.numberOfLines = 3;
        lab2.tag = 2;
        
        UILabel *lab3 = [[UILabel alloc] initWithFrame:CGRectMake(5, 60, ScreenWidth-10, 15)];
        lab3.font = [AllMethods getFontWithSize:16];;
        lab3.textColor = [UIColor whiteColor];
        lab3.backgroundColor = [UIColor clearColor];
        lab3.tag = 3;
        lab3.textAlignment = NSTextAlignmentRight;
        
        [cell.contentView addSubview:lab1];
        [cell.contentView addSubview:lab2];
        [cell.contentView addSubview:lab3];
        
        DJQRateView *rate = [[DJQRateView alloc] initWithFrame:CGRectMake(5, 60, 100, 15)];
        rate.userInteractionEnabled = NO;
        rate.tag = 4;
        [cell.contentView addSubview:rate];
    }
    
    NSDictionary *dic = [arrList objectAtIndex:indexPath.row];
    UILabel *lab = (UILabel *)[cell.contentView viewWithTag:1];
    lab.text = [dic valueForKey:@"user_nick"];
    
    lab = (UILabel *)[cell.contentView viewWithTag:2];
    lab.text = [dic valueForKey:@"content"];
    
    lab = (UILabel *)[cell.contentView viewWithTag:3];
    lab.text = [dic valueForKey:@"date"];
    
    DJQRateView *rate = (DJQRateView *)[cell.contentView viewWithTag:4];
    rate.rate = [[dic valueForKey:@"star"] floatValue];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.headView.frame.size.height;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setBackgroundColor:MAIN_COLOR];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

#pragma mark - MY

- (IBAction)backToUpView:(id)sender{
    [mjHead free];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)addComment{
    addComent = [[BWAddComment alloc] init];
    addComent.dicBook = dicBook;
    addComent.delegate = self;
    [addComent.view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [addComent.view.layer setBorderWidth:.7f];
    [addComent.view.layer setCornerRadius:3.6f];
    [self.view addSubview:addComent.view];
    CGRect frame = addComent.view.frame;
    frame.origin.y = [AllMethods getContentInset];
    addComent.view.frame = frame;
    [addComent show];
}

- (void)getComment{
    if (!arrList) {
        arrList = [[NSMutableArray alloc] initWithCapacity:3];
    }
    [arrList removeAllObjects];
    dispatch_queue_t queue = dispatch_queue_create("get_book_comment_queue", Nil);
    dispatch_async(queue, ^{
        NSDictionary *dic_user = [[NSUserDefaults standardUserDefaults] objectForKey:kUSER_INFO];
        NSString *surl = [NSString stringWithFormat:@"%@/%@?method=5&book_id=%@&uid=%@",kMAIN_URL,kUSER_URL,
                          [dicBook valueForKey:@"id"],[dic_user valueForKey:@"uid"]];
        
        surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:surl];
        NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:Nil];
        NSDictionary *dic = [res JSONValue];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([[dic valueForKey:@"err_code"] intValue] == 0) {
                [arrList addObjectsFromArray:[dic objectForKey:@"comment"]];
                NSDictionary *dic_com =[dic objectForKey:@"user"];
                self.labCom.text = [NSString stringWithFormat:@"我的评价:%@",[dic_com valueForKey:@"content"]];
                [self.myTable reloadData];
                
                [mjHead endRefreshing];
            }
            else{
                
            }
        });
    });
}

#pragma mark - MJRefreshBaseViewDelegate

- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
    if ([refreshView isEqual:mjHead]) {
        [self getComment];
    }
}

#pragma mark - SYS

- (id)initWithBook:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        // Custom initialization
        dicBook = [[NSDictionary alloc] initWithDictionary:dic];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.labTitle.text = [dicBook valueForKey:kNAME];
    self.labTitle.font = [AllMethods getFontWithSize:16];//[UIFont fontWithName:@"LEXUS-HeiS-Light-U" size:16];
    self.labTitle.textColor = [UIColor whiteColor];
    
    self.labCom.font = [AllMethods getFontWithSize:16];//[UIFont fontWithName:@"LEXUS-HeiS-Light-U" size:16];
    self.labCom.textColor = [UIColor whiteColor];
    self.labCom.text = @"我的评价:";
    
    self.navigationItem.title = @"评  论";
    
    [self getComment];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
	[btn setFrame:CGRectMake(0, 0, 55, 35)];
	[btn setTitle:NSLocalizedString(@"Nav_Back", @"") forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
	[btn addTarget:self action:@selector(backToUpView:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.leftBarButtonItem = item;
    
    UIButton *btn_add = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_add.frame = CGRectMake(0, 0, 32, 32);
    [btn_add addTarget:self action:@selector(addComment) forControlEvents:UIControlEventTouchUpInside];
    [btn_add setShowsTouchWhenHighlighted:YES];
    FFCircularProgressView *circ = [[FFCircularProgressView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)
                                                                         andType:kFFCIRCLE_ADD_TYPE];
    [circ addSubview:btn_add];
    [circ setTintColor:[UIColor whiteColor]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:circ];
    [self.headView setBackgroundColor:MAIN_COLOR];
    [self.headView.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.headView.layer setBorderWidth:.5f];
    
    mjHead = [[MJRefreshHeaderView alloc] initWithScrollView:self.myTable];
    mjHead.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
