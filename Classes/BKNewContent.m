//
//  BKNewContent.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/15.
//
//

#import "BKNewContent.h"

#import "SwipeView.h"
#import "RJBookData.h"
#import "LSYChapterModel.h"
#import "LSYReadConfig.h"

#import "HorizontalCell.h"

#import "BKNewReaderMenu.h"

#import "BKNewChapterList.h"

#import "HorizontalFlowLayout.h"
#import "PageMoveOut.h"
#import "PageMoveIn.h"

#import "ACPageViewController.h"
#import "BKPageContent.h"

#define TAG_FROM 10000

@interface BKNewContent () <UICollectionViewDelegate, UICollectionViewDataSource,BKNewReaderMenuDelegate,BKNewChapterListDelegate>{
    
    NSInteger bookIndex;
    
    NSString *allContent;//书籍内容
    
    long gb30;//编码方式
    long gbKK;//编码方式
    long gb18030;
    
    long curLoadContentLength;             //当前以加载内容长度
    
    NSMutableDictionary *dicChapters;      //分页信息
    CGSize contentSize;                    //内容页面大小
    BOOL conLoad;                          //是否继续分页
    NSString *pagesPath;                   //分页记录地址
    
    NSInteger curChapter,curPage;          //记录当前读的章节数、页数
    
    NSString *oldRecord;                   //标志是否是从老的分页方式重新加载分页的
    
    NSMutableDictionary *dicAttribute;            //字体样式、字间距、行间距、字体大小
    
    CGSize fontSize;                       //字体的尺寸
    
    NSInteger loadThread;                  //分页线程数
    NSInteger chapterNum;                  //章节总数量
    
    BKNewReaderMenu *readMenu;             //菜单
    
    NSArray *arrMatch;                     //章节正则结果
    
    BOOL isChangeFontSize;                 //记录当前是否是改变字体进行分页
    long lastReadLengthBeforeChangeSize;   //记录改变字体之前阅读的位置
    
    NSTimer *timerSavePages;               //保存分页记录到文件
    
    UIView *viewLoad;                      //加载进度提示
    
    BKNewChapterList *chapterList;         //目录页面
    
    BOOL isChangeConfig;                   //修改了全局配置后，重新加载进度
    long recordChapter,recordPage;         //修改全局配置前，阅读的章节数
    
//    BKPageContent *contentLeft,*contentRight,*contentDetail; //PageViewController 的左右视图
}

@property (nonatomic, weak) UICollectionView *myCollect;
@property (nonatomic, strong) ACPageViewController *pageCtrl;

@end


@implementation BKNewContent

#pragma mark - Notification 

- (void)pageChangeNotification:(NSNotification *)notify {
    
    NSDictionary *dic = notify.userInfo;
    
    curPage = [dic[@"page"] integerValue];
    curChapter = [dic[@"chapter"] integerValue];
    
}

- (void)pageClickNotification:(NSNotification *)notify {
    
    if (!readMenu) {
        [self initinalMenu];
    }
    else [readMenu showInController:self];
    
}

#pragma mark - PageController Method

- (void)showPageCtrl {
    
    if ([LSYReadConfig shareInstance].pageTurn != 3) {//拟真翻页
        return;
    }
    
    [self buildPageController];
    
    NSInteger read_page = (_myCollect.contentOffset.x+ScreenWidth/2)/ScreenWidth+1;
    NSInteger all_page = _myCollect.contentSize.width/ScreenWidth+1;
    
    [_pageCtrl setBookAllPages:all_page andCurReadPage:read_page];
    
    if ((curPage ==0 && curChapter == 0) && (recordChapter!=0 || recordPage!=0)) {
        [_pageCtrl setCurrentPageInfoWithChapter:recordChapter andPage:recordPage];
    }
    else [_pageCtrl setCurrentPageInfoWithChapter:curChapter andPage:curPage];
    
    [self addPageOnView];
}

- (void)removePageFromView {
    
    if (!self.pageCtrl.view.superview) {
        return;
    }
    
    NSIndexPath *index = [NSIndexPath indexPathForItem:curPage inSection:curChapter];
    [self.myCollect scrollToItemAtIndexPath:index
                           atScrollPosition:UICollectionViewScrollPositionNone
                                   animated:NO];
    
    [self.pageCtrl removeFromParentViewController];
    [self.pageCtrl.view removeFromSuperview];
    
}

- (void)addPageOnView {
    
    if (_pageCtrl.parentViewController) {
        return;
    }
    
    self.pageCtrl.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [self addChildViewController:_pageCtrl];
    
    [self.view addSubview:_pageCtrl.view];
    [self.view bringSubviewToFront:readMenu];
}





#pragma mark - BKNewChapterListDelegate

- (void)chapterDidSelectChapter:(NSInteger )index {
    
    curChapter = index;
    curPage = 0;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:curPage inSection:curChapter];
    [self.myCollect scrollToItemAtIndexPath:indexPath
                           atScrollPosition:UICollectionViewScrollPositionNone
                                   animated:NO];
    
    [chapterList dismissView];
    
    [self showPageCtrl];
    [_pageCtrl setCurrentPage];
}

#pragma mark - BKNewReaderMenuDelegate


- (void)menuWillShow {
    
    
#if kFREE
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del setADHidden:YES];
#endif
    
    [self.view bringSubviewToFront:readMenu];
    
}


- (void)menuDidDismiss {
    
   
#if kFREE
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [self.view addSubview:[del getADView]];
#endif
    
}

- (void)menuDidChangeTheme {
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    NSDictionary *dic_theme = config.arrThemes[config.theme];
    
    dicAttribute[NSForegroundColorAttributeName] = [UIColor colorFromeString:dic_theme[@"font_color"]];
    
    HorizontalCell *cell;
    NSArray *arr = [self.myCollect visibleCells];
    if (arr.count > 0) {
        cell = [arr lastObject];
    }
    else {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:curPage inSection:curChapter];
        cell = (HorizontalCell *)[self.myCollect cellForItemAtIndexPath:indexPath];
    }
    
    if (!cell) {
        return;
    }
    
    UIColor *col = [UIColor colorFromeString:dic_theme[@"font_color"]];
    
    if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
        cell.ivBk.image = nil;
        cell.backgroundColor = [UIColor colorFromeString:dic_theme[@"bk"]];
        self.view.backgroundColor = cell.backgroundColor;
        
        cell.labTitle.textColor = [col mix:cell.backgroundColor];
        cell.labPage.textColor = cell.labTitle.textColor;
    }
    else {
        
        NSString *sname = [NSString stringWithFormat:@"%@.jpg",dic_theme[@"bk"]];
        cell.ivBk.image = ImageNamed(sname);
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:cell.ivBk.image]];
        
        cell.labTitle.textColor = [col lighten];
        cell.labPage.textColor = cell.labTitle.textColor;
    }
    
    
    NSAttributedString *str = cell.labContent.attributedText;
    NSMutableAttributedString *att_str = [[NSMutableAttributedString alloc] initWithAttributedString:str];
    [att_str setAttributes:dicAttribute range:NSMakeRange(0, [att_str length])];
    cell.labContent.attributedText = att_str;
    
    if (_pageCtrl) {
        [_pageCtrl setCurrentPage];
    }
    
    if (arr.count > 1) {
        for (int i=0; i<arr.count-1; i++) {
            
            HorizontalCell *hc = arr[i];
            
            if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
                hc.ivBk.image = nil;
                hc.backgroundColor = cell.backgroundColor;
                hc.labTitle.textColor = cell.labTitle.textColor;
                hc.labPage.textColor = cell.labPage.textColor;
            }
            else {
                hc.ivBk.image = cell.ivBk.image;
                hc.labTitle.textColor = cell.labTitle.textColor;
                hc.labPage.textColor = cell.labPage.textColor;
            }
            hc.labContent.textColor = cell.labContent.textColor;
        }
    }
}

- (void)menuDidClickedBack {
    
    [self saveRecord];
    
    if (timerSavePages) {
        [timerSavePages invalidate];
        timerSavePages = nil;
    }
    
    conLoad = NO;
    [self dismissViewControllerAnimated:YES completion:^{
        
#if kFREE
        [[BookWorksAppAppDelegate shareInstance].tabCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
#endif
        
    }];
    
}

- (void)menuFontSizeChanged:(float)size {
    
    if (loadThread > 0) {//还在加载中
        
        [WSProgressHUD showWithStatus:@"Loading..." maskType:WSProgressHUDMaskTypeClear];
        
        conLoad = NO;
        
        __weak BKNewContent *weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.75 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf menuFontSizeChanged:size];
        });
        return;
    }
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    NSString *file_path = [AllMethods applicationDocumentsDirectory];
    pagesPath = [NSString stringWithFormat:@"%@/%@-%.0f.chr",file_path,book.name,size];
    
    [WSProgressHUD dismiss];
    conLoad = YES;
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",curChapter];
    NSDictionary *dic = dicChapters[key];
    NSArray *arr = dic[@"pages"];
    NSRange ran_content = NSRangeFromString(arr[curPage]);
    NSRange ran_chap = NSRangeFromString(dic[@"range"]);
    
    lastReadLengthBeforeChangeSize = ran_content.location + ran_chap.location + 10;
    
    curLoadContentLength = 0;
    loadThread = 0;
    [[NSFileManager defaultManager] removeItemAtPath:pagesPath error:nil];
    
    config.fontSize = size;
    dicAttribute[NSFontAttributeName] = [AllMethods getFontWithSize:size];
//    dicAttribute = [NSMutableDictionary dictionaryWithDictionary:[LSYReadConfig parserAttribute:config]];
    
    NSString *str = @"阅";
    fontSize = [str sizeWithAttributes:dicAttribute];
    
    
    isChangeFontSize = YES;
    
    
    if (viewLoad.hidden) {
        UIActivityIndicatorView *act = (UIActivityIndicatorView *)[viewLoad viewWithTag:1];
        [act startAnimating];
        viewLoad.hidden = NO;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             viewLoad.alpha = 1.0f;
                         }completion:^(BOOL finished){
                             
                         }];
    }
    
    if (arrMatch.count > 0) {
        loadThread ++;
        loadThread ++;
        [self loadChapterPagesForChapter:curChapter andMatch:arrMatch andThread:1];
        [self loadChapterPagesForChapter:curChapter-1 andMatch:arrMatch andThread:2];
    }
    else {
        loadThread++;
        [self loadChapterPagesWithoutChapter];
    }
    
    [self performSelectorInBackground:@selector(createAsyncTimerToSavePageRecord) withObject:nil];
}


- (void)menuDidClickedCatagory {
    
    [readMenu dismissView];
    
    if (!chapterList) {
        chapterList = [[BKNewChapterList alloc] initWithChapterList:dicChapters];
        chapterList.delegate = self;
    }
    else {
        [chapterList setChapterList:dicChapters];
    }
    
    [chapterList showInView:self.view];
    
}

- (void)menuPageTurnChanged:(NSInteger)type {
    
    [self updatePageTurnAnimator];
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!readMenu) {
        [self initinalMenu];
    }
    else [readMenu showInController:self];
    
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    curPage = indexPath.item;
    curChapter = indexPath.section;
    
    
    HorizontalCell *hcell = (HorizontalCell *)cell;
    
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",indexPath.section];
    NSDictionary *dic = dicChapters[key];
    
    hcell.labTitle.text = dic[@"title"];
    
    hcell.labPage.text = [NSString stringWithFormat:@"%.0f/%.0f",
                          (collectionView.contentOffset.x+ScreenWidth/2)/ScreenWidth,
                          (collectionView.contentSize.width+10)/ScreenWidth];
    
    NSArray *arr = dic[@"pages"];
    NSRange ran_content = NSRangeFromString(arr[indexPath.row]);
    //[arr[indexPath.row] rangeValue];
    NSRange ran_chap = NSRangeFromString(dic[@"range"]);
    NSString *str = [[allContent substringWithRange:ran_chap] substringWithRange:ran_content];
    
    hcell.labContent.frame = CGRectMake(10, 40, contentSize.width, contentSize.height);
    hcell.labContent.attributedText = [[NSAttributedString alloc] initWithString:str
                                                                     attributes:dicAttribute];
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    NSDictionary *dic_theme = config.arrThemes[config.theme];
    
    UIColor *col = [UIColor colorFromeString:dic_theme[@"font_color"]];
    if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
        if (hcell.ivBk.image) {
            hcell.ivBk.image = nil;
        }
        if (![[cell.backgroundColor colorToString] isEqualToString:dic_theme[@"bk"]]) {
            cell.backgroundColor = [UIColor colorFromeString:dic_theme[@"bk"]];
            
            hcell.labTitle.textColor = [col mix:cell.backgroundColor];
            hcell.labPage.textColor = hcell.labTitle.textColor;
        }
        
    }
    else {
        NSString *sname = [NSString stringWithFormat:@"%@.jpg",dic_theme[@"bk"]];
        hcell.ivBk.image = ImageNamed(sname);
        
        hcell.labTitle.textColor = [col lighten];
        hcell.labPage.textColor = hcell.labTitle.textColor;
    }
    
//    if (arr.count == indexPath.item+1) {//最后一页
//        hcell.labContent.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
//    }
//    else {
//        hcell.labContent.verticalAlignment = TTTAttributedLabelVerticalAlignmentCenter;
//    }
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",section];
    NSDictionary *dic = dicChapters[key];
    
    if (!dic) {
        return 0;
    }
    
    return [dic[@"cnt"] intValue];
    
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HorizontalCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PageCell"
                                                                     forIndexPath:indexPath];
    
    
//    NSLog(@"content size width : %@",NSStringFromCGSize(collectionView.contentSize));
    
    switch ([LSYReadConfig shareInstance].pageTurn) {
        case 0:{//无动画 平移
            [cell hideShadow];
            break;
        }
        case 1:{//移入 move out
            
            [cell setShadowOri:SHADOW_LEFT];
            break;
        }
        case 2:{//移出 move in
            [cell setShadowOri:SHADOW_RIGHT];
            break;
        }
        case 3:{//拟真
            [cell hideShadow];
            break;
        }
        default:
            break;
    }
    
    return cell;
    
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return chapterNum;
    
    //    return [[dicChapters allKeys] count];
    
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(ScreenWidth, ScreenHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}


#pragma mark - Private

//创建异步定时器

- (void)createAsyncTimerToSavePageRecord {
    
    if (![NSThread isMainThread]) {
        if (timerSavePages) {
            return;
        }
        timerSavePages = [NSTimer timerWithTimeInterval:1.5f
                                                 target:self
                                               selector:@selector(savePagesRecord)
                                               userInfo:nil
                                                repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timerSavePages forMode:NSDefaultRunLoopMode];
        [[NSRunLoop currentRunLoop] run];
    }
}

//保存分页记录

- (void)savePagesRecord {
    
    
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:dicChapters];
    
    if(![dic writeToFile:pagesPath atomically:YES]) {
        NSLog(@"error : save record failed");
    }
    
    if (loadThread == 0) { //加载完成
        
        if (timerSavePages) {
            [timerSavePages invalidate];
            timerSavePages = nil;
        }
        
    }
}

//获取阅读记录
- (void)getReadRecord {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    
    NSString *sk = [NSString stringWithFormat:@"chp%@",book.name];
    NSString *str = [AllMethods getReadProgressWithKey:sk];
    
    if (str) { //存在记录
        NSArray *arr = [str componentsSeparatedByString:@"-"];
        
        str = arr[0];
        arr = [str componentsSeparatedByString:@","];
        curPage = [arr[1] intValue];
        curChapter = [arr[0] intValue];
    }
    else {
        
        curPage = 0;
        curChapter = 0;
    }
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",curChapter];
    
    if (curChapter>0 || curPage>0) {
        
        NSDictionary *dic_chap = dicChapters[key];
        if (dic_chap && [dic_chap[@"pages"] count] >= curPage) {
            
            [self showPageCtrl];
            [_pageCtrl setCurrentPage];
            
            [self.myCollect scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:curPage inSection:curChapter] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
            [WSProgressHUD dismiss];
            
        }
        else {
            isChangeConfig = YES;
            recordChapter = curChapter;
            recordPage = curPage;
            [WSProgressHUD showWithStatus:@"加载阅读记录..."];
        }
    }
    else [WSProgressHUD dismiss];
    
    
}


- (void)loadBookContent {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    
    NSString *file_enc = [[AllMethods applicationDocumentsDirectory] stringByAppendingPathComponent:@"file_encode.enc"];
    NSMutableDictionary *dic_enc ;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:file_enc]) {
        dic_enc = [NSMutableDictionary dictionaryWithContentsOfFile:file_enc];
    }
    else {
        dic_enc = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
    if (dic_enc[book.bookFile]) {
        
        long enc = [dic_enc[book.bookFile] longValue];
        allContent = [NSString stringWithContentsOfFile:book.bookFile
                                               encoding:enc
                                                  error:nil];
        
        
    }
    else {
        NSStringEncoding encode = NSUTF8StringEncoding;
        allContent = [NSString stringWithContentsOfFile:book.bookFile
                                               encoding:NSUTF8StringEncoding
                                                  error:nil];
        
        if (allContent && [allContent rangeOfString:@"HTTP Error"].location != NSNotFound) {
            [WSProgressHUD dismiss];
            [AllMethods showAltMsg:@"由于版权问题书籍暂时被删除，请稍后重试"];
            return ;
        }
        
        if (!allContent) {
            encode = gb30;
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:gb30
                                                      error:nil];
        }
        if (!allContent) {
            encode = gbKK;
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:gbKK
                                                      error:nil];
        }
        
        if (!allContent) {
            encode = gb18030;
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:gb18030
                                                      error:nil];
        }
        
        if (!allContent) {
            
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
            
            
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        
        if (!allContent) {
            
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_2312_80);
            
            
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        
        if (!allContent) {
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGBK_95);
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        if (!allContent) {
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingISO_2022_CN);
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        if (!allContent) {
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingISO_2022_CN_EXT);
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        if (!allContent) {
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingEUC_CN);
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        if (!allContent) {
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingHZ_GB_2312);
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        if (!allContent) {
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5);
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        if (!allContent) {
            encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingMacChineseSimp);
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:encode
                                                      error:nil];
        }
        if (!allContent) {
            encode = NSUTF16LittleEndianStringEncoding;
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:NSUTF16LittleEndianStringEncoding
                                                      error:nil];
        }
        if (!allContent) {
            encode = NSUTF16BigEndianStringEncoding;
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:NSUTF16BigEndianStringEncoding
                                                      error:nil];
        }
        if (!allContent) {
            encode = NSUnicodeStringEncoding;
            allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                   encoding:NSUnicodeStringEncoding
                                                      error:nil];
        }
        
        
        if (!allContent) {
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [WSProgressHUD dismiss];
                [AllMethods showAltMsg:@"暂不支持此编码类型的文件"];
                //                        [self.navigationController popViewControllerAnimated:YES];
                [self dismissViewControllerAnimated:YES completion:^{
                    [[BookWorksAppAppDelegate shareInstance].tabCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
                }];
            });
            
            return;
        }
        dic_enc[book.bookFile] = @(encode);
        [dic_enc writeToFile:file_enc atomically:YES];
    }
    
    
    if (oldRecord) {//有老版本的阅读记录
        isChangeFontSize = YES;
        NSArray *arr = [oldRecord componentsSeparatedByString:@"-"];
        float progress = [arr[1] floatValue]/100.0;
        lastReadLengthBeforeChangeSize = progress * [allContent length];
    }
    

    
//    allContent = [allContent stringByReplacingOccurrencesOfString:@"\r\n" withString:@"\n"];
//    allContent = [allContent stringByReplacingOccurrencesOfString:@"\n\r" withString:@"\n"];
//    allContent = [allContent stringByReplacingOccurrencesOfString:@"\n\t" withString:@"\n"];
//    allContent = [allContent stringByReplacingOccurrencesOfString:@"\t\n" withString:@"\n"];
//    
//    while ([allContent rangeOfString:@"\n\n"].location != NSNotFound) {
//        allContent = [allContent stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
//    }
//    
    NSArray *arr = [allContent componentsSeparatedByString:@"\n"];
    allContent = [arr componentsJoinedByString:@"\n    "];
}

- (void)updatePageTurnAnimator {
    
    HorizontalFlowLayout *layout = (HorizontalFlowLayout *)[self.myCollect collectionViewLayout];
    switch ([LSYReadConfig shareInstance].pageTurn) {
        case 0:{//无动画 平移
            layout.animator = nil;
            [self.myCollect reloadData];
            [self removePageFromView];
            break;
        }
        case 1:{//移入 move out
            
            layout.animator = [PageMoveOut new];
            [self.myCollect reloadData];
            [self removePageFromView];
            break;
        }
        case 2:{//移出 move in
            
            layout.animator = [PageMoveIn new];
            [self.myCollect reloadData];
            [self removePageFromView];
            break;
        }
        case 3:{//拟真
            layout.animator = nil;
            [self.myCollect reloadData];
            
            if (self.pageCtrl) {
                [_pageCtrl setCurrentPageInfoWithChapter:curChapter andPage:curPage];
            }
            [self showPageCtrl];
            
            break;
        }
        default:
            break;
    }
    
}

#pragma mark - Public

- (void)setOldRecord:(NSString *)str {
    
    oldRecord = [NSString stringWithString:str];
    
//#warning 上架之前去掉注释
    [AllMethods clearOldReadInfoWithBookIndex:bookIndex];
    
}

//保存阅读记录
- (void)saveRecord {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    NSString *sk = [NSString stringWithFormat:@"chp%@",book.name];
    
    float per = 100*(curChapter*1.0)/[[dicChapters allKeys] count];
    
    if ([[dicChapters allKeys] count] == 1) {//没有分章节
        NSDictionary *dic_chapter = [[dicChapters allValues] objectAtIndex:0];
        per = 100*(curPage*1.0)/[dic_chapter[@"pages"] count];
    }
    
    NSString *str = [NSString stringWithFormat:@"%ld,%ld-%.2f",curChapter,curPage,per];
    NSDictionary *dic = @{@"id":sk,@"name":sk,@"progress":str};
    [AllMethods saveReadProgressInfo:dic];
    
    [USER_DEFAULT setValue:str forKey:book.name];
    
}

#pragma mark - Septate Page Pagation

/*

 thread = 1 : 从当前章节向后加载
 thread = 2 : 从当前章节的前一章向前加载
 
 */

- (void)updateLoadProgress {
    
    UILabel *lab = (UILabel *)[viewLoad viewWithTag:2];
    double persent = 1.0*curLoadContentLength/[allContent length];
    lab.text = [NSString stringWithFormat:@"分页中%.0f%%",100.0*persent];
    
    if (loadThread <= 0 && persent>0.95) {//
        
        UIActivityIndicatorView *act = (UIActivityIndicatorView *)[viewLoad viewWithTag:1];
        [act stopAnimating];
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             viewLoad.alpha = .0f;
                         }completion:^(BOOL finished){
                             viewLoad.hidden = YES;
                         }];
        
    }
    else if (viewLoad.hidden) {
        UIActivityIndicatorView *act = (UIActivityIndicatorView *)[viewLoad viewWithTag:1];
        [act startAnimating];
        viewLoad.hidden = NO;
    }
    
//    NSLog(@"loading progress :  %.2f%%",100.0*curLoadContentLength/[allContent length])
}

- (void)loadChapterPagesWithoutChapter {
    
    if (!isChangeFontSize && [[dicChapters allKeys] count] == 1) { //已经全部加载完成
        if(loadThread>0)loadThread --;
        curLoadContentLength = [allContent length];
        [self updateLoadProgress];
        [self.myCollect reloadData];
        return;
    }
    
    LSYChapterModel *model = [[LSYChapterModel alloc] init];
    model.chapterIndex = 0;
    model.contentRect = CGRectMake(0, 0, contentSize.width,contentSize.height);
    model.fontSize = fontSize; //字体长宽数据
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06d",0];
    
    if (!isChangeFontSize && dicChapters[key]) {
        
        NSDictionary *dic_chap = dicChapters[key];
        [model addPagesFromeRecord:dic_chap[@"pages"]]; //已经加载的页面信息
    }
    
    __weak LSYChapterModel *weak_model = model;
    __weak BKNewContent *weakSelf = self;
    
    NSRange content_range = NSMakeRange(0, [allContent length]);
    
    [model setLoadingBlock:^(long length){
        
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
        NSDictionary *dic_info = @{@"cnt":[NSString stringWithFormat:@"%td",weak_model.pageCount],
                                   @"title":book.name,
                                   @"pages":[weak_model getPageList],
                                   @"range":NSStringFromRange(content_range),
                                   @"length":[NSString stringWithFormat:@"%ld",content_range.length],
                                   @"finish":[NSString stringWithFormat:@"%td",[weak_model getIsFinishLoad]]};
        
        [dicChapters setObject:dic_info forKey:key];
        
        curLoadContentLength+=length;
        [weakSelf updateLoadProgress];
    }];
    [model setLoadPageBlock:^(long cnt,LSYChapterModel *wmodel){
        
        if (!conLoad) {
            if(loadThread>0)loadThread --;
            return ;
        }
        
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];

        NSDictionary *dic_info = @{@"cnt":[NSString stringWithFormat:@"%td",weak_model.pageCount],
                                   @"title":book.name,
                                   @"pages":[weak_model getPageList],
                                   @"range":NSStringFromRange(content_range),
                                   @"length":[NSString stringWithFormat:@"%ld",content_range.length],
                                   @"finish":[NSString stringWithFormat:@"%td",[weak_model getIsFinishLoad]]};
        
        [dicChapters setObject:dic_info forKey:key];
        
        
        curLoadContentLength = allContent.length;
        [weakSelf updateLoadProgress];
        
        [weakSelf.myCollect performBatchUpdates:^{
            [weakSelf.myCollect reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } completion:^(BOOL finished) {
            
            if (finished) {
                [_pageCtrl setBookAllPages:weakSelf.myCollect.contentSize.width/ScreenWidth+1
                            andCurReadPage:-1];
            }
            
            if (isChangeFontSize && finished &&
                content_range.location <= lastReadLengthBeforeChangeSize &&
                content_range.length+content_range.location > lastReadLengthBeforeChangeSize) {
                
                isChangeFontSize = NO;
                
                NSArray *arr_pages = dic_info[@"pages"];
                for (int i=0; i<arr_pages.count; i++) {
                    NSRange ran = NSRangeFromString(arr_pages[i]);
                    //[arr_pages[i] rangeValue];
                    if (ran.location+content_range.location <= lastReadLengthBeforeChangeSize &&
                        ran.location+ran.length+content_range.location > lastReadLengthBeforeChangeSize) {
                        
                        lastReadLengthBeforeChangeSize=0;
                        
                        if (_pageCtrl && _pageCtrl.parentViewController) {
                            curChapter = 0;
                            curPage = i;
                            [weakSelf showPageCtrl];
                            [_pageCtrl setCurrentPage];
                        }
                        
                        NSIndexPath *index_path = [NSIndexPath indexPathForItem:i
                                                                      inSection:0];
                        [weakSelf.myCollect scrollToItemAtIndexPath:index_path
                                                   atScrollPosition:UICollectionViewScrollPositionNone
                                                           animated:NO];
                        break;
                    }
                }
            }
            
            if (isChangeConfig && [dic_info[@"pages"] count] > recordPage) {
                [WSProgressHUD dismiss];
                isChangeConfig = NO;
                NSIndexPath *index_path = [NSIndexPath indexPathForItem:(recordPage-5>=0?recordPage-5:0)
                                                              inSection:0];
                
                if (_pageCtrl && _pageCtrl.parentViewController) {
                    curPage = index_path.item;
                    curChapter = 0;
                    [weakSelf showPageCtrl];
                    [_pageCtrl setCurrentPage];
                }
                
                [weakSelf.myCollect scrollToItemAtIndexPath:index_path
                                           atScrollPosition:UICollectionViewScrollPositionNone
                                                   animated:NO];
                
            }
        }];
        
        
        
        if(loadThread>0)loadThread --;
        [weakSelf updateLoadProgress];
        
    }];
    
    model.content = [allContent substringWithRange:content_range];
    
}

- (void )loadChapterPagesForChapter:(NSInteger)idx
                           andMatch:(NSArray *)match
                          andThread:(NSInteger)thread{
    
    if (!conLoad) {
        
        if(loadThread>0)loadThread --;
        
        [dicChapters writeToFile:pagesPath atomically:NO];
        return ;
    }
    
    
    if (!isChangeFontSize && [[dicChapters allKeys] count] == match.count) { //已经全部加载完成
        if(loadThread>0)loadThread --;
        curLoadContentLength = [allContent length];
        [self updateLoadProgress];
        [self.myCollect reloadData];
        return;
    }
    
    if (idx < 0 || idx >= match.count) { //加载完成
        if(loadThread>0)loadThread --;
        return;
    }
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",idx];
    NSArray *arr_record = nil;
    
     __weak BKNewContent *weakSelf = self;
    
    
    NSTextCheckingResult *res = match[idx];
    NSRange range = [res range];
    NSInteger local = range.location;
    
    NSRange content_range;
   
    LSYChapterModel *model = [[LSYChapterModel alloc] init];
    model.chapterIndex = idx;
    model.contentRect = CGRectMake(0, 0, contentSize.width,contentSize.height);
    model.fontSize = fontSize; //字体长宽数据
    if (arr_record) {
        [model addPagesFromeRecord:arr_record];
    }
    
    if (idx == 0) {
        model.title = @"开始";
        NSUInteger len = local;
        content_range = NSMakeRange(0, len);
    }
    else if (idx == match.count-1) {
        model.title = [allContent substringWithRange:range];
        content_range = NSMakeRange(local, allContent.length-local);
    }
    else if (idx > 0 ) {
        NSTextCheckingResult *res_last = match[idx-1];
        NSRange lastRange = [res_last range];
        
        model.title = [allContent substringWithRange:lastRange];
        NSUInteger len = local-lastRange.location;
        content_range = NSMakeRange(lastRange.location, len);
    }
    
    __weak LSYChapterModel *weak_model = model;
    [model setLoadPageBlock:^(long cnt,LSYChapterModel *wmodel){
        
        if (!conLoad) {
            if(loadThread>0)loadThread --;
            return ;
        }
        
        NSDictionary *dic_info = @{@"cnt":[NSString stringWithFormat:@"%td",weak_model.pageCount],
                                   @"title":weak_model.title,
                                   @"pages":[weak_model getPageList],
                                   @"range":NSStringFromRange(content_range),
                                   @"length":[NSString stringWithFormat:@"%ld",content_range.length],
                                   @"finish":[NSString stringWithFormat:@"%td",[weak_model getIsFinishLoad]]};
        [dicChapters setObject:dic_info forKey:key];
        
        curLoadContentLength += content_range.length;

        [weakSelf updateLoadProgress];
        
        [weakSelf.myCollect performBatchUpdates:^{
            [weakSelf.myCollect reloadSections:[NSIndexSet indexSetWithIndex:idx]];
        } completion:^(BOOL finished) {
            
            if (finished) {
                [_pageCtrl setBookAllPages:weakSelf.myCollect.contentSize.width/ScreenWidth+1
                            andCurReadPage:-1];
            }
            
            if (isChangeFontSize && finished &&
                content_range.location <= lastReadLengthBeforeChangeSize &&
                content_range.length+content_range.location > lastReadLengthBeforeChangeSize) {
                isChangeFontSize = NO;
                
                NSArray *arr_pages = dic_info[@"pages"];
                for (int i=0; i<arr_pages.count; i++) {
                    NSRange ran = NSRangeFromString(arr_pages[i]);
                    //[arr_pages[i] rangeValue];
                    if (ran.location+content_range.location <= lastReadLengthBeforeChangeSize &&
                        ran.location+ran.length+content_range.location > lastReadLengthBeforeChangeSize) {
                        
                        lastReadLengthBeforeChangeSize = 0;
                        
                        if (_pageCtrl && _pageCtrl.parentViewController) {
                            curChapter = idx;
                            curPage = i;
                            [weakSelf showPageCtrl];
                            [_pageCtrl setCurrentPage];
                        }
                        
                        NSIndexPath *index_path = [NSIndexPath indexPathForItem:i
                                                                      inSection:idx];
                        [weakSelf.myCollect scrollToItemAtIndexPath:index_path
                                                   atScrollPosition:UICollectionViewScrollPositionNone
                                                           animated:NO];
                        
                        break;
                    }
                }
            }
            if (isChangeConfig && idx == recordChapter) {
                [WSProgressHUD dismiss];
                isChangeConfig = NO;
                NSIndexPath *index_path = [NSIndexPath indexPathForItem:(recordPage-5>=0?recordPage-5:0)
                                                              inSection:recordChapter];
                
                if (_pageCtrl && _pageCtrl.parentViewController) {
                    curPage = index_path.item;
                    curChapter = recordChapter;
                    [weakSelf showPageCtrl];
                    [_pageCtrl setCurrentPage];
                }
                
                [weakSelf.myCollect scrollToItemAtIndexPath:index_path
                                           atScrollPosition:UICollectionViewScrollPositionNone
                                                   animated:NO];
                
            }
        }];
        
        
        long chap_idx = idx;
        if (thread == 1) {
            chap_idx = idx+1;
            
            if (chap_idx < match.count) {
                
                //                NSLog(@"-->>. chapter   %td",idx);
                [weakSelf loadChapterPagesForChapter:chap_idx
                                            andMatch:match
                                           andThread:thread];
                
            }
            else {
//                NSLog(@"----->>>>   load finished  %td",idx);
                if(loadThread>0)loadThread --;
                [weakSelf updateLoadProgress];
            }
        }
        else if (thread == 2) {
            chap_idx = idx-1;
            if (chap_idx < 0) {
//                NSLog(@"backward----->>>>   load finished  %td",idx);
                if(loadThread>0)loadThread --;
            }
            else {
//                NSLog(@"backward-->>. chapter   %td",idx);
                [weakSelf loadChapterPagesForChapter:chap_idx
                                            andMatch:match
                                           andThread:thread];
                [weakSelf updateLoadProgress];
            }
        }
        
    }];
    
    
    model.content = [allContent substringWithRange:content_range];
    
}

- (void)separateChapter {
    
    [WSProgressHUD dismiss];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:pagesPath]) {
        dicChapters = [NSMutableDictionary dictionaryWithContentsOfFile:pagesPath];
        [self.myCollect reloadData];
    }
    else if (!dicChapters) {
        dicChapters = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
//    dicChapters = [NSMutableDictionary dictionaryWithCapacity:3];
    
    
    NSString *parten = @"第[0-9两零一二三四五六七八九十百千]*[章回节].*";
    NSError* error = NULL;
    NSRegularExpression *reg = [NSRegularExpression regularExpressionWithPattern:parten options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSArray* match = [reg matchesInString:allContent
                                  options:NSMatchingReportCompletion
                                    range:NSMakeRange(0, [allContent length])];
    
    arrMatch = [NSArray arrayWithArray:match];
    
    chapterNum = match.count>0?match.count:1;
    [self.myCollect reloadData];
    
    
    loadThread ++;
    
    [self performSelectorInBackground:@selector(createAsyncTimerToSavePageRecord) withObject:nil];
    
    if (match.count > 0) {
        
        NSInteger start_index = 0;
        if ([[dicChapters allKeys] count] > 0) {
            NSArray *arr = [dicChapters allKeys];
            arr = [arr sortedArrayUsingSelector:@selector(compare:)];
            NSString *sk = [arr lastObject];
            
            NSDictionary *dic_chap = dicChapters[sk];
            NSRange ran = NSRangeFromString(dic_chap[@"range"]);
            curLoadContentLength = ran.location+ran.length;
            
            start_index = [arr count];
        }
        
        [self loadChapterPagesForChapter:start_index andMatch:match andThread:1];
    }
    else {
        
        [self loadChapterPagesWithoutChapter];
    }
    
}

- (void)startLoadContent {
    
    [WSProgressHUD showWithStatus:@"Loading..."];
    
    dispatch_async(dispatch_queue_create("load_content", nil), ^{
        [self loadBookContent];
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self separateChapter];
            [self getReadRecord];
        });
    });
    
}

#pragma mark - Build UI

- (void)buildPageController {
    
    if (self.pageCtrl) {

        return;
    }
    
    NSDictionary *option = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:UIPageViewControllerSpineLocationMin] forKey:UIPageViewControllerOptionSpineLocationKey];
    _pageCtrl =  [[ACPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl
                                                                navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                              options:option];
    
    [_pageCtrl setCurrentPageIndex:bookIndex];
    
}

- (void)buildLoadView{
    
    viewLoad = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth-100, ScreenHeight-22, 95, 20)];
    
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    act.tag = 1;
    act.center = CGPointMake(SIZE_W(viewLoad)/2-30, viewLoad.frame.size.height/2);
    [act startAnimating];
    [viewLoad addSubview:act];
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, viewLoad.frame.size.width-15,
                                                             viewLoad.frame.size.height)];
    lab.textAlignment = NSTextAlignmentRight;
    lab.textColor = [UIColor whiteColor];
    lab.font = [AllMethods getChapterFontWithSize:11];
    lab.tag = 2;
    lab.text = @"分页中...";
    [viewLoad addSubview:lab];
    lab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:viewLoad];
    
    viewLoad.backgroundColor = [UIColor colorWithWhite:0 alpha:.6f];
    viewLoad.center = CGPointMake(ScreenWidth/2, viewLoad.center.y);
    viewLoad.layer.cornerRadius = 4.6f;
    
}

- (void)buildContentView {
    
    
    HorizontalFlowLayout *lay = [HorizontalFlowLayout new];
    lay.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    lay.itemSize = CGSizeMake(ScreenWidth, ScreenHeight);
    lay.minimumLineSpacing = 0;
    lay.minimumInteritemSpacing = 0;
    
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) collectionViewLayout:lay];
    [collect registerNib:[UINib nibWithNibName:@"HorizontalCell"
                                        bundle:nil]
forCellWithReuseIdentifier:@"PageCell"];
    collect.pagingEnabled = YES;
    collect.delegate = self;
    collect.dataSource = self;
    collect.backgroundColor = [UIColor clearColor];
    collect.decelerationRate = UIScrollViewDecelerationRateFast;
    collect.showsHorizontalScrollIndicator = NO;
    collect.bounces = NO;
    [self.view addSubview:collect];
    
    self.myCollect = collect;
    [self updatePageTurnAnimator];
    
}

#pragma mark - Init 

- (void)initinalData {
    
    
    
    lastReadLengthBeforeChangeSize = 0;
    isChangeFontSize = NO;
    isChangeConfig = NO;
    recordChapter = 0;
    recordPage = 0;
    
    loadThread = 0;
    curLoadContentLength = 0;
    
    gb30= 0x80000632;
    gbKK= 0x80000631;
    gb18030 = 0x80000630;

    LSYReadConfig *config = [LSYReadConfig shareInstance];
    dicAttribute = [NSMutableDictionary dictionaryWithDictionary:[LSYReadConfig parserAttribute:config]];
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    NSString *file_path = [AllMethods applicationDocumentsDirectory];
    pagesPath = [NSString stringWithFormat:@"%@/%@-%.0f.chr",file_path,book.name,config.fontSize];
    
    
    conLoad = YES;
    contentSize = CGSizeMake(ScreenWidth-20, ScreenHeight-20-20-20);
#if kFREE
    contentSize = CGSizeMake(ScreenWidth-20, ScreenHeight-20-20-20-50);
#endif
    
    NSString *str = @"阅";
    fontSize = [str sizeWithAttributes:dicAttribute];
    
    NSDictionary *dic_theme = [config.arrThemes objectAtIndex:config.theme];
    
    if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
        self.view.backgroundColor = [UIColor colorFromeString:dic_theme[@"bk"]];
    }
    else {
        NSString *sname = [NSString stringWithFormat:@"%@.jpg",dic_theme[@"bk"]];
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:ImageNamed(sname)]];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pageChangeNotification:)
                                                 name:kPAGE_CHANGE_NOTI
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pageClickNotification:)
                                                 name:kPAGE_CLICK_NOTI
                                               object:nil];

    
}

- (void)initinalMenu {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    
    readMenu = [BKNewReaderMenu new];
    readMenu.delegate = self;
    [readMenu setLabTitle:book.name];
    
    [readMenu showInController:self];
    
}

#pragma mark - Status Bar


- (BOOL)prefersStatusBarHidden {
    
    return readMenu.isHidden;
    
}

#pragma mark - SYS

- (void)dealloc {
    conLoad = NO;
}

- (id)initWithBookIndex:(int)index{
    self = [super init];
    if (self) {
        bookIndex = index;
    }
    
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [readMenu dismissView];
    });
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self saveRecord];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.modalPresentationCapturesStatusBarAppearance = YES;
    [self buildLoadView];
    [self initinalData];
    [self buildContentView];
    [self initinalMenu];
    [self startLoadContent];
    
    
#if kFREE
    //移动广告条
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-20-SIZE_H([del getADView])/2-5)];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 
 //判断当前章节是否加载完成
 if (!isChangeFontSize && dicChapters[key]) {
 NSDictionary *dic_chap = dicChapters[key];
 
 [self.myCollect performBatchUpdates:^{
 [weakSelf.myCollect reloadSections:[NSIndexSet indexSetWithIndex:idx]];
 } completion:^(BOOL finished) {
 
 }];
 
 if ([dic_chap[@"finish"] integerValue] == 1) {//本章节加载完成
 long chap_idx = idx;
 if (thread == 1) {
 chap_idx = idx+1;
 
 if (chap_idx < match.count) {
 
 curLoadContentLength += [dic_chap[@"length"] longValue];
 
 NSLog(@"-->>. chapter   %td",idx);
 [self updateLoadProgress];
 [self loadChapterPagesForChapter:chap_idx
 andMatch:match
 andThread:thread];
 
 }
 else {
 NSLog(@"----->>>>   load finished  %td",idx);
 }
 }
 else if (thread == 2) {
 chap_idx = idx-1;
 if (chap_idx < 0) {
 NSLog(@"backward----->>>>   load finished  %td",idx);
 }
 else {
 NSLog(@"backward-->>. chapter   %td",idx);
 [self loadChapterPagesForChapter:chap_idx
 andMatch:match
 andThread:thread];
 }
 }
 return;
 }
 else { //本章节加载中，未加载完成
 arr_record = dic_chap[@"pages"];
 }
 }
 */

@end
