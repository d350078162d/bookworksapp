//
//  BKContent.m
//  BookWorksApp
//
//  Created by 刁志远 on 15/1/10.
//
//

#import "BKChapterContent.h"

#import "RJBookData.h"
#import "RTLabel.h"
//#import "PunchScrollView.h"

#import "SwipeView.h"
#import "SOZOChromoplast.h"

#import <PopoverView/PopoverView.h>

#import "TFHpple.h"
#import "TFHppleElement.h"


#import "BookWorksAppAppDelegate.h"
#import "BKChapterList.h"

#define TAG_FROM 100000
#define NUM_BK 7

#if kFREE


@interface BKChapterContent ()<SwipeViewDataSource,SwipeViewDelegate, UIWebViewDelegate, GDTMobInterstitialDelegate, RTLabelDelegate> {
    
    GDTMobInterstitial *adView;
    UIView *preAdView;
    
#else 
    
@interface BKChapterContent ()<SwipeViewDataSource,SwipeViewDelegate, UIWebViewDelegate, RTLabelDelegate>{
    
#endif
    
    NSMutableArray *arrPageDic;//分线程加载页面
    NSMutableDictionary *dicList;//每一页数据
    NSString *allContent;//书籍内容
    
    long curLength;//当前已解析长度
    
    
    int numPerPage;//一页的理想字符数
    int widPerLine;//一行的理想字符数
    int heiPerPage;//一页的高度字符数
    long lazyPage;//理想页面数量
    
    int curLoadPage;//当前正在加载的页码
    
    BOOL conLoad;//如果书籍页码正在加载，判断返回上一层时，是否需要继续加载。
    
    NSString *curBkImgName;//阅读的背景图片名称
    
    UILabel *labContent;//用于计算每页内容的label
    
    float lineSpace; //行间距
    
    UIColor *colorTitle;//书名字体颜色
    UIColor *colorTxt;//书籍内容字体颜色
    
    BOOL isDone;//是否加载完成（yes为完成，no未完成）
    
    UIView *viewLoad;//加载进度提示
    
    long gb30;//编码方式
    long gbKK;//编码方式
    
    UIScrollView *scrReadBk;//阅读背景选择
    
    PopoverView *popView;//弹出视图
    float fontSize; //字体大小
    
    float adHeight;
    
    BOOL shouldAuto;
    
    NSMutableArray *arrChapters;
    BKChapterList *chapterView;
    NSDictionary *dicBookInfo;
    NSInteger curChapter;
    NSDictionary *dicBookDetail;
    
    NSString *strBookID;
}

@end

@implementation BKChapterContent

#if kFREE
    
#define AD_SEP 30*60
    
#pragma mark - GDTMobInterstitialDelegate
    
    // 详解: 插屏广告展示结束回调该函数
    - (void)interstitialDidDismissScreen:(GDTMobInterstitial *)interstitial
    {
        
    }
    - (void)interstitialFailToLoadAd:(GDTMobInterstitial *)interstitial error:(NSError *)error
    {
        [adView loadAd];
    }
    - (void)interstitialSuccessToLoadAd:(GDTMobInterstitial *)interstitial
    {
        [adView presentFromRootViewController:self];
    }
#endif

    
    
#pragma mark - RTLabelDelegate
    
    - (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL*)url {
        
        NSString *str = [url absoluteString];
        if ([str isEqualToString:@"next"]) {
            curChapter++;
        }
        else {
            curChapter--;
        }
        
        if (curChapter < 0) {
            [WSProgressHUD showErrorWithStatus:@"已经是第一章了"];
            return;
        }
        if (curChapter >= arrChapters.count) {
            [WSProgressHUD showErrorWithStatus:@"已经是最后一章了"];
            return;
        }
        
        
        
        [self loadChapter:curChapter];
        //[self buildAdView];
#if kFREE
        [adView loadAd];
#endif
    }

#pragma mark - SwipeViewDelegate


- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{
    [self toggleShowOrHideToolbar];
}

#pragma mark - SwipeViewDataSource

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    return lazyPage+1;
}



- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
//    UIView *view = (UIView *)[scrollView dequeueRecycledPage];
    if (!view) {
        
        view = [[UIView alloc] initWithFrame:self.scrollView.bounds];
        [view setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        RTLabel *lab = [[RTLabel alloc] initWithFrame:CGRectMake(10, 45, labContent.frame.size.width,
                                                        ScreenHeight-45)];
        lab.font = [AllMethods getFontWithSize:fontSize];
        lab.tag = TAG_FROM+1;
        lab.lineSpacing = lineSpace;
        [view addSubview:lab];
//        lab.layer.borderColor = colorTitle.CGColor;
//        lab.layer.borderWidth = .5f;
        
        
        
        UILabel *lab_title = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, view.frame.size.width, 20)];
        lab_title.font = [AllMethods getFontWithSize:12];
        lab_title.text = dicBookInfo[@"name"];
        lab_title.textColor = colorTitle;
        [view addSubview:lab_title];
        lab_title.tag = TAG_FROM+11;

        UILabel *lab_page = [[UILabel alloc] initWithFrame:CGRectMake(20, view.frame.size.height-25,
                                                                      view.frame.size.width-40, 20)];
        lab_page.textAlignment = NSTextAlignmentRight;
        lab_page.textColor = colorTitle;
        lab_page.font = [AllMethods getFontWithSize:14];
        lab_page.tag = TAG_FROM+2;
        [view addSubview:lab_page];
        
    }
    
    
    UILabel *lab_tit = (UILabel *)[view viewWithTag:TAG_FROM+11];
    lab_tit.textColor = colorTitle;
    
    RTLabel *lab = (RTLabel *)[view viewWithTag:TAG_FROM+1];
    lab.textColor = colorTxt;
    lab.delegate = self;
    
    NSValue *val = [dicList objectForKey:[NSString stringWithFormat:@"%ld",index+1]];
    
    if (val) {
        NSRange ran = [val rangeValue];
        if (ran.location+ran.length >= allContent.length) {
            if (allContent.length > ran.location)
                lab.text = [allContent substringFromIndex:ran.location];
            else lab.text = @"全书完";
        }
        else {
            NSString *content = [allContent substringWithRange:ran];
            content = [content stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
            lab.text = content;
        }
    }
    else {
        if(!dicList)
            lab.text = @"加载中...";
        else {
            lab.text = @"<br><br><br><br><p align=center><a href='previous'>上一章</a><font size=18>  |  </font><a href='next'>下一章</a></p>";
        }
    }
    
    UILabel *lab_page = (UILabel *)[view viewWithTag:TAG_FROM+2];
    lab_page.text = [NSString stringWithFormat:@"%ld / %ld",index+1,[[dicList allKeys] count]+1];
    lab_page.textColor = colorTitle;
    
    
    return view;
}

#pragma mark - My

- (void)toggleShowOrHideToolbar{
    
    if (!self.viewProgress.hidden) {
        self.viewProgress.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.viewProgress.alpha = 0;
                         }completion:^(BOOL finished){
                             self.viewProgress.hidden = YES;
                         }];
        
        return;
    }
    
    if (self.toolBar.hidden) {
        self.toolBar.alpha = 0;
        self.toolBar.hidden = NO;
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.toolBar.alpha = 1;
                         }completion:^(BOOL finished){
                             self.toolBar.hidden = NO;
                         }];
    }
    else {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
        self.toolBar.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.toolBar.alpha = 0;
                         }completion:^(BOOL finished){
                             self.toolBar.hidden = YES;
                         }];
    }
    
}
    - (void)loadChapter:(NSInteger )index {
        
        curChapter = index;
        [self.scrollView scrollToPage:0 duration:0];
        dicBookInfo = arrChapters[index];
        allContent = nil;
        [self loadBook];
    }
    

- (void)toggleShowOrHideProgressView{
    
    if (!chapterView) {
        chapterView = [[BKChapterList alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) andChapterList:arrChapters];
        chapterView.hidden = YES;
        [self.view addSubview:chapterView];
        
        __weak BKChapterContent *weakSelf = self;
        [chapterView setSelectTitle:^(NSString *title, NSInteger index) {
            [weakSelf loadChapter:index];
        }];
    }
    
    if (chapterView.hidden) {
        chapterView.alpha = 0;
        chapterView.hidden = NO;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             chapterView.alpha = 1;
                             if (!self.toolBar.hidden) {
                                 self.toolBar.alpha = 0;
                             }
                         }completion:^(BOOL finished){
                             chapterView.hidden = NO;
                             self.toolBar.hidden = YES;
                         }];
    }
    else {
        chapterView.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             chapterView.alpha = 0;
                         }completion:^(BOOL finished){
                             chapterView.hidden = YES;
                         }];
    }

    
    
    /*
    self.labPage.text = [NSString stringWithFormat:@"%ld/%d",[self.scrollView currentItemIndex]+1,
                         [[dicList allKeys] count]];
    self.slideProgress.value = ([self.scrollView currentItemIndex]+1)*1.0/[[dicList allKeys] count];
    
    if (self.viewProgress.hidden) {
        self.viewProgress.alpha = 0;
        self.viewProgress.hidden = NO;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.viewProgress.alpha = 1;
                             if (!self.toolBar.hidden) {
                                 self.toolBar.alpha = 0;
                             }
                         }completion:^(BOOL finished){
                             self.viewProgress.hidden = NO;
                             self.toolBar.hidden = YES;
                         }];
    }
    else {
        self.viewProgress.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.viewProgress.alpha = 0;
                         }completion:^(BOOL finished){
                             self.viewProgress.hidden = YES;
                         }];
    }
     */
}


//二分查找算法就算每页显示字数

- (BOOL)loadContentForPage:(int)page{
    
    if (!dicList) {
        dicList = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
//    NSString *str_punct = @"，。？！：“”";
    
    NSString *all_content = allContent;//[NSString stringWithString:allContent];
    
    
    long curPage = page;
    long offset = 0;
    if (page > 1) {
        NSValue *val = [dicList objectForKey:[NSString stringWithFormat:@"%d",page-1]];
        NSRange ran = [val rangeValue];
        offset = ran.length+ran.location;
    }
    
    if (offset >= [all_content length]) {
        return NO;
    }
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpace];
    NSDictionary *dic_att = @{NSFontAttributeName:[AllMethods getFontWithSize:fontSize],
                              NSParagraphStyleAttributeName:paragraphStyle};
    
    long len = numPerPage;
    NSRange ran_long = NSMakeRange(0, 0);
    NSRange ran_short = NSMakeRange(0, 0);
    
    if (len+offset <= [all_content length]) {
        
        while (true) {
            NSRange ran = NSMakeRange(offset, len);
            
            NSString *sc = [all_content substringWithRange:ran];
            
            CGSize sz;
            @autoreleasepool {
                sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:dic_att
                                      context:nil].size;
            }
            
            
            if (sz.height > labContent.frame.size.height) {
                ran_long = ran;
                if (ran_short.length+ran_short.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len-=widPerLine;
                }
            }
            else {
                ran_short = ran;
                if (ran_long.length+ran_long.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len+=widPerLine;
                    if (len+offset>=[allContent length]) {
                        if (ran_short.length == [allContent length]-offset) {
                            NSValue *val = [NSValue valueWithRange:ran_short];
                            [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                            curLength+=ran_short.length;
                            break;
                        }
                        else {
                            len = [allContent length]-offset;
                        }
                    }
                }
            }
            
            if (fabs(ran_long.length-ran_short.length) <= 1) {
                NSValue *val = [NSValue valueWithRange:ran_short];
                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                curLength+=ran_short.length;
                break;
            }
        }
        
        return YES;
    }
    else {
        len = [all_content length]-offset;
        while (true) {
            NSRange ran = NSMakeRange(offset, len);
            NSString *sc = [all_content substringWithRange:ran];
            
            CGSize sz;
            @autoreleasepool {
                sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:dic_att
                                      context:nil].size;
            }
            
            if (sz.height > labContent.frame.size.height) {
                ran_long = ran;
                if (ran_short.length+ran_short.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len-=widPerLine;
                }
            }
            else {
                ran_short = ran;
                if (ran_long.length+ran_long.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    NSValue *val = [NSValue valueWithRange:ran_short];
                    [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                    curLength+=ran_short.length;
                    break;
                }
            }
            if (fabs(ran_long.length-ran_short.length) <= 1) {
                NSValue *val = [NSValue valueWithRange:ran_short];
                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                curLength+=ran_short.length;
                break;
            }
        }
        return YES;
    }
    
    
    if (offset >= [all_content length]) {
        return NO;
    }
    
    NSRange ran = NSMakeRange(offset, [all_content length]-offset);
    
    
    
    NSString *sc = [all_content substringWithRange:ran];
    
    CGSize sz;
    @autoreleasepool {
        sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                              options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:dic_att
                              context:nil].size;
    }
    
    if (sz.height < labContent.frame.size.height) {
        ran_short = ran;
        if (ran_long.length+ran_long.length > 0) {
            len = (ran_long.length+ran_short.length)/2;
        }
        else {
            NSValue *val = [NSValue valueWithRange:ran_short];
            [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
            curLength+=ran_short.length;
        }
        return YES;
        
    }
    else {
        len = [all_content length]-offset-widPerLine;
        while (true) {
            NSRange ran = NSMakeRange(offset, len);
            NSString *sc = [all_content substringWithRange:ran];
            
            CGSize sz;
            @autoreleasepool {
                sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:dic_att
                                      context:nil].size;
            }
            
            if (sz.height > labContent.frame.size.height) {
                ran_long = ran;
                if (ran_short.length+ran_short.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len-=widPerLine;
                }
            }
            else {
                ran_short = ran;
                if (ran_long.length+ran_long.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len+=widPerLine;
                }
            }
            if (fabs(ran_long.length-ran_short.length) <= 1) {
                NSValue *val = [NSValue valueWithRange:ran_short];
                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                curLength+=ran_short.length;
                break;
            }
        }
        return YES;
    }
}

- (void)loadPageInQueue{
    
    dispatch_queue_t queue = dispatch_queue_create("get_page_list", nil);
    dispatch_async(queue, ^{
        
        BOOL bol = [self loadContentForPage:curLoadPage++];
        dispatch_sync(dispatch_get_main_queue(), ^{
            UILabel *lab = (UILabel *)[viewLoad viewWithTag:2];
            
            if(bol && conLoad) {
                lab.text = [NSString stringWithFormat:@"已加载%.0f%%",(curLength*100.0)/(1.0*[allContent length])];
                [self loadPageInQueue];
            }
            else if(conLoad){
                
                UIActivityIndicatorView *act = (UIActivityIndicatorView *)[viewLoad viewWithTag:1];
                [act stopAnimating];
                lazyPage = [[dicList allKeys] count];
                [self.scrollView reloadData];
                
                [UIView animateWithDuration:.2f
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     viewLoad.alpha = .0f;
                                 }completion:^(BOOL finished){
                                     viewLoad.hidden = YES;
                                     
                                     [self firstShowContent];
                                 }];
            }
        });
        
    });
}

- (IBAction)itemClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
    
    
    

    
    - (void)updateReadChapter {
        
        
        /*
         NSString *sql = [NSString stringWithFormat:@"insert into 'main'.'bookbag' (id,book_name,book_id,book_author,book_img,book_other)"\
         " values (NULL,'%@','%@','%@','%@',NULL)",
         [dic valueForKey:@"title"],
         [dic valueForKey:@"_id"],
         dic[@"author"],
         simg];
         [AllMethods insertBookWithSQL:sql];
         */
        
        
        /*
        NSString *sql = [NSString stringWithFormat:@"update 'main'.'bookbag' set book_other='%@' where book_id='%@'",
                         dicBookInfo[@"title"],strBookID];
        [AllMethods insertBookWithSQL:sql];
         */
        
        
        NSDictionary *dic = @{@"id":dicBookDetail[@"href"],@"name":dicBookDetail[@"name"],
                              @"chap_href":dicBookInfo[@"href"],
                              @"chap_name":dicBookInfo[@"name"],
                              @"chap_index":@(curChapter),
                              @"aut_name":dicBookDetail[@"aut_name"],
                              @"detail_href":dicBookDetail[@"href"],
                              @"img":dicBookDetail[@"img"],
                              @"cata_name":@"",
                              @"cata_href":@""};
        
        if ([AllMethods saveNewUpdateInfo:dic]) {
            [WSProgressHUD showSuccessWithStatus:@"添加成功"];
        }
        
    }
    
    
    
    
- (void)loadBook{
    
    [self updateReadChapter];
    [WSProgressHUD showWithStatus:@"Loading..." maskType:WSProgressHUDMaskTypeBlack];
    
    
    BOOL isFirst = YES;//是否首次打开书籍
    NSString *str = @"阅";
    CGSize size = [str sizeWithAttributes:@{NSFontAttributeName: [AllMethods getFontWithSize:fontSize]}];
    
    
    NSString *surl = [NSString stringWithFormat:@"%@&action=content&href=%@/%@",
                      [AllMethods updateURL],dicBookDetail[@"href"],dicBookInfo[@"href"]];
    
    
    [AllMethods requestWithType:REQUEST_GET
                        andPara:nil
                         andUrl:surl andGetDataBlock:^(id object) {
                             [WSProgressHUD dismiss];
                             
                             NSDictionary *dic = object;
                             
                             if (![dic isKindOfClass:[NSDictionary class]]) {
                                 
                                 [AllMethods showAltMsg:[NSString stringWithFormat:@"%@",object]];
                                 return ;
                             }
                             
                             allContent = [NSString stringWithFormat:@"%@",
                                           dic[@"content"]];
                             
                             
                             if (!allContent || [allContent length] <= 0) {
                                 [NSObject cancelPreviousPerformRequestsWithTarget:self];
                                 UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                                 btn.tag = 0;
                                 [self toolBtnClicked:btn];
                                 [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
                                 [AllMethods showAltMsg:@"无法加载书籍内容"];
                                 return;
                             }
                             
                             
                             
                             NSArray *arr_para = [allContent componentsSeparatedByString:@"\n"];
                             allContent = [NSString stringWithFormat:@"%@",[arr_para componentsJoinedByString:@"\n  "]];
                             
                             int t_w =  (labContent.frame.size.width/(size.width+5));
                             int t_h = (labContent.frame.size.height/(size.height+5));
                             lazyPage = [allContent length]/(t_h*t_w);
                             if (lazyPage == 0) {
                                 lazyPage = 1;
                             }
                             
                             
                             
                             curLoadPage = 1;
                             
                             
                             
                             widPerLine = (labContent.frame.size.width/(size.width+2));
                             heiPerPage = (labContent.frame.size.height/(size.height+2));
                             numPerPage = widPerLine*heiPerPage;
                             
                             
                             [self.view bringSubviewToFront:viewLoad];
                             [self.view bringSubviewToFront:self.toolBar];
                             UIActivityIndicatorView *act = (UIActivityIndicatorView *)[viewLoad viewWithTag:1];
                             [act startAnimating];
                             [self loadPageInQueue];
                             
                             if (!self.scrollView.delegate) {
                                 self.scrollView.dataSource = self;
                                 self.scrollView.delegate = self;
                             }
                             if(!isFirst){
                                 [WSProgressHUD dismiss];
                                 [self firstShowContent];
                             }
                             else [self performSelector:@selector(firstShowContent) withObject:nil afterDelay:.5f];
                             
                             
                         }];
    
    
    
}

- (void)firstShowContent{
    
    [self.scrollView reloadData];

    [WSProgressHUD dismiss];
}

- (void)buildLoadView{
    viewLoad = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth-100, ScreenHeight-22, 95, 20)];
    
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    act.tag = 1;
    act.center = CGPointMake(13, viewLoad.frame.size.height/2);
    [act stopAnimating];
    [viewLoad addSubview:act];
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, viewLoad.frame.size.width-15,
                                                             viewLoad.frame.size.height)];
    lab.textAlignment = NSTextAlignmentRight;
    lab.textColor = colorTitle;//[UIColor whiteColor];
    lab.font = [AllMethods getFontWithSize:11];
    lab.tag = 2;
    [viewLoad addSubview:lab];
    lab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:viewLoad];
    
//    viewLoad.backgroundColor = [UIColor colorWithWhite:0 alpha:.6f];
    
    viewLoad.center = CGPointMake(ScreenWidth/2, viewLoad.center.y);
    
    viewLoad.layer.cornerRadius = 4.6f;
    
    [self.view bringSubviewToFront:self.toolBar];
}

- (void)selBkBtnClicked:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSString *sname = [NSString stringWithFormat:@"reading_bg%d",(int)btn.tag];
    curBkImgName = [NSString stringWithString:sname];
    
    UIImage *img = [UIImage imageNamed:curBkImgName];
    [self.scrollView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    [self.btnReadBk setBackgroundImage:img forState:UIControlStateNormal];
    
    [popView dismiss];
    
    SOZOChromoplast *cas = [[SOZOChromoplast alloc] initWithImage:img];
    colorTitle = [cas secondHighlight];
    if(btn.tag == NUM_BK) colorTxt = [cas secondHighlight];
    else colorTxt = [cas firstHighlight];
    cas = nil;
    
    [[NSUserDefaults standardUserDefaults] setValue:curBkImgName forKey:@"novel_back"];
    [self.scrollView reloadData];
    
//    UIGraphicsBeginImageContext(self.view.bounds.size);
//    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    UIImageView *iv = [[UIImageView alloc] initWithFrame:self.view.bounds];
//    iv.image = image;
//    [self.view addSubview:iv];
//    
//    int index = [self.scrollView currentItemIndex];
//    
//    [self.scrollView reloadData];
//    [self.scrollView scrollToItemAtIndex:index duration:0];
//    
//    [UIView animateWithDuration:.25f
//                          delay:.15f
//                        options:UIViewAnimationOptionCurveEaseOut
//                     animations:^{
//                         iv.alpha = 0;
//                     }completion:^(BOOL finished){
//                         [iv removeFromSuperview];
//                     }];
    
}

- (void)iniitialBkSelectView{
    if (scrReadBk) {
        return;
    }
    
    scrReadBk = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 180, 160)];
    
    float wid = 50;
    float hei = 50;
    float jian = (scrReadBk.frame.size.width-wid*3)/4.0;
    float size_h = 0;
    for (int i=0; i<NUM_BK; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = i+1;
        btn.frame = CGRectMake(((i%3)+1)*jian+(i%3)*wid, (i/3+1)*jian+(i/3)*hei, wid, hei);
        NSString *sname = [NSString stringWithFormat:@"reading_bg%d",i+1];
        [btn setBackgroundImage:[UIImage imageNamed:sname] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(selBkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [scrReadBk addSubview:btn];
        if (i==NUM_BK-1) {
            size_h = btn.frame.origin.y+btn.frame.size.height+10;
        }
    }
    
    [scrReadBk setContentSize:CGSizeMake(1, size_h)];
}

- (IBAction)toolBtnClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 0) {

        BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
        [del setADHidden:YES];
        del.canHideAd = NO;
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if(btn.tag == 1){//进度
        [self toggleShowOrHideProgressView];
    }
    else if (btn.tag == 2){//阅读背景
        [self iniitialBkSelectView];
        popView = [PopoverView showPopoverAtPoint:self.toolBar.center
                                           inView:self.view
                                  withContentView:scrReadBk
                                         delegate:nil];
    }
}

- (IBAction)slideValChanged:(id)sender {
    long page = self.slideProgress.value*[[dicList allKeys] count];
    self.labPage.text = [NSString stringWithFormat:@"%ld/%ld",page,[[dicList allKeys] count]];
}

- (IBAction)slidePageChanged:(id)sender {
    int page = self.slideProgress.value*[[dicList allKeys] count];
    if (page<1) {
        page = 1;
    }
//    [self.scrollView scrollToIndexPath:[NSIndexPath indexPathForRow:page-1 inSection:0]
//                              animated:NO];
    [self.scrollView scrollToItemAtIndex:page-1 duration:0];
}

    
    
    - (void)buildAdView {
        
#if kFREE
//        adView = [[BaiduMobAdInterstitial alloc] init];
//        adView.delegate = self;
//        adView.AdUnitTag = @"2084519";
//        adView.interstitialType = BaiduMobAdViewTypeInterstitialOther;
        
        
        adView = [[GDTMobInterstitial alloc] initWithAppkey:kGDT_APP_ID
                                                placementId:kGDT_INTER_ID];
        
        adView.delegate = self;
        [adView loadAd];
        
#endif
    }



#pragma mark - SYS
    
    - (id)initWithChapterList:(NSArray *)arr andChapterIndex:(NSInteger )index andBookId:(NSString *)bid {
        
        self = [super init];
        if (self) {
            curChapter = index;
            arrChapters = [NSMutableArray arrayWithArray:arr];
            dicBookInfo = arrChapters[index];
            strBookID = [NSString stringWithString:bid];
        }
        return self;
    }
    - (id)initWithChapterList:(NSArray *)arr andChapterIndex:(NSInteger )index andBookInfo:(NSDictionary *)dic {
        
        self = [super init];
        if (self) {
            curChapter = index;
            arrChapters = [NSMutableArray arrayWithArray:arr];
            dicBookInfo = arrChapters[index];
            dicBookDetail = [NSDictionary dictionaryWithDictionary:dic];
        }
        return self;
    }

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    
    
    if ([self isMovingFromParentViewController]) {
        conLoad = NO;

#if kFREE
        [NSObject cancelPreviousPerformRequestsWithTarget:adView];
#endif
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    labContent = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-20, ScreenHeight-70-adHeight)];
    CGRect frame = self.scrollView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = ScreenWidth;
    frame.size.height = ScreenHeight;
    self.scrollView.frame = frame;
    
    [self loadBook];
    
    [self performSelector:@selector(toggleShowOrHideToolbar)
               withObject:nil
               afterDelay:1.0f];
#if kFREE
    [NSObject cancelPreviousPerformRequestsWithTarget:adView];
    [adView performSelector:@selector(loadAd) withObject:nil afterDelay:AD_SEP];
#endif
   
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    shouldAuto = YES;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_auto"];
    
    gb30= 0x80000632;
    gbKK= 0x80000631;
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    isDone = NO;
    conLoad = YES;
    lazyPage = 0;
    lineSpace = 6.6;
    fontSize = [[[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE] floatValue];//18.5;
    
    
    if (kFREE == 1) {
        adHeight = 50+fontSize;
    }
    else {
        adHeight = 0;
    }
    
    dicList = [NSMutableDictionary dictionaryWithCapacity:3];
    
    curBkImgName = [[NSUserDefaults standardUserDefaults] valueForKey:@"novel_back"];
    if(!curBkImgName) curBkImgName = @"reading_bg1";
    int index = [[curBkImgName substringFromIndex:[@"reading_bg" length]] intValue];
    UIImage *img = [UIImage imageNamed:curBkImgName];
    [self.scrollView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    [self.btnReadBk setBackgroundImage:img forState:UIControlStateNormal];
    
    self.btnReadBk.layer.cornerRadius = self.btnBack.frame.size.width/2;
    self.btnReadBk.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnReadBk.layer.borderWidth = .5f;
    self.btnReadBk.clipsToBounds = YES;
    
    SOZOChromoplast *cas = [[SOZOChromoplast alloc] initWithImage:img];
    colorTitle = [cas secondHighlight];
    if(index < NUM_BK) colorTxt = [cas firstHighlight];
    else colorTxt = [cas secondHighlight];
    cas = nil;
    [self buildLoadView];
    
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"v3_reading_menu_back_normal"]
                            forState:UIControlStateNormal];
    self.btnBack.layer.cornerRadius = self.btnBack.frame.size.width/2;
    self.btnBack.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnBack.layer.borderWidth = .5f;
    
    
//    [self.btnProgress setBackgroundImage:[UIImage imageNamed:@"v3_reading_menu_progress"] forState:UIControlStateNormal];
    
    self.viewProgress.frame = CGRectMake(0, ScreenHeight-self.viewProgress.frame.size.height,
                                         ScreenWidth, self.viewProgress.frame.size.height);
    self.viewProgress.backgroundColor = [UIColor colorWithWhite:0 alpha:.75f];
    
    self.viewProgress.hidden = YES;
    self.labPage.font = [AllMethods getFontWithSize:15];
    self.labPage.textColor = [UIColor whiteColor];
    [self.view addSubview:self.viewProgress];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-48-49+24+10)];
    [del setADHidden:NO];
    del.canHideAd = YES;
    
    [self buildAdView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
