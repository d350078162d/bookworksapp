//
//  BWBookDetail.h
//  BookWorksApp
//
//  Created by 刁志远 on 14-1-13.
//
//

#import <UIKit/UIKit.h>

@interface BWBookDetail : UIViewController{
    NSDictionary *dicDetail;
    NSDictionary *dicUrl;
    NSDictionary *dicBook;
    NSMutableArray *arrLocal;
}

@property (weak, nonatomic) IBOutlet UITableView *myTable;
@property (strong, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UILabel *labTtile;
@property (weak, nonatomic) IBOutlet UILabel *labAuthor;
@property (weak, nonatomic) IBOutlet UILabel *labState;
@property (weak, nonatomic) IBOutlet UIImageView *urlImg;

- (void)setCurBook:(NSDictionary *)dic;

@end
