//
//  BWChapterContent.m
//  BookWorksApp
//
//  Created by 刁志远 on 14/12/25.
//
//

#import "BWChapterContent.h"

#import "ASIHTTPRequest.h"

@interface BWChapterContent (){
    
    NSDictionary *dicUrl;
    NSDictionary *dicChapter;
    NSDictionary *dicDetail;
    
    NSMutableArray *arrList;
}

@end

@implementation BWChapterContent



#pragma mark - My

- (IBAction)itemClicked:(id)sender {
    UIBarButtonItem *item = (UIBarButtonItem *)sender;
    switch (item.tag) {
        case 1:{
            [WSProgressHUD dismiss];
            [self.navigationController popViewControllerAnimated:YES];
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            break;
        }
        case 2:{//上一章
            int index = [arrList indexOfObject:dicChapter];
            index -- ;
            if (index<0) {
                [AllMethods showAltMsg:@"已是第一章"];
                return;
            }
            dicChapter = [arrList objectAtIndex:index];
            [self getChapterContent];
            break;
        }
        case 3:{//下一章
            int index = [arrList indexOfObject:dicChapter];
            index ++ ;
            if (index>= [arrList count]) {
                [AllMethods showAltMsg:@"已是最后一章"];
                return;
            }
            dicChapter = [arrList objectAtIndex:index];
            [self getChapterContent];
            break;
        }
        default:
            break;
    }
}

- (void)updateContent{
    NSDictionary *dic = [dicDetail objectForKey:@"chapter"];
    [self.txtContent setText:[dic valueForKey:@"body"]];
    self.spaceBar.title = [dic valueForKey:@"title"];
}

- (NSString *)URLEncodeStringFromString:(NSString *)string
{
    static CFStringRef charset = CFSTR("!@#$%&*()+'\";:=,/?[] ");
    CFStringRef str = (__bridge CFStringRef)string;
    CFStringEncoding encoding = kCFStringEncodingUTF8;
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, str, NULL, charset, encoding));
}

- (void)getChapterContent{
    
    [WSProgressHUD showWithStatus:@"Loading..."];
    
    NSString *str_link = [dicChapter valueForKey:@"link"];
    str_link = [self URLEncodeStringFromString:str_link];
    
    //"http://172.16.85.128/books/zhuishu.php?method=4&book_url=http%3A%2F%2Fwww.69shu.com%2Ftxt%2F5277%2F9653304"
    NSString *str_url = [NSString stringWithFormat:@"%@/zhuishu.php?method=4&book_url=%@",kZHUI_URL,str_link];
    
    dispatch_queue_t queue = dispatch_queue_create("get_content", nil);
    dispatch_async(queue, ^{
        NSString *str = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSDictionary *dic = [str JSONValue];
            if (!dic) {
                [self performSelector:@selector(getChapterContent)
                           withObject:nil afterDelay:.5f];
                return ;
            }
            
            dicDetail = [NSDictionary dictionaryWithDictionary:dic];
            [self updateContent];
            [WSProgressHUD dismiss];
        });
        
    });
}

#pragma mark - SYS

- (id)initWithCurChapter:(NSDictionary *)dic andChapterList:(NSArray *)arr{
    self = [super init];
    if (self) {
        dicChapter = [NSDictionary dictionaryWithDictionary:dic];
        arrList = [NSMutableArray arrayWithArray:arr];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //const long gb30= 0x80000632;
    
    self.txtContent.font = [AllMethods getFontWithSize:18];
    
    dicUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"book_url"];
    [self getChapterContent];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
