//
//  BKPageContent.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/28.
//
//

#import "BKPageContent.h"

#import "LSYReadConfig.h"

#import "RJBookData.h"

@interface BKPageContent () {
    
    
//    NSDictionary *dicContent;
    
//    BOOL isMove;
    
    NSInteger bookIndex;
    NSString *allContent;
    
    NSInteger curChapter,curPage;          //记录当前读的章节数、页数
    
    NSInteger allPage,readPage;
    
//    NSMutableDictionary *dicAttribute;            //字体样式、字间距、行间距、字体大小
    
//    NSString *pagesPath;                   //分页记录地址
    CGSize contentSize;
    
    NSInteger retryTime;
    
    CGPoint potBegin;
}

@end

@implementation BKPageContent

#pragma mark - Touch Event 

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
//    isMove = NO;
    potBegin = [[touches anyObject] locationInView:self.view];
}

//- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    
//    if(!isMove)
//        isMove = YES;
//    
//}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    
    CGPoint pot = [[touches anyObject] locationInView:self.view];
    
    if (ABS(pot.x - potBegin.x) < 30) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kPAGE_CLICK_NOTI
                                                            object:nil];
    }
    
}


#pragma mark - Page Methods 


- (void)getReadRecord {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    
    NSString *sk = [NSString stringWithFormat:@"chp%@",book.name];
    NSString *str = [AllMethods getReadProgressWithKey:sk];
    
    if (str) { //存在记录
        NSArray *arr = [str componentsSeparatedByString:@"-"];
        
        str = arr[0];
        arr = [str componentsSeparatedByString:@","];
        curPage = [arr[1] intValue];
        curChapter = [arr[0] intValue];
    }
    else {
        
        curPage = 0;
        curChapter = 0;
    }
    
}

- (NSIndexPath *)getNextPageIndexPath {
    
    NSInteger page = curPage+1;
    NSInteger sec = curChapter;
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
    NSString *file_path = [AllMethods applicationDocumentsDirectory];
    NSString *pagesPath = [NSString stringWithFormat:@"%@/%@-%.0f.chr",file_path,book.name,config.fontSize];
    
    NSDictionary *dicChapters = [NSDictionary dictionaryWithContentsOfFile:pagesPath];
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",sec];
    NSDictionary *dic = dicChapters[key];
    NSArray *arr = dic[@"pages"];
    
    if (arr.count <= page) {
        
        sec = curChapter+1;
        page = 0;
        
        key = [NSString stringWithFormat:@"chapter_%06td",sec];
        dic = dicChapters[key];
        
        if (!dic) {
            return nil;
        }
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:page inSection:sec];
    
    return [indexPath copy];
    
}

- (NSIndexPath *)getPreviousPageIndexPath {
    
    
    NSInteger page = curPage - 1;
    NSInteger sec = curChapter;
    
    if (page < 0) {
        sec = curChapter - 1;
        
        if (sec < 0) {
            return nil;
        }
        
        LSYReadConfig *config = [LSYReadConfig shareInstance];
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
        NSString *file_path = [AllMethods applicationDocumentsDirectory];
        NSString *pagesPath = [NSString stringWithFormat:@"%@/%@-%.0f.chr",file_path,book.name,config.fontSize];
        
    
        NSDictionary *dicChapters = [NSDictionary dictionaryWithContentsOfFile:pagesPath];
        NSString *key = [NSString stringWithFormat:@"chapter_%06td",sec];
        NSDictionary *dic = dicChapters[key];
        NSArray *arr = dic[@"pages"];
        
        page = arr.count-1;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:page inSection:sec];
    
    
    return indexPath;
}


#pragma mark - Public 

- (void)updateBookAllPage:(NSInteger)all_page andReadPage:(NSInteger)read_page {
    
    
    allPage = all_page;
    readPage = read_page;
}

- (void)updateCurrentPageInfoWithChapter:(NSInteger)chap andPage:(NSInteger)page {
    
    curChapter = chap;
    curPage = page;
    
}

- (void)updateAttribute {
    
//    LSYReadConfig *config = [LSYReadConfig shareInstance];
//    dicAttribute = [NSMutableDictionary dictionaryWithDictionary:[LSYReadConfig parserAttribute:config]];
    
}

- (NSIndexPath *)getCurrentIndexPath {
    
    return [NSIndexPath indexPathForItem:curPage inSection:curChapter];
}

#pragma mark - Page Change Notification 

- (void)pageChangeNotification:(NSNotification *)notify {
    
    NSDictionary *dic = notify.userInfo;
    
    curPage = [dic[@"page"] integerValue];
    curChapter = [dic[@"chapter"] integerValue];
    
}


#pragma mark - Build UI

- (void)initinalContent {
    
//    [self getReadRecord];
    
    
//    LSYReadConfig *config = [LSYReadConfig shareInstance];
//    NSMutableDictionary *dicAttribute = [NSMutableDictionary dictionaryWithDictionary:[LSYReadConfig parserAttribute:config]];
    
    
    contentSize = CGSizeMake(ScreenWidth-20, ScreenHeight-20-20-20);
#if kFREE
    contentSize = CGSizeMake(ScreenWidth-20, ScreenHeight-20-20-20-50);
#endif
    
    
    VerticallyAlignedLabel *lab = [[VerticallyAlignedLabel alloc] initWithFrame:CGRectZero];
    lab.numberOfLines = 0;
    lab.backgroundColor = [UIColor clearColor];
    lab.verticalAlignment = VerticalAlignmentTop;
    
    [self.view addSubview:lab];
    self.labContent = lab;
    self.labContent.frame = CGRectMake(10, 40, contentSize.width, contentSize.height);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pageChangeNotification:)
                                                 name:kPAGE_CHANGE_NOTI
                                               object:nil];
    
}

#pragma mark - SYS


- (void)viewWillAppear:(BOOL)animated {
    
    
    @autoreleasepool {
        __weak BKPageContent *weak = self;
        
        if (!allContent) {
            RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
            
            NSString *file_enc = [[AllMethods applicationDocumentsDirectory] stringByAppendingPathComponent:@"file_encode.enc"];
            NSMutableDictionary *dic_enc ;
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:file_enc]) {
                dic_enc = [NSMutableDictionary dictionaryWithContentsOfFile:file_enc];
            }
            
            if (dic_enc[book.bookFile]) {
                
                long enc = [dic_enc[book.bookFile] longValue];
                allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                       encoding:enc
                                                          error:nil];
                
            }
            
            NSArray *arr = [allContent componentsSeparatedByString:@"\n"];
            allContent = [arr componentsJoinedByString:@"\n    "];
        }
        
        if (!allContent) {
            [weak performSelector:@selector(viewWillAppear:)
                       withObject:nil
                       afterDelay:0.5f];
            return;
        }
        
        NSIndexPath *index ;
        if (weak.pageFlag == -1 ){
            index = [weak getPreviousPageIndexPath];
        }
        else if(weak.pageFlag == 1){
            index = [weak getNextPageIndexPath];
        }
        else {
            index = [NSIndexPath indexPathForItem:curPage inSection:curChapter];
        }
        
        
        LSYReadConfig *config = [LSYReadConfig shareInstance];
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
        NSString *file_path = [AllMethods applicationDocumentsDirectory];
        NSString *pagesPath = [NSString stringWithFormat:@"%@/%@-%.0f.chr",file_path,book.name,config.fontSize];
    
        NSDictionary *dicChapters = [NSDictionary dictionaryWithContentsOfFile:pagesPath];
        
        NSString *key = [NSString stringWithFormat:@"chapter_%06td",index.section];
        NSDictionary *dic = dicChapters[key];
        if (!dic) {
            _labTitle.text = @"";
            _labPage.text = @"";
            retryTime ++;
            
            if (retryTime <= 3) {
                [weak performSelector:@selector(viewWillAppear:)
                           withObject:nil
                           afterDelay:0.5f];
            }
            return;
        }
        
        retryTime = 0;
        
        curPage = index.item;
        curChapter = index.section;
        
        
        NSString *pinfo = [NSString stringWithFormat:@"%td/%td",readPage,allPage];
        
        NSArray *arr = dic[@"pages"];
        NSRange ran_content = NSRangeFromString(arr[index.row]);
        NSRange ran_chap = NSRangeFromString(dic[@"range"]);
        NSString *str = [[allContent substringWithRange:ran_chap] substringWithRange:ran_content];
        
        if (!str) {
            return;
        }
        
        _labTitle.text = dic[@"title"];
        _labPage.text = pinfo;
        
        NSMutableDictionary *dicAttribute = [NSMutableDictionary dictionaryWithDictionary:[LSYReadConfig parserAttribute:config]];

        _labContent.attributedText = [[NSAttributedString alloc] initWithString:str
                                                                     attributes:dicAttribute];
        
        NSDictionary *dic_theme = config.arrThemes[config.theme];
        
        UIColor *col = [UIColor colorFromeString:dic_theme[@"font_color"]];

        if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
            if (_ivBk.image) {
                _ivBk.image = nil;
            }
            if (![[weak.view.backgroundColor colorToString] isEqualToString:dic_theme[@"bk"]]) {
                weak.view.backgroundColor = [UIColor colorFromeString:dic_theme[@"bk"]];
                _labTitle.textColor = [col mix:weak.view.backgroundColor];
                _labPage.textColor = _labTitle.textColor;
            }
            
        }
        else {
            NSString *sname = [NSString stringWithFormat:@"%@.jpg",dic_theme[@"bk"]];
            _ivBk.image = ImageNamed(sname);
            
            _labTitle.textColor = [col lighten];
            _labPage.textColor = _labTitle.textColor;
        }
        
        col = nil;
        index = nil;
    }
    
    
    
    [super viewWillAppear:animated];
    
}


- (id)initWithBookIdex:(NSInteger)index {
    
    self = [super init];
    if (self) {
        bookIndex = index;
        retryTime = 0;
        [self initinalContent];
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    NSLog(@"-------...... >>>>>>>> release ");
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
