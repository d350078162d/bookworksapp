//
//  BKNewMainBookList.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/18.
//
//

#import "BKNewMainBookList.h"

#import "Layout.h"

#import "BKMainBookCell.h"
//#import "BKUpdateCell.h"

#import "RJBookData.h"

#import "BKContent.h"
#import "BKNewContent.h"

#import "OpenBookTrans.h"
#import "CloseBookTrans.h"

#import "SGActionSheet.h"
#import "SGAlertView.h"

@interface BKNewMainBookList () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIViewControllerTransitioningDelegate,SGActionSheetDelegate,SGAlertViewDelegate> {
    
    BKMainBookCell *curBkCell;
    OpenBookTrans *openTrans;
    CloseBookTrans *closeTrans;
    
    BOOL isEidt;
    
    NSInteger delIndex;
    
    NSDictionary *dicDownloadBookInfo;
    
    int curSelectItem;
}

@property (weak, nonatomic) UICollectionView *myCollect;

@end

@implementation BKNewMainBookList

#pragma mark - SGAlertViewDelegate

- (void)didSelectedSureButtonClick {
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curSelectItem];
    NSString *str = [AllMethods getReadProgressWithKey:book.name];
    if (!str) {
        str = [[NSUserDefaults standardUserDefaults] valueForKey:book.name];
    }
    
    BKNewContent *myContentCtrl = [[BKNewContent alloc] initWithBookIndex:curSelectItem];
    if(str)[myContentCtrl setOldRecord:str];
    
    
    myContentCtrl.transitioningDelegate = self;
    
    [self presentViewController:myContentCtrl animated:YES completion:^{
        
#if kFREE
        [myContentCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
#endif
        
    }];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    del.canHideAd = YES;
    
}

- (void)didSelectedCancelButtonClick {
    
    [USER_DEFAULT setValue:@"1" forKey:@"no_tip"];
    
    BKContent *myContentCtrl = [[BKContent alloc] initWithBookIndex:curSelectItem];
    myContentCtrl.transitioningDelegate = self;
    
    [self presentViewController:myContentCtrl animated:YES completion:^{
        
#if kFREE
        [myContentCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
#endif
        
    }];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    del.canHideAd = YES;
    
}

#pragma mark - SGActionSheetDelegate


- (void)SGActionSheet:(SGActionSheet *)actionSheet didSelectRowAtIndexPath:(NSInteger)indexPath {
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        NSIndexPath *inx = [NSIndexPath indexPathForItem:delIndex inSection:0];
//        BKMainBookCell *cell = (BKMainBookCell *)[self.myCollect cellForItemAtIndexPath:inx];
//        [cell.bkView boom];
//    });
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        NSIndexPath *inx = [NSIndexPath indexPathForItem:delIndex inSection:0];
//        BKMainBookCell *cell = (BKMainBookCell *)[self.myCollect cellForItemAtIndexPath:inx];
//        [cell.bkView boom];
//    });
    
    
    
//    return;
    
    
    /*
     
     封装单一方法 [AllMethod deleteBookWithIndex:delIndex]
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:delIndex];
    [AllMethods deleteReadProgressWithKey:book.name];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: [NSString stringWithFormat:@"%@-progress",
                                                                book.name]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:book.name];
    
    NSString *path = [AllMethods applicationDocumentsDirectory];
    
    NSString *file_r = [NSString stringWithFormat:@"%@/%@.r",path,book.name];
    NSString *file = [NSString stringWithFormat:@"%@/%@",path,book.name];
    
    [AllMethods delRecordWith:book.name];
    
    [[NSFileManager defaultManager] removeItemAtPath:file error:nil];
    [[RJBookData sharedRJBookData].books removeObjectAtIndex:delIndex];
    [self.myCollect deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:delIndex inSection:0]]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    file_r = [NSString stringWithFormat:@"%@/%@-%@.r",path,book.name,
              [[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil]; */
    
    [AllMethods deleteBookWithIndex:delIndex];
    [self.myCollect deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:delIndex inSection:0]]];
    
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    if (openTrans) {
        openTrans = nil;
    }
    
    openTrans = [[OpenBookTrans alloc] init];
//    trans.bookView = curBkView;
    
    return (id<UIViewControllerAnimatedTransitioning>)openTrans;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    
    if (closeTrans) {
        closeTrans = nil;
    }
    
    closeTrans = [[CloseBookTrans alloc] init];
    [closeTrans setBookOriPoint:[openTrans getOrinPot]
                    andOriFrame:[openTrans getBkOriFrame]
                   andSuperView:[openTrans getBkSuperView]];
    
    return (id<UIViewControllerAnimatedTransitioning>)closeTrans;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isEidt) {
        
        delIndex = indexPath.item;
        [self delConfirm];
        
        return;
    }
    
    
    curSelectItem = (int)indexPath.item;
    BKMainBookCell *cell = (BKMainBookCell *)[collectionView cellForItemAtIndexPath:indexPath];
    curBkCell = cell;
    
    if ([USER_DEFAULT valueForKey:@"no_tip"] &&
        [[USER_DEFAULT valueForKey:@"no_otip"] intValue] == 1) {
        
        [self didSelectedCancelButtonClick];
        return;

    }
    
//    [self didSelectedCancelButtonClick];
//    return;
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:indexPath.item];
    [USER_DEFAULT setValue:book.name forKey:LAST_READ];
    
    NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:book.bookFile error:nil];
    double size = 1.0*attrs.fileSize/pow(10, 6);
    
    NSString *str = [AllMethods getReadProgressWithKey:book.name];
    
    if (str && size > 1.0) {
        NSString *msg = [NSString stringWithFormat:@"检测到旧版本阅读器的阅读记录，是否使用全新版本阅读器？\n\n新版本阅读器可定制化程度更高，使用全新分页算法\n\n使用新版阅读器需要对书籍重新分页，大约需要%.1f秒",size*6.0];
        SGAlertView *alert = [[SGAlertView alloc] initWithTitle:@"提示"
                                                       delegate:self
                                                   contentTitle:msg
                                        alertViewBottomViewType:SGAlertViewBottomViewTypeTwo];
        [alert show];
    }
    else {
        
        BKNewContent *myContentCtrl = [[BKNewContent alloc] initWithBookIndex:(int)indexPath.item];
//        self.modalPresentationCapturesStatusBarAppearance = YES;
//        myContentCtrl.modalTransitionStyle = UIModalPresentationOverFullScreen;
        if(str)[myContentCtrl setOldRecord:str];
        
        
        myContentCtrl.transitioningDelegate = self;
        
        [self presentViewController:myContentCtrl animated:YES completion:^{
            
#if kFREE
            [myContentCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
#endif
            
        }];
        
        BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
        del.canHideAd = YES;
    }
    
}


#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BKMainBookCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collect_cell"
                                                                         forIndexPath:indexPath];
    
    
    __weak BKNewMainBookList *weakSelf = self;
    [cell setCellLongClickEvent:^(UICollectionViewCell *cell){
        
        NSIndexPath *index = [weakSelf.myCollect indexPathForCell:cell];
        delIndex = index.item;
        [weakSelf delConfirm];
    }];
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:indexPath.item];
    
    NSString *name = [book.name stringByDeletingPathExtension];
    
    cell.imageView.image = nil;
    cell.labAuthor.text = @"";
    
    if (dicDownloadBookInfo[name]) {
        NSDictionary *dic = dicDownloadBookInfo[name];
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"img"]]
                          placeholderImage:ImageNamed(@"TXT_80x107_")];
        cell.labAuthor.text = dic[@"aut_name"];
        cell.labAuthor.textColor = RGBACOLOR(150, 150, 150, 1.0);
        cell.labAuthor.font = [UIFont systemFontOfSize:13];
        
    }
    else {
        cell.imageView.image = ImageNamed(@"TXT_80x107_");
        cell.labAuthor.textColor = RGBACOLOR(200, 200, 200, 1.0);
        cell.labAuthor.font = [UIFont systemFontOfSize:12];
    }
    cell.titleLabel.text = [name stringByReplacingOccurrencesOfString:@"下载" withString:@""];
    
    if ([USER_DEFAULT valueForKey:LAST_READ] &&
        [[USER_DEFAULT valueForKey:LAST_READ] isEqualToString:book.name]) {
        cell.imageMark.hidden = NO;
    }
    else {
        cell.imageMark.hidden = YES;
    }
    
    
    NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:book.name];
    if (str) {
//        cell.viewProgress.hidden = NO;
        NSArray *arr = [str componentsSeparatedByString:@"-"];
        cell.labAuthor.text = [NSString stringWithFormat:@"%@%%",[arr lastObject]];
        
    }
    else {
//        cell.viewProgress.hidden = YES;
//        cell.labProgress.text = @"";
    }
    
    [cell setDeleteHidden:!isEidt];
    
    return cell;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [[RJBookData sharedRJBookData].books count];
    
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    float width = 60*1.7;
    float height = 75*1.7+50;
    return CGSizeMake(width, height);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    int num_per_row = 3;
    
    if (isPad) {
        num_per_row = 5;
    }
    
    float width = 60*1.7;
    
    
    float margin_top = (ScreenWidth-num_per_row*width)/(num_per_row+1);
    if (margin_top<20) {
        margin_top = 20;
    }
    
    if (isPad) {
        return UIEdgeInsetsMake(margin_top, (ScreenWidth-num_per_row*width)/(num_per_row+2),
                                0, (ScreenWidth-num_per_row*width)/(num_per_row+2));
    }
    
    return UIEdgeInsetsMake(margin_top, (ScreenWidth-num_per_row*width)/(num_per_row+1),
                            0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    
}



#pragma mark - Private

- (void)delConfirm {
    
    SGActionSheet *actSheet = [[SGActionSheet alloc] initWithTitle:@"确定要删除吗"
                                                          delegate:self
                                                 cancelButtonTitle:@"取消"
                                             otherButtonTitleArray:@[@"确定删除"]
                                                          showType:ActTypeDelete];
    actSheet.cancelButtonTitleColor = [UIColor redColor];
    [actSheet show];
    
}

- (void)editTable:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    if ([[btn titleForState:UIControlStateNormal] isEqualToString:@"编辑"]) {
        isEidt = YES;
//        [btn setTitle:@"取消" forState:UIControlStateNormal];
    }
    else {
        isEidt = NO;
//        [btn setTitle:@"编辑" forState:UIControlStateNormal];
    }
    
    [self.myCollect reloadData];
    [self updateEditButoon:sender];
}

- (void)updateEditButoon:(UIButton *)btn {
    
    if (isEidt) {
        [btn setTitle:@"取消" forState:UIControlStateNormal];
    }
    else {
        [btn setTitle:@"编辑" forState:UIControlStateNormal];
    }
    
    [self.myCollect reloadData];
    
}

- (void)getDownloadBookInfo {
    
    dicDownloadBookInfo = [NSDictionary dictionaryWithDictionary:[AllMethods getBookDownloadList]];
    [self.myCollect reloadData];
}

#pragma mark - Public 

- (void)reloadLocalBookList {
    
    [[RJBookData sharedRJBookData] loadBooks];
    [self getDownloadBookInfo];
    
    if (!self.presentedViewController) {
        [self.myCollect reloadData];
    }
    else if (self.tabBarController.selectedIndex != 0) {
        [self.myCollect reloadData];
    }
}

- (UIView *)currentOpenBookView {
    
    return curBkCell.bkView;
}

#pragma mark - Build UI

- (void)buildCollectView {
    
    float width = 80*1.25;
    
    Layout *lay = [[Layout alloc] initWithItemWidth:width
                                          andHeight:(107+15)*1.25];
    lay.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    lay.sectionInset = UIEdgeInsetsMake(0, (ScreenWidth-3*width)/4.0,
                                         0, (ScreenWidth-3*width)/4.0);
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                                   collectionViewLayout:lay];
    [collect registerClass:[BKMainBookCell class] forCellWithReuseIdentifier:@"collect_cell"];
    
    collect.delegate = self;
    collect.dataSource = self;
    collect.tag = 1;
    collect.backgroundColor = [UIColor whiteColor];
    collect.contentInset = UIEdgeInsetsMake(10, 0.0, 50.0, 0.0);
    
    self.myCollect = collect;
    [self.myCollect setAlwaysBounceVertical:YES];
    
    [self.view addSubview:self.myCollect];
}

- (void)buildEditButton {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 55, 44)];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn setTitle:@"编辑" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
    [btn addTarget:self action:@selector(editTable:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = item;
}

#pragma mark - SYS

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (openTrans) {
        openTrans = nil;
    }
    if (closeTrans) {
        closeTrans = nil;
    }
    
    self.navigationController.tabBarItem.badgeValue = nil;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_auto"];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-48-49+24)];
    del.canHideAd = NO;
    [del setADHidden:NO];
    
    /*清除本地存储数据*/
    //    [self clearAllRange];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    isEidt = NO;
    [self updateEditButoon:self.navigationItem.rightBarButtonItem.customView];
    [[RJBookData sharedRJBookData] loadBooks];
    [self getDownloadBookInfo];
//    [self.myCollect reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self buildCollectView];
    [self buildEditButton];
    
    self.navigationItem.title = @"书架";
    
    self.transitioningDelegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
