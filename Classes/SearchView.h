//
//  SearchView.h
//  BookWorksApp
//
//  Created by 刁志远 on 13-8-12.
//
//

#import <UIKit/UIKit.h>

#import "AllMethods.h"

#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"

@interface SearchView : UIViewController{
    NSMutableArray *arrList,*arrDownList,*arrBooks;
    ASINetworkQueue *down_queue;
    BOOL isDowning;
}

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *myTable;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;
@end
