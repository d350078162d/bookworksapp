//
//  BKBookListCtrl.m
//  BookWorksApp
//
//  Created by 刁志远 on 15/1/10.
//
//

#import "BKBookListCtrl.h"

#import "RJBookData.h"

#import "BKContent.h"

#import <objc/runtime.h>

#import "BookWorksAppAppDelegate.h"

#import "UIWebView+Noti.h"



@interface BKBookListCtrl () {
    
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layBottom;
@end

@implementation BKBookListCtrl


#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BKContent *myContentCtrl = [[BKContent alloc] initWithBookIndex:(int)indexPath.row];
//    LYBookContent *myContentCtrl = [[LYBookContent alloc] initWithBookIndex:(int)indexPath.row];
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [myContentCtrl setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:myContentCtrl animated:YES];
    [self.tabBarController setHidesBottomBarWhenPushed:NO];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    del.canHideAd = YES;
    
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  @"删除";
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:indexPath.row];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: [NSString stringWithFormat:@"%@-progress",
                                                                book.name]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:book.name];
    
    NSString *path = [AllMethods applicationDocumentsDirectory];//[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/"];
    
    NSString *file_r = [NSString stringWithFormat:@"%@/%@.r",path,book.name];
    NSString *file = [NSString stringWithFormat:@"%@/%@",path,book.name];
    
    [AllMethods delRecordWith:book.name];
    
    [[NSFileManager defaultManager] removeItemAtPath:file error:nil];
    [[RJBookData sharedRJBookData].books removeObjectAtIndex:[indexPath row]];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    file_r = [NSString stringWithFormat:@"%@/%@-%@.r",path,book.name,
              [[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_r])
        [[NSFileManager defaultManager] removeItemAtPath:file_r error:nil];
    
    
}

#pragma mark - UITableViewDataSource



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[RJBookData sharedRJBookData].books count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *str_id = @"book_list_cell_id";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str_id];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:str_id];
        cell.textLabel.textColor = NAV_SHADOW_COLOR;
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:indexPath.row];
    cell.textLabel.text = book.name;
    
    
    NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:book.name];
    if (str) {
        NSArray *arr = [str componentsSeparatedByString:@"-"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@%%",[arr lastObject]];
    }
    
    return cell;
}

#pragma mark - My

-(void)editTable:(id)sender{
    UIButton *item = (UIButton *)sender;
    [_myTable setEditing:!_myTable.editing animated:YES];
    if (_myTable.editing) {
        [item setTitle:NSLocalizedString(@"Done", @"") forState:UIControlStateNormal];
    }
    else {
        [item setTitle:NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
    }
}

- (void)clearAllRange{
    [AllMethods clearCacheWithExtention:@"r"];
    for (int i=0; i<[RJBookData sharedRJBookData].books.count; i++) {
        RJSingleBook *book = [RJBookData sharedRJBookData].books[i];
        NSString *str = [NSString stringWithFormat:@"%@-progress",book.name];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:str];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:book.name];
    }

}

#pragma mark - Tabbar Delegate

- (NSString *)tabTitle
{
    return NSLocalizedString(@"Tab_Novel", @"");
}

- (UIFont *)tabTitleFont{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if ([[def valueForKey:kFONT] length]>0) {
        return [UIFont fontWithName:[def valueForKey:kFONT]
                               size:[[def valueForKey:kFONT_SIZE] floatValue]];
    }
    else{
        return [UIFont fontWithName:[FONT_ARR objectAtIndex:0]
                               size:[[def valueForKey:kFONT_SIZE] floatValue]];
    }
}


#pragma mark - Swizzle

- (void)pre:(UIViewController *)ctrl ani:(BOOL)ani {
    
    if ([ctrl isKindOfClass:NSClassFromString(@"BaiduMobAdWebLPController")]) {
        
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"is_auto"]) {
            
            Method ori_Method =  class_getInstanceMethod([self class],
                                                         @selector(presentViewController:animated:completion:));
            Method my_Method = class_getInstanceMethod(NSClassFromString(@"BKBookListCtrl"), @selector(pre:ani:));
            method_exchangeImplementations(my_Method, ori_Method);
            
            [self presentViewController:ctrl animated:YES completion:^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    method_exchangeImplementations(ori_Method , my_Method);
                });
            }];
            
            return;
        }
        
        
        NSArray *arr = ctrl.view.subviews;
        if (!arr || arr.count<=0) {
            return;
        }
        UIWebView *web = arr[0];
        
        UIWebView_Noti *mweb = [[UIWebView_Noti alloc] init];
        [mweb loadRequest:web.request];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_auto"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [ctrl dismissViewControllerAnimated:YES completion:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
    }
    else {
        Method ori_Method =  class_getInstanceMethod([self class],
                                                     @selector(presentViewController:animated:completion:));
        Method my_Method = class_getInstanceMethod(NSClassFromString(@"BKBookListCtrl"), @selector(pre:ani:));
        method_exchangeImplementations(my_Method, ori_Method);
        [self presentViewController:ctrl animated:YES completion:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                method_exchangeImplementations(ori_Method , my_Method);
            });
        }];
    }
    
}

- (void)myOpenUrl:(NSURL *)url{
    
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"is_auto"]) {
        Method ori_Method_open =  class_getInstanceMethod([UIApplication class], @selector(openURL:));
        Method my_Method_open = class_getInstanceMethod(NSClassFromString(@"BKBookListCtrl"), @selector(myOpenUrl:));
        method_exchangeImplementations(my_Method_open , ori_Method_open);
        [(UIApplication *)self openURL:url];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            method_exchangeImplementations(ori_Method_open , my_Method_open);
        });
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_auto"];
    
}


#pragma mark - Notication 

- (void)receiveWebLoadFinishNotification:(NSNotification *)noti {
    
    [WSProgressHUD dismiss];
    UIWebView *webView = noti.object;
    [webView stringByEvaluatingJavaScriptFromString:@"window.alert=null;"];
    
}



#pragma mark - SYS

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.tabBarItem.badgeValue = nil;
    [[RJBookData sharedRJBookData] loadBooks];
    [self.myTable reloadData];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_auto"];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-48-49+24)];
    del.canHideAd = NO;
    [del setADHidden:NO];
    
    /*清除本地存储数据*/
//    [self clearAllRange];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_auto"];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveWebLoadFinishNotification:)
//                                                 name:kWebViewDidFinishLoadNotification
//                                               object:nil];
//    [self presentViewController:<#(UIViewController *)#> animated:<#(BOOL)#> completion:<#^(void)completion#>]
//    Method ori_Method =  class_getInstanceMethod([UIViewController class], @selector(presentViewController:animated:completion:));
//    Method my_Method = class_getInstanceMethod([self class], @selector(pre:ani:));
//    method_exchangeImplementations(ori_Method, my_Method);
//    Method ori_Method_open =  class_getInstanceMethod([UIApplication class], @selector(openURL:));
//    Method my_Method_open = class_getInstanceMethod([self class], @selector(myOpenUrl:));
//    method_exchangeImplementations(ori_Method_open, my_Method_open);
    
    self.navigationItem.title = NSLocalizedString(@"Nav_Novel", @"");
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 55, 44)];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn setTitle:NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
    [btn addTarget:self action:@selector(editTable:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = item;
    
    self.myTable.tableFooterView = [UIView new];
    
#if kFREE
    
    self.layBottom.constant = 48;
    
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end







//@implementation UIViewController (Present)
//
//- (void)setModalPresentationStyle:(UIModalPresentationStyle)modalPresentationStyle {
//    self.modalPresentationStyle = UIModalPresentationFullScreen;
//    NSLog(@"---->>>>>> present");
//}
//
//@end








