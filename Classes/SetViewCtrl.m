//
//  SetViewCtrl.m
//  BookWorksApp
//
//  Created by 刁志远 on 12-3-30.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SetViewCtrl.h"
#import "About.h"

#import "BWUserRegView.h"

#import "ChatViewController.h"
#import "EMIMHelper.h"


@implementation SetViewCtrl

@synthesize myTable;


#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [popoverListView.listView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:identifier];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 200, 44)];
        lab.tag = 1;
        [cell.contentView addSubview:lab];
        
        UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(lab.frame.size.width+lab.frame.origin.x,5,
                                                                  popoverListView.frame.size.width-200, 44)];
        lab2.tag = 2;
        [cell.contentView addSubview:lab2];
    }
    
    int row = indexPath.row;
    
    UILabel *lab = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *lab2 = (UILabel *)[cell.contentView viewWithTag:2];
    lab.font = [UIFont fontWithName:[FONT_ARR objectAtIndex:row] size:17];
    lab.text = [NSString stringWithFormat:@"小说阅读 -- %@",[FONT_ARR_NAME objectAtIndex:row]];
    lab.backgroundColor = [UIColor clearColor];
    lab2.text = @"";
    
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    return [FONT_ARR count];
}

#pragma mark - UIPopoverListViewDelegate
- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    //    NSLog(@"%s : %d", __func__, indexPath.row);
    // your code here
    curFont = [FONT_ARR objectAtIndex:indexPath.row];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                    message:@"设置应用使用当前字体需要跳出应用然后返回，是否确定跳出应用？"
                                                   delegate:self
                                          cancelButtonTitle:@"不跳出"
                                          otherButtonTitles:@"跳出，再返回", nil];
    alert.tag = 1;
    [alert show];
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}


#pragma mark -
#pragma mark My-

- (IBAction)toLoginView:(id)sender {
    QLLoginView *log = [[QLLoginView alloc] init];
    [log setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:log animated:YES];

}

-(void)setScreen:(BOOL)ison{
    [UIApplication sharedApplication].idleTimerDisabled = ison;
}

- (void)login{
    [WSProgressHUD showWithStatus:@"正在登录..."];
    dispatch_queue_t queue = dispatch_queue_create("regist_queue", Nil);
    dispatch_async(queue, ^{
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSString *surl = [NSString stringWithFormat:@"%@/%@?method=2&psd=%@&log_id=%@",kMAIN_URL,kUSER_URL,[def valueForKey:kUSER_PSD],[def valueForKey:kUSER_NAME]];
        
        surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:surl];
        NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:Nil];
        NSDictionary *dic = [res JSONValue];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([[dic valueForKey:@"err_code"] intValue] == 0) {
                [WSProgressHUD dismiss];
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:dic forKey:kUSER_INFO];
                [self refreshInfo];
            }
            else{
                [AllMethods showAltMsg:[dic valueForKey:@"err_desc"]];
            }
        });
    });
}

- (void)refreshInfo{
    NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:kUSER_INFO];
    NSArray *arr_down = [dic objectForKey:@"download"];
    NSArray *arr_rec = [dic objectForKey:@"recommen"];
    
    [segButton setTitle:[NSString stringWithFormat:@"已下载%d本书",[arr_down count]]
      forSegmentAtIndex:0];
    [segButton setTitle:[NSString stringWithFormat:@"已评论%d本书",[arr_rec count]]
      forSegmentAtIndex:1];
    
    UILabel *lab = (UILabel *)[self.headView viewWithTag:1];
    lab.text = [dic valueForKey:@"nick"];
}

- (void)toggleRefresh{
    refresh = YES;
    [timerRefresh invalidate];
    timerRefresh = Nil;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        if (buttonIndex != [alertView cancelButtonIndex]) {
            NSString *surl = [NSString stringWithFormat:@"http://dydog.sinaapp.com/reboot.php?font=%@",curFont];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:surl]];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kEXIT];
        }
    }
}

#pragma mark - 

#pragma mark -
#pragma mark UITableViewDataSource UITableViewDelegate-

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	curSel = indexPath.row;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    switch (indexPath.row) {
//        case 0:{
//            CGFloat xWidth = self.view.bounds.size.width - 20.0f;
//            CGFloat yHeight = self.view.bounds.size.height - 44 -20 -40;
//            CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
//            
//            UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
//            poplistview.delegate = self;
//            poplistview.datasource = self;
//            [poplistview setTitle:NSLocalizedString(@"Set_Font", @"")];
//            [poplistview show];
//            break;
//        }
//        case 1:{
//            BOOL bol = [[NSUserDefaults standardUserDefaults] boolForKey:kAUTO_SAVE];
//            bol = !bol;
//            [[NSUserDefaults standardUserDefaults] setBool:bol forKey:kAUTO_SAVE];
//            if (bol) {
//                cell.detailTextLabel.text = NSLocalizedString(@"ON", @"");
//            }
//            else cell.detailTextLabel.text = NSLocalizedString(@"OFF", @"");
//            break;
//        }
//        case 0:{
//            BOOL bol = [[NSUserDefaults standardUserDefaults] boolForKey:kKEEP_ON];
//            bol = !bol;
//            [[NSUserDefaults standardUserDefaults] setBool:bol forKey:kKEEP_ON];
//            if (bol) {
//                cell.detailTextLabel.text = NSLocalizedString(@"ON", @"");
//            }
//            else cell.detailTextLabel.text = NSLocalizedString(@"OFF", @"");
//            [self setScreen:bol];
//            break;
//        }
        case 0:{
            QLLoginView *log = [[QLLoginView alloc] init];
            [log setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:log animated:YES];
            break;
        }
        case 1:{
            About *a = [[About alloc] init];
            [a setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:a animated:YES];
            break;
        }
        case 2:{
            NSString *surl = @"https://itunes.apple.com/cn/app/yue-du-xiao-shuo-mian-fei/id673879657?ls=1&mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:surl]];
            break;
        }
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	NSString *strIden = [NSString stringWithFormat:@"cell%d",indexPath.row];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strIden];
	if (cell==nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
									   reuseIdentifier:strIden];
	}
	
	cell.textLabel.backgroundColor = [UIColor clearColor];
//	cell.textLabel.textColor = [UIColor colorWithRed:228.0/255.0 green:187.0/255.0 blue:78.0/255.0
//											   alpha:1.0f];
	cell.detailTextLabel.backgroundColor = [UIColor clearColor];
	cell.detailTextLabel.textColor = [UIColor whiteColor];
	
	cell.textLabel.text = [NSString stringWithFormat:@"    %@",[arrList objectAtIndex:[indexPath row]]];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    [cell.textLabel.layer setShadowColor:[UIColor colorWithRed:230/255.0 green:180/255.0
                                                          blue:105/255.0 alpha:1.0].CGColor];
    [cell.textLabel.layer setShadowOffset:CGSizeMake(0, -1)];
    cell.textLabel.font = [UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                          size:17];
    
//    switch (indexPath.row) {
//
//        case 1:
//            cell.detailTextLabel.text = @"";
//            break;
//
//        case 0:{
//            if ([[NSUserDefaults standardUserDefaults] boolForKey:kKEEP_ON]) {
//                cell.detailTextLabel.text =  NSLocalizedString(@"ON", @"");
//            }
//            else cell.detailTextLabel.text =  NSLocalizedString(@"OFF", @"");
//            break;
//        }
//        default:
//            break;
//    }
    
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [arrList count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
	[cell setBackgroundColor:NAV_SHADOW_COLOR];
	[cell setSelectionStyle:UITableViewCellSelectionStyleGray];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	return self.headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.headView.frame.size.height;
}

#pragma mark - Custom Action

- (void)showFeedback {
    
//    [[Helpshift sharedInstance] showConversation:self withOptions:nil];
    
    [[EMIMHelper defaultHelper] loginEasemobSDK];
    NSString *cname = @"d350078162d";//[[EMIMHelper defaultHelper] cname];
    ChatViewController *chatController = [[ChatViewController alloc] initWithChatter:cname type:eSaleTypeNone];
    chatController.title = @"问题反馈";
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:chatController];
    [self presentViewController:nav animated:YES completion:nil];
//    [self.navigationController pushViewController:chatController animated:YES];
}

#pragma mark - Build UI

- (void)buildQuestion {
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 50)];
    view.backgroundColor = [UIColor clearColor];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = view.bounds;
    [btn setTitle:@"问题反馈" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [view addSubview:btn];
    
    [btn addTarget:self action:@selector(showFeedback) forControlEvents:UIControlEventTouchUpInside];
    
    self.myTable.tableFooterView = view;
    
}

#pragma mark sys-

- (NSString *)tabTitle
{
	return NSLocalizedString(@"Tab_Set", @"");
}

- (UIFont *)tabTitleFont{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if ([[def valueForKey:kFONT] length]>0) {
        return [UIFont fontWithName:[def valueForKey:kFONT]
                               size:[[def valueForKey:kFONT_SIZE] floatValue]];
    }
    else{
        return [UIFont fontWithName:[FONT_ARR objectAtIndex:0]
                               size:[[def valueForKey:kFONT_SIZE] floatValue]];
    }
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:NSLocalizedString(@"Nav_Set", @"")];
	isFirst = YES;
    refresh = YES;
	//UIImageView *imgv2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back.png"]];
	//[myTable setBackgroundView:imgv2];
	[self.myTable setBackgroundColor:NAV_SHADOW_COLOR];
	//[imgv2 release];
    
    if (kFREE == 1) {
        arrList = [[NSArray alloc] initWithObjects:
                   NSLocalizedString(@"Set_Change_User", @""),
                   NSLocalizedString(@"About", @""),
                   @"下载无广告版本",nil];
    }
	else{
        arrList = [[NSArray alloc] initWithObjects:
                   NSLocalizedString(@"Set_Change_User", @""),
                   NSLocalizedString(@"About", @""),nil];
    }
	
    
//    segButton = [[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(0, self.headView.frame.size.height-44, ScreenWidth,
//                                                                          44)
//                                                         items:@[@{@"text":@"已下载0次",@"icon":@""},
//                                                                 @{@"text":@"以评论0次",@"icon":@""}]
//                                                  iconPosition:IconPositionRight
//                                             andSelectionBlock:^(NSUInteger segIndex){
//                                                 
//                                             }];
//    segButton.color=NAV_COLOR;
//    segButton.borderWidth=0.5;
//    segButton.borderColor=[UIColor whiteColor];
//    segButton.selectedColor=NAV_COLOR ;
//    segButton.textAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:14],
//                             NSForegroundColorAttributeName:[UIColor whiteColor]};
//    segButton.selectedTextAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:14],
//                                     NSForegroundColorAttributeName:[UIColor whiteColor]};
//    [self.headView addSubview:segButton];
    [self.headView setBackgroundColor:NAV_COLOR];
    
    [self buildQuestion];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if ([def valueForKey:kUSER_NAME]) {
        if (refresh) {
            refresh = NO;
            [self login];
            if (!timerRefresh) {
                timerRefresh = [NSTimer scheduledTimerWithTimeInterval:600
                                                                target:self
                                                              selector:@selector(toggleRefresh)
                                                              userInfo:Nil
                                                               repeats:NO];
            }
        }
        else{
            [self refreshInfo];
        }
        self.btnLogin.hidden = YES;
    }
    else if(isFirst){
        isFirst = NO;
        self.btnLogin.hidden = NO;
    }

}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	self.myTable = nil;
}



@end
