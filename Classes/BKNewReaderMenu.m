//
//  BKNewReaderMenu.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/20.
//
//

#import "BKNewReaderMenu.h"

#import "BKNewMenuTop.h"
#import "BKNewMenuBottom.h"

#import "LSYReadConfig.h"

@interface BKNewReaderMenu () {
    
    BKNewMenuTop *topMenu;
    BKNewMenuBottom *bottomMenu;
    
    NSString *strTitle;
    
    UIViewController *showCtrl;
    
    UIButton *btnTheme;
    
}

@end


@implementation BKNewReaderMenu

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - Touch Events

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    
    UITouch *touch = [[touches allObjects] objectAtIndex:0];
    CGPoint pot = [touch locationInView:self];
    
    CGRect rect = CGRectMake(0, ORIGINAL_Y(topMenu), SIZE_W(self),
                             SIZE_H(self)-ORIGINAL_Y(topMenu)-SIZE_H(bottomMenu));
    
    if (CGRectContainsPoint(rect, pot)) {
        [self dismissView];
    }
}

#pragma mark - Private

- (void)btnThemeClicked:(id)sender {
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    btnTheme.selected = !btnTheme.isSelected;
    if (btnTheme.isSelected) {
        config.theme = 7;
    }
    else {
        config.theme = 6;
    }
    
    __weak BKNewMenuBottom *weak_menu = bottomMenu;
    [weak_menu updateTheme];
    [self bottomMenuFunction:103 andValue:0];
    
}

- (void)bottomMenuFunction:(NSInteger )type andValue:(float)value {
    
    
    switch (type) {
        case 101:{//字体大小
            if (self.delegate && [self.delegate respondsToSelector:@selector(menuFontSizeChanged:)]) {
                
                [self.delegate menuFontSizeChanged:value];
            }
            break;
        }
        case 102:{//目录
            if (self.delegate && [self.delegate respondsToSelector:@selector(menuDidClickedCatagory)]) {
                [self.delegate menuDidClickedCatagory];
            }
            break;
        }
        case 103:{//主题更改
            if (self.delegate && [self.delegate respondsToSelector:@selector(menuDidChangeTheme)]) {
                [self.delegate menuDidChangeTheme];
            }
            break;
        }
        case 104:{//翻页动画
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(menuPageTurnChanged:)]) {
                [self.delegate menuPageTurnChanged:value];
            }
            
            break;
        }
        default:
            break;
    }
}

#pragma mark - Public

- (void)setLabTitle:(NSString *)tit {
    
    strTitle = [NSString stringWithString:tit];
    if (topMenu) {
        [topMenu setTitleText:strTitle];
    }
}

- (void)dismissView {
    
    if (!self.superview) {
        return;
    }
    
    _isHidden = YES;
    
    if (showCtrl) {
        [showCtrl setNeedsStatusBarAppearanceUpdate];
    }
    
    
    __weak BKNewReaderMenu *weakSelf = self;
    [UIView animateWithDuration:0.25f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         weakSelf.backgroundColor = RGBACOLOR(0, 0, 0, 0.0f);
                         CGRect frame = topMenu.frame;
                         frame.origin.y = -64;
                         topMenu.frame = frame;
                         
                         frame = bottomMenu.frame;
                         frame.origin.y = SIZE_H(weakSelf);
                         bottomMenu.frame = frame;
                         
                         btnTheme.alpha = .0f;
                         btnTheme.frame = CGRectMake(SIZE_W(self)-40-15, SIZE_H(self)-40-15, 40, 40);
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             [weakSelf removeFromSuperview];
                             
                             if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(menuDidDismiss)]) {
                                 [weakSelf.delegate menuDidDismiss];
                             }
                         }
                         
                     }];
    
}

- (void)showInController:(UIViewController *)ctrl {
    
    showCtrl = ctrl;
    [self showInView:ctrl.view];
    
}

- (void)showInView:(UIView *)view {
    
    _isHidden = NO;
    
    if (showCtrl) {
        [showCtrl setNeedsStatusBarAppearanceUpdate];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(menuWillShow)]) {
        [self.delegate menuWillShow];
    }
    
//     [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
    
    if (!topMenu) {
        topMenu = [[BKNewMenuTop alloc] initWithFrame:CGRectMake(0, -64, SIZE_W(self), 64)];
        if (strTitle) {
            [topMenu setTitleText:strTitle];
        }
        [self addSubview:topMenu];
        
        __weak BKNewReaderMenu *weakSelf = self;
        [topMenu setClickedEvent:^(UIButton *btn) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(menuDidClickedBack)]) {
                [weakSelf.delegate menuDidClickedBack];
            }
        }];
    }
    
    if (!bottomMenu) {
        
        if ([showCtrl isKindOfClass:NSClassFromString(@"BKNewContent")]) { //本地书籍
            bottomMenu = [[BKNewMenuBottom alloc] initWithFrame:CGRectMake(0, SIZE_H(self), SIZE_W(self), 150)];
        }
        else if ([showCtrl isKindOfClass:NSClassFromString(@"BKChapterReader")]) { //网络书籍
            bottomMenu = [[BKNewMenuBottom alloc] initWithMenuShowType:MENU_SHOW_ONLINE_BOOK
                                                              andFrame:CGRectMake(0, SIZE_H(self), SIZE_W(self), 100)];
            
        }
        
        
        __weak BKNewReaderMenu *weakSelf = self;
        
        [bottomMenu setClickEvent:^(NSInteger type, float value) {
            [weakSelf bottomMenuFunction:type andValue:value];
        }];
        [self addSubview:bottomMenu];
    }
    
    if (!btnTheme) {
        btnTheme = [UIButton buttonWithType:UIButtonTypeCustom];
        btnTheme.frame = CGRectMake(SIZE_W(self)-40-15, SIZE_H(self)-15, 40, 40);
        [btnTheme setBackgroundColor:RGBACOLOR(0, 0, 0, 0.8f)];
        [btnTheme addTarget:self action:@selector(btnThemeClicked:)
           forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnTheme];
        [btnTheme setShowsTouchWhenHighlighted:YES];
        btnTheme.alpha = .0f;
        btnTheme.clipsToBounds = YES;
        btnTheme.layer.cornerRadius = SIZE_H(btnTheme)/2;
        
        [btnTheme setImage:ImageNamed(@"read_sun") forState:UIControlStateNormal];
        [btnTheme setImage:ImageNamed(@"read_moon") forState:UIControlStateSelected];
    }
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    
    if (config.theme == 6) { //夜间模式
        [btnTheme setSelected:NO];
    }
    else if (config.theme == 7) {//白天模式
        [btnTheme setSelected:YES];
    }
    else {
        [btnTheme setSelected:NO];
    }
    
    if (!self.superview) {
        self.backgroundColor = RGBACOLOR(0, 0, 0, 0);
        [view addSubview:self];
    }
    
    __weak BKNewReaderMenu *weakSelf = self;
    
    [UIView animateWithDuration:0.25f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         weakSelf.backgroundColor = RGBACOLOR(0, 0, 0, 0.2f);
                         CGRect frame = topMenu.frame;
                         frame.origin.y = 0;
                         topMenu.frame = frame;
                         
                         frame = bottomMenu.frame;
                         frame.origin.y = SIZE_H(weakSelf)-SIZE_H(bottomMenu);
                         bottomMenu.frame = frame;
                         
                         CGRect frame_theme = btnTheme.frame;
                         frame_theme.origin.y = frame.origin.y-SIZE_H(btnTheme)-15;
                         btnTheme.frame = frame_theme;
                         btnTheme.alpha = 1.0f;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    
}


#pragma mark - Build UI

#pragma mark - SYS

- (id)init {
    
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = RGBACOLOR(0, 0, 0, 0);
    }
    return self;
    
}


@end
