//
//  BKQuestionAddAnswer.h
//  BookWorksApp
//
//  Created by 刁志远 on 2016/12/10.
//
//

#import <UIKit/UIKit.h>

@interface BKQuestionAddAnswer : UIViewController


- (id)initWithQuestion:(NSDictionary *)dic;

@end
