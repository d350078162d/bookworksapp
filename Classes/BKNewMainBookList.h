//
//  BKNewMainBookList.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/18.
//
//

#import <UIKit/UIKit.h>


@interface BKNewMainBookList : UIViewController


- (void)reloadLocalBookList;
- (UIView *)currentOpenBookView;

@end
