//
//  BWWebView.m
//  BookWorksApp
//
//  Created by 刁志远 on 14-1-13.
//
//

#import "BWWebView.h"

#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"

#import "TFHpple.h"

@interface BWWebView ()<NJKWebViewProgressDelegate,UIWebViewDelegate>{
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;
}

@end

@implementation BWWebView

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
}

#pragma mark - UIWebViewDelegate 

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self animateLoadButton];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.btnRefresh.layer removeAllAnimations];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self.btnRefresh.layer removeAllAnimations];
    [WSProgressHUD showErrorWithStatus:@"请刷新重试"];
}

#pragma mark - MY

- (void)animateLoadButton{
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [self.btnRefresh.layer addAnimation:rotationAnimation forKey:@"begin_refresh"];
}

- (void)getBookContent{
    NSString *sname = [dicBook valueForKey:@"book_name"];
    if (!sname) {
        sname = [dicBook valueForKey:@"title"];
    }
    NSRange ran = [sname rangeOfString:@"II"];
    if (ran.location != NSNotFound) {
        sname = [sname substringFromIndex:ran.location+ran.length];
    }
    ran = [sname rangeOfString:@"I"];
    if (ran.location != NSNotFound) {
        sname = [sname substringFromIndex:ran.location+ran.length];
    }
    ran = [sname rangeOfString:@"III"];
    if (ran.location != NSNotFound) {
        sname = [sname substringFromIndex:ran.location+ran.length];
    }
    NSDictionary *dic_url = [[NSUserDefaults standardUserDefaults] objectForKey:@"book_url"];
    NSString *str_url = [NSString stringWithFormat:[dic_url valueForKey:@"book_chapter"],sname];
    str_url = [str_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str_url]]];
}

- (void)backUp{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setCurBook:(NSDictionary *)dic{
    dicBook = [[NSDictionary alloc] initWithDictionary:dic];
}

- (IBAction)btnClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 0:{
            [self.webView goBack];
            break;
        }
        case 1:{
            [self.webView goForward];
            break;
        }
        case 2:{
            [self.webView reload];
            break;
        }
        default:
            break;
    }
}

#pragma mark - SYS

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    float hei = 44;
    
    CGFloat progressBarHeight = 2.5f;
    CGRect navigaitonBarBounds = CGRectMake(0, 0, ScreenWidth, hei);
    
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    
    [self getBookContent];
    
    if ([dicBook valueForKey:@"book_name"]) {
        self.navigationItem.title = [dicBook valueForKey:@"book_name"];
    }
    else{
        self.navigationItem.title = [dicBook valueForKey:@"title"];
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 32, 32);
    [btn setBackgroundImage:[UIImage imageNamed:@"back_up.png"] forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn addTarget:self action:@selector(backUp) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
