//
//  UIWebView+Noti.h
//  BookWorksApp
//
//  Created by 刁志远 on 15/8/14.
//
//


#define kWebViewDidStartLoadNotification @"webview_did_start_load"
#define kWebViewDidFinishLoadNotification @"webview_did_finish_load"
#define kWebViewDidFailLoadNotification @"webview_did_fail_load"

#import <UIKit/UIKit.h>

@interface UIWebView_Noti : UIWebView


- (id)init;

@end
