//
//  BKChapterList.m
//  BookWorksApp
//
//  Created by 刁志远 on 16/6/23.
//
//

#import "BKChapterList.h"

@interface BKChapterList () <UITableViewDelegate, UITableViewDataSource>{
    
    NSArray *arrChap;
}

@property (nonatomic, weak) UITableView *myTable;

@end

@implementation BKChapterList


#pragma mark - SYS
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame andChapterList:(NSArray *)arr{
    self = [super initWithFrame:frame];
    if (self) {
        arrChap = arr;
        self.frame = frame;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.75f];
        [self initinalData];
    }
    return self;
}

#pragma mark - Initinal

- (void)initinalData {
    
    [self buildTableView];
    [self buildBackButton];
}

#pragma mark - UI

- (void)buildBackButton {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(7.5, 15+20, 30, 30);
    [btn setImage:[UIImage imageNamed:@"v3_reading_menu_back_normal"]
         forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    btn.clipsToBounds = YES;
    btn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7f];
    btn.layer.cornerRadius = btn.frame.size.width/2;
    [btn addTarget:self action:@selector(backClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    
    
}


- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(20, 20,
                                                                       self.frame.size.width-20,
                                                                       self.frame.size.height-20)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    self.myTable.backgroundColor = [UIColor clearColor];
    
    self.myTable.tableFooterView = [UIView new];
    
    [self addSubview:self.myTable];
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectTitle) {
        self.selectTitle(arrChap[indexPath.row],indexPath.row);
    }
    [self backClicked:nil];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrChap.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.textLabel.font = [AllMethods getFontWithSize:16];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    id obj = arrChap[indexPath.row];
    
    if ([obj isKindOfClass:[NSString class]]) {
        cell.textLabel.text = obj;
    }
    else if ([obj isKindOfClass:[NSDictionary class]]) {
        cell.textLabel.text = obj[@"title"];
    }
    
    
    
    return cell;
}


#pragma mark - Private

- (void)backClicked:(id)sender {
    
    self.alpha = 1;
    [UIView animateWithDuration:.2f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.alpha = 0;
                     }completion:^(BOOL finished){
                         self.hidden = YES;
                     }];
}



@end
