//
//  CustomPickerView.h
//  BookWorksApp
//
//  Created by 刁志远 on 12-11-20.
//
//

#import <UIKit/UIKit.h>


@protocol PickerViewDelegate;


@interface CustomPickerView : UIView<UIPickerViewDataSource,UIPickerViewDelegate>{
    id<PickerViewDelegate> pdelegate;
    
    UIPickerView *_pickerView;
    UIToolbar *_toolBar;
    
    int curSelIndex;
}

@property (nonatomic,retain) id<PickerViewDelegate> pdelegate;
@property (nonatomic,assign) int curSelIndex;

-(id)initWithDelegate:(id<PickerViewDelegate>)delegate;

-(void)showFromView:(UIView *)view;

@end


@protocol PickerViewDelegate <NSObject>

@required

- (NSInteger)customPickerView:(CustomPickerView *)picker numberOfComponentsInPickerView:(UIPickerView *)pickerView;

- (NSInteger)customPickerView:(CustomPickerView *)picker
                   pickerView:(UIPickerView *)pickerView
      numberOfRowsInComponent:(NSInteger)component;

- (void)customPickerView:(CustomPickerView *)picker didClickedButtonWithTag:(NSInteger )tag;

@optional

// returns width of column and height of row for each component.
- (CGFloat)customPickerView:(CustomPickerView *)picker
                 pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component;
- (CGFloat)customPickerView:(CustomPickerView *)picker
                 pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component;

- (NSString *)customPickerView:(CustomPickerView *)picker
                    pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;

- (UIView *)customPickerView:(CustomPickerView *)picker
                  pickerView:(UIPickerView *)pickerView
                  viewForRow:(NSInteger)row
                forComponent:(NSInteger)component
                 reusingView:(UIView *)view;

- (void)customPickerView:(CustomPickerView *)picker
              pickerView:(UIPickerView *)pickerView
            didSelectRow:(NSInteger)row inComponent:(NSInteger)component;

@end
