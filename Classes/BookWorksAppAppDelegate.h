//
//  BookWorksAppAppDelegate.h
//  BookWorksApp
//
//  Created by diaozhiyuan on 11-11-3.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#include <sys/xattr.h> 
#import <UIKit/UIKit.h>

@interface BookWorksAppAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic) BOOL canHideAd;
@property (strong, nonatomic) NSString *callid;

@property (strong, nonatomic) UITabBarController *tabCtrl;

@property (copy) void (^backgroundSessionCompletionHandler)();


- (void)changeADCenter:(CGPoint)pot;
- (void)setADHidden:(BOOL)hidden;
- (void)buildADView;

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
- (BOOL)addSkipBackupAttributeToFileAtURL:(NSURL *)URL;

- (UIView *)getADView;


+(BookWorksAppAppDelegate *)shareInstance;

@end

