//
//  BKNewUpdateBookList.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/23.
//
//

#import "BKNewUpdateBookList.h"

#import "RTLabel.h"

#import "BKNewUpdateBookDetail.h"

@interface BKNewUpdateBookList () <UITableViewDelegate, UITableViewDataSource>{
    
    NSMutableArray *arrList;
    
    NSDictionary *dicCata;
    
    NSInteger curPage;
    
}

@property (nonatomic, weak) UITableView *myTable;

@end

@implementation BKNewUpdateBookList

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    UIImageView *iv = [cell.contentView viewWithTag:1];
    
    return ORIGINAL_Y(iv)+10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BKNewUpdateBookDetail *detail = [[BKNewUpdateBookDetail alloc] initWithBookDesc:arrList[indexPath.row]];
    
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [detail setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        float width = 60;
        float height = 75;
        
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, width, height)];
        iv.tag = 1;
        
        [cell.contentView addSubview:iv];
        
        RTLabel *rt = [[RTLabel alloc] initWithFrame:CGRectMake(ORIGINAL_X(iv)+6.5, 11,
                                                                ScreenWidth-ORIGINAL_X(iv)-10, 50)];
        rt.lineSpacing = 0.01;
        rt.tag = 2;
        
        [cell.contentView addSubview:rt];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(rt.frame.origin.x,
                                                                 ORIGINAL_Y(rt),
                                                                 SIZE_W(rt), height-rt.frame.origin.x+20)];
        lab.numberOfLines = 3;
        lab.font = [UIFont systemFontOfSize:15];
        lab.textColor = RGBACOLOR(160, 160, 160, 1.0f);
        lab.tag = 3;
        [cell.contentView addSubview:lab];
        
    }
    
    
    
    if (indexPath.row >= arrList.count) {
        return cell;
    }
    
    NSDictionary *dic = arrList[indexPath.row];
    
    UIImageView *iv = [cell.contentView viewWithTag:1];
    [iv sd_setImageWithURL:[NSURL URLWithString:dic[@"img"]]
          placeholderImage:ImageNamed(@"TXT_80x107_")];
    
    NSString *html = [NSString stringWithFormat:@"<font size=16 color=#313131>%@</font><br><font size=13 color=#9b9b9b>作者  </font><font size=15 color=#313131>%@</font>",
                      dic[@"name"],dic[@"aut_name"]];
    RTLabel *rt = [cell.contentView viewWithTag:2];
    [rt setText:html];
    
    UILabel *lab = [cell.contentView viewWithTag:3];
    NSString *desc = [NSString stringWithFormat:@"%@",dic[@"desc"]];
    desc = [desc stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    lab.text = desc;
    
    return cell;
}


#pragma mark - Request Data 

- (void)requestData {
    
    NSString *surl = [NSString stringWithFormat:@"%@&action=cata&href=%@&page=%d",
                      [AllMethods updateURL],dicCata[@"href"],curPage];
    
    __weak BKNewUpdateBookList *weakSelf = self;
    [AllMethods requestWithType:REQUEST_GET
                        andPara:nil
                         andUrl:surl
                andGetDataBlock:^(id obj) {
                    
                    if ([obj isKindOfClass:[NSArray class]]) {
                        
                        NSArray *arr = obj;
                        [arrList addObjectsFromArray:arr];
                        
                        if (arr.count < 30) {
                            [self.myTable.mj_footer resetNoMoreData];
                        }
                        
                    }
                    else {
                        [AllMethods showAltMsg:[NSString stringWithFormat:@"%@",obj]];
                    }
                    
                    
                    [weakSelf.myTable.mj_header endRefreshing];
                    [weakSelf.myTable.mj_footer endRefreshing];
                    [weakSelf.myTable reloadData];
                    
                }];
    
}

#pragma mark - Refresh 

- (void)headRefresh {
    
    curPage = 0;
    if (!arrList) {
        arrList = [NSMutableArray arrayWithCapacity:3];
    }
    
    self.myTable.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footRefresh)];
    
    [self requestData];
    
}

- (void)footRefresh {
    curPage++;
    [self requestData];
}


#pragma mark - Build UI

- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:self.view.bounds
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
}



#pragma mark - SYS

- (id)initWithCataInfo:(NSDictionary *)dic {
    
    self = [super init];
    
    if (self) {
        dicCata = [NSDictionary dictionaryWithDictionary:dic];
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = dicCata[@"name"];
    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"back_up"
                                                                            andSelect:@selector(popViewControllerAnimated:) andTarget:self.navigationController];
    
    [self buildTableView];
    
    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self
                                                              refreshingAction:@selector(headRefresh)];
    
    [self.myTable.mj_header beginRefreshing];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
