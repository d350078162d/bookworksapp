//
//  BKQuestionAddAnswer.m
//  BookWorksApp
//
//  Created by 刁志远 on 2016/12/10.
//
//

#import "BKQuestionAddAnswer.h"

#import "RTLabel.h"
//#import "MJRefreshHeaderView.h"

#import "JKBEditView.h"

#import <UITableView-NXEmptyView/UITableView+NXEmptyView.h>

@interface BKQuestionAddAnswer () <UITableViewDelegate, UITableViewDataSource,JKBEditViewDelegate>{
    
    NSDictionary *dicQues;
    
    NSMutableArray *arrList;
    
//    MJRefreshHeaderView *mjHead;
    JKBEditView *editView;
    
}

@property (nonatomic, weak) UITableView *myTable;

@end

@implementation BKQuestionAddAnswer


#pragma mark - UITableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self tableView:tableView
                      cellForRowAtIndexPath:indexPath];
    
    RTLabel *rt = (RTLabel *)[cell.contentView viewWithTag:1];
    
    return ORIGINAL_Y(rt)+15;
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        RTLabel *rt = [[RTLabel alloc] initWithFrame:CGRectMake(20, 15, ScreenWidth-30, 0)];
        rt.tag = 1;
        [cell.contentView addSubview:rt];
    }
    
    
    if (arrList.count <= indexPath.row) {
        return cell;
    }
    
    RTLabel *rt = (RTLabel *)[cell.contentView viewWithTag:1];
    
    NSDictionary *dic = arrList[indexPath.row];
    
    
    NSString *html = [NSString stringWithFormat:@"<font size=16 color='#3a3a3a'>%@</font><p align=right><font size=13 color='#9a9a9a'>%@</font></p>",
                      dic[@"content"],dic[@"date"]];
    [rt setText:html];
    
    CGRect frame = rt.frame;
    frame.size.height = [rt optimumSize].height;
    rt.frame = frame;
    
    
    
    return cell;
}

#pragma mark - JKBEditViewDelegate

- (void)editView:(JKBEditView *)edit didClickedOK:(NSString *)str {
    
    [editView setActAnimate:YES];
    [editView makeEditScale:0.9f];
    [self addAnswer:str];
    
}

#pragma mark - MJRefresh

- (void)headRefresh {
    
    if (!arrList) {
        arrList = [NSMutableArray arrayWithCapacity:3];
    }
    [arrList removeAllObjects];
    
    [self getAnswerList];
    
}

//- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
//    if ([refreshView isEqual:mjHead]) {
//        if (!arrList) {
//            arrList = [NSMutableArray arrayWithCapacity:3];
//        }
//        [arrList removeAllObjects];
//        
//        [self getAnswerList];
//    }
//}


#pragma mark - Request 

- (void)addAnswer:(NSString *)ques {
    
    if (!ques || ques.length <= 0) {
        return;
    }
    
    __weak BKQuestionAddAnswer *weak = self;
    
    
    dispatch_async(dispatch_queue_create("data", nil), ^{
        
        NSString *str_url = [NSString stringWithFormat:@"%@/%@?method=113&content=%@&qid=%@",
                             kMAIN_URL,kBOOK_URL,ques,dicQues[@"id"]];
        str_url = [str_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_url]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (data) {
                
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:nil];
                
                if ([dic[@"result"] isEqualToString:@"true"]) {
                    [weak.myTable.mj_header beginRefreshing];
                    [editView setActAnimate:NO];
                    [editView dismissEdit];
                }
                
            }
            else {
                [weak reActEdit];
            }
            
        });
    });
    
}

- (void)getAnswerList {
    
    __weak BKQuestionAddAnswer *weak = self;
    
    dispatch_async(dispatch_queue_create("data", nil), ^{
        
        NSString *str_url = [NSString stringWithFormat:@"%@/%@?method=112&qid=%@",
                             kMAIN_URL,kBOOK_URL,dicQues[@"id"]];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_url]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (data) {
                
                NSArray *arr = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingAllowFragments
                                                                 error:nil];
                if([arr isKindOfClass:[NSArray class]])
                    [arrList addObjectsFromArray:arr];
            }
            
            [weak.myTable.mj_header endRefreshing];
            
            [weak.myTable reloadData];
            [weak buildTableEmptyView];
            
        });
    });
    
}

#pragma mark - Private

- (void)reActEdit {
    
    [editView setActAnimate:NO];
    [editView makeEditScale:1.0f];
    [WSProgressHUD showErrorWithStatus:@"提交失败，请重试"];
    
}

- (void)addAnswerClicked:(id)sender {
    
    if (!editView) {
        editView = [[JKBEditView alloc] initWithTitle:@"添加回复"
                                              andDesc:@""
                                         withEditType:EDIT_REPLY];
        editView.delegate = self;
    }
    
    [editView showEdit];
    editView.txtDesc.placeholder = @"输入回复";
    [editView.txtDesc becomeFirstResponder];
}

#pragma mark - Build UI

- (void)buildHeadView {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 0)];
    view.backgroundColor = [UIColor whiteColor];
    
    RTLabel *rt = [[RTLabel alloc] initWithFrame:CGRectMake(15, 15, view.frame.size.width-25, 0)];
    [view addSubview:rt];
    
    NSString *html = [NSString stringWithFormat:@"<font size=17 color='#000000'><b>%@</b></font><p align=right><font size=15 color='#9a9a9a'>%@</font></p>",
                      dicQues[@"content"],dicQues[@"date"]];
    [rt setText:html];
    
    CGRect frame = rt.frame;
    frame.size.height = [rt optimumSize].height;
    rt.frame = frame;
    
    
    UILabel *lab_line = [[UILabel alloc] initWithFrame:CGRectMake(0, ORIGINAL_Y(rt)+15, ScreenWidth, 5)];
    lab_line.backgroundColor = RGBACOLOR(239, 239, 239, 1.0f);
    [view addSubview:lab_line];
    
    frame = view.frame;
    frame.size.height = ORIGINAL_Y(lab_line);
    view.frame = frame;
    
    self.myTable.tableHeaderView = view;
}



- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth,
                                                                       ScreenHeight-64-44)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
}

- (void)buildReplyButton {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-44, ScreenWidth, 44)];
    view.backgroundColor = RGBACOLOR(239, 239, 239, 1.0f);
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, ScreenWidth*0.8, 30);
    btn.backgroundColor = [UIColor whiteColor];
    btn.layer.cornerRadius = 3.6f;
    btn.clipsToBounds = YES;
    [btn setTitle:@"回复" forState:UIControlStateNormal];
    btn.titleLabel.font = [AllMethods getFontWithSize:15];
    [btn setTitleColor:RGBACOLOR(180, 180, 180, 1.0f) forState:UIControlStateNormal];
    btn.center = CGPointMake(view.frame.size.width/2, view.frame.size.height/2);
    [btn addTarget:self action:@selector(addAnswerClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    
    [self.view addSubview:view];
    
}

- (void)buildTableEmptyView {
    
    if (!self.myTable.nxEV_emptyView) {
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight/2)];
        lab.text = @"暂无回复";
        lab.textColor = RGBACOLOR(219, 219, 219, 1.0f);
        lab.textAlignment = NSTextAlignmentCenter;
        lab.font = [AllMethods getFontWithSize:19];
        self.myTable.nxEV_emptyView = lab;
    }
    
}


#pragma mark - SYS

- (id)initWithQuestion:(NSDictionary *)dic {
    
    self =  [super init];
    if (self) {
        
        dicQues = [NSDictionary dictionaryWithDictionary:dic];
    }
    
    return self;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del setADHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 32, 32);
    [btn setBackgroundImage:[UIImage imageNamed:@"back_up"] forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.title = @"详情";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self buildTableView];
    [self buildHeadView];
    [self buildReplyButton];
    
//    mjHead = [[MJRefreshHeaderView alloc] initWithScrollView:self.myTable];
//    mjHead.delegate = self;
//    [mjHead beginRefreshing];
    
    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefresh)];
    [self.myTable.mj_header beginRefreshing];
    self.myTable.tableFooterView = [UIView new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
