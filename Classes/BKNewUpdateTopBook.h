//
//  BKNewUpdateTopBook.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/24.
//
//

#import <UIKit/UIKit.h>

@interface BKNewUpdateTopBook : UIViewController

- (id)initWithBookList:(NSArray *)arr;

@end
