//
//  BKMainBookCell.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/18.
//
//

#import "BKMainBookCell.h"

#import "LayoutViewProperties.h"

#import "OpenBookView.h"

@implementation BKMainBookCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initinalContent];
    }
    return self;
}


#pragma mark - Custom

- (void)longClickCell:(UILongPressGestureRecognizer *)ges {
    
    
//    NSLog(@"--->>>>> long click %ld",(long)ges.state);
    
    if (ges.state == UIGestureRecognizerStateBegan) {
        if (self.CellLongClickEvent) {
            self.CellLongClickEvent((UICollectionViewCell *)ges.view);
        }
    }
    
}

- (void)buildShadow {
    
    _bkView.layer.shadowColor = RGBACOLOR(217, 217, 217, 1).CGColor;//shadowColor阴影颜色
    _bkView.layer.shadowOffset = CGSizeMake(0,0);//shadowOffset阴影偏移,x向右偏移4，y向下偏移4，默认(0, -3),这个跟shadowRadius配合使用
    _bkView.layer.shadowOpacity = 0.9;//阴影透明度，默认0
    _bkView.layer.shadowRadius = 8;//阴影半径，默认3
    
}

- (void)initinalContent {
    
    float pro_hei = 15;
    
    float xishu = CELL_ITEM_SIZE_HEIGHT/(107+pro_hei);
    float cell_hei = (CELL_ITEM_SIZE_HEIGHT/xishu-pro_hei)*xishu;
    
    OpenBookView *bk = [[OpenBookView alloc] initWithFrame:CGRectMake(0, 0,
                                                              CELL_ITEM_SIZE_WIDTH,
                                                              cell_hei)];
    self.bkView = bk;
    [self addSubview:bk];
    
    [self addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                           action:@selector(longClickCell:)]];
    
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                    CELL_ITEM_SIZE_WIDTH,
                                                                    cell_hei)];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    [self.bkView addSubview:iv];
    self.imageView = iv;
    self.imageView.clipsToBounds = YES;
    self.imageView.layer.borderWidth = .5f;
    self.imageView.layer.borderColor = RGBACOLOR(232, 232, 232, 1.0).CGColor;
    
    
    UIImageView *iv_mark = [[UIImageView alloc] initWithImage:ImageNamed(@"shelf_recomend_37x37_")];
    [self.imageView addSubview:iv_mark];
    self.imageMark = iv_mark;
    self.imageMark.layer.anchorPoint = CGPointMake(1, 0);
    self.imageMark.center = CGPointMake(SIZE_W(iv_mark), 0);
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, ORIGINAL_Y(self.imageView)+2,
                                                            SIZE_W(self.imageView), 45)];
    view.backgroundColor = [UIColor clearColor];
    [self.bkView addSubview:view];
    self.viewTitle = view;
    
    UILabel *lab_pro = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SIZE_W(view), 20)];
    lab_pro.textAlignment = NSTextAlignmentLeft;
    lab_pro.textColor = RGBACOLOR(100, 100, 100, 1.0);
    lab_pro.font = [UIFont systemFontOfSize:15];
    lab_pro.numberOfLines = 1;
    lab_pro.adjustsFontSizeToFitWidth = YES;
    lab_pro.minimumScaleFactor = 13.0/16.0;
    
    self.titleLabel = lab_pro;
    [self.viewTitle addSubview:lab_pro];
    
    UILabel *lab_aut = [[UILabel alloc] initWithFrame:CGRectMake(0, ORIGINAL_Y(lab_pro)-2.5,
                                                                 SIZE_W(lab_pro),
                                                                 SIZE_H(view)-SIZE_H(lab_pro))];
    lab_aut.textAlignment = NSTextAlignmentLeft;
    lab_aut.textColor = RGBACOLOR(150, 150, 150, 1.0);
    lab_aut.font = [UIFont systemFontOfSize:13];
    lab_aut.numberOfLines = 1;
    self.labAuthor = lab_aut;
    [self.viewTitle addSubview:lab_aut];
    
    [self buildShadow];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(SIZE_W(bk)-22, 2, 20, 20);
    btn.backgroundColor = [UIColor redColor];
    btn.layer.cornerRadius = SIZE_W(btn)/2;
    btn.clipsToBounds = YES;
    btn.enabled = NO;
    [btn setImage:ImageNamed(@"del")
         forState:UIControlStateNormal];
    btn.tag = 1234;
    [self.bkView addSubview:btn];

}

#pragma mark - Public

- (void)setDeleteHidden:(BOOL)bol {
    
    [self.bkView viewWithTag:1234].hidden = bol;
    
}

@end
