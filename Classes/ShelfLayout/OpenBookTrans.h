//
//  OpenBookTrans.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/18.
//
//

#import <Foundation/Foundation.h>

@interface OpenBookTrans : NSObject

- (CGPoint)getOrinPot;
- (UIView *)getBkSuperView;
- (CGRect)getBkOriFrame;

@end
