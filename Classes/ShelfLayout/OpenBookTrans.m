//
//  OpenBookTrans.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/18.
//
//

#import "OpenBookTrans.h"

#import "BKNewMainBookList.h"
#import "BKContent.h"

@interface OpenBookTrans () <UIViewControllerAnimatedTransitioning> {
    
    CGPoint _bookViewOrignCenter;
    
    UIView *bkSuperView;
    CGRect bkOriFrame;
}

@end

@implementation OpenBookTrans

- (CGPoint)getOrinPot {
    
    return _bookViewOrignCenter;
    
}

- (UIView *)getBkSuperView {
    return bkSuperView;
}

- (CGRect)getBkOriFrame {
    
    return bkOriFrame;
}

// 返回动画的时间
- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.8;
}
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    BKNewMainBookList * fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    if ([fromVC isKindOfClass:[UITabBarController class]]) {
        
        UITabBarController *tc = (UITabBarController *)fromVC;
        UINavigationController *nav = [[tc viewControllers] objectAtIndex:0];
        fromVC = [[nav viewControllers] objectAtIndex:0];
        
    }
    
    BKContent * toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    if ([toVC isKindOfClass:[UINavigationController class]]) {

        UINavigationController *nav = (UINavigationController *)toVC;
        toVC = [[nav viewControllers] objectAtIndex:0];
    }
    
    UIView * container = [transitionContext containerView];
    
    bkSuperView = [fromVC currentOpenBookView].superview;
    _bookViewOrignCenter = [fromVC currentOpenBookView].center;
    
    
    CGPoint pot = [container convertPoint:[fromVC currentOpenBookView].center
                                 fromView:[[fromVC currentOpenBookView] superview]];
    
    [container addSubview:toVC.view];
    [container bringSubviewToFront:fromVC.view];
    [container addSubview:[fromVC currentOpenBookView]];
    [fromVC currentOpenBookView].center = pot;
    bkOriFrame = [fromVC currentOpenBookView].frame;

    CGFloat scaleX = ScreenWidth / [fromVC currentOpenBookView].bounds.size.width;
    CGFloat scaleY = ScreenHeight / [fromVC currentOpenBookView].bounds.size.height;
    
    toVC.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    toVC.view.transform = CGAffineTransformMakeScale(1.0/scaleX, 1.0/scaleY);
    toVC.view.center = [fromVC currentOpenBookView].center;
    
    CATransform3D transformblank = CATransform3DMakeRotation(-M_PI_2 , 0.0, 1.0, 0.0);
    transformblank.m34 = 1.0f / 250.0f;
    
    [fromVC currentOpenBookView].layer.anchorPoint = CGPointMake(0, 0.5);
    
    pot = CGPointMake(pot.x-[fromVC currentOpenBookView].frame.size.width/2, pot.y);
    [fromVC currentOpenBookView].center = pot;
    [fromVC currentOpenBookView].opaque = YES;
    
    
    CGFloat duringTime = .5;
    
    [UIView animateWithDuration:0.3f
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionShowHideTransitionViews
                     animations:^{
                         
                         [fromVC currentOpenBookView].layer.transform = CATransform3DMakeScale(scaleX*0.6, scaleY*0.6, 1);
                     } completion:^(BOOL finished) {
                         
                     }];
    
    
    [UIView animateWithDuration:duringTime delay:0.12f options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionShowHideTransitionViews animations:^{
        
//        [fromVC currentOpenBookView].transform = CGAffineTransformMakeScale(scaleX,scaleY);
//        [fromVC currentOpenBookView].transform = CGAffineTransformScale(transformblank, scaleX, scaleY);
        
        [fromVC currentOpenBookView].center = CGPointMake(0, ScreenHeight/2);
        [fromVC currentOpenBookView].layer.transform = CATransform3DScale(transformblank, scaleX, scaleY, 1);//transformblank;
        
        toVC.view.transform = CGAffineTransformMakeScale(1,1);
        toVC.view.center = CGPointMake(ScreenWidth/2, ScreenHeight/2);
        
    } completion:^(BOOL finished) {
        if (finished) {
            if (finished) {
                [transitionContext completeTransition:YES];
                
                [fromVC currentOpenBookView].layer.anchorPoint = CGPointMake(0.5, 0.5);
            }
            
        }
        
    } ];
     
    
}

@end
