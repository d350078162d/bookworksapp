//
//  CloseBookTrans.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/19.
//
//

#import <Foundation/Foundation.h>

@interface CloseBookTrans : NSObject

- (void)setBookOriPoint:(CGPoint )pot andOriFrame:(CGRect)frame andSuperView:(UIView *)sView;

@end
