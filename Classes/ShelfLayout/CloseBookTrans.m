//
//  CloseBookTrans.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/19.
//
//

#import "CloseBookTrans.h"

#import "BKNewMainBookList.h"
#import "BKContent.h"

@interface CloseBookTrans () <UIViewControllerAnimatedTransitioning> {
    
    CGPoint _bookViewOrignCenter;
    
    UIView *bkSuperView;
    CGRect bkFrame;
}

@end

@implementation CloseBookTrans

- (void)setBookOriPoint:(CGPoint )pot andOriFrame:(CGRect)frame andSuperView:(UIView *)sView {
    
    _bookViewOrignCenter = pot;
    
    bkSuperView = sView;
    bkFrame = frame;
}

// 返回动画的时间
- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.8;
}
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    BKContent * fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    if ([fromVC isKindOfClass:[UINavigationController class]]) {
        
        UINavigationController *nav = (UINavigationController *)fromVC;
        fromVC = [[nav viewControllers] objectAtIndex:0];
    }
    
    BKNewMainBookList * toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    if ([toVC isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tc = (UITabBarController *)toVC;
        UINavigationController *nav = [[tc viewControllers] objectAtIndex:0];
        toVC = [[nav viewControllers] objectAtIndex:0];
    }
    
    UITabBarController * tabVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *to_sup = tabVC.view.superview;
    
    UIView * container = [transitionContext containerView];
    [container addSubview:tabVC.view];
    [container bringSubviewToFront:fromVC.view];
    [container bringSubviewToFront:[toVC currentOpenBookView]];
    
    CGFloat duringTime = .4;
    
    CGFloat scaleX = ScreenWidth / bkFrame.size.width;
    CGFloat scaleY = ScreenHeight / bkFrame.size.height;
    
    
    [toVC currentOpenBookView].layer.anchorPoint = CGPointMake(0, 0.5);
//    pot = CGPointMake(pot.x-[fromVC currentOpenBookView].frame.size.width/2, pot.y);
//    [toVC currentOpenBookView].center = pot;
    [toVC currentOpenBookView].opaque = YES;
    
//    toVC.view.transform = CGAffineTransformMakeScale(scaleX, scaleY);
    
//    toVC.view.layer.anchorPoint = CGPointMake(0, 0.5);
    
//    __weak CloseBookTrans *weak = self;
    
    __weak id <UIViewControllerContextTransitioning> trans = transitionContext;
    
    [UIView animateWithDuration:duringTime delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionShowHideTransitionViews animations:^{
        
        //        [fromVC currentOpenBookView].transform = CGAffineTransformMakeScale(scaleX,scaleY);
        //        [fromVC currentOpenBookView].transform = CGAffineTransformScale(transformblank, scaleX, scaleY);
        
        [toVC currentOpenBookView].center = CGPointMake(CGRectGetMidX(bkFrame)-bkFrame.size.width/2,
                                                        CGRectGetMidY(bkFrame));
        [toVC currentOpenBookView].layer.transform = CATransform3DIdentity;//transformblank;
        
//        toVC.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        
        fromVC.view.transform = CGAffineTransformMakeScale(1.0/scaleX,1.0/scaleY);
        fromVC.view.center = CGPointMake(CGRectGetMidX(bkFrame),
                                         CGRectGetMidY(bkFrame));
        
        
    } completion:^(BOOL finished) {
        if (finished) {
            if (finished) {
//                [weak transComplete:transitionContext];
                
                [trans completeTransition:YES];
                [toVC currentOpenBookView].layer.anchorPoint = CGPointMake(0.5, 0.5);
                [to_sup addSubview:tabVC.view];
                [bkSuperView addSubview:[toVC currentOpenBookView]];
                [toVC currentOpenBookView].center = _bookViewOrignCenter;
                
            }
        }
        
    } ];
}


@end
