//
//  LayoutViewProperties.h
//  BookShelfDevelopment
//
//  Created by Rico on 15/3/3.
//  Copyright (c) 2015年 zhanzhenchao. All rights reserved.
//

#ifndef BookShelfDevelopment_LayoutViewProperties_h
#define BookShelfDevelopment_LayoutViewProperties_h

#pragma mark - Size
//Item Size
//#define CELL_ITEM_SIZE_WIDTH 72.5
//#define CELL_ITEM_SIZE_HEIGHT 107.5

#define CELL_ITEM_SIZE_WIDTH 80*1.25
#define CELL_ITEM_SIZE_HEIGHT (107+15)*1.25

//Section Inset
//#define SECTION_INSET_TOP 10
//#define SECTION_INSET_LEFT 33
//#define SECTION_INSET_BOTTOM 10
//#define SECTION_INSET_RIGHT 33

#define SECTION_INSET_TOP 0
#define SECTION_INSET_LEFT 10
#define SECTION_INSET_BOTTOM 10
#define SECTION_INSET_RIGHT 10

//Minimum Inter Item Spacing
//#define MINIMUM_INTER_ITEM_SPACING 45.75f

#define MINIMUM_INTER_ITEM_SPACING 0

//Minimum Line Spacing
//#define MINIMUM_LINE_SPACING 30.0f
#define MINIMUM_LINE_SPACING 7.5f

//Decoration View Content size
#define DECORATION_VIEW_SIZE_HEIGHT 20

#endif
