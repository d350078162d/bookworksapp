//
//  BKUpdateHead.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/22.
//
//

#import <UIKit/UIKit.h>

@interface BKUpdateHead : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
