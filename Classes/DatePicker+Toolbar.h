//
//  DatePicker+Toolbar.h
//  BookWorksApp
//
//  Created by 刁志远 on 13-6-3.
//
//

#import <UIKit/UIKit.h>


@protocol DatePickerDelagate;

@interface DatePicker_Toolbar : UIView{
    id<DatePickerDelagate> pdelegate;
    
    UIDatePicker *_pickerView;
    UIToolbar *_toolBar;
}

@property (nonatomic,retain) id<DatePickerDelagate> pdelegate;

-(id)initWithDelegate:(id<DatePickerDelagate>)delegate;

-(void)showFromView:(UIView *)view;

@end

@protocol DatePickerDelagate <NSObject>

@optional
- (void)datePicker:(UIDatePicker *)picker didSelectDate:(NSDate *)date;

@end
