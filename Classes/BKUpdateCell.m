//
//  BKUpdateCell.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/22.
//
//

#import "BKUpdateCell.h"

#import "RTLabel.h"

@implementation BKUpdateCell


- (void)buildShadow {
    
    self.viewContent.layer.shadowColor = RGBACOLOR(217, 217, 217, 1).CGColor;//shadowColor阴影颜色
    self.viewContent.layer.shadowOffset = CGSizeMake(0,0);//shadowOffset阴影偏移,x向右偏移4，y向下偏移4，默认(0, -3),这个跟shadowRadius配合使用
    self.viewContent.layer.shadowOpacity = 0.9;//阴影透明度，默认0
    self.viewContent.layer.shadowRadius = 8;//阴影半径，默认3
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initinalContent];
    }
    return self;
}


- (void)initinalContent {
    
    UIView *view_c = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 75*1.7)];
    self.viewContent = view_c;
    [self addSubview:view_c];
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,60*1.7,75*1.7)];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    [self.viewContent addSubview:iv];
    self.imageView = iv;
    self.imageView.clipsToBounds = YES;
    self.imageView.layer.borderWidth = .5f;
    self.imageView.layer.borderColor = RGBACOLOR(232, 232, 232, 1.0).CGColor;
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, ORIGINAL_Y(self.imageView)+2,
                                                            SIZE_W(self.imageView), 45)];
    view.backgroundColor = [UIColor clearColor];
    [self.viewContent addSubview:view];
    self.viewTitle = view;
    
    UILabel *lab_pro = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SIZE_W(view), 25)];
    lab_pro.textAlignment = NSTextAlignmentLeft;
    lab_pro.textColor = RGBACOLOR(100, 100, 100, 1.0);
    lab_pro.font = [UIFont systemFontOfSize:15];
    lab_pro.numberOfLines = 1;
    lab_pro.adjustsFontSizeToFitWidth = YES;
    lab_pro.minimumScaleFactor = 13.0/16.0;
    
    self.labTitle = lab_pro;
    [self.viewTitle addSubview:lab_pro];
    
    UILabel *lab_aut = [[UILabel alloc] initWithFrame:CGRectMake(0, ORIGINAL_Y(lab_pro),
                                                                 SIZE_W(lab_pro),
                                                                 SIZE_H(view)-SIZE_H(lab_pro))];
    lab_aut.textAlignment = NSTextAlignmentLeft;
    lab_aut.textColor = RGBACOLOR(150, 150, 150, 1.0);
    lab_aut.font = [UIFont systemFontOfSize:13];
    lab_aut.numberOfLines = 1;
    self.labAuthor = lab_aut;
    [self.viewTitle addSubview:lab_aut];
    
    RTLabel *rlb = [[RTLabel alloc] initWithFrame:CGRectMake(10, 5, SIZE_W(view_c)-15, 50)];
    self.rtUpdate = rlb;
    [self addSubview:rlb];
    
    TMMuiLazyScrollView *scr = [[TMMuiLazyScrollView alloc] initWithFrame:CGRectMake(0, 0, SIZE_W(view_c), 80)];
    self.scrCata = scr;
    [self addSubview:scr];
    
    [self buildShadow];
    
    
    UIButton *im_rec = [UIButton buttonWithType:UIButtonTypeCustom];
    im_rec.frame = CGRectMake(0, 0,
                              ScreenWidth,
                              ORIGINAL_Y(self.viewTitle)-20);
    self.imgNoRecord = im_rec;
    [self addSubview:im_rec];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(SIZE_W(self.imageView)-22, 2, 20, 20);
    btn.backgroundColor = [UIColor redColor];
    btn.layer.cornerRadius = SIZE_W(btn)/2;
    btn.clipsToBounds = YES;
    btn.enabled = NO;
    [btn setImage:ImageNamed(@"del")
         forState:UIControlStateNormal];
    btn.tag = 1234;
    btn.hidden = YES;
    [self.viewContent addSubview:btn];
    
    UIImageView *iv_mark = [[UIImageView alloc] initWithImage:ImageNamed(@"shelf_recomend_37x37_")];
    [self.imageView addSubview:iv_mark];
    self.imageMark = iv_mark;
    self.imageMark.layer.anchorPoint = CGPointMake(1, 0);
    self.imageMark.center = CGPointMake(SIZE_W(iv_mark), 0);
    self.imageMark.hidden = YES;
    
}

#pragma mark - Public

- (void)setDeleteHidden:(BOOL)bol {
    
    [self.viewContent viewWithTag:1234].hidden = bol;
    
}



@end
