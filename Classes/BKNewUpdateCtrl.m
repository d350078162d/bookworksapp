//
//  BKNewUpdateCtrl.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/22.
//
//

#import "BKNewUpdateCtrl.h"

#import "BKUpdateCell.h"
#import "Layout.h"

#import "BKUpdateHead.h"
#import "BKUpdateFoot.h"

#import "BKNewUpdateBookList.h"
#import "BKNewUpdateBookDetail.h"
#import "BKNewUpdateSearchList.h"
#import "BKNewUpdateTopBook.h"
#import "BKNewUpdateLastest.h"
#import "BKNewUpdateBookBacklist.h"
#import "BKNewUpdateAllRecord.h"

#import "SGActionSheet.h"

@interface BKNewUpdateCtrl () <UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,TMMuiLazyScrollViewDelegate,TMMuiLazyScrollViewDataSource,UISearchBarDelegate,SGActionSheetDelegate> {
    
    NSMutableArray *arrHistory;
    
    NSMutableDictionary *dicData;
    
    NSInteger delIndex;
}

@property (weak, nonatomic) UICollectionView *myCollect;

@end

@implementation BKNewUpdateCtrl

#pragma mark - SGActionSheetDelegate

- (void)SGActionSheetDidDismiss {
    
    delIndex = -1;
    
}

- (void)SGActionSheet:(SGActionSheet *)actionSheet didSelectRowAtIndexPath:(NSInteger)indexPath {
    
    
    NSDictionary *dic = arrHistory[delIndex];
    
    if ([AllMethods delBookInNewUpdateTable:dic[@"id"]]) {
        [arrHistory removeObject:dic];
        [self.myCollect reloadData];
        
    }
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    UIButton *cancleBtn = [searchBar valueForKey:@"cancelButton"];
    [cancleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    
    if (!searchBar.text || searchBar.text.length<=0 ){
        return;
    }
    [self toSearchViewWithKey:searchBar.text];
}

#pragma mark - TMMuiLazyScrollViewDelegate

- (void)muiViewClicked:(UITapGestureRecognizer *)ges {
    
    UIView *view = ges.view;
    
    NSInteger index = [view.muiID integerValue];
    NSArray *arr = dicData[@"cata"];
    
    if (arr.count <= index) {
        return;
    }
    
    NSDictionary *dic = arr[index];
    
    BKNewUpdateBookList *list = [[BKNewUpdateBookList alloc] initWithCataInfo:dic];
    
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [list setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:list animated:YES];
    [self.tabBarController setHidesBottomBarWhenPushed:NO];
    
}

#pragma mark - TMMuiLazyScrollViewDataSource

- (NSUInteger)numberOfItemInScrollView:(nonnull TMMuiLazyScrollView *)scrollView {
    
    if (dicData && dicData[@"cata"]) {
        return [dicData[@"cata"] count];
    }
    
    return 0;
}

- (nonnull TMMuiRectModel *)scrollView:(nonnull TMMuiLazyScrollView *)scrollView
                      rectModelAtIndex:(NSUInteger)index {
    
    float width = 130;
    CGRect rect = CGRectMake(15*(index+1)+index*width, 10, width, 64);
    TMMuiRectModel *rectModel = [[TMMuiRectModel alloc]init];
    rectModel.absoluteRect = rect;
    rectModel.muiID = [NSString stringWithFormat:@"%td",index];
    return rectModel;
    
}
- (nullable UIView *)scrollView:(nonnull TMMuiLazyScrollView *)scrollView itemByMuiID:(nonnull NSString *)muiID {
    
    UIView *view = [scrollView dequeueReusableItemWithIdentifier:@"cata_view"];
    NSInteger index = [muiID integerValue];
    if (!view)
    {
        TMMuiRectModel *model = [self scrollView:scrollView rectModelAtIndex:index];
        
        CGRect rect = model.absoluteRect;//CGRectMake(15*(index+1), 7.5, 85, 55);
        view = [[UIView alloc]initWithFrame:rect];
        
//        view.backgroundColor = NAV_COLOR;
        view.layer.cornerRadius = 6.6f;
        view.clipsToBounds = YES;
        view.reuseIdentifier = @"cata_view";
        
        UIImageView *iv = [[UIImageView alloc] initWithFrame:view.bounds];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        iv.tag = 1000;
        [view addSubview:iv];
        
        UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
        label.textAlignment = NSTextAlignmentCenter;
        label.tag = 1234;
        label.font = [UIFont systemFontOfSize:16];
        label.textColor = [UIColor whiteColor];
        [view addSubview:label];
    }
    
    
    NSArray *arr = dicData[@"cata"];
    
    NSDictionary *dic = arr[[muiID intValue]];
    
    UILabel *lab = (UILabel *)[view viewWithTag:1234];
    lab.text = dic[@"name"];
    
    NSString *sn = [NSString stringWithFormat:@"type%td",(index%5)+1];
    UIImageView *iv = (UIImageView *)[view viewWithTag:1000];
    iv.image = ImageNamed(sn);
    
    [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                       action:@selector(muiViewClicked:)]];
    
    
    [scrollView addSubview:view];
    
    return view;
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    switch (indexPath.section) {
        case 0:{//追书记录
            
            NSInteger cnt = arrHistory.count;
            cnt--;
            NSDictionary *dic = arrHistory[cnt-indexPath.item];
            
            NSMutableDictionary *dic_detail = [NSMutableDictionary dictionaryWithDictionary:dic];
            dic_detail[@"href"] = dic[@"detail_href"];
            
            BKNewUpdateBookBacklist *backlist = [[BKNewUpdateBookBacklist alloc] initWithBookInfo:dic_detail];
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [backlist setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:backlist animated:YES];
            
            break;
        }
        case 1:{//推荐书籍
            NSArray *arr = dicData[@"books"];
            
            if (!arr || arr.count <= 0) {
                return;
            }
            
            NSDictionary *dic = arr[0];
            arr = dic[@"book"];
            
            if (!arr || arr.count <= indexPath.item) {
                return;
            }
            
            dic = arr[indexPath.item];
            
            BKNewUpdateBookDetail *detail = [[BKNewUpdateBookDetail alloc] initWithBookDesc:dic];
            
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [detail setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:detail animated:YES];
            [self.tabBarController setHidesBottomBarWhenPushed:NO];
            break;
        }
        case 3:{//最近更新
            
            NSArray *arr = dicData[@"update"];
            NSDictionary *dic = arr[indexPath.item];
            BKNewUpdateBookDetail *detail = [[BKNewUpdateBookDetail alloc] initWithBookDesc:dic];
            
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [detail setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:detail animated:YES];
            [self.tabBarController setHidesBottomBarWhenPushed:NO];
            
            break;
        }
        default:
            break;
    }
    

    
    
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BKUpdateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collect_cell"
                                                                     forIndexPath:indexPath];
    
    
    NSArray *arr_ges = [cell.viewContent gestureRecognizers];
    if (arr_ges && arr_ges.count > 0) {
        for (UIGestureRecognizer *reg in arr_ges) {
            [cell.viewContent removeGestureRecognizer:reg];
        }
    }
    switch (indexPath.section) {
        case 0:{
            cell.scrCata.hidden = YES;
            cell.rtUpdate.hidden = YES;
            cell.viewContent.hidden = NO;
            
            if (arrHistory && arrHistory.count > 0) {
                cell.imgNoRecord.hidden = YES;
                NSInteger cnt = arrHistory.count;
                cnt--;
                NSDictionary *dic = arrHistory[cnt-indexPath.item];
                
                [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"img"]]
                                  placeholderImage:ImageNamed(@"TXT_80x107_")];
                cell.labTitle.text = dic[@"name"];
                cell.labAuthor.text = dic[@"aut_name"];
                
                cell.viewContent.tag = cnt-indexPath.item;
                [cell.viewContent addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(recordBookLongPress:)]];
            }
            else {
                cell.viewContent.hidden = YES;
                cell.imgNoRecord.hidden = NO;
                [cell.imgNoRecord setImage:ImageNamed(@"no_rec") forState:UIControlStateNormal];
            }
            
            break;
        }
        case 1:{
            cell.scrCata.hidden = YES;
            cell.rtUpdate.hidden = YES;
            cell.viewContent.hidden = NO;
            cell.imgNoRecord.hidden = YES;
            
            NSArray *arr = dicData[@"books"];
            NSDictionary *dic = arr[0];
            arr = dic[@"book"];
            dic = arr[indexPath.item];
            
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dic[@"img"]]
                              placeholderImage:ImageNamed(@"TXT_80x107_")];
            cell.labTitle.text = dic[@"name"];
            cell.labAuthor.text = dic[@"aut_name"];
            
            
            break;
        }
        case 2:{//分类
            cell.scrCata.hidden = NO;
            cell.rtUpdate.hidden = YES;
            cell.viewContent.hidden = YES;
            cell.imgNoRecord.hidden = YES;
            
//            cell.scrCata.delegate = self;
            cell.scrCata.dataSource = self;
            
            float width = 0;
            if (dicData && dicData[@"cata"]) {
                
                TMMuiRectModel *model = [self scrollView:cell.scrCata rectModelAtIndex:0];
                CGRect frame = model.absoluteRect;
                
                width = [dicData[@"cata"] count]*(frame.origin.x+frame.size.width)+15;
            }
            
            cell.scrCata.contentSize = CGSizeMake(width, 80);
            [cell.scrCata reloadData];
            
            break;
        }
        case 3:{//最近更新
            cell.scrCata.hidden = YES;
            cell.rtUpdate.hidden = NO;
            cell.viewContent.hidden = YES;
            cell.imgNoRecord.hidden = YES;
            
            NSArray *arr = dicData[@"update"];
            NSDictionary *dic = arr[indexPath.item];
            
//            NSString *html = [NSString stringWithFormat:@"<font size=13 color=#208DDB><a href=type%td>%@</a></font><font size=13 color=#9b9b9b> 更新 </font><font size=15 color=#208DDB><a href=book%td>%@</a></font><br><font size=13 color=#9b9b9b> 最新 </font><font size=15 color=#208DDB><a href=chapter%td>%@</a></font>",
//                              indexPath.item,dic[@"cata_name"],
//                              indexPath.item,dic[@"name"],
//                              indexPath.item,dic[@"chap_name"]];
            
            NSString *html = [NSString stringWithFormat:@"<font size=13 color=#208DDB>%@</font><font size=13 color=#9b9b9b> 更新 </font><font size=15 color=#208DDB>%@</font><br><font size=13 color=#9b9b9b>最新  </font><font size=15 color=#208DDB>%@</font>",
                              dic[@"cata_name"],
                              dic[@"name"],
                              dic[@"chap_name"]];
            [cell.rtUpdate setText:html];
            
            break;
        }
        default:
            break;
    }
    
    return cell;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    switch (section) {
        case 0:{//历史记录
            
            NSInteger cnt = 1;
            
            NSInteger max = 3;
            if (isPad) {
                max = 6;
            }
            
            if(arrHistory && arrHistory.count>0) {
                
                
                cnt = arrHistory.count;
                if (cnt > max) {
                    cnt = max;
                }
            }
            return cnt;
            break;
        }
        case 1:{//热门推荐
            
            NSArray *arr = dicData[@"books"];
            NSDictionary *dic = arr[0];
            
            NSUInteger cnt = [dic[@"book"] count];
            if (cnt>6) {
                cnt = 6;
            }
            
            return 6;
            
            break;
        }
          
        case 2: {//展示分类
            return 1;
            break;
        }
            
        case 3:{//展示最近更新
            NSArray *arr = dicData[@"update"];
            
            return arr.count;
            break;
        }
        default:
            break;
    }
    
    return 0;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *view = nil;
    
    
    if (kind == UICollectionElementKindSectionHeader) {
        view = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                  withReuseIdentifier:@"update_head"
                                                         forIndexPath:indexPath];
        

        CGSize size = [self collectionView:collectionView layout:collectionView.collectionViewLayout referenceSizeForHeaderInSection:indexPath.section];
        BKUpdateHead *head = (BKUpdateHead *)view;
        CGRect frame = head.titleLabel.frame;
        
        
        switch (indexPath.section) {
            case 0:{
                head.titleLabel.text = @"追书记录";
                frame.size = size;
                head.titleLabel.frame = frame;
                
                break;
            }
            case 1:{
                head.titleLabel.text = @"热门推荐";
                frame.size = size;
                head.titleLabel.frame = frame;
                
                break;
            }
            case 2:{
                head.titleLabel.text = @"";
                head.frame = CGRectZero;
                break;
            }
            case 3:{
                head.titleLabel.text = @"最近更新";
                frame.size = size;
                head.titleLabel.frame = frame;
                break;
            }
            default:
                break;
        }
    }
    else if (kind == UICollectionElementKindSectionFooter) {
        
        view = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                  withReuseIdentifier:@"update_foot"
                                                         forIndexPath:indexPath];
        BKUpdateFoot *foot = (BKUpdateFoot *)view;
        
        CGSize size = [self collectionView:collectionView
                                    layout:collectionView.collectionViewLayout
           referenceSizeForFooterInSection:indexPath.section];
        
        CGRect frame = foot.frame;
        frame.size = size;
        foot.frame = frame;
        
        frame = foot.btnTitle.frame;
        frame.size.height = size.height;
        foot.btnTitle.frame = frame;
        
        frame = foot.labLine.frame;
        frame.origin.y = size.height-0.65;
        foot.labLine.frame = frame;
        
        [foot.btnTitle addTarget:self action:@selector(footViewButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        foot.btnTitle.tag = indexPath.section;
        
        
        switch (indexPath.section) {
            case 0:{
                
                NSInteger max = 3;
                if (isPad) {
                    max = 6;
                }
                
                if (arrHistory && arrHistory.count>max) {
                    [foot.btnTitle setTitle:@"查看全部记录" forState:UIControlStateNormal];
                    [foot.btnTitle setTitleColor:NAV_COLOR forState:UIControlStateNormal];
                }
                else {
                    
                    if (arrHistory && arrHistory.count > 0) {
                        [foot.btnTitle setTitle:@"已显示全部记录" forState:UIControlStateNormal];
                        [foot.btnTitle setTitleColor:RGBACOLOR(180, 180, 180, 1.0f)
                                            forState:UIControlStateNormal];
                    }
                    else {
                        [foot.btnTitle setTitle:@"" forState:UIControlStateNormal];
                    }
                    
                }
                
                break;
            }
            case 1:{
                
                NSArray *arr = dicData[@"books"];
                NSUInteger cnt=0;
                
                for (NSDictionary *dic in arr) {
                    cnt += [dic[@"book"] count];
                }
                
                
                [foot.btnTitle setTitleColor:NAV_COLOR forState:UIControlStateNormal];
                [foot.btnTitle setTitle:[NSString stringWithFormat:@"查看全部%lu本书",(unsigned long)cnt]
                               forState:UIControlStateNormal];
                
                break;
            }
            case 2:{
                [foot.btnTitle setTitle:@"" forState:UIControlStateNormal];
                break;
            }
            case 3:{
                [foot.btnTitle setTitleColor:NAV_COLOR forState:UIControlStateNormal];
                [foot.btnTitle setTitle:@"查看全部更新" forState:UIControlStateNormal];
                break;
            }
            default:
                break;
        }
        
    }
    
    
    return view;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.section == 0) {
        
        float height = 75*1.7+50;
        if (arrHistory && arrHistory.count > 0) {
            float width = 60*1.7;
            
            return CGSizeMake(width, height);
        }
        
        return CGSizeMake(ScreenWidth, height);
    }
    else if (indexPath.section == 1) {
        
        float width = 60*1.7;
        float height = 75*1.7+50;
        return CGSizeMake(width, height);
    }
    else if (indexPath.section == 2){//分类
        
        return CGSizeMake(ScreenWidth, 85);
        
    }
    else if (indexPath.section == 3) {//最近更新
        
        return CGSizeMake(ScreenWidth, 60);
    }
    
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    int num_per_row = 3;
    float width = 60*1.7;

    if (isPad) {
        num_per_row = 5;
    }
    
    if (isPad) {
        return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+2),
                                0, (ScreenWidth-num_per_row*width)/(num_per_row+2));
    }
    
    return UIEdgeInsetsMake(0, (ScreenWidth-num_per_row*width)/(num_per_row+1),
                            0, (ScreenWidth-num_per_row*width)/(num_per_row+1));
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    
    switch (section) {
        case 0://记录
//            return CGSizeMake(ScreenWidth, 70);
//            break;
        case 1:
        case 3:{
            return CGSizeMake(ScreenWidth, 50);
            break;
        }
        
        case 2:{//分类
            return CGSizeZero;
            break;
        }
        
        default:
            break;
    }
    
    return CGSizeZero;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    switch (section) {
        case 0:{//记录
            
            if (arrHistory && arrHistory.count > 0) {
                return CGSizeMake(ScreenWidth, 45);
            }
            return CGSizeMake(ScreenWidth, 1);
            
            break;
        }
        case 3://最近更新
        case 1:{//t推荐
            return CGSizeMake(ScreenWidth, 45);
            break;
        }
            
        
        case 2:{//分类
            return CGSizeMake(ScreenWidth, 1);
            break;
        }
            
        default:
            break;
    }
    
    return CGSizeZero;
    
}

#pragma mark - Request Data 

- (void)requestData {
    
    NSString *surl = [NSString stringWithFormat:@"%@&action=home",[AllMethods updateURL]];
    
    __weak BKNewUpdateCtrl *weakSelf = self;
    [AllMethods requestWithType:REQUEST_GET
                        andPara:nil
                         andUrl:surl andGetDataBlock:^(id object) {
                             
                             NSDictionary *dic = object;
                             
                             if (![dic isKindOfClass:[NSDictionary class]]) {
                                 
                                 [AllMethods showAltMsg:[NSString stringWithFormat:@"%@",dic]];
                                 [weakSelf.myCollect.mj_header endRefreshing];
                                 return ;
                             }
                             
                             dicData = [NSMutableDictionary dictionaryWithDictionary:dic];
                             
                             [weakSelf.myCollect reloadData];
                             
                             [weakSelf.myCollect.mj_header endRefreshing];
                         }];
}

#pragma mark - Private

- (void)recordBookLongPress:(UILongPressGestureRecognizer *)reg {
    
    if (delIndex >= 0) {
        return;
    }
    
    delIndex = reg.view.tag;
//    NSDictionary *dic = arrHistory[index];
//    
//    NSLog(@"%@",dic);
    
    
    SGActionSheet *actSheet = [[SGActionSheet alloc] initWithTitle:@"确定要删除追书吗？"
                                                          delegate:self
                                                 cancelButtonTitle:@"取消"
                                             otherButtonTitleArray:@[@"确定删除"]
                                                          showType:ActTypeDelete];
    actSheet.cancelButtonTitleColor = [UIColor redColor];
    [actSheet show];
    
}

- (void)getHistoryBookList {
    
    NSArray *arr = [AllMethods getBookListInNewUpdateTable];
    
    
    if (!arrHistory) {
        arrHistory = [NSMutableArray arrayWithCapacity:3];
    }
    [arrHistory removeAllObjects];
    [arrHistory addObjectsFromArray:arr];
    
    [self.myCollect reloadData];
    
}

- (void)footViewButtonClicked:(UIButton *)btn {
    
//    NSLog(@"-- %td",btn.tag);
    
    switch (btn.tag) {
        case 0:{//全部追书记录
            BKNewUpdateAllRecord *all = [[BKNewUpdateAllRecord alloc] initWithBookList:arrHistory];
            
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [all setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:all animated:YES];
            [self.tabBarController setHidesBottomBarWhenPushed:NO];
            
            break;
        }
        case 1:{//推荐记录
            
            NSArray *arr = dicData[@"books"];
            if (!arr) {
                return;
            }
            BKNewUpdateTopBook *top = [[BKNewUpdateTopBook alloc] initWithBookList:arr];
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [top setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:top animated:YES];
            [self.tabBarController setHidesBottomBarWhenPushed:NO];
            
            break;
        }
        case 3:{//更新记录
            
            if (!dicData[@"update"]) {
                return;
            }
            
            NSArray *arr = @[@{@"name":@"最近更新",@"book":dicData[@"update"]},
                             @{@"name":@"点击排行",@"book":dicData[@"top"]}];
            
            BKNewUpdateLastest *last = [[BKNewUpdateLastest alloc] initWithBookList:arr];
            [self.tabBarController setHidesBottomBarWhenPushed:YES];
            [last setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:last animated:YES];
            [self.tabBarController setHidesBottomBarWhenPushed:NO];
            
            break;
        }
        default:
            break;
    }
}

- (void)toSearchViewWithKey:(NSString *)sk {
    
    BKNewUpdateSearchList *search = [[BKNewUpdateSearchList alloc] initWithSearchKey:sk];
    
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    [search setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:search animated:YES];
    [self.tabBarController setHidesBottomBarWhenPushed:NO];
}

- (UITextField *)getTextfieldInView:(UIView *)view {
    
    UITextField *searchField=nil;
    NSUInteger numViews = [view.subviews count];
    for(int i = 0; i < numViews; i++) {
        UIView *sub = [view.subviews objectAtIndex:i];
        if([sub isKindOfClass:[UITextField class]]) {
            searchField = (UITextField *)sub;//[view.subviews objectAtIndex:i];
        }
        else {
            searchField = [self getTextfieldInView:sub];
        }
    }
    
    return searchField;
}

#pragma mark - Build UI

- (void)buildTitleView {
    
    UISearchBar *search = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth*0.6, 34)];
    search.placeholder = @"搜索书籍";
    
//    search.barTintColor = NAV_COLOR;
    search.tintColor = NAV_COLOR;
    
    search.delegate = self;
    self.navigationItem.titleView = search;
    
}

- (void)buildCollectView {
    
    float width = 60*1.7;
    
    Layout *lay = [[Layout alloc] initWithItemWidth:width andHeight:75*1.7+50];
    lay.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                                   collectionViewLayout:lay];
    [collect registerClass:[BKUpdateCell class] forCellWithReuseIdentifier:@"collect_cell"];
    [collect registerClass:[BKUpdateHead class]
forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
       withReuseIdentifier:@"update_head"];
    [collect registerClass:[BKUpdateFoot class]
forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
       withReuseIdentifier:@"update_foot"];
    
    collect.delegate = self;
    collect.dataSource = self;
    collect.tag = 1;
    collect.backgroundColor = [UIColor whiteColor];
    collect.contentInset = UIEdgeInsetsMake(0, 0.0, 50.0, 0.0);

    
    self.myCollect = collect;
    [self.myCollect setAlwaysBounceVertical:YES];
    
    [self.view addSubview:self.myCollect];
}


#pragma mark - SYS

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self getHistoryBookList];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-48-49+24)];
    [del setADHidden:NO];
    del.canHideAd = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delIndex = -1;
    [self buildCollectView];
    
    [self buildTitleView];
    
    self.myCollect.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self
                                                                refreshingAction:@selector(requestData)];
    [self.myCollect.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
