//
//  BKContent.h
//  BookWorksApp
//
//  Created by 刁志远 on 15/1/10.
//
//

#import <UIKit/UIKit.h>

@class SwipeView;

@interface BKContent : UIViewController

@property (weak, nonatomic) IBOutlet SwipeView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnProgress;
@property (weak, nonatomic) IBOutlet UIButton *btnReadBk;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (strong, nonatomic) IBOutlet UIView *viewProgress;
@property (weak, nonatomic) IBOutlet UISlider *slideProgress;
@property (weak, nonatomic) IBOutlet UILabel *labPage;


- (IBAction)toolBtnClicked:(id)sender;
- (IBAction)slideValChanged:(id)sender;
- (IBAction)slidePageChanged:(id)sender;

- (id)initWithBookIndex:(int)index;
- (IBAction)itemClicked:(id)sender;

- (void)saveProgress;

@end
