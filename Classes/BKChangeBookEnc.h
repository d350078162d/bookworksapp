//
//  BKChangeBookEnc.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/1/16.
//
//

#import <UIKit/UIKit.h>

@protocol BKChangeBookEncDelegate ;

@class RJSingleBook;

@interface BKChangeBookEnc : UIViewController


@property (nonatomic) id<BKChangeBookEncDelegate> delegate;

- (id)initWithBook:(RJSingleBook *)book andDelegate:(id<BKChangeBookEncDelegate>)delegate;

@end


@protocol BKChangeBookEncDelegate <NSObject>

- (void)didChangeEncode;

@end


