//
//  BKContent.h
//  BookWorksApp
//
//  Created by 刁志远 on 15/1/10.
//
//

#import <UIKit/UIKit.h>

@class SwipeView;

@interface BKChapterContent : UIViewController

@property (weak, nonatomic) IBOutlet SwipeView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnProgress;
@property (weak, nonatomic) IBOutlet UIButton *btnReadBk;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (strong, nonatomic) IBOutlet UIView *viewProgress;
@property (weak, nonatomic) IBOutlet UISlider *slideProgress;
@property (weak, nonatomic) IBOutlet UILabel *labPage;


- (id)initWithChapterList:(NSArray *)arr andChapterIndex:(NSInteger )index andBookId:(NSString *)bid;
- (id)initWithChapterList:(NSArray *)arr andChapterIndex:(NSInteger )index andBookInfo:(NSDictionary *)dic;


- (IBAction)toolBtnClicked:(id)sender;
- (IBAction)slideValChanged:(id)sender;
- (IBAction)slidePageChanged:(id)sender;


- (IBAction)itemClicked:(id)sender;

@end
