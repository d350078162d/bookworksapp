//
//  CustomPickerView.m
//  BookWorksApp
//
//  Created by 刁志远 on 12-11-20.
//
//

#import "CustomPickerView.h"


@interface CustomPickerView (private)
-(void)hidePicker;
-(void)rowButtonClicked:(id)sender;
@end


@implementation CustomPickerView

@synthesize pdelegate,curSelIndex;



#pragma UIPickerViewDataSource  UIPickerViewDelegate--

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{

    return [self.pdelegate customPickerView:self numberOfComponentsInPickerView:pickerView];
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    return [self.pdelegate customPickerView:self pickerView:pickerView numberOfRowsInComponent:component];
}

- (void)customPickerView:(CustomPickerView *)picker didClickedButtonWithTag:(NSInteger )tag{
    [self.pdelegate customPickerView:self didClickedButtonWithTag:tag];
    [self hidePicker];
}


// returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if ([self.pdelegate respondsToSelector:@selector(customPickerView:pickerView:widthForComponent:)]) {
        return [self.pdelegate customPickerView:self pickerView:pickerView widthForComponent:component];
    }
    
    return 320;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    if ([self.pdelegate respondsToSelector:@selector(customPickerView:pickerView:rowHeightForComponent:)]) {
        return [self.pdelegate customPickerView:self pickerView:pickerView rowHeightForComponent:component];
    }
    return 44;
}

// these methods return either a plain NSString, a NSAttributedString, or a view (e.g UILabel) to display the row for the component.
// for the view versions, we cache any hidden and thus unused views and pass them back for reuse.
// If you return back a different object, the old one will be released. the view will be centered in the row rect
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (curSelIndex == row) {
        return @"√";
    }
    
    return nil;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    if ([self.pdelegate respondsToSelector:@selector(customPickerView:pickerView:viewForRow:forComponent:reusingView:)]) {
        UIView *v = [self.pdelegate customPickerView:self pickerView:pickerView viewForRow:row
                                        forComponent:component reusingView:view];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = row;
        [btn setBackgroundImage:[UIImage imageNamed:@"tit.png"] forState:UIControlStateHighlighted];
        [btn setFrame:CGRectMake(0, 0, 300, v.frame.size.height)];
        [btn addTarget:self action:@selector(rowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.titleLabel setTextAlignment:NSTextAlignmentRight];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if (curSelIndex == row) {
            [btn setTitle:@"√     " forState:UIControlStateNormal];
        }
        else{
            [btn setTitle:@"" forState:UIControlStateNormal];
        }
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [v addSubview:btn];
        [v sendSubviewToBack:btn];
        return v;
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if ([self.pdelegate respondsToSelector:@selector(customPickerView:pickerView:didSelectRow:inComponent:)]) {
        [self.pdelegate customPickerView:self pickerView:pickerView didSelectRow:row inComponent:component];
    }
}

#pragma my-

-(void)rowButtonClicked:(id)sender{
    //NSLog(@"%d",[sender tag]);
    curSelIndex = [sender tag];
    [_pickerView reloadComponent:0];
}

-(void)clickedButton:(id)sender{
    [self customPickerView:self didClickedButtonWithTag:[sender tag]];
    [self.pdelegate customPickerView:self didClickedButtonWithTag:[sender tag]];
}

-(void)showFromView:(UIView *)view{
    
    //NSLog(@"%f - %f",self.frame.size.width,self.frame.size.height);
    
    [UIView animateWithDuration:0.6f animations:^(void){
        self.frame = CGRectMake(0, view.frame.size.height-self.frame.size.height,
                                self.frame.size.width, self.frame.size.height);
    }];
}
-(void)hidePicker{
    [UIView animateWithDuration:0.6f animations:^(void){
        self.frame = CGRectMake(0, self.frame.origin.y+self.frame.size.height,
                                self.frame.size.width, self.frame.size.height);
    }completion:^(BOOL finished){
        if (finished) {
            [self removeFromSuperview];
            //[self release];
        }
    }];
}

#pragma sys-

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithDelegate:(id<PickerViewDelegate>)delegate{
    self = [super init];
    if (self) {
        
        curSelIndex = -1;
        
        CGRect winSize = [[UIScreen mainScreen] bounds];
        self.frame = CGRectMake(0, winSize.size.height, winSize.size.width, 260);
        
        self.pdelegate = delegate;
        [self setBackgroundColor:[UIColor clearColor]];
        
        _toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 44)];
        [_toolBar setBarStyle:UIBarStyleBlackTranslucent];
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, _toolBar.frame.size.height,
                                                                     self.frame.size.width, 216)];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered
                                                                 target:self action:@selector(clickedButton:)];
        item1.tag = 1;
        
        UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self action:nil];
        UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone
                                                                 target:self action:@selector(clickedButton:)];
        item3.tag = 2;
        
        _toolBar.items = [NSArray arrayWithObjects:item1,item2,item3, nil];
        
        [self addSubview:_toolBar];
        [self addSubview:_pickerView];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
