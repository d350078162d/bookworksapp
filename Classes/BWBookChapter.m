//
//  BWBookChapter.m
//  BookWorksApp
//
//  Created by 刁志远 on 14/12/25.
//
//

#import "BWBookChapter.h"

#import "BWChapterContent.h"

#import "ASIHTTPRequest.h"


@interface BWBookChapter (){
    
    NSDictionary *dicUrl;
    NSDictionary *dicBook;
    
    NSMutableArray *arrList;
    
}

@end

@implementation BWBookChapter



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int index = [arrList count]-1-indexPath.row;
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    NSDictionary *dic = [arrList objectAtIndex:index];
    BWChapterContent *chap = [[BWChapterContent alloc] initWithCurChapter:dic andChapterList:arrList];
    [chap setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:chap animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str_id = @"resource_cell_id";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str_id];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:str_id];
        
        UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, WINSIZE.width-10, 20)];
        lab1.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0f];
        lab1.tag = 1;
        lab1.font = [UIFont systemFontOfSize:15];
        [cell.contentView addSubview:lab1];
        
        UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 24, WINSIZE.width-10, 20)];
        lab2.textColor = [UIColor colorWithRed:83/255.0 green:83/255.0 blue:83/255.0 alpha:1.0f];
        lab2.tag = 2;
        lab2.font = [UIFont systemFontOfSize:16];
        [cell.contentView addSubview:lab2];
    }
    
    int index = [arrList count]-1-indexPath.row;
    
    if (index < 0 ) {
        return cell;
    }
    
    
    //[0]	(null)	@"unreadble" : @"0"
    //[1]	(null)	@"title" : @"第一千七百九十章 灭雷罚"
    //[2]	(null)	@"link" : @"http://www.hunhun.net/book/wujitianxia/10846724.html"
    NSDictionary *dic = [arrList objectAtIndex:index];
    cell.textLabel.text = [dic valueForKey:@"title"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    
    return cell;
}


#pragma mark - My


- (void)getChapterList{
    if (!arrList) {
        arrList = [NSMutableArray arrayWithCapacity:3];
    }
    [arrList removeAllObjects];
    
    [WSProgressHUD showWithStatus:@"Loading..."];
    
    NSString *str_url = [NSString stringWithFormat:@"%@/zhuishu.php?method=3&book_id=%@",kZHUI_URL,[dicBook valueForKey:@"_id"]];
    
    
    dispatch_queue_t queue = dispatch_queue_create("get_chapters", nil);
    dispatch_async(queue, ^{
        NSString *str = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [WSProgressHUD dismiss];
            
            NSDictionary *dic_chap = [str JSONValue];
            NSArray *arr = [dic_chap objectForKey:@"chapters"];
            [arrList addObjectsFromArray:arr];
            [self.myTable reloadData];
        });
        
    });
}


#pragma mark - SYS


- (id)initWithBookDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        dicBook = [NSDictionary dictionaryWithDictionary:dic];
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dicUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"book_url"];
    
    self.navigationItem.title = @"章节";
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 32, 32);
    [btn setBackgroundImage:[UIImage imageNamed:@"back_up.png"] forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    
    [self getChapterList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
