//
//  BKChapterList.h
//  BookWorksApp
//
//  Created by 刁志远 on 16/6/23.
//
//

#import <UIKit/UIKit.h>

typedef void(^HandleSelectChapterBlock)(NSString *title, NSInteger index);

@interface BKChapterList : UIView

@property (copy) HandleSelectChapterBlock selectTitle;

- (id)initWithFrame:(CGRect)frame andChapterList:(NSArray *)arr;

@end
