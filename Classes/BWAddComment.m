//
//  BWAddComment.m
//  BookWorksApp
//
//  Created by 刁志远 on 13-11-29.
//
//

#import "BWAddComment.h"

@interface BWAddComment ()

@end

@implementation BWAddComment

@synthesize dicBook;

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - MY

- (void)show{
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.layer addAnimation:popAnimation forKey:nil];
}

- (void)hide {
    CAKeyframeAnimation *hideAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    hideAnimation.duration = 0.5;
    hideAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0f, 1.0f, 1.0f)],
                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.30f, 0.30f, 0.30f)]
                             ];
    hideAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f];
    hideAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    hideAnimation.delegate = self;
    [self.view.layer addAnimation:hideAnimation forKey:nil];
    [self performSelector:@selector(remove) withObject:Nil afterDelay:.5];
    
}

- (void)remove{
    [self.view removeFromSuperview];
}

- (IBAction)dismissWithAnimation:(id)sender {
    [self hide];
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentView:didDismissWithClick:)]) {
        [self.delegate commentView:self didDismissWithClick:0];
    }
}

- (IBAction)btnClicked:(id)sender {
    if ([self.txtContent.text length]<8) {
        [AllMethods showAltMsg:@"至少8个字..."];
        return;
    }
    [WSProgressHUD showWithStatus:@"正在提交..."];
    dispatch_queue_t queue = dispatch_queue_create("add_book_comment_queue", Nil);
    dispatch_async(queue, ^{
        NSDictionary *dic_user = [[NSUserDefaults standardUserDefaults] objectForKey:kUSER_INFO];
        NSString *surl = [NSString stringWithFormat:@"%@/%@?method=4&book_id=%@&uid=%@&rate=%.1f&content=%@&nick=%@",kMAIN_URL,kUSER_URL,
                          [dicBook valueForKey:@"id"],[dic_user valueForKey:@"uid"],self.rateView.rate,self.txtContent.text,[dic_user valueForKey:@"nick"]];
        
        surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:surl];
        NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:Nil];
        NSDictionary *dic = [res JSONValue];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([[dic valueForKey:@"err_code"] intValue] == 0) {
                [WSProgressHUD showSuccessWithStatus:@"添加成功" ];
                [self hide];
                if (self.delegate && [self.delegate respondsToSelector:@selector(commentView:didDismissWithClick:)]) {
                    [self.delegate commentView:self didDismissWithClick:1];
                }
                
            }
            else{
                [WSProgressHUD showErrorWithStatus:@"添加失败,请重试." ];
            }
        });
    });
}


#pragma mark - SYS

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.rateView setRate:2.5f];
    self.view.backgroundColor = MAIN_COLOR;
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:1];
    [btn setBackgroundColor:NAV_COLOR];
    btn = (UIButton *)[self.view viewWithTag:2];
    [btn setBackgroundColor:NAV_COLOR];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.txtContent becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
