//
//  BKNewMenuBottom.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/20.
//
//

#import "BKNewMenuBottom.h"

#import "LSYReadConfig.h"

#import "TMMuiLazyScrollView.h"

#import "FMStepper.h"

@interface BKNewMenuBottom () <TMMuiLazyScrollViewDelegate,TMMuiLazyScrollViewDataSource>{
    
    MenuShowType showType;
    
}

@property (nonatomic, weak) TMMuiLazyScrollView *lazyScroll;

@end


@implementation BKNewMenuBottom

#pragma mark - TMMuiLazyScrollViewDelegate

- (void)muiViewClicked:(UITapGestureRecognizer *)ges {
    
    UIView *view = ges.view;
    NSInteger index = [view.muiID integerValue];
    [LSYReadConfig shareInstance].theme = index;
    
    [self.lazyScroll reloadData];
    
    if (self.clickEvent) {
        self.clickEvent(103,0);
    }
    
}

#pragma mark - TMMuiLazyScrollViewDataSource

- (NSUInteger)numberOfItemInScrollView:(nonnull TMMuiLazyScrollView *)scrollView {
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    return config.arrThemes.count;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// Return the view model by spcial index.
- (nonnull TMMuiRectModel *)scrollView:(nonnull TMMuiLazyScrollView *)scrollView
                      rectModelAtIndex:(NSUInteger)index {
    
    float width = 30;
    CGRect rect = CGRectMake(15*(index)+index*width, 5, width, width);
    TMMuiRectModel *rectModel = [[TMMuiRectModel alloc]init];
    rectModel.absoluteRect = rect;
    rectModel.muiID = [NSString stringWithFormat:@"%td",index];
    return rectModel;
    
}

/**
 * You should render the item view here. And the view is probably . Item view display. You should
 * *always* try to reuse views by setting each view's reuseIdentifier and querying for available
 * reusable views with dequeueReusableItemWithIdentifier:
 */
- (nullable UIView *)scrollView:(nonnull TMMuiLazyScrollView *)scrollView itemByMuiID:(nonnull NSString *)muiID {
    
    
    UIView *view = [scrollView dequeueReusableItemWithIdentifier:@"theme_view"];
    NSInteger index = [muiID integerValue];
    if (!view)
    {
        TMMuiRectModel *model = [self scrollView:scrollView rectModelAtIndex:index];
        
        CGRect rect = model.absoluteRect;//CGRectMake(15*(index+1), 7.5, 85, 55);
        view = [[UIView alloc]initWithFrame:rect];
        
        //        view.backgroundColor = NAV_COLOR;
        view.layer.cornerRadius = 6.6f;
        view.clipsToBounds = YES;
        view.reuseIdentifier = @"theme_view";
        
        UIImageView *iv = [[UIImageView alloc] initWithFrame:view.bounds];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        iv.tag = 1000;
        [view addSubview:iv];
        
        view.clipsToBounds = YES;
        
        view.layer.borderWidth = 1.5f;
        view.layer.cornerRadius = SIZE_W(view)/2;
        
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                           action:@selector(muiViewClicked:)]];
        
        
        
    }
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    
    NSDictionary *dic = config.arrThemes[index];
    
    UIImageView *iv = (UIImageView *)[view viewWithTag:1000];
    
    if ([dic[@"bk_type"] isEqualToString:@"color"]) {
        iv.image = nil;
        view.backgroundColor = [UIColor colorFromeString:dic[@"bk"]];
    }
    else {
        view.backgroundColor = [UIColor whiteColor];
        NSString *sn = [NSString stringWithFormat:@"thumb_%@",dic[@"bk"]];
        iv.image = ImageNamed(sn);
    }
    
    if (config.theme == index) {
        view.layer.borderColor = NAV_COLOR.CGColor;
    }
    else {
        view.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    [scrollView addSubview:view];
    
    return view;
    
    
}

#pragma mark - Public 

- (void)updateTheme {
    
    __weak BKNewMenuBottom *weak = self;
    [weak.lazyScroll reloadData];
}


#pragma mark - Private

- (void)btnPageTurnTypeClicked:(id)sender {
    
    
    UIButton *btn_sel = (UIButton *)sender;
    
    [LSYReadConfig shareInstance].pageTurn = btn_sel.tag - 1000;
    NSArray *arr = [LSYReadConfig shareInstance].arrPageTurn;
    
    for (int i = 0; i<arr.count; i++) {
        
        UIButton *btn = [self viewWithTag:i+1000];
        
        if ([LSYReadConfig shareInstance].pageTurn == i) {
            [btn setTitleColor:NAV_COLOR forState:UIControlStateNormal];
            btn.layer.borderColor = NAV_COLOR.CGColor;
        }
        else {
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btn.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        
    }
    
    if (self.clickEvent) {
        self.clickEvent(104,btn_sel.tag-1000);
    }
    
}

- (void)stepValueChanged:(id)sender {
    
    UIStepper *step = (UIStepper *)sender;
    UILabel *lab = (UILabel *)[self viewWithTag:101];
    lab.text = [NSString stringWithFormat:@"%.0f",step.value];
    
    if (self.clickEvent) {
        self.clickEvent(101,step.value);
    }
}

- (void)btnCatagoryClicked:(id)sender {
    
    if (self.clickEvent) {
        self.clickEvent(102,0);
    }
    
}

#pragma mark - Build UI

- (void)buildContent {
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    
    UILabel *lab_tit_font = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, 40, 20)];
    lab_tit_font.textColor = [UIColor whiteColor];
    lab_tit_font.text = @"字号";
    lab_tit_font.font = [AllMethods getFontWithSize:16];
    lab_tit_font.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lab_tit_font];
    
    
    FMStepper *step = [FMStepper stepperWithFrame:CGRectMake(ORIGINAL_X(lab_tit_font)+10, 0, 94+SIZE_W(lab_tit_font)/2, 30)
                                              min:14 max:28 step:1.0
                                            value:config.fontSize];
    step.center = CGPointMake(step.center.x, lab_tit_font.center.y);
    [step setFont:@"a" size:17];
    step.layer.borderWidth = 0.65f;
    step.layer.borderColor = NAV_COLOR.CGColor;
    step.layer.cornerRadius = SIZE_H(step)/2;
    [step setTintColor:[UIColor clearColor]];
    [step addTarget:self action:@selector(stepValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:step];
    
    UIButton *btn_cata = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_cata.frame = CGRectMake(SIZE_W(self)-90-15, 0, 90, 30);
    [btn_cata setTitle:@" 目录" forState:UIControlStateNormal];
    [btn_cata setTitleColor:NAV_COLOR forState:UIControlStateNormal];
    btn_cata.titleLabel.font = [AllMethods getFontWithSize:16];
    [btn_cata setImage:ImageNamed(@"catalog") forState:UIControlStateNormal];
    btn_cata.center = CGPointMake(btn_cata.center.x, lab_tit_font.center.y);
    [btn_cata setShowsTouchWhenHighlighted:YES];
    [btn_cata addTarget:self action:@selector(btnCatagoryClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn_cata];
    
    float ori_y = ORIGINAL_Y(lab_tit_font);
    
    float width = 0;
    
    if (showType == MENU_SHOW_LOCAL_BOOK) {
        
        UILabel *lab_tit_page = [[UILabel alloc] initWithFrame:CGRectMake(15, ORIGINAL_Y(lab_tit_font)+25, 40, 20)];
        lab_tit_page.textColor = [UIColor whiteColor];
        lab_tit_page.text = @"翻页";
        lab_tit_page.font = [AllMethods getFontWithSize:16];
        lab_tit_page.textAlignment = NSTextAlignmentCenter;
        [self addSubview:lab_tit_page];
        
        NSArray *arr_btn = [LSYReadConfig shareInstance].arrPageTurn;
        float btn_ori_x = ORIGINAL_X(lab_tit_page)+10;
        
        float jian = 12;
        width = (ScreenWidth -btn_ori_x - jian*arr_btn.count)/(arr_btn.count);;
        for (int i=0; i<arr_btn.count; i++) {
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(btn_ori_x+(width+jian)*i, 0, width, 30);
            btn.center = CGPointMake(btn.center.x, lab_tit_page.center.y);
            [self addSubview:btn];
            [btn setTitle:arr_btn[i] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(btnPageTurnTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
            btn.titleLabel.font = [AllMethods getFontWithSize:16];
            
            btn.layer.cornerRadius = 16.0f;
            btn.layer.borderWidth = 0.65f;
            
            btn.tag = 1000+i;
            
            if ([LSYReadConfig shareInstance].pageTurn == i) {
                [btn setTitleColor:NAV_COLOR forState:UIControlStateNormal];
                btn.layer.borderColor = NAV_COLOR.CGColor;
            }
            else {
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                btn.layer.borderColor = [UIColor whiteColor].CGColor;
            }
        }
        
        ori_y = ORIGINAL_Y(lab_tit_page);
    }
    
    
    
    
    UILabel *lab_tit_theme = [[UILabel alloc] initWithFrame:CGRectMake(15, ori_y+25, 40, 20)];
    lab_tit_theme.textColor = [UIColor whiteColor];
    lab_tit_theme.text = @"主题";
    lab_tit_theme.font = [AllMethods getFontWithSize:16];
    lab_tit_theme.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lab_tit_theme];
    
    TMMuiLazyScrollView *scroll = [[TMMuiLazyScrollView alloc] initWithFrame:CGRectMake(ORIGINAL_X(lab_tit_theme)+10, 0, SIZE_W(self)-ORIGINAL_X(lab_tit_theme)-10-10, 40)];
    
    _lazyScroll = scroll;
    self.lazyScroll.delegate = self;
    self.lazyScroll.dataSource = self;
    self.lazyScroll.center = CGPointMake(scroll.center.x, lab_tit_theme.center.y);
    [self addSubview:_lazyScroll];
    
    width = 0;
    TMMuiRectModel *model = [self scrollView:scroll rectModelAtIndex:0];
    CGRect frame = model.absoluteRect;
    
    width = [LSYReadConfig shareInstance].arrThemes.count*(frame.size.width+15)+15;
    self.lazyScroll.contentSize = CGSizeMake(width, 40);
    [self.lazyScroll reloadData];
    
}

#pragma mark - SYS

- (id)initWithMenuShowType:(MenuShowType)type andFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        showType = type;
        self.frame = frame;
        self.backgroundColor = RGBACOLOR(0, 0, 0, 0.8f);
        
        [self buildContent];
    }
    return self;
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        showType = MENU_SHOW_LOCAL_BOOK;
        self.frame = frame;
        self.backgroundColor = RGBACOLOR(0, 0, 0, 0.8f);
        
        [self buildContent];
    }
    return self;
}

@end
