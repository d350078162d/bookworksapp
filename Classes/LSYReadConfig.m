//
//  LSYReadConfig.m
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYReadConfig.h"

@implementation LSYReadConfig
+(instancetype)shareInstance
{
    static LSYReadConfig *readConfig = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        readConfig = [[self alloc] init];
        
    });
    return readConfig;
}

- (void)initinalThemes {
    
    NSArray *arr_theme = @[@{@"font_color":[RGBACOLOR(51, 49, 0, 1) colorToString],
                             @"bk":[RGBACOLOR(250, 249, 222, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(57, 79, 8, 1) colorToString],
                             @"bk":[RGBACOLOR(227, 237, 205, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(73, 15, 0, 1) colorToString],
                             @"bk":[RGBACOLOR(253, 230, 224, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(3, 8, 62, 1) colorToString],
                             @"bk":[RGBACOLOR(233, 235, 254, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(12, 60, 20, 1) colorToString],
                             @"bk":[RGBACOLOR(220, 226, 221, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(102, 102, 130, 1) colorToString],
                             @"bk":[RGBACOLOR(255, 255, 255, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(127, 127, 127, 1) colorToString],
                             @"bk":[RGBACOLOR(0, 0, 0, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(0, 0, 0, 1) colorToString],
                             @"bk":[RGBACOLOR(255, 255, 255, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(21, 42, 95, 1) colorToString],
                             @"bk":[RGBACOLOR(250, 249, 241, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(255, 255, 255, 1) colorToString],
                             @"bk":[RGBACOLOR(58, 72, 107, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(41, 41, 84, 1) colorToString],
                             @"bk":[RGBACOLOR(234, 234, 239, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(255, 255, 255, 1) colorToString],
                             @"bk":[RGBACOLOR(0, 0, 0, 1) colorToString],
                             @"bk_type":@"color"},
                           @{@"font_color":[RGBACOLOR(255, 255, 255, 1) colorToString],
                             @"bk":@"bk_red",
                             @"bk_type":@"file"},
                           @{@"font_color":[RGBACOLOR(139, 87, 42, 1) colorToString],
                             @"bk":@"bk_white",
                             @"bk_type":@"file"},
                           @{@"font_color":[RGBACOLOR(0, 0, 0, 1) colorToString],
                             @"bk":@"bk_yellow",
                             @"bk_type":@"file"}];
    
    _arrThemes = [NSArray arrayWithArray:arr_theme];
    
}

- (void)initinalPageTurn {
    
    NSArray *arr_btn = @[@"平移",@"移入",@"移出",@"拟真"];
    _arrPageTurn = [NSArray arrayWithArray:arr_btn];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSString *file_path = [AllMethods applicationDocumentsDirectory];
        NSString *configPath = [NSString stringWithFormat:@"%@/%@.config",file_path,kREAD_CONFIG];
        
//        [[NSFileManager defaultManager] removeItemAtPath:configPath error:nil];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:configPath]) {
            NSData *data = [NSData dataWithContentsOfFile:configPath];
            
            NSKeyedUnarchiver *unarchive = [[NSKeyedUnarchiver alloc]initForReadingWithData:data];
            LSYReadConfig *config = [unarchive decodeObjectForKey:@"ReadConfig"];
            [config addObserver:config forKeyPath:@"fontSize" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"lineSpace" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"fontColor" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"theme" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"paraSpace" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"pageTurn" options:NSKeyValueObservingOptionNew context:NULL];
            return config;
        }
        _lineSpace = 9.0f;
        _fontSize = 17.0f;
        _paraSpace = 5.0f;
        _pageTurn = 0;
        
        if (ScreenWidth >= 414) {//plus
            _fontSize = 18;
        }
        
        _kernSpace = 1.0f;
        _theme = 0;
        _fontColor = RGBACOLOR(51, 49, 0, 1);
        [self initinalThemes];
        [self initinalPageTurn];
        [self addObserver:self forKeyPath:@"fontSize" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"lineSpace" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"fontColor" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"theme" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"pageTurn" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"paraSpace" options:NSKeyValueObservingOptionNew context:NULL];
        [LSYReadConfig updateLocalConfig:self];
        
    }
    return self;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    
    if ([keyPath isEqualToString:@"theme"]) {
        NSDictionary *dic_theme = self.arrThemes[self.theme];
        self.fontColor = [UIColor colorFromeString:dic_theme[@"font_color"]];
    }
    else {
        [LSYReadConfig updateLocalConfig:self];
    }
}
+(void)updateLocalConfig:(LSYReadConfig *)config
{
    NSMutableData *data=[[NSMutableData alloc]init];
    NSKeyedArchiver *archiver=[[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
    [archiver encodeObject:config forKey:@"ReadConfig"];
    [archiver finishEncoding];
    
    NSString *file_path = [AllMethods applicationDocumentsDirectory];
    NSString *configPath = [NSString stringWithFormat:@"%@/%@.config",file_path,kREAD_CONFIG];
    [data writeToFile:configPath atomically:YES];
    
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:self.fontSize forKey:@"fontSize"];
    [aCoder encodeDouble:self.kernSpace forKey:@"kernSpace"];
    [aCoder encodeDouble:self.lineSpace forKey:@"lineSpace"];
    [aCoder encodeObject:self.fontColor forKey:@"fontColor"];
    [aCoder encodeDouble:self.paraSpace forKey:@"paraSpace"];
    [aCoder encodeInteger:self.theme forKey:@"theme"];
    [aCoder encodeObject:self.arrThemes forKey:@"arrThemes"];
    [aCoder encodeInteger:self.pageTurn forKey:@"pageTurn"];
    [aCoder encodeObject:self.arrPageTurn forKey:@"arrPageTurn"];
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.fontSize = [aDecoder decodeDoubleForKey:@"fontSize"];
        self.lineSpace = [aDecoder decodeDoubleForKey:@"lineSpace"];
        self.fontColor = [aDecoder decodeObjectForKey:@"fontColor"];
        self.theme = [aDecoder decodeIntegerForKey:@"theme"];
        self.pageTurn = [aDecoder decodeIntegerForKey:@"pageTurn"];
        self.arrThemes = [aDecoder decodeObjectForKey:@"arrThemes"];
        self.arrPageTurn = [aDecoder decodeObjectForKey:@"arrPageTurn"];
        self.kernSpace = [aDecoder decodeDoubleForKey:@"kernSpace"];
        self.paraSpace = [aDecoder decodeDoubleForKey:@"paraSpace"];
    }
    return self;
}

+(NSDictionary *)parserAttribute:(LSYReadConfig *)config
{
    /*
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 10;// 字体的行间距
    paragraphStyle.firstLineHeadIndent = 20.0f;//首行缩进
    paragraphStyle.alignment = NSTextAlignmentJustified;//（两端对齐的）文本对齐方式：（左，中，右，两端对齐，自然）
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;//结尾部分的内容以……方式省略 ( "...wxyz" ,"abcd..." ,"ab...yz")
    paragraphStyle.headIndent = 20;//整体缩进(首行除外)
    paragraphStyle.tailIndent = 20;//
    paragraphStyle.minimumLineHeight = 10;//最低行高
    paragraphStyle.maximumLineHeight = 20;//最大行高
    paragraphStyle.paragraphSpacing = 15;//段与段之间的间距
    paragraphStyle.paragraphSpacingBefore = 22.0f;//段首行空白空间
    paragraphStyle.baseWritingDirection = NSWritingDirectionLeftToRight;//从左到右的书写方向（一共三种）
    paragraphStyle.lineHeightMultiple = 15;// Natural line height is multiplied by this factor (if positive) before being constrained by minimum and maximum line height.
    paragraphStyle.hyphenationFactor = 1;//连字属性 在iOS，唯一支持的值分别为0和1
    */
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[NSForegroundColorAttributeName] = config.fontColor;
    dict[NSFontAttributeName] = [AllMethods getFontWithSize:config.fontSize];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = config.lineSpace;
//    paragraphStyle.paragraphSpacing = config.paraSpace;  //段间距
    paragraphStyle.baseWritingDirection = NSWritingDirectionLeftToRight;
//    paragraphStyle.paragraphSpacingBefore = 20.0f;//段首行空白空间
    paragraphStyle.firstLineHeadIndent = 0.0f;//20.0f;//首行缩进
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    dict[NSParagraphStyleAttributeName] = paragraphStyle;
    return [dict copy];
}

@end
