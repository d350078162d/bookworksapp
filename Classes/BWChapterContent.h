//
//  BWChapterContent.h
//  BookWorksApp
//
//  Created by 刁志远 on 14/12/25.
//
//

#import <UIKit/UIKit.h>

@interface BWChapterContent : UIViewController


@property (weak, nonatomic) IBOutlet UITextView *txtContent;
- (id)initWithCurChapter:(NSDictionary *)dic andChapterList:(NSArray *)arr;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *spaceBar;

- (IBAction)itemClicked:(id)sender;
@end
