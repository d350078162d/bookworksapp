//
//  UIWebView+Noti.m
//  BookWorksApp
//
//  Created by 刁志远 on 15/8/14.
//
//

#import "UIWebView+Noti.h"


@interface UIWebView_Noti () <UIWebViewDelegate>{
    
    
    
}

@end


@implementation UIWebView_Noti 

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [[NSNotificationCenter defaultCenter] postNotificationName:kWebViewDidStartLoadNotification object:webView];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [[NSNotificationCenter defaultCenter] postNotificationName:kWebViewDidFinishLoadNotification object:webView];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[NSNotificationCenter defaultCenter] postNotificationName:kWebViewDidFailLoadNotification object:webView];
}



#pragma mark - SYS


- (id)init {
    self = [super init];
    if (self) {
        self.delegate = self;
    }
    return self;
}

@end
