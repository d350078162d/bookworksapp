//
//  CustomTextView.m
//  BookWorksApp
//
//  Created by 刁志远 on 12-9-25.
//
//

#import "CustomTextView.h"

@implementation CustomTextView

@synthesize tdelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if ([tdelegate respondsToSelector:@selector(customTextView:touchBegan:withEvent:)]) {
        [tdelegate customTextView:self touchBegan:touches withEvent:event];
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    if ([tdelegate respondsToSelector:@selector(customTextView:touchMoved:withEvent:)]) {
        [tdelegate customTextView:self touchMoved:touches withEvent:event];
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if ([tdelegate respondsToSelector:@selector(customTextView:touchEnded:withEvent:)]) {
        [tdelegate customTextView:self touchEnded:touches withEvent:event];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
