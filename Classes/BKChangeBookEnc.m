//
//  BKChangeBookEnc.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/1/16.
//
//

#import "BKChangeBookEnc.h"

#import "RJBookData.h"

@interface BKChangeBookEnc () <UITableViewDelegate, UITableViewDataSource>{
    
    NSMutableArray *arrList;
    NSMutableDictionary *dicEncList;
    
    RJSingleBook *curBook;
    
    UITextView *txtContent;
}

@property (nonatomic, weak) UITableView *myTable;

@end

@implementation BKChangeBookEnc

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *dic = arrList[indexPath.row];
    
    long enc = [dic[@"enc"] longValue];
    NSString *allContent = [NSString stringWithContentsOfFile:curBook.bookFile
                                                     encoding:enc
                                                        error:nil];
    if (allContent.length > 1000) {
        txtContent.text = [allContent substringToIndex:1000];
    }
    else {
        txtContent.text = allContent;
    }
    
    [txtContent setContentOffset:CGPointMake(0, txtContent.contentSize.height - txtContent.bounds.size.height)];
    
    [dicEncList setObject:@(enc) forKey:curBook.bookFile];
    [tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    NSDictionary *dic = arrList[indexPath.row];
    
    cell.textLabel.text = dic[@"title"];
    
    if ([dicEncList[curBook.bookFile] isEqual:dic[@"enc"]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    return cell;
}

#pragma mark - Custom

- (void)backUp {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)confirmClicked:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeEncode)]) {
        [self.delegate didChangeEncode];
    }
    
    NSString *file_enc = [[AllMethods applicationDocumentsDirectory] stringByAppendingPathComponent:@"file_encode.enc"];
    [dicEncList writeToFile:file_enc atomically:YES];
    
    NSString *file_r = [NSString stringWithFormat:@"%@.r",curBook.bookFile];
    [[NSFileManager defaultManager] removeItemAtPath:file_r
                                               error:nil];
    NSString *file_chap = [NSString stringWithFormat:@"%@.chapter",curBook.bookFile];
    [[NSFileManager defaultManager] removeItemAtPath:file_chap
                                               error:nil];
    
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - Build UI


- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 264, ScreenWidth, ScreenHeight-264)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
}

- (void)buildContentView {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 64, ScreenWidth, 200)];
    
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, ScreenWidth-20, 25)];
    lab.text = @"内容预览";
    lab.textAlignment = NSTextAlignmentCenter;
    lab.font = [UIFont systemFontOfSize:13];
    lab.textColor = RGBACOLOR(180, 180, 180, 1.0f);
    [view addSubview:lab];
    
    txtContent = [[UITextView alloc] initWithFrame:CGRectMake(5, 25, ScreenWidth-10, 175)];
    [view addSubview:txtContent];
    txtContent.font = [UIFont systemFontOfSize:15];
    
    [self.view addSubview:view];
    
}

- (void)buildConfirmButton {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, ScreenWidth/3, 30);
    [btn setBackgroundColor:NAV_COLOR];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    
    btn.center = CGPointMake(ScreenWidth/2, ScreenHeight-30);
    
    [btn addTarget:self action:@selector(confirmClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.cornerRadius = 3.6f;
    [btn setClipsToBounds:YES];
    
    [self.view addSubview:btn];
    
}

#pragma mark - Initinal Data 

- (void)initinalEncList {
    
    /*
     gb30= 0x80000632;
     gbKK= 0x80000631;
     gb18030 = 0x80000630;
     */
    
    NSStringEncoding encode1 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSStringEncoding encode2 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGBK_95);
    NSStringEncoding encode3 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_2312_80);
    NSStringEncoding encode4 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingISO_2022_CN);
    NSStringEncoding encode5 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingISO_2022_CN_EXT);
    NSStringEncoding encode6 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingEUC_CN);
    NSStringEncoding encode7 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingHZ_GB_2312);
    NSStringEncoding encode8 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5);
    NSStringEncoding encode9 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingMacChineseSimp);
    
    NSArray *arr = @[@{@"title":@"UTF-8",@"enc":@(NSUTF8StringEncoding)},
                     @{@"title":@"Unicode",@"enc":@(NSUnicodeStringEncoding)},
                     @{@"title":@"Unicode(UTF-16)",@"enc":@(NSUTF16LittleEndianStringEncoding)},
                     @{@"title":@"Unicode(UTF-16BE)",@"enc":@(NSUTF16BigEndianStringEncoding)},
                     @{@"title":@"中文_GBK_95",@"enc":@(encode2)},
                     @{@"title":@"中文_2312_80",@"enc":@(encode3)},
                     @{@"title":@"中文_18030_2000",@"enc":@(encode1)},
                     @{@"title":@"ISO_2022_CN",@"enc":@(encode4)},
                     @{@"title":@"ISO_2022_CN_EXT",@"enc":@(encode5)},
                     @{@"title":@"EUC_CN",@"enc":@(encode6)},
                     @{@"title":@"HZ_GB_2312",@"enc":@(encode7)},
                     @{@"title":@"Big5",@"enc":@(encode8)},
                     @{@"title":@"Mac 简体中文",@"enc":@(encode9)},];
    
    arrList = [NSMutableArray arrayWithArray:arr];
    
    NSString *file_enc = [[AllMethods applicationDocumentsDirectory] stringByAppendingPathComponent:@"file_encode.enc"];
    dicEncList = [NSMutableDictionary dictionaryWithContentsOfFile:file_enc];
    
}

- (void)initinalContent {
    
    long enc = [dicEncList[curBook.bookFile] longValue];
    NSString *allContent = [NSString stringWithContentsOfFile:curBook.bookFile
                                                     encoding:enc
                                                        error:nil];
    
    if (allContent.length > 1000) {
        txtContent.text = [allContent substringToIndex:1000];
    }
    else {
        txtContent.text = allContent;
    }
    
    [txtContent setContentOffset:CGPointMake(0, txtContent.contentSize.height - txtContent.bounds.size.height)];
    
}

#pragma mark - SYS

- (id)initWithBook:(RJSingleBook *)book andDelegate:(id<BKChangeBookEncDelegate>)delegate{
    
    self=  [super init];
    
    if (self) {
        self.delegate = delegate;
        curBook = book;
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initinalEncList];
    [self buildTableView];
    [self buildContentView];
    [self initinalContent];
    [self buildConfirmButton];
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.title = @"编码选择";
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn setBackgroundColor:[UIColor blackColor]];
    [btn setFrame:CGRectMake(0, 8, 55, 35)];
    [btn setTitle:NSLocalizedString(@"Nav_Back", @"") forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
    [btn addTarget:self action:@selector(backUp) forControlEvents:UIControlEventTouchUpInside];
    
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = item;
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del setADHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
