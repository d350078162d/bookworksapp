//
//  BKPageContent.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/28.
//
//

#import <UIKit/UIKit.h>

#import "VerticallyAlignedLabel.h"

@class BKNewContent;

@interface BKPageContent : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labPage;
@property (weak, nonatomic) IBOutlet UIImageView *ivBk;
@property (weak, nonatomic) IBOutlet VerticallyAlignedLabel *labContent;

@property (assign) NSInteger pageFlag; //用于标示唯一视图

//@property (nonatomic, copy) void(^PageClickEvent)(void);

- (id)initWithBookIdex:(NSInteger)index;

- (NSIndexPath *)getCurrentIndexPath;
- (void)updateCurrentPageInfoWithChapter:(NSInteger)chap andPage:(NSInteger)page;
- (void)updateBookAllPage:(NSInteger)all_page andReadPage:(NSInteger)read_page;
//- (void)updateAttribute;

- (NSIndexPath *)getNextPageIndexPath;
- (NSIndexPath *)getPreviousPageIndexPath;

@end
