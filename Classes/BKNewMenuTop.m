//
//  BKNewMenuTop.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/20.
//
//

#import "BKNewMenuTop.h"


@interface BKNewMenuTop ()

@end


@implementation BKNewMenuTop

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/







#pragma mark - Public

- (void)setTitleText:(NSString *)str {
    
    UILabel *lab = (UILabel *)[self viewWithTag:102];
    lab.text = str;
    
}


#pragma mark - Private

- (void)backupClicked:(id)sender {
    
    if (self.clickedEvent) {
        self.clickedEvent(sender);
    }
}


#pragma mark - Build UI

- (void)buildContent {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(15, 20, 40, 44);
    [btn setImage:ImageNamed(@"bk_small") forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn addTarget:self action:@selector(backupClicked:)
  forControlEvents:UIControlEventTouchUpInside];
    
    btn.tag = 101;
    [self addSubview:btn];
    
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(ORIGINAL_X(btn)+5, 20,
                                                             SIZE_W(self)-2*(ORIGINAL_X(btn)+5), 44)];
    lab.textAlignment = NSTextAlignmentCenter;
    lab.textColor = [UIColor whiteColor];
    lab.font = [AllMethods getFontWithSize:19];
    lab.tag = 102;
    [self addSubview:lab];
    
}


#pragma mark - SYS

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        self.backgroundColor = RGBACOLOR(0, 0, 0, 0.8f);
        [self buildContent];
    }
    return self;
}

@end
