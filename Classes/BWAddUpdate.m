//
//  BWAddUpdate.m
//  BookWorksApp
//
//  Created by 刁志远 on 14-1-13.
//
//

#import "BWAddUpdate.h"

#import "WSProgressHUD.h"

#import "MSCellAccessory.h"

#import "BWBookDetail.h"

#import "ASIHTTPRequest.h"

#import "UIImageView+WebCache.h"

#import "FFCircularProgressView.h"

#import "BookWorksAppAppDelegate.h"

@interface BWAddUpdate (){
    long gb30;
    long gbKK;
}

@end

@implementation BWAddUpdate

#pragma mark - UITableViewDelegate UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setBackgroundColor:NAV_SHADOW_COLOR];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str_id = [NSString stringWithFormat:@"book_list_cell_id_%d",indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str_id];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str_id];
        
        UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(70, 3, ScreenWidth-100, 17)];
        lab1.backgroundColor = [UIColor clearColor];
        lab1.font = [AllMethods getFontWithSize:17];
        lab1.tag = 1;
        lab1.textColor = [UIColor whiteColor];
        [cell.contentView addSubview:lab1];
        
        UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(70, 17, ScreenWidth-100, 60)];
        lab2.backgroundColor = [UIColor clearColor];
        lab2.font = [AllMethods getFontWithSize:15];
        lab2.numberOfLines = 10;
        lab2.tag = 2;
        lab2.textColor = [UIColor whiteColor];
        [cell.contentView addSubview:lab2];
        
        UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(5, 3, 60, 75)];
        [cell.contentView addSubview:imgv];
        imgv.tag = 3;
        
        FFCircularProgressView *circ = [[FFCircularProgressView alloc] initWithFrame:CGRectMake(ScreenWidth-30,1.6, 20, 20)
                                                                             andType:kFFCIRCLE_ADD_TYPE];
        [circ setTintColor:[UIColor whiteColor]];
        [circ setTickColor:NAV_COLOR];
        circ.center = CGPointMake(circ.center.x, 40);
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 40, 40);
        btn.center = CGPointMake(circ.frame.size.width/2, circ.frame.size.height/2);
        [btn setShowsTouchWhenHighlighted:YES];
        btn.tag = indexPath.row;
        [btn addTarget:self action:@selector(insertBook:) forControlEvents:UIControlEventTouchUpInside];
        [circ addSubview:btn];
        circ.tag = 4;
        [cell.contentView addSubview:circ];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    
    if (arrList.count <= indexPath.row) {
        return cell;
    }
    
    NSDictionary *dic = [arrList objectAtIndex:indexPath.row];
    
    UILabel *lab = (UILabel *)[cell.contentView viewWithTag:1];
    lab.text = [dic valueForKey:@"title"];
    
    lab = (UILabel *)[cell.contentView viewWithTag:2];
    lab.text = [NSString stringWithFormat:@"%@\n%@",[dic valueForKey:@"shortIntro"],[dic valueForKey:@"lastChapter"]];
    
    UIImageView *imgv = (UIImageView *)[cell.contentView viewWithTag:3];
    NSString *surl = [NSString stringWithFormat:@"%@%@",[dicURL valueForKey:@"book_img"],[dic valueForKey:@"cover"]];
    surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [imgv sd_setImageWithURL:[NSURL URLWithString:surl] placeholderImage:nil
                     options:SDWebImageProgressiveDownload];
    
//    NSString *path = [AllMethods applicationDocumentsDirectory];
//    NSString *file = [path stringByAppendingPathComponent:[surl lastPathComponent]];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:file]) {
//        imgv.image = [UIImage imageWithData:[NSData dataWithContentsOfFile:file]];
//    }
//    else{
//        [imgv loadURL:[NSURL URLWithString:surl] withFilePath:file];
//    }
    
    FFCircularProgressView *ff = (FFCircularProgressView *)[cell.contentView viewWithTag:4];
    if ([arrLocal containsObject:[dic valueForKey:@"_id"]]) {
        [ff setProgress:1.0f];
        UIButton *btn = (UIButton *)[ff viewWithTag:indexPath.row];
        btn.enabled = NO;
    }
    else{
        [ff setProgress:0];
        [ff changeCircleType:kFFCIRCLE_ADD_TYPE];
    }
    
    
    return cell;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIScrollView *scr = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 44)];
//    [scr setContentSize:CGSizeMake(segType.frame.size.width, 1)];
//    [scr addSubview:segType];
//    [scr setPagingEnabled:YES];
//    [scr setShowsHorizontalScrollIndicator:NO];
//    return scr;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return  44;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    BWBookDetail *detail = [[BWBookDetail alloc] init];
//    [detail setCurBook:[arrList objectAtIndex:indexPath.row]];
//    [detail setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:detail animated:YES];
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = indexPath.row;
    
    [self insertBook:btn];
    
}


#pragma mark - MY

- (void)getLocalBook{
    NSArray *arr = [AllMethods getLocalBookID];
    if (!arrLocal) {
        arrLocal = [[NSMutableArray alloc] initWithCapacity:3];
    }
    [arrLocal addObjectsFromArray:arr];
    [self.myTable reloadData];
}

- (void)insertBook:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSDictionary *dic = [arrList objectAtIndex:btn.tag];
    
    NSString *sql = [NSString stringWithFormat:@"insert into 'main'.'bookbag' (id,book_name,book_id,book_author,book_img,book_other)"\
                     " values (NULL,'%@','%@','%@','%@',NULL)",[dic valueForKey:@"title"],[dic valueForKey:@"_id"],@"",@""];
    [AllMethods insertBookWithSQL:sql];
    
    [self getLocalBook];
    [self.myTable reloadData];
}

- (void)getBookList{
    [WSProgressHUD showWithStatus:@"Loading..."];
    int index = [segType currentSelected];
    NSDictionary *dic = [arrType objectAtIndex:index];
    
    NSString *str_url = [NSString stringWithFormat:@"%@/zhuishu.php?method=1&type=%@&page=%d",
                         kZHUI_URL,[dic valueForKey:@"type_id"],curPage*20];
    //[NSString stringWithFormat:[dicURL valueForKey:@"book_list"],[dic valueForKey:@"type_id"],curPage*20];
    str_url = [str_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    dispatch_queue_t queue = dispatch_queue_create("add_book_list", nil);
    dispatch_async(queue, ^{
        
        NSString *str_res = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                           encoding:NSUTF8StringEncoding
                                                              error:nil];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [WSProgressHUD dismiss];
            
            NSArray *arr = [str_res JSONValue];
            [arrList addObjectsFromArray:arr];
            
            [self.myTable reloadData];
            if([arr count] < 20){
                mjFoot.hidden = YES;
            }
            else mjFoot.hidden = NO;
            
            [mjFoot endRefreshing];
            [mjHead endRefreshing];
        });
        
    });
    
    
/*
    dispatch_queue_t queue = dispatch_queue_create("get_book_type_list", nil);
    dispatch_async(queue, ^(void){
        
        NSString *str_url = [NSString stringWithFormat:[dicURL valueForKey:@"book_list"],[dic valueForKey:@"type_id"],curPage*20];
        NSString *res = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        
        if ([res length] <= 0) {
        }
        else{
            NSArray *arr = [res JSONValue];
            [arrList addObjectsFromArray:arr];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                [self.myTable reloadData];
                if([arr count] < 20){
                    mjFoot.hidden = YES;
                }
                else mjFoot.hidden = NO;
                
                [mjFoot endRefreshing];
                [mjHead endRefreshing];
                
            });
            [WSProgressHUD dismiss];
        }
    });
 */
}

- (void)getBookTypeList{
    [WSProgressHUD showWithStatus:@"Loading..."];
    if (!arrType) {
        arrType = [[NSMutableArray alloc] initWithCapacity:3];
    }
    [arrType removeAllObjects];
    
    dispatch_queue_t queue = dispatch_queue_create("get_book_type_list", nil);
    dispatch_async(queue, ^(void){
        
        NSString *str_url = [NSString stringWithFormat:@"%@/%@?method=%d",kMAIN_URL,kBOOK_URL,9];
        NSString *res = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        
        if ([res length] <= 0) {
            [WSProgressHUD dismiss];
            [self.myTable reloadData];
        }
        else{
            NSArray *arr = [res JSONValue];
            [arrType addObjectsFromArray:arr];
            
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSMutableArray *arr_type = [NSMutableArray arrayWithCapacity:3];
                for (int i=0; i<[arrType count]; i++) {
                    NSDictionary *dic_type = [arrType objectAtIndex:i];
                    NSDictionary *dic_item = @{@"text":[dic_type valueForKey:@"type_name"],@"icon":@""};
                    [arr_type addObject:dic_item];
                }
                segType = [[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, [arr_type count]*75, 44)
                                                                   items:arr_type
                                                            iconPosition:IconPositionRight
                                                       andSelectionBlock:^(NSUInteger segmentInde){
                                                           [mjHead beginRefreshing];
                                                       }];
                
                
                segType.color=[UIColor colorWithWhite:.1 alpha:.9];
                segType.borderWidth=0.5;
                segType.borderColor=[UIColor darkGrayColor];
                segType.selectedColor=NAV_COLOR ;
                segType.textAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:14],
                                         NSForegroundColorAttributeName:[UIColor whiteColor]};
                segType.selectedTextAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:14],
                                                 NSForegroundColorAttributeName:[UIColor whiteColor]};
                
                float oy = 0;
                if ([AllMethods sysversion]>=7.0) {
                    oy = 64;
                }
                UIScrollView *scr = [[UIScrollView alloc] initWithFrame:CGRectMake(0, oy, ScreenWidth, 44)];
                [scr setContentSize:CGSizeMake(segType.frame.size.width, 1)];
                [scr addSubview:segType];
                [scr setPagingEnabled:YES];
                [scr setShowsHorizontalScrollIndicator:NO];
                [self.view addSubview:scr];
                [self.myTable reloadData];
                if (dicURL) {
                    [mjHead beginRefreshing];
                }
                
            });
            [WSProgressHUD dismiss];
        }
    });
}

- (void)getUpdateBookURL{
    dicURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"book_url"];
}

- (void)backUp{
    [mjHead free];
    [mjFoot free];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - MJRefreshBaseViewDelegate

- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
    if ([refreshView isEqual:mjHead]) {
        if (!arrList) {
            arrList = [[NSMutableArray alloc] initWithCapacity:3];
        }
        [arrList removeAllObjects];
        curPage = 0;
        [self getBookList];
    }
    else{
        if (!mjFoot.hidden) {
            curPage++;
            [self getBookList];
        }
    }
    
}

#pragma mark - SYS

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
#if kFREE == 1
    
    if (kFREE == 1 ) {
        
        CGRect frame = self.myTable.frame;
        frame.size.height = frame.size.height-48;
        self.myTable.frame = frame;
    }
#endif
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-48+24)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self getUpdateBookURL];
    [self getBookTypeList];
    [self getLocalBook];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 32, 32);
    [btn setBackgroundImage:[UIImage imageNamed:@"back_up.png"] forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn addTarget:self action:@selector(backUp) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    mjFoot = [[MJRefreshFooterView alloc] initWithScrollView:self.myTable];
    mjFoot.delegate = self;
    
    mjHead = [[MJRefreshHeaderView alloc] initWithScrollView:self.myTable];
    mjHead.delegate = self;
    
    self.navigationItem.title = @"添加图书";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
