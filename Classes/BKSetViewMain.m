//
//  BKSetViewMain.m
//  BookWorksApp
//
//  Created by 刁志远 on 16/5/17.
//
//

#import "BKSetViewMain.h"


#import "UIPopoverListView.h"


#import "About.h"



@interface BKSetViewMain () <UITableViewDelegate, UITableViewDataSource, UIPopoverListViewDelegate,UIPopoverListViewDataSource,UITextFieldDelegate> {
    
    NSMutableArray *arrList;
    
    UIImageView *ivHead;
    UITextField *txtNick;
    UILabel *labUID;
}

@property (nonatomic, weak) UITableView *myTable;

@end

@implementation BKSetViewMain


#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [popoverListView.listView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.text = @"阅读小说";
    cell.textLabel.font = [AllMethods getFontWithSize:16.5+indexPath.row];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE] floatValue] == 16.5+indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section{
    
    return 5;
}

#pragma mark - UIPopoverListViewDelegate


- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath{
    
    [[NSUserDefaults standardUserDefaults] setValue:@(16.5+indexPath.row) forKey:kFONT_SIZE];
    [popoverListView.listView reloadData];
    
    [popoverListView performSelector:@selector(dismiss) withObject:nil
                          afterDelay:.5f];
}


- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{ //设置字体大小
            CGFloat xWidth = self.view.bounds.size.width - 60.0f;
            CGFloat yHeight = self.view.bounds.size.height - 64 -100;
            CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
            
            UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
            poplistview.delegate = self;
            poplistview.datasource = self;
            [poplistview setTitle:NSLocalizedString(@"Set_Font", @"")];
            [poplistview show];

            break;
        }
        case 1:{//关于程序
            About *a = [[About alloc] init];
            [a setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:a animated:YES];
            break;
        }
        case 2:{//去广告
            NSString *surl = @"https://itunes.apple.com/cn/app/yue-du-xiao-shuo-mian-fei/id673879657?ls=1&mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:surl]];
            break;
        }
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (!kFREE) {
        return arrList.count-1;
    }
    
    return arrList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    cell.textLabel.text = arrList[indexPath.row];
    
    /*
    if (indexPath.row == 1) {//推送
        UISwitch *swi = [UISwitch new];
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:kCHAT_NOTI_KEY] intValue] == 0) {
            swi.on = NO;
        }
        else {
            swi.on = YES;
        }
        [swi addTarget:self action:@selector(swiChanged:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = swi;
    }
    else {
        cell.accessoryView = nil;
    }
     */
    
    return cell;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [btn addTarget:txtNick action:@selector(resignFirstResponder) forControlEvents:UIControlEventTouchDown];
    btn.tag = 1357;
    [self.view addSubview:btn];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:1357];
    [btn removeFromSuperview];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.text && textField.text.length > 0) {
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:textField.text forKey:@"nick"];
        txtNick.text = textField.text;
        
        [self updatePersonalInfo:textField.text];
        
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)updatePersonalInfo:(NSString *)nick {
    
}
- (void)updatePersonalHead:(NSString *)head {
}


#pragma mark - Custom Action

- (void)swiChanged:(id)sender {
    
}

- (void)showFontSize {
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = self.view.bounds.size.height - 44 -20 -40;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = self;
    poplistview.datasource = self;
    [poplistview setTitle:NSLocalizedString(@"Set_Font", @"")];
    [poplistview show];

}

- (void)showFeedback {
    
    //    [[Helpshift sharedInstance] showConversation:self withOptions:nil];
    
}

- (void)selectHead {
    
//    BKHeadList *head = [BKHeadList new];
//    [head setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:head animated:YES];
}

#pragma mark - Notification 

- (void)loginSuccessNotification:(NSNotification *)noti {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    labUID.text = [NSString stringWithFormat:@"ID:%@",[userDefaults valueForKey:@"username"]];
    
    if ([userDefaults valueForKey:@"head"]) {
        [ivHead sd_setImageWithURL:[userDefaults valueForKey:@"head"]];
        [self updatePersonalHead:[userDefaults valueForKey:@"head"]];
        
    }
    
    if ([userDefaults valueForKey:@"nick"]) {
        txtNick.text = [userDefaults valueForKey:@"nick"];
    }
}

#pragma mark - Build UI

- (void)buildQuestion {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 50)];
    view.backgroundColor = [UIColor clearColor];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = view.bounds;
    [btn setTitle:@"问题反馈" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [view addSubview:btn];
    
    [btn addTarget:self action:@selector(showFeedback) forControlEvents:UIControlEventTouchUpInside];
    
    self.myTable.tableFooterView = view;
    
}


- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
}


- (void)buildHeadView {
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 100)];
    view.backgroundColor = [UIColor whiteColor];
    
    ivHead = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 70, 70)];
    ivHead.backgroundColor = RGBACOLOR(238, 238, 238, 1);
    ivHead.clipsToBounds = YES;
    ivHead.layer.cornerRadius = ivHead.frame.size.height/2;
    ivHead.userInteractionEnabled = YES;
    [ivHead addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectHead)]];
    [view addSubview:ivHead];
    
    txtNick = [[UITextField alloc] initWithFrame:CGRectMake(ORIGINAL_X(ivHead)+10, 25, 100, 30)];
    txtNick.borderStyle = UITextBorderStyleNone;
    txtNick.placeholder = @"未设置";
    txtNick.returnKeyType = UIReturnKeyDone;
    txtNick.font = [AllMethods getFontWithSize:17];
    txtNick.delegate = self;
    [view addSubview:txtNick];
    
    labUID = [[UILabel alloc] initWithFrame:CGRectMake(txtNick.frame.origin.x, ORIGINAL_Y(txtNick),
                                                       ScreenWidth-txtNick.frame.origin.x-5, 20)];
    labUID.textColor = [UIColor lightGrayColor];
    labUID.font = [AllMethods getFontWithSize:12];
    [view addSubview:labUID];
    
    self.myTable.tableHeaderView = view;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults objectForKey:@"username"]) {
        labUID.text = [NSString stringWithFormat:@"ID:%@",[userDefaults objectForKey:@"username"]];
    }
    else {
        labUID.text = @"用户ID";
    }
    
    if ([userDefaults valueForKey:@"head"]) {
        
        [ivHead sd_setImageWithURL:[userDefaults valueForKey:@"head"]];
    }
    
    if ([userDefaults valueForKey:@"nick"]) {
        txtNick.text = [userDefaults valueForKey:@"nick"];
    }

}

#pragma mark - Initinal

- (void)initinalData {
    
    NSArray *arr = @[NSLocalizedString(@"Set_Font", nil),
                     NSLocalizedString(@"About", nil),
                     NSLocalizedString(@"NO_AD", nil)];
    
    arrList = [NSMutableArray arrayWithArray:arr];
    
}



#pragma mark - SYS

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self loginSuccessNotification:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setTitle:@"设置"];
    [self initinalData];
    [self buildTableView];
//    [self buildQuestion];
//    [self buildHeadView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginSuccessNotification:)
                                                 name:@"loginsuccess"
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
