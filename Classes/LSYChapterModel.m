//
//  LSYChapterModel.m
//  LSYReader
//
//  Created by Labanotation on 16/5/31.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYChapterModel.h"
#import "LSYReadConfig.h"


#import <TTTAttributedLabel/TTTAttributedLabel.h>

//#import "NSString+HTML.h"
//#include <vector>
@interface LSYChapterModel () {
    
    BOOL conLoad;//如果书籍页码正在加载，判断返回上一层时，是否需要继续加载。
    
    NSDictionary *dicAttribute;
    
    float pageWidth,pageHeight;//页面大小
    int lineNumber; //行数
    int curLineNumber; //当前行数
    NSUInteger curLoadPage;
    
    int numLengthPerPage;  //理想状态下 一页的字数
    
    CTTypesetterRef typeSetter;
    
    BOOL isLoadFinished;
    
    long curLoadLength;
}
@property (nonatomic,strong) NSMutableArray *pageArray;
@end

@implementation LSYChapterModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _pageArray = [NSMutableArray array];
        [_pageArray removeAllObjects];
        curLoadPage = 1;
    }
    return self;
}


-(void)setContent:(NSString *)content
{
    _content = content;
    [self loadBook];
    
}
-(void)updateFont
{
    [self loadBook];
    
}

- (void)loadBook{
    
    conLoad = YES;
    isLoadFinished = NO;
    curLoadLength = 0;
    
    //    NSLog(@"__ content length : %td",[_content length]);
    dispatch_queue_t queue = dispatch_queue_create("load_book_initinal", nil);
    dispatch_async(queue, ^{
        //        BOOL isFirst;//是否首次打开书籍
        
        
        LSYReadConfig *config = [LSYReadConfig shareInstance];
        dicAttribute = [LSYReadConfig parserAttribute:config];
        
        numLengthPerPage = (_contentRect.size.width/config.fontSize)*(_contentRect.size.height/config.fontSize);
        
        pageHeight = _contentRect.size.height-_fontSize.height;
        pageWidth = _contentRect.size.width;
        lineNumber = pageHeight/(_fontSize.height+config.lineSpace);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [self loadPageInQueue];
        });
        
    });
}

- (void)loadPageInQueue{
    
    curLineNumber = 0;
    dispatch_queue_t queue = dispatch_queue_create("get_page_list", nil);
    dispatch_async(queue, ^{
        
        BOOL bol = [self loadContentForPage:curLoadPage++];
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            if(bol && conLoad) {
                //                NSString *str = [NSString stringWithFormat:@"已加载%.0f%%",(curLength*100.0)/(1.0*[_content length])];
                [self loadPageInQueue];
            }
            else if(conLoad){//load finished
                [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                         selector:@selector(loadingCallback)
                                                           object:nil];
                isLoadFinished = YES;
                self.pageCount = self.pageArray.count;
                if (self.loadPageBlock) {
                    self.loadPageBlock(_pageArray.count,self);
                }
                
            }
        });
        
    });
    
    [self performSelector:@selector(loadingCallback)
               withObject:nil
               afterDelay:1.0f
                  inModes:@[NSDefaultRunLoopMode]];
}

- (void)loadingCallback {
    
    if (self.loadingBlock) {
        self.loadingBlock(curLoadLength);
    }
    
}

- (BOOL)loadContentForPage:(NSUInteger)page{
    
    NSString *all_content = _content;
    
    long offset = 0;
    if (page > 1 && _pageArray.count > page-2) {
        NSRange ran = NSRangeFromString(_pageArray[page-2]);
        //[_pageArray[page-2] rangeValue];
        offset = ran.location+ran.length;
    }
    
    if (offset >= [all_content length]) {
        return NO;
    }
    
    
    NSString *page_content=@"";
    
    if(offset + numLengthPerPage >= [all_content length]) {
        page_content = [all_content substringFromIndex:offset];
    }
    else page_content = [all_content substringWithRange:NSMakeRange(offset, numLengthPerPage)];
    
    /*break text 算法*/
    NSAttributedString *att_str = [[NSAttributedString alloc] initWithString:page_content
                                                                  attributes:dicAttribute];
    CTTypesetterRef ts = CTTypesetterCreateWithAttributedString((__bridge CFAttributedStringRef) att_str);
    
    long cur_len = 0;
    while (true) {
        CFIndex count = CTTypesetterSuggestLineBreak(ts, cur_len, pageWidth);
        cur_len+=count;
        curLineNumber ++;
        if (curLineNumber >= lineNumber) {
            break;
        }
        else if (offset+cur_len >= [all_content length]) {
            cur_len = [all_content length] - offset;
            break;
        }
    }
    
    CFRelease(ts);
    NSRange ran_res = NSMakeRange(offset, cur_len);
//    [_pageArray addObject:[NSValue valueWithRange:ran_res]];
    [_pageArray addObject:NSStringFromRange(ran_res)];
    _pageCount = _pageArray.count;
    curLoadLength+=cur_len;
//    NSLog(@"page  -->>>%td",_pageCount);
    return YES;
    
    /* 优化版查找算法  */
    
    
    
    /*
     CGSize size = [page_content boundingRectWithSize:CGSizeMake(_contentRect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dicAttribute
     context:nil].size;
     
     if (size.height > _contentRect.size.height) {
     
     while (size.height - _contentRect.size.height > fontHeight) {
     cur_len -= widPerLine/2;
     page_content = [all_content substringWithRange:NSMakeRange(offset, cur_len)];
     
     
     size = [page_content boundingRectWithSize:CGSizeMake(_contentRect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dicAttribute
     context:nil].size;
     }
     
     }
     else if(size.height < _contentRect.size.height) {
     
     if (!is_end_of_the_content) {
     while (_contentRect.size.height - size.height > fontHeight) {
     cur_len += widPerLine/2;
     
     if(offset + cur_len >= [all_content length]) {
     
     page_content = [all_content substringFromIndex:offset];
     cur_len = [all_content length] - offset;
     
     size = [page_content boundingRectWithSize:CGSizeMake(_contentRect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dicAttribute
     context:nil].size;
     break;
     }
     else page_content = [all_content substringWithRange:NSMakeRange(offset, cur_len)];
     
     size = [page_content boundingRectWithSize:CGSizeMake(_contentRect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dicAttribute
     context:nil].size;
     }
     }
     else {
     [_pageArray addObject:@(offset+cur_len)];
     return YES;
     }
     
     }
     
     int add_font = widPerLine/2;
     BOOL end = YES;
     if (size.height > _contentRect.size.height) {
     end = NO;
     }
     
     int type;
     
     while (true) {
     
     if (size.height > _contentRect.size.height) {
     
     if (add_font == 2 && end) {
     type = 0;
     break;
     }
     
     cur_len -= add_font;
     }
     else {
     cur_len += add_font;
     if(offset + cur_len >= [all_content length]) {
     if(add_font == 2){
     type = 1;
     break;
     }
     else {
     cur_len -= add_font;
     }
     }
     else if (add_font == 2 && !end) {
     type = 2;
     cur_len-=add_font;
     break;
     }
     }
     if (add_font != 2) {
     add_font /= 2;
     }
     if (add_font < 2) {
     add_font = 2;
     }
     
     if (offset+cur_len > [all_content length]) {
     cur_len = [all_content length] - offset;
     }
     
     page_content = [all_content substringWithRange:NSMakeRange(offset, cur_len)];
     
     size = [page_content boundingRectWithSize:CGSizeMake(_contentRect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dicAttribute
     context:nil].size;
     }
     
     float ori_hei = size.height;
     
     if (type == 0) {// 内容高度大于frame高度
     
     while (true) {
     cur_len--;
     page_content = [all_content substringWithRange:NSMakeRange(offset, cur_len)];
     
     size = [page_content boundingRectWithSize:CGSizeMake(_contentRect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dicAttribute
     context:nil].size;
     if (size.height < ori_hei) {
     break;
     }
     }
     
     }
     else if(type == 1){//内容到尾
     cur_len = [all_content length]-offset;
     }
     else if (type == 2) {//内容高度小于frame高度
     
     while (true) {
     cur_len++;
     page_content = [all_content substringWithRange:NSMakeRange(offset, cur_len)];
     size = [page_content boundingRectWithSize:CGSizeMake(_contentRect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dicAttribute
     context:nil].size;
     if (size.height > ori_hei) {
     cur_len--;
     break;
     }
     }
     
     }
     
     numPerPage = cur_len;
     [_pageArray addObject:@(offset+cur_len)];
     
     return YES;
     
     
     NSDictionary *dic_att = [LSYReadParser parserAttribute:[LSYReadConfig shareInstance]];
     
     CGRect rect = CGRectMake(LeftSpacing, TopSpacing, [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing);
     
     long len = numPerPage;
     NSRange ran_long = NSMakeRange(0, 0);
     NSRange ran_short = NSMakeRange(0, 0);
     
     if (len+offset <= [all_content length]) {
     
     while (true) {
     NSRange ran = NSMakeRange(offset, len);
     
     NSString *sc = [all_content substringWithRange:ran];
     
     CGSize sz;
     @autoreleasepool {
     sz = [sc boundingRectWithSize:CGSizeMake(rect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dic_att
     context:nil].size;
     }
     
     
     if (sz.height > rect.size.height) {
     ran_long = ran;
     if (ran_short.length+ran_short.length > 0) {
     len = (ran_long.length+ran_short.length)/2;
     }
     else {
     len-=widPerLine;
     }
     }
     else {
     ran_short = ran;
     if (ran_long.length+ran_long.length > 0) {
     len = (ran_long.length+ran_short.length)/2;
     }
     else {
     len+=widPerLine;
     if (len+offset>=[all_content length]) {
     if (ran_short.length == [all_content length]-offset) {
     //                            NSValue *val = [NSValue valueWithRange:ran_short];
     //                            [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
     [_pageArray addObject:@(ran_short.location+ran_short.length)];
     curLength+=ran_short.length;
     break;
     }
     else {
     len = [all_content length]-offset;
     }
     }
     }
     }
     
     if (fabs(ran_long.length-ran_short.length) <= 1) {
     //                NSValue *val = [NSValue valueWithRange:ran_short];
     //                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
     [_pageArray addObject:@(ran_short.location+ran_short.length)];
     curLength+=ran_short.length;
     break;
     }
     }
     
     return YES;
     }
     else {
     len = [all_content length]-offset;
     while (true) {
     NSRange ran = NSMakeRange(offset, len);
     NSString *sc = [all_content substringWithRange:ran];
     
     CGSize sz;
     @autoreleasepool {
     sz = [sc boundingRectWithSize:CGSizeMake(rect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dic_att
     context:nil].size;
     }
     
     if (sz.height > rect.size.height) {
     ran_long = ran;
     if (ran_short.length+ran_short.length > 0) {
     len = (ran_long.length+ran_short.length)/2;
     }
     else {
     len-=widPerLine;
     }
     }
     else {
     ran_short = ran;
     if (ran_long.length+ran_long.length > 0) {
     len = (ran_long.length+ran_short.length)/2;
     }
     else {
     //                    NSValue *val = [NSValue valueWithRange:ran_short];
     //                    [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
     [_pageArray addObject:@(ran_short.location+ran_short.length)];
     curLength+=ran_short.length;
     break;
     }
     }
     if (fabs(ran_long.length-ran_short.length) <= 1) {
     //                NSValue *val = [NSValue valueWithRange:ran_short];
     //                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
     [_pageArray addObject:@(ran_short.location+ran_short.length)];
     curLength+=ran_short.length;
     break;
     }
     }
     return YES;
     }
     
     NSRange ran = NSMakeRange(offset, [all_content length]-offset);
     
     
     
     NSString *sc = [all_content substringWithRange:ran];
     
     CGSize sz;
     @autoreleasepool {
     sz = [sc boundingRectWithSize:CGSizeMake(rect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dic_att
     context:nil].size;
     }
     
     if (sz.height < rect.size.height) {
     ran_short = ran;
     if (ran_long.length+ran_long.length > 0) {
     len = (ran_long.length+ran_short.length)/2;
     }
     else {
     //            NSValue *val = [NSValue valueWithRange:ran_short];
     //            [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
     [_pageArray addObject:@(ran_short.location+ran_short.length)];
     curLength+=ran_short.length;
     }
     return YES;
     
     }
     else {
     len = [all_content length]-offset-widPerLine;
     while (true) {
     NSRange ran = NSMakeRange(offset, len);
     NSString *sc = [all_content substringWithRange:ran];
     
     CGSize sz;
     @autoreleasepool {
     sz = [sc boundingRectWithSize:CGSizeMake(rect.size.width, CGFLOAT_MAX)
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:dic_att
     context:nil].size;
     }
     
     if (sz.height > rect.size.height) {
     ran_long = ran;
     if (ran_short.length+ran_short.length > 0) {
     len = (ran_long.length+ran_short.length)/2;
     }
     else {
     len-=widPerLine;
     }
     }
     else {
     ran_short = ran;
     if (ran_long.length+ran_long.length > 0) {
     len = (ran_long.length+ran_short.length)/2;
     }
     else {
     len+=widPerLine;
     }
     }
     if (fabs(ran_long.length-ran_short.length) <= 1) {
     //                NSValue *val = [NSValue valueWithRange:ran_short];
     //                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
     [_pageArray addObject:@(ran_short.location+ran_short.length)];
     curLength+=ran_short.length;
     break;
     }
     }
     return YES;
     }
     
     */
}

#pragma mark - Public

- (void)addPagesFromeRecord:(NSArray *)arr {
    
    [_pageArray addObjectsFromArray:arr];
    curLoadPage = [_pageArray count];
}

- (NSInteger)getIsFinishLoad {
    
    if (isLoadFinished) {
        return 1;
    }
    return 0;
}

- (NSArray *)getPageList {
    
    return _pageArray;
}

-(NSString *)stringOfPage:(NSUInteger)index {
    
    NSUInteger local_end = [_pageArray[index] integerValue];
    
    if (index == 0) {
        return [_content substringWithRange:NSMakeRange(0, local_end)];
    }
    
    NSUInteger local_start = [_pageArray[index-1] integerValue];
    NSUInteger length = local_end - local_start;
    
    return [_content substringWithRange:NSMakeRange(local_start, length)];
}


#pragma mark - SYS -->>>> Archive


-(id)copyWithZone:(NSZone *)zone
{
    LSYChapterModel *model = [[LSYChapterModel allocWithZone:zone] init];
    model.content = self.content;
    model.title = self.title;
    model.pageCount = self.pageCount;
    model.pageArray = self.pageArray;
    return model;
    
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.content forKey:@"content"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeInteger:self.pageCount forKey:@"pageCount"];
    [aCoder encodeObject:self.pageArray forKey:@"pageArray"];
    /**
     @property (nonatomic,copy) NSArray *epubframeRef;
     @property (nonatomic,copy) NSString *epubImagePath;
     @property (nonatomic,copy) NSArray <LSYImageData *> *imageArray;
     
     */
    //    [aCoder encodeObject:self.epubframeRef forKey:@"epubframeRef"];
    //    [aCoder encodeObject:self.epubImagePath forKey:@"epubImagePath"];
    
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _content = [aDecoder decodeObjectForKey:@"content"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.pageCount = [aDecoder decodeIntegerForKey:@"pageCount"];
        self.pageArray = [aDecoder decodeObjectForKey:@"pageArray"];
    }
    return self;
}
@end

