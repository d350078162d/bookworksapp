//
//  BKChapterReader.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/25.
//
//

#import "BKChapterReader.h"

#import "LSYChapterModel.h"
#import "LSYReadConfig.h"

#import "RJBookData.h"

#import "BKNewReaderMenu.h"
#import "BKNewChapterList.h"

#import "HorizontalCell.h"

#import "HorizontalFoot.h"
#import "HorizontalHead.h"

#define ZY_HEAD_FOOT_WIDTH 64.0

@interface BKChapterReader () <UICollectionViewDelegate, UICollectionViewDataSource,BKNewReaderMenuDelegate,BKNewChapterListDelegate>{
    
    NSMutableArray *arrChapters;
    NSDictionary *dicBookInfo;
    NSInteger curChapter,curPage;
    NSDictionary *dicBookDetail;
    
    NSString *strBookID;
    
    NSString *allContent;
    
    NSMutableDictionary *dicAttribute;            //字体样式、字间距、行间距、字体大小
    
    CGSize fontSize;                       //字体的尺寸
    BKNewReaderMenu *readMenu;             //菜单
    
    long curLoadContentLength;             //当前以加载内容长度
    
    NSMutableDictionary *dicChapters;      //分页信息
    CGSize contentSize;                    //内容页面大小
    BOOL isChangeFontSize;                 //记录当前是否是改变字体进行分页
    long lastReadLengthBeforeChangeSize;   //记录改变字体之前阅读的位置
    
    BKNewChapterList *chapterList;         //目录页面
    
    BOOL conLoad;                          //是否继续分页
    NSInteger loadThread;                  //分页线程数
    NSInteger allPages;                     //全部页数
}

@property (nonatomic, weak) UICollectionView *myCollect;


@property (nonatomic, strong) HorizontalFoot *footer;
@property (nonatomic, strong) HorizontalHead *header;

@end

@implementation BKChapterReader



#pragma mark - BKNewChapterListDelegate

- (void)chapterDidSelectChapter:(NSInteger )index {
    
//    curChapter = index;
    [self loadChapter:index];
//    curPage = 0;
//    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:curPage inSection:curChapter];
//    [self.myCollect scrollToItemAtIndexPath:indexPath
//                           atScrollPosition:UICollectionViewScrollPositionNone
//                                   animated:NO];
//    
    [chapterList dismissView];
}

#pragma mark - BKNewReaderMenuDelegate

- (void)menuWillShow {
    
    
#if kFREE
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del setADHidden:YES];
#endif
    
}


- (void)menuDidDismiss {

#if kFREE
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del setADHidden:NO];
#endif
    
}

- (void)menuDidChangeTheme {
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    NSDictionary *dic_theme = config.arrThemes[config.theme];
    
    dicAttribute[NSForegroundColorAttributeName] = [UIColor colorFromeString:dic_theme[@"font_color"]];
    
    HorizontalCell *cell;
    NSArray *arr = [self.myCollect visibleCells];
    if (arr.count > 0) {
        cell = [arr lastObject];
    }
    else {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:curPage inSection:0];
        cell = (HorizontalCell *)[self.myCollect cellForItemAtIndexPath:indexPath];
    }
    
    if (!cell) {
        return;
    }
    
    UIColor *col = [UIColor colorFromeString:dic_theme[@"font_color"]];
    
    if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
        cell.ivBk.image = nil;
        cell.backgroundColor = [UIColor colorFromeString:dic_theme[@"bk"]];
        
        cell.labTitle.textColor = [col mix:cell.backgroundColor];
        cell.labPage.textColor = cell.labTitle.textColor;
    }
    else {
        
        NSString *sname = [NSString stringWithFormat:@"%@.jpg",dic_theme[@"bk"]];
        cell.ivBk.image = ImageNamed(sname);
        
        cell.labTitle.textColor = [col lighten];
        cell.labPage.textColor = cell.labTitle.textColor;
    }
    
    NSAttributedString *str = cell.labContent.attributedText;
    NSMutableAttributedString *att_str = [[NSMutableAttributedString alloc] initWithAttributedString:str];
    [att_str setAttributes:dicAttribute range:NSMakeRange(0, [att_str length])];
    cell.labContent.attributedText = att_str;
    
    
    //    [self.myCollect performBatchUpdates:^{
    //        [_myCollect reloadData];
    //    } completion:^(BOOL finished) {
    //
    //    }];
}

- (void)menuDidClickedBack {
    
#if kFREE
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-48-49+24)];
    del.canHideAd = NO;
    [del setADHidden:NO];
#endif
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)menuFontSizeChanged:(float)size {
    
    if (loadThread > 0) {//还在加载中
        
        [WSProgressHUD showWithStatus:@"Loading..." maskType:WSProgressHUDMaskTypeClear];
        
        conLoad = NO;
        
        __weak BKChapterReader *weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.75 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf menuFontSizeChanged:size];
        });
        return;
    }
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
//    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:bookIndex];
//    NSString *file_path = [AllMethods applicationDocumentsDirectory];
//    pagesPath = [NSString stringWithFormat:@"%@/%@-%.0f.chr",file_path,book.name,config.fontSize];
    
    [WSProgressHUD dismiss];
    conLoad = YES;
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06d",0];
    NSDictionary *dic = dicChapters[key];
    NSArray *arr = dic[@"pages"];
    NSRange ran_content = NSRangeFromString(arr[curPage]);
    //[arr[curPage] rangeValue];
    NSRange ran_chap = NSRangeFromString(dic[@"range"]);
    
    lastReadLengthBeforeChangeSize = ran_content.location + ran_chap.location;
    
    curLoadContentLength = 0;
    loadThread = 0;
//    [[NSFileManager defaultManager] removeItemAtPath:pagesPath error:nil];
    
    config.fontSize = size;
    dicAttribute[NSFontAttributeName] = [AllMethods getFontWithSize:size];
    
    NSString *str = @"阅";
    fontSize = [str sizeWithAttributes:dicAttribute];
    
    
    isChangeFontSize = YES;
    
    loadThread++;
    [self loadBookPages];
}


- (void)menuDidClickedCatagory {
    
    [readMenu dismissView];
    
    if (!chapterList) {
        chapterList = [[BKNewChapterList alloc] initWithOnlineChapterList:arrChapters];
        chapterList.delegate = self;
    }
    else {
        [chapterList setChapterList:dicChapters];
    }
    
    [chapterList showInView:self.view];
    
}

#pragma mark header footer相关

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([[dicChapters allValues] count] <= 0) {
        return;
    }
    NSDictionary *dic_chap = [[dicChapters allValues] objectAtIndex:0];
    NSInteger itemCount = [dic_chap[@"cnt"] intValue];
    if (scrollView.contentOffset.x > ScreenWidth*(itemCount-1)) {//最后一页
        
        CGFloat footerDisplayOffset = (scrollView.contentOffset.x - (self.myCollect.frame.size.width * (itemCount - 1)));
        
        // footer的动画
        if (footerDisplayOffset > 0)
        {
            // 开始出现footer
            if (footerDisplayOffset > ZY_HEAD_FOOT_WIDTH) {
                self.footer.state = HorizontalFooterStateTrigger;
            } else {
                self.footer.state = HorizontalFooterStateIdle;
            }
        }
        
    }
    else if(scrollView.contentOffset.x < 0){ //第一页
        
        CGFloat footerDisplayOffset = -scrollView.contentOffset.x;
        
        // footer的动画
        if (footerDisplayOffset > 0)
        {
            // 开始出现footer
            if (footerDisplayOffset > ZY_HEAD_FOOT_WIDTH) {
                self.header.state = HorizontalHeaderStateTrigger;
            } else {
                self.header.state = HorizontalHeaderStateIdle;
            }
        }
    }
    
//    static CGFloat lastOffset;
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    if ([[dicChapters allValues] count] <= 0) {
        return;
    }
    NSDictionary *dic_chap = [[dicChapters allValues] objectAtIndex:0];
    NSInteger itemCount = [dic_chap[@"cnt"] intValue];
    
    if (scrollView.contentOffset.x > ScreenWidth*(itemCount-1)) {//最后一页
        
        CGFloat footerDisplayOffset = (scrollView.contentOffset.x - (self.myCollect.frame.size.width * (itemCount - 1)));
        
        // 通知footer代理
        if (footerDisplayOffset > ZY_HEAD_FOOT_WIDTH) {
            
            NSInteger index = curChapter;
            index++;
            [self loadChapter:index];
        }
        
    }
    else if (scrollView.contentOffset.x < 0) {
        
        CGFloat footerDisplayOffset = -scrollView.contentOffset.x;
        if (footerDisplayOffset > ZY_HEAD_FOOT_WIDTH) {
            
            NSInteger index = curChapter;
            index--;
            [self loadChapter:index];
        }
    }
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!readMenu) {
        [self initinalMenu];
    }
    else [readMenu showInController:self];
    
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    curPage = indexPath.item;
    
    HorizontalCell *hcell = (HorizontalCell *)cell;
    
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",indexPath.section];
    NSDictionary *dic = dicChapters[key];
    
    hcell.labTitle.text = dic[@"title"];
    
    NSArray *arr = dic[@"pages"];
    
    hcell.labPage.text = [NSString stringWithFormat:@"%td/%td",
                          indexPath.item+1,[arr count]];
    
//    if (arr.count == indexPath.item+1) {//最后一页
//        hcell.labContent.verticalAlignment = VerticalAlignmentTop;
//    }
//    else {
//        hcell.labContent.verticalAlignment = VerticalAlignmentMiddle;
//    }
    
    
    NSRange ran_content = NSRangeFromString(arr[indexPath.row]);
    NSRange ran_chap = NSRangeFromString(dic[@"range"]);
    NSString *str = [[allContent substringWithRange:ran_chap] substringWithRange:ran_content];
    
    hcell.labContent.frame = CGRectMake(10, 40, contentSize.width, contentSize.height);
    hcell.labContent.attributedText = [[NSAttributedString alloc] initWithString:str
                                                                      attributes:dicAttribute];
    
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06td",section];
    NSDictionary *dic = dicChapters[key];
    
    if (!dic) {
        return 0;
    }
    
    return [dic[@"cnt"] intValue];
    
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HorizontalCell *hcell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PageCell"
                                                                     forIndexPath:indexPath];
    
    
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    NSDictionary *dic_theme = config.arrThemes[config.theme];
    
    UIColor *col = [UIColor colorFromeString:dic_theme[@"font_color"]];
    if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
        if (hcell.ivBk.image) {
            hcell.ivBk.image = nil;
        }
        if (![[hcell.backgroundColor colorToString] isEqualToString:dic_theme[@"bk"]]) {
            hcell.backgroundColor = [UIColor colorFromeString:dic_theme[@"bk"]];
            
            hcell.labTitle.textColor = [col mix:hcell.backgroundColor];
            hcell.labPage.textColor = hcell.labTitle.textColor;
        }
        
    }
    else {
        NSString *sname = [NSString stringWithFormat:@"%@.jpg",dic_theme[@"bk"]];
        hcell.ivBk.image = ImageNamed(sname);
        
        hcell.labTitle.textColor = [col lighten];
        hcell.labPage.textColor = hcell.labTitle.textColor;
    }
    
    return hcell;
    
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
    //    return [[dicChapters allKeys] count];
    
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(ScreenWidth, ScreenHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}


#pragma mark - Seperate Pages 

- (void)loadBookPages {
    
    if (!dicChapters) {
        dicChapters = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
    LSYChapterModel *model = [[LSYChapterModel alloc] init];
    model.chapterIndex = 0;
    model.contentRect = CGRectMake(0, 0, contentSize.width,contentSize.height);
    model.fontSize = fontSize; //字体长宽数据
    
    NSString *key = [NSString stringWithFormat:@"chapter_%06d",0];
    
    __weak LSYChapterModel *weak_model = model;
    __weak BKChapterReader *weakSelf = self;
    
    NSRange content_range = NSMakeRange(0, [allContent length]);
    
    
    [model setLoadPageBlock:^(long cnt,LSYChapterModel *wmodel){
        
        
        NSDictionary *dic_info = @{@"cnt":[NSString stringWithFormat:@"%td",weak_model.pageCount],
                                   @"title":dicBookInfo[@"name"],
                                   @"pages":[weak_model getPageList],
                                   @"range":NSStringFromRange(content_range),
                                   @"length":[NSString stringWithFormat:@"%ld",content_range.length],
                                   @"finish":[NSString stringWithFormat:@"%td",[weak_model getIsFinishLoad]]};
        
        NSInteger itemCount = weak_model.pageCount;
        
        CGRect frame = weakSelf.footer.frame;
        frame.origin.x = (itemCount)*ScreenWidth;
        weakSelf.footer.frame = frame;
        
        [dicChapters setObject:dic_info forKey:key];
        
        
        curLoadContentLength = allContent.length;
        
        [weakSelf.myCollect performBatchUpdates:^{
            [weakSelf.myCollect reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } completion:^(BOOL finished) {
            
            [weakSelf.myCollect scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
            
            if (isChangeFontSize && finished &&
                content_range.location < lastReadLengthBeforeChangeSize &&
                content_range.length+content_range.location > lastReadLengthBeforeChangeSize) {
                
                isChangeFontSize = NO;
                
                NSArray *arr_pages = dic_info[@"pages"];
                for (int i=0; i<arr_pages.count; i++) {
                    NSRange ran = NSRangeFromString(arr_pages[i]);
                    //[arr_pages[i] rangeValue];
                    if (ran.location+ran.length+content_range.location > lastReadLengthBeforeChangeSize) {
                        
                        lastReadLengthBeforeChangeSize=0;
                        
                        NSIndexPath *index_path = [NSIndexPath indexPathForItem:i
                                                                      inSection:0];
                        [weakSelf.myCollect scrollToItemAtIndexPath:index_path
                                                   atScrollPosition:UICollectionViewScrollPositionNone
                                                           animated:NO];
                        break;
                    }
                }
            }
            
        }];
        
        if (loadThread > 0) {
            loadThread --;
        }
    }];
    
    model.content = allContent;
}

#pragma mark - Private




- (void)loadChapter:(NSInteger )index {
    
    if (index < 0) {
        [WSProgressHUD showErrorWithStatus:@"已经是第一章了"];
        return;
    }
    if (index >= arrChapters.count) {
        [WSProgressHUD showErrorWithStatus:@"已经是最后一章了"];
        return;
    }
    
    curChapter = index;
//    [self.myCollect scrollToPage:0 duration:0];
    dicBookInfo = arrChapters[index];
    [readMenu setLabTitle:dicBookInfo[@"name"]];
    allContent = nil;
    
//    [self initinalData];
    [self loadBook];
}


- (void)updateReadChapter {
    
    
    NSDictionary *dic = @{@"id":dicBookDetail[@"href"],@"name":dicBookDetail[@"name"],
                          @"chap_href":dicBookInfo[@"href"],
                          @"chap_name":dicBookInfo[@"name"],
                          @"chap_index":@(curChapter),
                          @"aut_name":dicBookDetail[@"aut_name"],
                          @"detail_href":dicBookDetail[@"href"],
                          @"img":dicBookDetail[@"img"],
                          @"cata_name":@"",
                          @"cata_href":@""};
    
    if ([AllMethods saveNewUpdateInfo:dic]) {
//        [WSProgressHUD showSuccessWithStatus:@"添加成功"];
    }
    
}

- (void)loadBook {
    
    [self updateReadChapter];
    [WSProgressHUD showWithStatus:@"Loading..." maskType:WSProgressHUDMaskTypeClear];
    
    NSString *surl = [NSString stringWithFormat:@"%@&action=content&href=%@/%@",
                      [AllMethods updateURL],dicBookDetail[@"href"],dicBookInfo[@"href"]];
    
    
    __weak BKChapterReader *weakSelf = self;
    [AllMethods requestWithType:REQUEST_GET
                        andPara:nil
                         andUrl:surl andGetDataBlock:^(id object) {
                             [WSProgressHUD dismiss];
                             
                             NSDictionary *dic = object;
                             
                             if (![dic isKindOfClass:[NSDictionary class]]) {
                                 
                                 [AllMethods showAltMsg:[NSString stringWithFormat:@"%@",object]];
                                 return ;
                             }
                             
                             allContent = [NSString stringWithFormat:@"%@",
                                           dic[@"content"]];
                             allContent = [allContent stringByReplacingOccurrencesOfString:@"\r\n" withString:@"\n"];
                             allContent = [allContent stringByReplacingOccurrencesOfString:@"\n\r" withString:@"\n"];
                             allContent = [allContent stringByReplacingOccurrencesOfString:@"\n\t" withString:@"\n"];
                             allContent = [allContent stringByReplacingOccurrencesOfString:@"\t\n" withString:@"\n"];
                             NSArray *arr = [allContent componentsSeparatedByString:@"\n"];
                             
                             allContent = [arr componentsJoinedByString:@"\n    "];

                             
                             [weakSelf loadBookPages];
                             
                         }
     ];
}


#pragma mark - Build UI

- (void)buildContentView {
    
    
    UICollectionViewFlowLayout *lay = [UICollectionViewFlowLayout new];
    lay.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    lay.itemSize = CGSizeMake(ScreenWidth, ScreenHeight);
    lay.minimumLineSpacing = 0;
    lay.minimumInteritemSpacing = 0;
//    lay.footerReferenceSize = CGSizeMake(ZY_HEAD_FOOT_WIDTH, ScreenHeight);
//    lay.headerReferenceSize = CGSizeMake(ZY_HEAD_FOOT_WIDTH, ScreenHeight);
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) collectionViewLayout:lay];
    [collect registerNib:[UINib nibWithNibName:@"HorizontalCell"
                                        bundle:nil]
forCellWithReuseIdentifier:@"PageCell"];
    collect.pagingEnabled = YES;
    collect.delegate = self;
    collect.dataSource = self;
    collect.backgroundColor = [UIColor whiteColor];
    collect.decelerationRate = UIScrollViewDecelerationRateFast;
    collect.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:collect];

    
    self.myCollect = collect;
    
    _header = [[HorizontalHead alloc] initWithFrame:CGRectMake(-ZY_HEAD_FOOT_WIDTH, 0, ZY_HEAD_FOOT_WIDTH, ScreenHeight)];
    _footer = [[HorizontalFoot alloc] initWithFrame:CGRectMake(-ZY_HEAD_FOOT_WIDTH, 0, ZY_HEAD_FOOT_WIDTH, ScreenHeight)];
    
    [self.myCollect addSubview:_header];
    [self.myCollect addSubview:_footer];
    
}


#pragma mark - Init

- (void)initinalData {
    
    allPages = 0;
    loadThread = 0;
    lastReadLengthBeforeChangeSize = 0;
    isChangeFontSize = NO;
    conLoad = YES;
    
    curLoadContentLength = 0;
    
    LSYReadConfig *config = [LSYReadConfig shareInstance];
    dicAttribute = [NSMutableDictionary dictionaryWithDictionary:[LSYReadConfig parserAttribute:config]];
    
    contentSize = CGSizeMake(ScreenWidth-20, ScreenHeight-20-20-20);
#if kFREE
    contentSize = CGSizeMake(ScreenWidth-20, ScreenHeight-20-20-20-50);
#endif
    
    NSString *str = @"阅";
    fontSize = [str sizeWithAttributes:dicAttribute];
    
    NSDictionary *dic_theme = [config.arrThemes objectAtIndex:config.theme];
    
    if ([dic_theme[@"bk_type"] isEqualToString:@"color"]) {
        self.view.backgroundColor = [UIColor colorFromeString:dic_theme[@"bk"]];
    }
    
}

- (void)initinalMenu {
    
    readMenu = [BKNewReaderMenu new];
    readMenu.delegate = self;
    [readMenu setLabTitle:dicBookInfo[@"name"]];
    [readMenu showInController:self];
    
}

#pragma mark - Status Bar 

- (BOOL)prefersStatusBarHidden {
    
    return readMenu.isHidden;
    
}

#pragma mark - SYS

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [readMenu dismissView];
//    });
}

- (id)initWithChapterList:(NSArray *)arr andChapterIndex:(NSInteger )index andBookInfo:(NSDictionary *)dic {
    
    self = [super init];
    if (self) {
        curChapter = index;
        arrChapters = [NSMutableArray arrayWithArray:arr];
        dicBookInfo = arrChapters[index];
        dicBookDetail = [NSDictionary dictionaryWithDictionary:dic];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self initinalData];
    [self buildContentView];
    [self initinalMenu];
    
    [self loadBook];
    
#if kFREE
    //移动广告条
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-20-SIZE_H([del getADView])/2-5)];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
