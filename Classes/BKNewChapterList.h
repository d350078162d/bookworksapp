//
//  BKNewChapterList.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/22.
//
//

#import <UIKit/UIKit.h>

@protocol BKNewChapterListDelegate ;

typedef NS_ENUM(NSInteger,ChapterType) {
    CHAPTER_LOCAL=0,
    CHAPTER_ONLINE,
};

@interface BKNewChapterList : UIView


@property (nonatomic, strong) id<BKNewChapterListDelegate> delegate;


- (id)initWithChapterList:(NSDictionary *)dic_chapter;
- (id)initWithOnlineChapterList:(NSArray *)arr;

- (void)setChapterList:(NSDictionary *)dic_chapter;

- (void)showInView:(UIView *)view;
- (void)dismissView;

@end


@protocol BKNewChapterListDelegate <NSObject>

@optional
- (void)chapterDidSelectChapter:(NSInteger )index;

@end



