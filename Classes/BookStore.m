
//
//  BookStore.m
//  BookWorksApp
//
//  Created by 刁志远 on 15/5/12.
//
//

#import "BookStore.h"


#import "RTLabel.h"

#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"

#import "Unrar4iOS.h"
#import "RARExtractException.h"


#import "UIButton+Badge.h"
#import "MZDownloadManagerViewController.h"

#import "BKNewMainBookList.h"

@interface BookStore ()<UIWebViewDelegate>{
    
    UIWebView *webView;
    UIActivityIndicatorView *actView;
    NSString *curSelName;
    
    
    RTLabel *labInfo;
    NSMutableArray *arrDownload;
    
    ASINetworkQueue *queueDownload;
    UIProgressView *progressDownload;
    
    UIActivityIndicatorView *actViewDownload;
    
    UIButton *btnDownload;
    
    NSString *curRefer;
}

@end

@implementation BookStore


#pragma mark - UIWebViewDelegate

- (NSDictionary*)dictionaryFromQuery:(NSString*)query usingEncoding:(NSStringEncoding)encoding {
    NSCharacterSet* delimiterSet = [NSCharacterSet characterSetWithCharactersInString:@"&;"];
    NSMutableDictionary* pairs = [NSMutableDictionary dictionary];
    NSScanner* scanner = [[NSScanner alloc] initWithString:query];
    while (![scanner isAtEnd]) {
        NSString* pairString = nil;
        [scanner scanUpToCharactersFromSet:delimiterSet intoString:&pairString];
        [scanner scanCharactersFromSet:delimiterSet intoString:NULL];
        NSArray* kvPair = [pairString componentsSeparatedByString:@"="];
        if (kvPair.count == 2) {
            NSString* key = [[kvPair objectAtIndex:0]
                             stringByReplacingPercentEscapesUsingEncoding:encoding];
            NSString* value = [[kvPair objectAtIndex:1]
                               stringByReplacingPercentEscapesUsingEncoding:encoding];
            [pairs setObject:value forKey:key];
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:pairs];
}

- (BOOL)webView:(UIWebView *)web shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"page-download\").innerHTML;"];
    if (title && [title length]>0) {
        
        NSString *href = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"page-href\").innerHTML;"];
        NSString *img = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"page-img\").innerHTML;"];
        NSString *author = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"page-aut\").innerHTML;"];
        
        NSDictionary *dic_info = @{@"id":title,@"name":title,
                                   @"href":href,@"img":img,
                                   @"aut_name":author};
        [AllMethods saveDownloadInfo:dic_info];
        
        
        NSString *str = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"page-refer\").innerHTML;"];
        
        curRefer = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (arrDownload && [arrDownload containsObject:request.URL]) {
            return YES;
        }
        [self addDownloadWithName:title andUrl:request.URL];
        [self goBackAction:nil];
        NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
        btnDownload.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
        return NO;
    }
//    else if ([title isEqualToString:@"书库"] || [title isEqualToString:@"搜索"]){
//        NSDictionary *dic = [self dictionaryFromQuery:[request.URL absoluteString] usingEncoding:NSUTF8StringEncoding];
//        curSelName = [NSString stringWithFormat:@"%@",dic[@"dn"]];
//    }
    
    
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)web{
    
    [actView startAnimating];
    UIButton *btn = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    [btn setImage:nil forState:UIControlStateNormal];
    
    if ([webView canGoBack]) {
        self.navigationItem.leftBarButtonItem.customView.hidden = NO;
    }
    else {
        self.navigationItem.leftBarButtonItem.customView.hidden = YES;
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)web{
    
    [actView stopAnimating];
    UIButton *btn = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    [btn setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    if ([webView canGoBack]) {
        self.navigationItem.leftBarButtonItem.customView.hidden = NO;
    }
    else {
        self.navigationItem.leftBarButtonItem.customView.hidden = YES;
    }
}
- (void)webView:(UIWebView *)web didFailLoadWithError:(NSError *)error{
    
    [actView stopAnimating];
    UIButton *btn = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    [btn setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    if ([webView canGoBack]) {
        self.navigationItem.leftBarButtonItem.customView.hidden = NO;
    }
    else {
        self.navigationItem.leftBarButtonItem.customView.hidden = YES;
    }
}

#pragma mark - Update UI

- (void)updateDownloadInfo {
    
    if (arrDownload.count <= 0) {
        labInfo.text = @"还没有下载任务";
        return;
    }
    
    NSString *html = [NSString stringWithFormat:@"<font face='FZLanTingHei-R-GBK' size=13 color='#232323'>当前有</font><font size=16 color='#ff7700' face='FZLanTingHei-R-GBK'>%d</font><font face='FZLanTingHei-R-GBK' size=13 color='#232323'>个任务正在下载。</font>",
                      (int)arrDownload.count];
    [labInfo setText:html];
}

#pragma mark - Queue Download State Block

- (void)queueDidStart{
    [actViewDownload startAnimating];
}

- (void)queueDidFinished{
    if ([queueDownload operationCount]<=0) {
        [actViewDownload stopAnimating];
    }
}

#pragma mark - Action 

- (NSString *)URLEncodedString:(NSString *)durl
{
    // CharactersToBeEscaped = @":/?&=;+!@#$()~',*";
    // CharactersToLeaveUnescaped = @"[].";
    
    NSString *unencodedString = durl;
    NSString *encodedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)unencodedString,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
    
    return encodedString;
}

- (void)addDownloadWithName:(NSString *)name andUrl:(NSURL *)surl{
    
//    NSString *durl = [self URLEncodedString:surl.absoluteString];
//    durl = [durl stringByReplacingOccurrencesOfString:@"%3A" withString:@":"];
//    durl = [durl stringByReplacingOccurrencesOfString:@"%2F" withString:@"/"];
//    durl = [durl stringByReplacingOccurrencesOfString:@"%25" withString:@"%"];
    
    
    
    [WSProgressHUD dismiss];
    [[MZDownloadManagerViewController sharedInstance] addDownloadTask:name
                                                              fileURL:surl.absoluteString
                                                             andRefer:curRefer];
    
    /*
     http://e.xiaoshuotxt.net/e/DownSys/doaction.php?enews=DownSoft&classid=19&id=379&pathid=0&pass=ef534c12904aba4acd8f618960738593&p=:::
     http://e.xiaoshuotxt.net/e/DownSys/doaction.php%3Fenews%3DDownSoft%26classid%3D5%26id%3D5339%26pathid%3D0%26pass%3Def534c12904aba4acd8f618960738593%26p%3D:::
     */
}


- (void)refreshAction:(id)sender{
    
    if (webView.request) {
        [webView reload];
    }
    else {
        NSURL *url = [NSURL URLWithString:@"https://dydog.sinaapp.com/book/BookStore.php"];
        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
        [req setValue:@"Mozilla/5.0" forHTTPHeaderField:@"User-Agent"];
        
        [webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
   
    
}

- (void)goBackAction:(id)sender{
    [webView goBack];
}

- (void)showDownloadList{
    MZDownloadManagerViewController *download = [MZDownloadManagerViewController sharedInstance];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:download];
    [self.navigationController presentViewController:nav
                                            animated:YES completion:nil];
}

#pragma mark - Build UI

- (void)buildWebView{
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    webView.delegate = self;
    webView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:webView];
    //dydog.sinaapp.com/book
    
    //10.1.30.7/xmpp
//    http://dydog.sinaapp.com/book/BookStore.php
//http://192.168.1.104/xmpp/BookStore.php
    NSString *surl = @"https://dydog.sinaapp.com/book/BookStore.php";
    NSURL *url = [NSURL URLWithString:surl];
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    
#if kFREE == 1
    
    if (kFREE == 1) {

        
        CGRect frame = webView.frame;
        frame.size.height = frame.size.height-48;
        webView.frame = frame;
    }
#endif
}

- (void)buildRightItem{
    actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [actView setHidesWhenStopped:YES];
    [actView startAnimating];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    [btn setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    [btn addSubview:actView];
    [btn addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setShowsTouchWhenHighlighted:YES];
    actView.center = CGPointMake(CGRectGetMidX(btn.bounds), CGRectGetMidY(btn.bounds));
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (void)buildLeftItem{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 30, 30)];
    [btn setImage:[UIImage imageNamed:@"back_up"] forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(goBackAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setShowsTouchWhenHighlighted:YES];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (void)buildDownloadInfo{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 64, ScreenWidth, 40)];
    view.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:247/255.0 alpha:.8f];
    
    labInfo = [[RTLabel alloc] initWithFrame:CGRectMake(10, 10, ScreenWidth-20, 20)];
    [view addSubview:labInfo];
    
    progressDownload = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    progressDownload.center = CGPointMake(ScreenWidth/2, view.bounds.size.height/2+15);
    CGRect fra = progressDownload.frame;
    fra.size.width = ScreenWidth-20;
    fra.origin.x = 10;
    progressDownload.frame = fra;
    [view addSubview:progressDownload];
    
    actViewDownload = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    actViewDownload.tintColor = [UIColor orangeColor];
    actViewDownload.center = CGPointMake(ScreenWidth-45, view.bounds.size.height/2-5);
    [view addSubview:actViewDownload];
    [actViewDownload setHidesWhenStopped:YES];
    [actViewDownload stopAnimating];
    
    [self.view addSubview:view];
    
    [self updateDownloadInfo];
    
}


#pragma mark - Notification 

- (void) downloadFinished:(NSNotification *)noti {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *name = noti.object[@"file_name"];
        NSString *destinationPath = [fileDest stringByAppendingPathComponent:name];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
            
            destinationPath = [[AllMethods applicationDocumentsDirectory] stringByAppendingPathComponent:name];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
                [self downloadFinished:noti];
                return ;
            }
        }
        
        
        NSData *data = [NSData dataWithContentsOfFile:destinationPath];
        
        if ([AllMethods isRarFile:data]) {
            Unrar4iOS *un = [Unrar4iOS new];
            BOOL ok = [un unrarOpenFile:destinationPath];
            if (ok) {
                NSArray *files = [un unrarListFiles];
                
                // Extract a stream
                @try {
                    
                    
                    for (int i=0; i<files.count; i++) {
                        NSString *filename = files[i];
                        if ([[[filename pathExtension] uppercaseString] isEqualToString:@"TXT"]) {
                            NSString *filepath = [[AllMethods applicationDocumentsDirectory]
                                                  stringByAppendingPathComponent:[filename lastPathComponent]];
                            NSData *data = [un extractStream:[files objectAtIndex:i]];
                            if (data != nil) {
                                [data writeToFile:filepath atomically:YES];
                            }
                        }
                    }
                    
                    
                    
                }
                @catch(RARExtractException *error) {
                    
                    if(error.status == RARArchiveProtected) {
                        
                        NSLog(@"Password protected archive!");
                    }
                    NSFileManager *man = [NSFileManager defaultManager];
                    NSString *path = [NSString stringWithFormat:@"%@/%@.txt",[AllMethods applicationDocumentsDirectory],
                                      name];
                    [man moveItemAtPath:destinationPath toPath:path error:nil];
                }
                
                [un unrarCloseFile];
            }
            else {
                [un unrarCloseFile];
                
                NSFileManager *man = [NSFileManager defaultManager];
                NSString *path = [NSString stringWithFormat:@"%@/%@.txt",[AllMethods applicationDocumentsDirectory],
                                  name];
                [man moveItemAtPath:destinationPath toPath:path error:nil];
            }
        }
        else {
            NSFileManager *man = [NSFileManager defaultManager];
            NSString *path = [NSString stringWithFormat:@"%@/%@.txt",[AllMethods applicationDocumentsDirectory],
                              name];
            [man moveItemAtPath:destinationPath toPath:path error:nil];
        }
        
        
        NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
        btnDownload.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
        
        [WSProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"《%@》下载完成",name]];
        
        UINavigationController *nav_list = (UINavigationController *)self.tabBarController.viewControllers[0];
        
        if (self.tabBarController.selectedIndex != 0) {
            nav_list.tabBarItem.badgeValue = @"NEW";
        }
        
        BKNewMainBookList *list = (BKNewMainBookList *)[nav_list viewControllers][0];
        [list reloadLocalBookList];
    });
    
}


#pragma mark - SYS

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
    btnDownload.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
    [self.view bringSubviewToFront:btnDownload];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    self.view.backgroundColor = [UIColor whiteColor];
    
    MZDownloadManagerViewController *mzDownloadingViewObj = [MZDownloadManagerViewController sharedInstance];
    mzDownloadingViewObj.sessionManager = [mzDownloadingViewObj backgroundSession];
    [mzDownloadingViewObj populateOtherDownloadTasks];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(ScreenWidth-60, ScreenHeight-64-50-40-20, 40, 40);
    [btn setImage:[UIImage imageNamed:@"gallerydownload"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(showDownloadList) forControlEvents:UIControlEventTouchUpInside];
    [btn setShowsTouchWhenHighlighted:YES];
    btnDownload = btn;
    btnDownload.backgroundColor = NAV_COLOR;
    btnDownload.layer.cornerRadius = btnDownload.frame.size.width/2;
    btnDownload.clipsToBounds = YES;
    btnDownload.shouldHideBadgeAtZero = YES;
    [self.view addSubview:btnDownload];
    
    
//    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"gallerydownload"
//                                                                            andSelect:@selector(showDownloadList)
//                                                                            andTarget:self];
//    self.navigationItem.leftBarButtonItem.shouldHideBadgeAtZero = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(downloadFinished:)
                                                 name:kMZDownloadFinishedNotification
                                               object:nil];
    
    [ASIHTTPRequest setDefaultUserAgentString:@"Mozilla/5.0"];
    
    [self buildRightItem];
    [self buildLeftItem];
    [self buildWebView];
//    [self buildDownloadInfo];
    
    self.navigationItem.title = @"书城";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
