//
//  BKNewMenuTop.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/20.
//
//

#import <UIKit/UIKit.h>

typedef void(^MenuClicked)(UIButton *);

@interface BKNewMenuTop : UIView


@property (nonatomic, copy) MenuClicked clickedEvent;

- (void)setTitleText:(NSString *)str;

@end
