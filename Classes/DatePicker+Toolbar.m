//
//  DatePicker+Toolbar.m
//  BookWorksApp
//
//  Created by 刁志远 on 13-6-3.
//
//

#import "DatePicker+Toolbar.h"

@implementation DatePicker_Toolbar

@synthesize pdelegate;

#pragma mark - MY

-(void)clickedButton:(id)sender{
    if ([sender tag] == 2) {
        if ([self.pdelegate respondsToSelector:@selector(datePicker:didSelectDate:)]) {
            [self.pdelegate datePicker:_pickerView didSelectDate:[_pickerView date]];
        }
    }
    [self hidePicker];
}

-(void)showFromView:(UIView *)view{
    
    //NSLog(@"%f - %f",self.frame.size.width,self.frame.size.height);
    
    [UIView animateWithDuration:0.6f animations:^(void){
        self.frame = CGRectMake(0, view.frame.size.height-self.frame.size.height,
                                self.frame.size.width, self.frame.size.height);
    }];
}
-(void)hidePicker{
    [UIView animateWithDuration:0.6f animations:^(void){
        self.frame = CGRectMake(0, self.frame.origin.y+self.frame.size.height,
                                self.frame.size.width, self.frame.size.height);
    }completion:^(BOOL finished){
        if (finished) {
            [self removeFromSuperview];
            //[self release];
        }
    }];
}

#pragma mark - SYS

-(id)initWithDelegate:(id<DatePickerDelagate>)delegate{
    self = [super init];
    if (self) {
        
        CGRect winSize = [[UIScreen mainScreen] bounds];
        self.frame = CGRectMake(0, winSize.size.height, winSize.size.width, 260);
        
        self.pdelegate = delegate;
        [self setBackgroundColor:[UIColor clearColor]];
        
        _toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 44)];
        [_toolBar setBarStyle:UIBarStyleBlackTranslucent];
        _pickerView = [[UIDatePicker alloc] init];
        CGRect frame = _pickerView.frame;
        frame.origin.y+=44;
        _pickerView.frame = frame;
        [_pickerView setDatePickerMode:UIDatePickerModeTime];
        
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered
                                                                 target:self action:@selector(clickedButton:)];
        item1.tag = 1;
        
        UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self action:nil];
        UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone
                                                                 target:self action:@selector(clickedButton:)];
        item3.tag = 2;
        
        _toolBar.items = [NSArray arrayWithObjects:item1,item2,item3, nil];
        
        [self addSubview:_toolBar];
        [self addSubview:_pickerView];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
