//
//  BKNewMenuBottom.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/20.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MenuShowType) {
    
    MENU_SHOW_LOCAL_BOOK=0,
    MENU_SHOW_ONLINE_BOOK,
    
};

typedef void(^BottomClickBlock)(NSInteger type, float value);

@interface BKNewMenuBottom : UIView

@property (nonatomic, copy) BottomClickBlock clickEvent;

- (id)initWithMenuShowType:(MenuShowType)type andFrame:(CGRect)frame;
- (void)updateTheme;

@end
