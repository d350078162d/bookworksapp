//
//  BWResourceList.h
//  BookWorksApp
//
//  Created by 刁志远 on 14/12/25.
//
//

#import <UIKit/UIKit.h>

@interface BWResourceList : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *myTable;


- (id)initWithBookDic:(NSDictionary *)dic;

@end
