//
//  BKQuestionList.m
//  BookWorksApp
//
//  Created by 刁志远 on 2016/12/10.
//
//

#import "BKQuestionList.h"
//#import "MJRefreshHeaderView.h"

#import "ASIFormDataRequest.h"
#import "RTLabel.h"

#import "JKBEditView.h"

#import "BKQuestionAddAnswer.h"

@interface BKQuestionList () <UITableViewDelegate, UITableViewDataSource,JKBEditViewDelegate> {
    
    NSMutableArray *arrList;
    
    JKBEditView *editView;
    
}

@property (nonatomic, weak) UITableView *myTable;

@end

@implementation BKQuestionList

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = arrList[indexPath.row];
    BKQuestionAddAnswer *ans = [[BKQuestionAddAnswer alloc] initWithQuestion:dic];
    [ans setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:ans animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self tableView:tableView
                      cellForRowAtIndexPath:indexPath];
    
    RTLabel *rt = (RTLabel *)[cell.contentView viewWithTag:1];
    
    return ORIGINAL_Y(rt)+15;
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        RTLabel *rt = [[RTLabel alloc] initWithFrame:CGRectMake(20, 15, ScreenWidth-30, 0)];
        rt.tag = 1;
        [cell.contentView addSubview:rt];
    }
    
    
    if (arrList.count <= indexPath.row) {
        return cell;
    }
    
    RTLabel *rt = (RTLabel *)[cell.contentView viewWithTag:1];
    
    NSDictionary *dic = arrList[indexPath.row];
    
    
    NSString *html = [NSString stringWithFormat:@"<font size=17 color='#000000'><b>%@</b></font><p align=right><font size=15 color='#9a9a9a'>%@回复</font></p>",
                      dic[@"content"],dic[@"reply"]];
    [rt setText:html];
    
    CGRect frame = rt.frame;
    frame.size.height = [rt optimumSize].height;
    rt.frame = frame;
    
    return cell;
}

#pragma mark - JKBEditViewDelegate

- (void)editView:(JKBEditView *)edit didClickedOK:(NSString *)str {
    
    [editView setActAnimate:YES];
    [editView makeEditScale:0.9f];
    [self addQuestion:str];
    
}

#pragma mark - MJRefresh

- (void)headRefresh {
    if (!arrList) {
        arrList = [NSMutableArray arrayWithCapacity:3];
    }
    [arrList removeAllObjects];
    
    [self getQuestionList];
}

//- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
//    if ([refreshView isEqual:mjHead]) {
//        if (!arrList) {
//            arrList = [NSMutableArray arrayWithCapacity:3];
//        }
//        [arrList removeAllObjects];
//        
//        [self getQuestionList];
//    }
//}

#pragma mark - Request Action 

- (void)addQuestion:(NSString *)ques {
    
    if (!ques || ques.length <= 0) {
        return;
    }
    
    __weak BKQuestionList *weak = self;
    
    
    dispatch_async(dispatch_queue_create("data", nil), ^{
        
        NSString *str_url = [NSString stringWithFormat:@"%@/%@?method=110&content=%@",
                             kMAIN_URL,kBOOK_URL,ques];
        str_url = [str_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_url]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (data) {
                
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingAllowFragments
                                                                 error:nil];
                
                if ([dic[@"result"] isEqualToString:@"true"]) {
                    [weak.myTable.mj_header beginRefreshing];
                    [editView setActAnimate:NO];
                    [editView dismissEdit];
                }
                
            }
            else {
                [weak reActEdit];
            }
            
        });
    });
    
}

- (void)getQuestionList {
    
    __weak BKQuestionList *weak = self;
    
    dispatch_async(dispatch_queue_create("data", nil), ^{
        
        NSString *str_url = [NSString stringWithFormat:@"%@/%@?method=111",kMAIN_URL,kBOOK_URL];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_url]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (data) {
                
                NSArray *arr = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingAllowFragments
                                                                 error:nil];
                if([arr isKindOfClass:[NSArray class]])
                    [arrList addObjectsFromArray:arr];
            }
            
            [weak.myTable.mj_header endRefreshing];
            
            [weak.myTable reloadData];
            
        });
    });
    
}

#pragma mark - Private

- (void)reActEdit {
    
    [editView setActAnimate:NO];
    [editView makeEditScale:1.0f];
    [WSProgressHUD showErrorWithStatus:@"提交失败，请重试"];
    
}

- (void)addQuestionClicked:(id)sender {
    
    if (!editView) {
        editView = [[JKBEditView alloc] initWithTitle:@"添加提问"
                                              andDesc:@""
                                         withEditType:EDIT_QUESTION];
        editView.delegate = self;
    }
    
    [editView showEdit];
    [editView.txtDesc becomeFirstResponder];
}


#pragma mark - Build UI


- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
}


#pragma mark - SYS

- (NSString *)tabTitle
{
    return @"问答";
}

- (UIFont *)tabTitleFont{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if ([[def valueForKey:kFONT] length]>0) {
        return [UIFont fontWithName:[def valueForKey:kFONT]
                               size:17];
    }
    else{
        return [UIFont fontWithName:[FONT_ARR objectAtIndex:0]
                               size:17];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self buildTableView];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addQuestionClicked:)];
    self.navigationItem.title = @"问答";
    
//    mjHead = [[MJRefreshHeaderView alloc] initWithScrollView:self.myTable];
//    mjHead.delegate = self;
//    [mjHead beginRefreshing];

    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefresh)];
    [self.myTable.mj_header beginRefreshing];
    self.myTable.tableFooterView = [UIView new];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del setADHidden:NO];
    
#if kFREE == 1
    if (kFREE == 1) {
        
        CGRect frame = self.myTable.frame;
        frame.size.height = ScreenHeight-49-48;
        self.myTable.frame = frame;
    }
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
