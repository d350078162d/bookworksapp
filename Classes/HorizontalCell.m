//
//  HorizontalCell.m
//  PageCollectionView
//
//  Created by 刁志远 on 2017/4/15.
//  Copyright © 2017年 刁志远. All rights reserved.
//

#import "HorizontalCell.h"

@interface HorizontalCell () {
    
    UIImageView *ivShadow;
    
}

@end


@implementation HorizontalCell



#pragma mark - SYS

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self initinalContent];
    [self addShadowImage];
    self.clipsToBounds = NO;
    
//     CGRect rect = CGRectMake(LeftSpacing, TopSpacing, [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing);
//    self.labContent.frame = rect;
}

#pragma mark - Public 

- (void)showShadow {
    
    if (ivShadow.hidden) {
        ivShadow.hidden = NO;
    }
}


- (void)hideShadow {
    
    if (!ivShadow.hidden) {
        ivShadow.hidden = YES;
    }
}

- (void)setShadowOri:(ShadowOri) ori {
    ivShadow.hidden = NO;
    if (ori == SHADOW_LEFT) {
        
        ivShadow.transform = CGAffineTransformMakeRotation(-M_PI);
        CGRect frame = ivShadow.frame;
        frame.origin.x = -frame.size.width;
        ivShadow.frame = frame;
    }
    else  {
        
        ivShadow.transform = CGAffineTransformMakeRotation(0);
        CGRect frame = ivShadow.frame;
        frame.origin.x = ScreenWidth;
        ivShadow.frame = frame;
        
    }
}


#pragma mark - UI

- (void)addShadowImage {
    
    ivShadow = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth, 0, 40, ScreenHeight)];
    ivShadow.image = ImageNamed(@"shadow");
    ivShadow.hidden = YES;
    
    [self addSubview:ivShadow];
}

- (void)initinalContent {
    
    VerticallyAlignedLabel *lab = [[VerticallyAlignedLabel alloc] initWithFrame:CGRectZero];
    lab.numberOfLines = 0;
    lab.backgroundColor = [UIColor clearColor];
    lab.verticalAlignment = VerticalAlignmentTop;
    
    [self.contentView addSubview:lab];
    self.labContent = lab;
    
}


@end
