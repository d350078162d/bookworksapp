//
//  BKNewChapterList.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/22.
//
//

#import "BKNewChapterList.h"

#import "UIScrollView+IndicatorExt.h"


@interface BKNewChapterList () <UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray *arrList;
    
    NSDictionary *dicChapters;
    
    BOOL isMoved;
    CGPoint potBegin;
}


@property (nonatomic, weak) UITableView *myTable;
@property (assign) ChapterType listType;

@end



@implementation BKNewChapterList


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chapterDidSelectChapter:)]) {
        [self.delegate chapterDidSelectChapter:indexPath.row];
    }
}


#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    if (self.listType == CHAPTER_LOCAL) {
        NSString *sk = arrList[indexPath.row];
        
        NSDictionary *dic = dicChapters[sk];
        cell.textLabel.text = dic[@"title"];
    }
    else if (self.listType == CHAPTER_ONLINE) {
        
        NSDictionary *dic = arrList[indexPath.row];
        cell.textLabel.text = dic[@"name"];
    }
    
    
    return cell;
}

#pragma mark - TouchEvents

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    isMoved = NO;
    
    UITouch *touch = [[touches allObjects] objectAtIndex:0];
    potBegin = [touch locationInView:self];
    
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    if(!isMoved) isMoved = YES;
    UITouch *touch = [[touches allObjects] objectAtIndex:0];
    CGPoint pot = [touch locationInView:self];
    
    float table_x = pot.x;
    table_x -= SIZE_W(self.myTable);
    
    CGRect frame = self.myTable.frame;
    
    if (table_x > 0) {
        table_x = 0;
    }
    else if (table_x < -SIZE_W(self.myTable)) {
        table_x = -SIZE_W(self.myTable);
    }
    frame.origin.x = table_x;
    
    float alpha = 0.8-fabs(0.8*table_x/SIZE_W(self.myTable));
    self.backgroundColor = RGBACOLOR(0, 0, 0, alpha);
    
    self.myTable.frame = frame;
    
    frame = self.myTable.indicator.frame;
    frame.origin.x = table_x+SIZE_W(self.myTable)-kILSDefaultSliderSize;
    self.myTable.indicator.frame = frame;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[touches allObjects] objectAtIndex:0];
    CGPoint pot = [touch locationInView:self];
    
    if (isMoved) {
        
        float move_length = pot.x - potBegin.x;
        if (move_length > 0) { //向右
            if (fabs(self.myTable.frame.origin.x) > SIZE_W(self.myTable)/2) {
                [self showChapterTable];
            }
            else {
                [self dismissView];
            }
        }
        else { //向左
            if (fabs(self.myTable.frame.origin.x) > SIZE_W(self.myTable)/2) {
                [self dismissView];
            }
            else {
                
                [self showChapterTable];
            }
        }
        
    }
    else {
        if(pot.x > SIZE_W(self.myTable)) {
            [self dismissView];
        }
    }
}

#pragma mark - Private

- (void)showChapterTable {
    
    float speed = ScreenWidth*0.7/0.3;
    
    float distance = fabs(self.myTable.frame.origin.x);
    float time = distance/speed;
    
    if (distance == 0) return;
    
    
    [UIView animateWithDuration:time
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.myTable.frame;
                         frame.origin.x = 0;
                         self.myTable.frame = frame;
                         self.backgroundColor = RGBACOLOR(0, 0, 0, 0.8f);
                         
                         frame = self.myTable.indicator.frame;
                         frame.origin.x = SIZE_W(self.myTable)-kILSDefaultSliderSize;
                         self.myTable.indicator.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
}

#pragma mark - Public 

- (void)dismissView {
    
    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.myTable.frame;
                         frame.origin.x = -SIZE_W(self.myTable);
                         self.myTable.frame = frame;
                         self.backgroundColor = RGBACOLOR(0, 0, 0, 0.0f);
                         
                         frame = self.myTable.indicator.frame;
                         frame.origin.x = -kILSDefaultSliderSize;
                         self.myTable.indicator.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         if(finished)
                             [self removeFromSuperview];
                     }];
    
}

- (void)showInView:(UIView *)view {
    
    self.backgroundColor = RGBACOLOR(0, 0, 0, 0);
    if (!self.superview) {
        [view addSubview:self];
    }
    
    [self showChapterTable];
}

- (void)setChapterList:(NSDictionary *)dic_chapter {
    
    arrList = [NSMutableArray arrayWithArray:[[dic_chapter allKeys] sortedArrayUsingSelector:@selector(compare:)]];
    
    dicChapters = dic_chapter;
    [self.myTable reloadData];
    
    
    if (!self.myTable.indicator) {
        [self.myTable registerILSIndicator];
    }
}

- (void)setOnLineChapterList:(NSArray *)arr {
    
    arrList = [NSMutableArray arrayWithArray:arr];
    [self.myTable reloadData];
    
    
    if (!self.myTable.indicator) {
        [self.myTable registerILSIndicator];
    }
    
}

#pragma mark - Build UI


- (void)buildTableView{
    
    float width = ScreenWidth*0.7;
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(-width, 0, width, ScreenHeight)
                                                      style:UITableViewStylePlain];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self addSubview:self.myTable];
}


#pragma mark - SYS

- (id)initWithOnlineChapterList:(NSArray *)arr {
    
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        
        _listType = CHAPTER_ONLINE;
        [self buildTableView];
        [self setOnLineChapterList:arr];
        
    }
    return self;
    
}

- (id)initWithChapterList:(NSDictionary *)dic_chapter {
    
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        
        _listType = CHAPTER_LOCAL;
        [self buildTableView];
        [self setChapterList:dic_chapter];
        
    }
    return self;
}

- (id)init {
    
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        
        [self buildTableView];
    }
    return self;
}


- (void)dealloc {
    
    if (self.myTable.indicator) {
        [self.myTable.indicator.scrollView removeObserver:self.myTable.indicator
                                               forKeyPath:@"contentOffset"];
    }
}

@end
