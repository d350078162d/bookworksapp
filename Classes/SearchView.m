//
//  SearchView.m
//  BookWorksApp
//
//  Created by 刁志远 on 13-8-12.
//
//

#import "SearchView.h"

#import <QuartzCore/QuartzCore.h>

#import "SBJson.h"
#import "WSProgressHUD.h"
#import "FFCircularProgressView.h"

#import "BookWorksAppAppDelegate.h"
#import "BWDownloadCell.h"

#import "UIBarButtonItem+Badge.h"
#import "MZDownloadManagerViewController.h"

@interface SearchView ()<MZDownloadDelegate>

@end

@implementation SearchView

#pragma mark - MZDownloadDelegate

- (void)downloadRequestFinished:(NSString *)fileName{
    for (NSDictionary *dic in arrDownList) {
        if ([dic[kNAME] isEqualToString:fileName]) {
            [self updateBookInfo:dic];
            [arrDownList removeObject:dic];
            break;
        }
    }
    
    
    NSString *file = [NSString stringWithFormat:@"%@/%@",[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"],
                      fileName];
    NSFileManager *man = [NSFileManager defaultManager];
    if ([man fileExistsAtPath:file]) {
        NSString *path = [NSString stringWithFormat:@"%@/%@",[AllMethods applicationDocumentsDirectory],
                          fileName];
        [man moveItemAtPath:file toPath:path error:nil];
    }
    
    [self getDocumentBooks];
    [self.myTable reloadData];
    
    NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
}


#pragma mark UISearchBarDelegate-

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
    [self getBookListInThread];
}

#pragma mark - UITableViewDelegate UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BWDownloadCell *cell = (BWDownloadCell *)[tableView dequeueReusableCellWithIdentifier:@"down_load_cell_id"];
    if (!cell) {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"BWDownloadCell"
                                                     owner:self
                                                   options:Nil];
        cell = [arr objectAtIndex:0];
        CGRect rect = [[UIScreen mainScreen] bounds];
        
        FFCircularProgressView *ff = [[FFCircularProgressView alloc] initWithFrame:CGRectMake(rect.size.width-50, 22.5, 30, 30)
                                                                           andType:kFFCIRCLE_ARROW_TYPE];
        ff.tag = 111;
        [cell.contentView addSubview:ff];
        [cell.backView setBackgroundColor:[UIColor clearColor]];
        [cell.contentView bringSubviewToFront:cell.backView];
        cell.btnRec.hidden = YES;
    }
    
    
    if (arrList && [arrList count] <= indexPath.row) {
        return cell;
    }
    
    NSDictionary *dic = [arrList objectAtIndex:indexPath.row];
    
    cell.labTitle.text = [NSString stringWithFormat:@"%@  %@",[dic valueForKey:kNAME],[dic valueForKey:kSIZE]];
    cell.labDown.text = [NSString stringWithFormat:@"下载%@次",[dic valueForKey:@"down_cnt"]];
//    [cell.btnRec setTitle:[NSString stringWithFormat:@"评论%@次",[dic valueForKey:@"rec_cnt"]]
//                 forState:UIControlStateNormal];
    
    [cell.labDown setFont:[AllMethods getFontWithSize:16]];
    [cell.labTitle setFont:[AllMethods getFontWithSize:16]];
    [cell.btnRec.titleLabel setFont:[AllMethods getFontWithSize:16]];
    [cell.btnRec setBackgroundColor:NAV_COLOR];
    cell.labTitle.textColor = [UIColor whiteColor];
    cell.labDown.textColor = [UIColor whiteColor];
    
    cell.btnDown.tag = indexPath.row;
	[cell.btnDown addTarget:self action:@selector(downLoadBook:) forControlEvents:UIControlEventTouchUpInside];
    
    FFCircularProgressView *md = (FFCircularProgressView *)[cell.contentView viewWithTag:111];
    if ([arrDownList containsObject:dic]) {
        cell.btnDown.enabled = NO;
        md.hidden = YES;
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectZero];
        lab.text = @"下载中...";
        lab.textColor = [UIColor whiteColor];
        lab.font = [UIFont systemFontOfSize:12];
        [lab sizeToFit];
        cell.accessoryView = lab;
    }
    else {
        cell.accessoryView = nil;
        
        md.hidden = NO;
        
        [md setProgress:0.0f];
        cell.btnDown.enabled = YES;
    }
    
    if ([arrBooks containsObject:[dic valueForKey:kNAME]]) {
        [md setProgress:1.0f];
        cell.btnDown.enabled = NO;
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return self.searchBar;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.searchBar.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setBackgroundColor:NAV_SHADOW_COLOR];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([self.searchBar isFirstResponder]) {
        [self.searchBar resignFirstResponder];
    }
}

#pragma mark - MY

- (void)updateBookInfo:(NSDictionary *)dic_book{
    dispatch_queue_t queue = dispatch_queue_create("update_book_info_queue", Nil);
    dispatch_async(queue, ^{
        NSDictionary *dic_user = [[NSUserDefaults standardUserDefaults] objectForKey:kUSER_INFO];
        NSString *surl = [NSString stringWithFormat:@"%@/%@?method=3&book_id=%@&uid=%@",kMAIN_URL,kUSER_URL,
                          [dic_book valueForKey:@"id"],[dic_user valueForKey:@"uid"]];
        
        surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:surl];
        NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:Nil];
        NSDictionary *dic = [res JSONValue];
        dispatch_sync(dispatch_get_main_queue(), ^{
        });
    });
}

- (void)downLoadBook:(id)sender{
    NSDictionary *dic_user = [[NSUserDefaults standardUserDefaults] objectForKey:kUSER_INFO];
    if (!dic_user) {
        [AllMethods showAltMsg:@"为了使用户更方便的下载到更好看的小说，特此增加用户系统，请先登录后再下载。\n特此声明:不会限制用户下载数量。"];
        return;
    }
    
    
    
    UIButton *btn = (UIButton *)sender;
    [btn setEnabled:NO];
    
    int index = (int)[btn tag];
    
    NSDictionary *dic = [arrList objectAtIndex:index];
    [arrDownList addObject:dic];
    
    NSString *surl = [dic valueForKey:kURL];
    surl = [NSString stringWithFormat:@"%@/%@",[dic valueForKey:kURL],[dic valueForKey:kNAME]];
    surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    MZDownloadManagerViewController *down = [MZDownloadManagerViewController sharedInstance];
    [down addDownloadTask:dic[kNAME] fileURL:surl];
    
    [self.myTable reloadData];
    
    
    NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
}

- (IBAction)backToUpView:(id)sender{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)getDocumentBooks{
    [arrBooks removeAllObjects];
    NSString *path = [AllMethods applicationDocumentsDirectory];//[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/"];
	NSArray *mua = (NSArray *)[[[NSFileManager defaultManager]
								contentsOfDirectoryAtPath:path
								error:nil]
							   pathsMatchingExtensions:[NSArray arrayWithObjects:@"txt",@"TXT",nil]];
	for (int i=0; i<[mua count]; i++) {
        if([arrBooks containsObject:[mua objectAtIndex:i]] == NO)
			[arrBooks addObject:[mua objectAtIndex:i]];
	}
    [self.myTable reloadData];
}

- (void)getBookListInThread{
    
    [WSProgressHUD showWithStatus:@""];
    [arrList removeAllObjects];
    dispatch_queue_t queue = dispatch_queue_create("get_book_list", nil);
    dispatch_async(queue, ^(void){
        
        NSString *str_url = [NSString stringWithFormat:@"%@/%@?method=5&name=%@",kMAIN_URL,kBOOK_URL,
                             self.searchBar.text];
        NSURL *url = [NSURL URLWithString:str_url];
        if (!url) {
            str_url = [str_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            url = [NSURL URLWithString:str_url];
        }
        NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        
        
        if (res && [res length] > 0) {
            NSArray *arr = [res JSONValue];
            [arrList addObjectsFromArray:arr];
            
            dispatch_sync(dispatch_get_main_queue(), ^(void){
                [WSProgressHUD dismiss];
                [self.myTable reloadData];
            });
        }
        
    });
}

- (void)showDownloadList{
    MZDownloadManagerViewController *download = [MZDownloadManagerViewController sharedInstance];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:download];
    [self.navigationController presentViewController:nav
                                            animated:YES completion:nil];
}

#pragma mark - SYS

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [MZDownloadManagerViewController sharedInstance].delegate = self;
    self.navigationItem.rightBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"gallerydownload"
                                                                            andSelect:@selector(showDownloadList)
                                                                            andTarget:self];
    
    arrList = [[NSMutableArray alloc] initWithCapacity:3];
    arrBooks = [[NSMutableArray alloc] initWithCapacity:3];
    arrDownList = [[NSMutableArray alloc] initWithCapacity:3];
    
    self.navigationItem.rightBarButtonItem.shouldHideBadgeAtZero = YES;

    NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
	[btn setFrame:CGRectMake(0, 0, 55, 35)];
	[btn setTitle:NSLocalizedString(@"Nav_Back", @"") forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
	[btn addTarget:self action:@selector(backToUpView:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.leftBarButtonItem = item;
    
    
    [self.navigationItem setTitle:NSLocalizedString(@"SEARCH", @"")];
    
    
    [self.searchBar setTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"down_background.png"]]];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getDocumentBooks];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMyTable:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
}
@end
