//
//  BWCommentView.h
//  BookWorksApp
//
//  Created by 刁志远 on 13-11-29.
//
//

#import <UIKit/UIKit.h>
#import "FFCircularProgressView.h"
#import "MJRefresh.h"

@interface BWCommentView : UIViewController<MJRefreshBaseViewDelegate>{
    NSDictionary *dicBook,*dicSelf;
    NSMutableArray *arrList;
    MJRefreshHeaderView *mjHead;
}

@property (weak, nonatomic) IBOutlet UITableView *myTable;
@property (strong, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labCom;

- (id)initWithBook:(NSDictionary *)dic;


@end
