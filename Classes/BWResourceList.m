//
//  BWResourceList.m
//  BookWorksApp
//
//  Created by 刁志远 on 14/12/25.
//
//

#import "BWResourceList.h"

#import "ASIHTTPRequest.h"

#import "BWBookChapter.h"

@interface BWResourceList (){
    NSDictionary *dicUrl;
    NSDictionary *dicBook;
    
    NSMutableArray *arrList;
}

@end

@implementation BWResourceList


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BWBookChapter *chap = [[BWBookChapter alloc] initWithBookDic:[arrList objectAtIndex:indexPath.row]];
    [chap setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:chap animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *str_id = @"resource_cell_id";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str_id];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:str_id];
        
        UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, WINSIZE.width-10, 20)];
        lab1.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0f];
        lab1.tag = 1;
        lab1.font = [UIFont systemFontOfSize:15];
        [cell.contentView addSubview:lab1];
        
        UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, WINSIZE.width-10, 20)];
        lab2.textColor = [UIColor colorWithRed:83/255.0 green:83/255.0 blue:83/255.0 alpha:1.0f];
        lab2.tag = 2;
        lab2.font = [UIFont systemFontOfSize:16];
        [cell.contentView addSubview:lab2];
    }
    
    NSDictionary *dic = [arrList objectAtIndex:indexPath.row];
    
    UILabel *lab = (UILabel *)[cell.contentView viewWithTag:1];
    lab.text = [dic valueForKey:@"name"];
    
    lab = (UILabel *)[cell.contentView viewWithTag:2];
    lab.text = [dic valueForKey:@"lastChapter"];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


#pragma mark - My

- (void)getBookResourceList{
    
    if (!arrList) {
        arrList = [NSMutableArray arrayWithCapacity:3];
    }
    [arrList removeAllObjects];
    
    [WSProgressHUD showWithStatus:@"Loading..."];
    
//    NSString *str_url = [NSString stringWithFormat:[dicUrl valueForKey:@"book_resource"],[dicBook valueForKey:@"book_id"]];
    
    NSString *str_url = [NSString stringWithFormat:@"%@/zhuishu.php?method=2&book_id=%@",kZHUI_URL,
                         [dicBook valueForKey:@"book_id"]];
    
    dispatch_queue_t queue = dispatch_queue_create("get_book_resource", nil);
    dispatch_async(queue, ^{
        NSString *str = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSArray *arr = [str JSONValue];
            [arrList addObjectsFromArray:arr];
            [self.myTable reloadData];
            [WSProgressHUD dismiss];
        });
        
    });
    
    
}


#pragma mark - SYS

- (id)initWithBookDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        dicBook = [NSDictionary dictionaryWithDictionary:dic];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dicUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"book_url"];
    [self getBookResourceList];
    
    self.navigationItem.title = @"小说资源";
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 32, 32);
    [btn setBackgroundImage:[UIImage imageNamed:@"back_up.png"] forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
