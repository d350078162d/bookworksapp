//
//  BKNewUpdateBookBacklist.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/23.
//
//

#import <UIKit/UIKit.h>

@interface BKNewUpdateBookBacklist : UIViewController

- (id)initWithBookInfo:(NSDictionary *)dic;

@end
