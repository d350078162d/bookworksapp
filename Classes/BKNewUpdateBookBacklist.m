//
//  BKNewUpdateBookBacklist.m
//  BookWorksApp
//
//  Created by 刁志远 on 2017/3/23.
//
//

#import "BKNewUpdateBookBacklist.h"
//#import "BKChapterContent.h"
#import "BKChapterReader.h"

#import "UIScrollView+IndicatorExt.h"

@interface BKNewUpdateBookBacklist () <UITableViewDelegate, UITableViewDataSource>{
    
    NSMutableArray *arrList;
    
    NSDictionary *dicBookInfo;
    
    NSMutableDictionary *dicExpand;
}


@property (nonatomic, weak) UITableView *myTable;


@end

@implementation BKNewUpdateBookBacklist


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = arrList[indexPath.section];
    NSArray *arr = dic[@"chapter"];
    
    dic = arr[indexPath.row];
    
    if (!dic[@"href"] || !dic[@"name"]) {
        [WSProgressHUD showErrorWithStatus:@"暂无本章地址"];
        return;
    }
    
    [self toShowBookContent:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 34;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 34)];
    view.backgroundColor = RGBACOLOR(240, 240, 240, 1.0f);
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, SIZE_W(view)-70, SIZE_H(view))];
    lab.textColor = RGBACOLOR(31, 31, 31, 1.0f);
    [view addSubview:lab];
    
    NSDictionary *dic = arrList[section];
    lab.text = dic[@"name"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(SIZE_W(view)-70, 0, 70, SIZE_H(view));
    [btn setTitleColor:NAV_COLOR forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    [btn addTarget:self action:@selector(expandClicked:) forControlEvents:UIControlEventTouchUpInside];
    NSString *sk = [NSString stringWithFormat:@"expand%td",section];
    
    if (dicExpand[sk]) {
        if ([dicExpand[sk] intValue] == 0) {
            [btn setTitle:@"+展开" forState:UIControlStateNormal];
        }
        else {
            [btn setTitle:@"-收起" forState:UIControlStateNormal];
        }
    }
    else {
        [btn setTitle:@"-收起" forState:UIControlStateNormal];
    }
    btn.tag = section;
    
    [view addSubview:btn];
    
    return view;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSString *sk = [NSString stringWithFormat:@"expand%td",section];
    
    if (dicExpand[sk]) {
        if ([dicExpand[sk] intValue] == 0) {
            return 0;
        }
    }
    
    NSDictionary *dic = arrList[section];
    NSArray *arr = dic[@"chapter"];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sid];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:sid];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.textLabel.font = [UIFont systemFontOfSize:16];
    }
    
    if (indexPath.section >= arrList.count) {
        return cell;
    }
    
    NSDictionary *dic = arrList[indexPath.section];
    NSArray *arr = dic[@"chapter"];
    
    if (indexPath.row >= arr.count) {
        return cell;
    }
    
    dic = arr[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"%@",dic[@"name"]];
    name = [name stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@",NULL]
                                           withString:@""];
    
    
    cell.textLabel.text = name;
    
    return cell;
}

#pragma mark - Request Data

- (void)requestContent:(NSDictionary *)dic_chap {
    
    [WSProgressHUD show];
    
    NSString *surl = [NSString stringWithFormat:@"%@&action=content&href=%@/%@",
                      [AllMethods updateURL],dicBookInfo[@"href"],dic_chap[@"href"]];
    
    //__weak BKNewUpdateBookBacklist *weakSelf = self;
    [AllMethods requestWithType:REQUEST_GET
                        andPara:nil
                         andUrl:surl andGetDataBlock:^(id object) {
                             [WSProgressHUD dismiss];
                             
                             NSArray *arr = object;
                             
                             if (![arr isKindOfClass:[NSArray class]]) {
                                 
                                 [AllMethods showAltMsg:[NSString stringWithFormat:@"%@",object]];
                                 return ;
                             }
                             
                             
                         }];
}

- (void)requestData {
    
    if (!dicExpand) {
        dicExpand = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
    NSString *surl = [NSString stringWithFormat:@"%@&action=backlist&href=%@",
                      [AllMethods updateURL],dicBookInfo[@"href"]];
    
    __weak BKNewUpdateBookBacklist *weakSelf = self;
    [AllMethods requestWithType:REQUEST_GET
                        andPara:nil
                         andUrl:surl andGetDataBlock:^(id object) {
                             
                             NSArray *arr = object;
                             
                             if (![arr isKindOfClass:[NSArray class]]) {
                                 
                                 [AllMethods showAltMsg:[NSString stringWithFormat:@"%@",object]];
                                 [weakSelf.myTable.mj_header endRefreshing];
                                 return ;
                             }
                             
                             if (!arrList) {
                                 arrList = [NSMutableArray arrayWithCapacity:2];
                             }
                             [arrList removeAllObjects];
                             [arrList addObjectsFromArray:arr];
                             
                             
                             [weakSelf.myTable reloadData];
                             
                             [weakSelf.myTable.mj_header endRefreshing];
                             
                             if (!weakSelf.myTable.indicator) {
                                 [weakSelf.myTable registerILSIndicator];
                                 
                                 if (weakSelf.myTable.indicator) {
                                     
                                     CGRect frame = weakSelf.myTable.indicator.frame;
                                     frame.origin.y = 64+6.5;
                                     frame.size.height = frame.size.height-64;
                                     weakSelf.myTable.indicator.frame = frame;
                                 }
                             }
                             
                         }];
}


#pragma mark Private

- (void)toShowBookContent:(NSIndexPath *)indexPath {
    
    NSMutableArray *arr_res = [NSMutableArray arrayWithCapacity:3];
    NSInteger index = 0;
    
    for (int i=0; i<arrList.count; i++) {
        
        NSDictionary *dic = arrList[i];
        NSArray *arr = dic[@"chapter"];
        
        if (indexPath.section > i) {
            index += arr.count;
        }
        else if (indexPath.section == i) {
            index += indexPath.row;
        }
        [arr_res addObjectsFromArray:arr];
    }
    
//    BKChapterContent *chapter = [[BKChapterContent alloc] initWithChapterList:arr_res
//                                                              andChapterIndex:index
//                                                                    andBookInfo:dicBookInfo];

    BKChapterReader *chapter = [[BKChapterReader alloc] initWithChapterList:arr_res
                                                            andChapterIndex:index
                                                                andBookInfo:dicBookInfo];
    
    [self.navigationController pushViewController:chapter animated:YES];
    
}

- (void)expandClicked:(UIButton *)btn {
    
    NSString *sk = [NSString stringWithFormat:@"expand%td",btn.tag];
    
    if (dicExpand[sk]) {
        if ([dicExpand[sk] intValue] == 0) {
            dicExpand[sk] = @"1";
        }
        else {
            dicExpand[sk] = @"0";
        }
    }
    else {
        dicExpand[sk] = @"0";
    }
    
    [self.myTable reloadSections:[NSIndexSet indexSetWithIndex:btn.tag]
                withRowAnimation:UITableViewRowAnimationFade];
}

- (void)continueToRead:(UIButton *)btn {
    
    NSInteger index = [dicBookInfo[@"chap_index"] intValue];
    
    NSInteger row = 0;
    NSInteger sec = 0;
    NSInteger tmp = 0;
    
    for (int i=0; i<arrList.count; i++) {
        NSDictionary *dic = arrList[i];
        NSArray *arr = dic[@"chapter"];
        tmp += arr.count;
        
        if (index < tmp) {
            sec = i;
            row = index-(tmp-arr.count);
            break;
        }
        
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:sec];
    [self tableView:self.myTable didSelectRowAtIndexPath:indexPath];
}

#pragma mark - Build UI

- (void)buildTableHeaderView {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 50)];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, SIZE_W(view)-15-70, SIZE_H(view))];
    lab.textColor = RGBACOLOR(31, 31, 31, 1.0f);
    [view addSubview:lab];
    lab.text = dicBookInfo[@"chap_name"];
    
    if (![lab.text isEqualToString:@"未观看"]) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(SIZE_W(view)-85, 0, 70, SIZE_H(view));
        [btn setTitleColor:NAV_COLOR forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn setTitle:@"继续阅读" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(continueToRead:) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:btn];
    }
    
    
    self.myTable.tableHeaderView = view;
    
}

- (void)buildTableView{
    
    UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)
                                                      style:UITableViewStyleGrouped];
    
    self.myTable = table;
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
    self.myTable.tableFooterView = [UIView new];
    
    [self.view addSubview:self.myTable];
    
}


#pragma mark - SYS

- (void)dealloc {
    
    if (self.myTable.indicator) {
        [self.myTable.indicator.scrollView removeObserver:self.myTable.indicator
                                               forKeyPath:@"contentOffset"];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (id)initWithBookInfo:(NSDictionary *)dic {
    
    self = [super init];
    if (self) {
        dicBookInfo = [NSDictionary dictionaryWithDictionary:dic];
    }
    
    return  self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = dicBookInfo[@"name"];
    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"back_up"
                                                                            andSelect:@selector(popViewControllerAnimated:) andTarget:self.navigationController];
    
    [self buildTableView];
    
    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self
                                                              refreshingAction:@selector(requestData)];
    
    [self.myTable.mj_header beginRefreshing];
    
    if (dicBookInfo[@"chap_name"]) {
        [self buildTableHeaderView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
