//
//  DownloadView.m
//  BookWorksApp
//
//  Created by 刁志远 on 13-5-27.
//
//

#import "DownloadView.h"

#import "AllMethods.h"
#import "SBJson.h"
#import "WSProgressHUD.h"

#import "FFCircularProgressView.h"
#import "BWDownloadCell.h"
#import "BWCommentView.h"

#import "MZDownloadManagerViewController.h"

#import "UIBarButtonItem+Badge.h"

#define MAX_SEC 10000
#define BOOK_PAGE_SIZE 10

@interface DownloadView ()<MZDownloadDelegate,UIWebViewDelegate>{
}

@end

@implementation DownloadView

@synthesize segType;

#pragma mark - MZDownloadDelegate

- (void)downloadRequestFinished:(NSString *)fileName{
    for (NSDictionary *dic in arrDownList) {
        if ([dic[kNAME] isEqualToString:fileName]) {
            [self updateBookInfo:dic];
            [arrDownList removeObject:dic];
            break;
        }
    }
    
    
    NSString *file = [NSString stringWithFormat:@"%@/%@",[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"],
                      fileName];
    NSFileManager *man = [NSFileManager defaultManager];
    if ([man fileExistsAtPath:file]) {
        NSString *path = [NSString stringWithFormat:@"%@/%@",[AllMethods applicationDocumentsDirectory],
                          fileName];
        [man moveItemAtPath:file toPath:path error:nil];
    }
    
    [self getDocumentBooks];
    [self.myTable reloadData];
    
    NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
    self.navigationItem.leftBarButtonItem.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
}

#pragma mark - UITableViewDelegate UITableViewDataSource

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return [[dicList allKeys] count];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BWDownloadCell *cell = (BWDownloadCell *)[tableView dequeueReusableCellWithIdentifier:@"down_load_cell_id"];
    if (!cell) {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"BWDownloadCell"
                                                     owner:self
                                                   options:Nil];
        cell = [arr objectAtIndex:0];
        CGRect rect = [[UIScreen mainScreen] bounds];
        
        FFCircularProgressView *ff = [[FFCircularProgressView alloc] initWithFrame:CGRectMake(rect.size.width-50, 22.5, 30, 30)
                                                                           andType:kFFCIRCLE_ARROW_TYPE];
        ff.tag = 111;
        [cell.contentView addSubview:ff];
        [cell.backView setBackgroundColor:[UIColor clearColor]];
        [cell.contentView bringSubviewToFront:cell.backView];
        
        cell.btnRec.hidden = YES;
    }
    
    
    if (arrList && [arrList count] <= indexPath.row) {
        return cell;
    }
    
    NSDictionary *dic = [arrList objectAtIndex:indexPath.row];
    
    cell.labTitle.text = [NSString stringWithFormat:@"%@  %@",[dic valueForKey:kNAME],[dic valueForKey:kSIZE]];
    cell.labDown.text = [NSString stringWithFormat:@"下载%@次",[dic valueForKey:@"down_cnt"]];
    [cell.btnRec setTitle:[NSString stringWithFormat:@"评论%@次",[dic valueForKey:@"rec_cnt"]]
                 forState:UIControlStateNormal];
    
    [cell.labDown setFont:[AllMethods getFontWithSize:16]];
    [cell.labTitle setFont:[AllMethods getFontWithSize:16]];
    [cell.btnRec.titleLabel setFont:[AllMethods getFontWithSize:16]];
    [cell.btnRec setBackgroundColor:NAV_COLOR];
    cell.labTitle.textColor = [UIColor whiteColor];
    cell.labDown.textColor = [UIColor whiteColor];
    
    cell.btnDown.tag = indexPath.row;
	[cell.btnDown addTarget:self action:@selector(downLoadBook:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnRec.tag = indexPath.row;
    [cell.btnRec addTarget:self action:@selector(pushToCommentView:) forControlEvents:UIControlEventTouchUpInside];
    
    FFCircularProgressView *md = (FFCircularProgressView *)[cell.contentView viewWithTag:111];
    cell.btnDown.center = md.center;
    if ([arrDownList containsObject:dic]) {
        cell.btnDown.enabled = NO;
        md.hidden = YES;
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectZero];
        lab.text = @"下载中...";
        lab.textColor = [UIColor whiteColor];
        lab.font = [UIFont systemFontOfSize:12];
        [lab sizeToFit];
        cell.accessoryView = lab;
    }
    else {
        cell.accessoryView = nil;
        
        md.hidden = NO;
        
        [md setProgress:0.0f];
        cell.btnDown.enabled = YES;
    }
    
    if ([arrBooks containsObject:[dic valueForKey:kNAME]]) {
        [md setProgress:1.0f];
        cell.btnDown.enabled = NO;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
	[cell setBackgroundColor:[UIColor clearColor]];
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return self.segType;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.segType.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

//#pragma mark - UIScrollViewDelegate
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"%@",NSStringFromCGPoint(scrollView.contentOffset));
//    
//}


#pragma mark - MY

- (NSString *)tabTitle
{
	return NSLocalizedString(@"Tab_Download", @"");
}

- (UIFont *)tabTitleFont{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if ([[def valueForKey:kFONT] length]>0) {
        return [UIFont fontWithName:[def valueForKey:kFONT]
                                              size:[[def valueForKey:kFONT_SIZE] floatValue]];
    }
    else{
        return [UIFont fontWithName:[FONT_ARR objectAtIndex:0]
                               size:[[def valueForKey:kFONT_SIZE] floatValue]];
    }
}


- (void)getDocumentBooks{
    [arrBooks removeAllObjects];
    NSString *path = [AllMethods applicationDocumentsDirectory];//[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/"];
	NSArray *mua = (NSArray *)[[[NSFileManager defaultManager]
								contentsOfDirectoryAtPath:path
								error:nil]
							   pathsMatchingExtensions:[NSArray arrayWithObjects:@"txt",@"TXT",nil]];
	for (int i=0; i<[mua count]; i++) {
        if([arrBooks containsObject:[mua objectAtIndex:i]] == NO)
			[arrBooks addObject:[mua objectAtIndex:i]];
	}
//    [self.myTable reloadData];
}

- (void)updateBookInfo:(NSDictionary *)dic_book{
    dispatch_queue_t queue = dispatch_queue_create("update_book_info_queue", Nil);
    dispatch_async(queue, ^{
        NSDictionary *dic_user = [[NSUserDefaults standardUserDefaults] objectForKey:kUSER_INFO];
        NSString *surl = [NSString stringWithFormat:@"%@/%@?method=3&book_id=%@&uid=%@&nick=%@",kMAIN_URL,kUSER_URL,
                          [dic_book valueForKey:@"id"],[dic_user valueForKey:@"uid"],[dic_user valueForKey:@"nick"]];
        
        surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:surl];
        NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:Nil];
        dispatch_sync(dispatch_get_main_queue(), ^{
        });
    });
}

- (void)downLoadBook:(id)sender{
    
//    NSDictionary *dic_user = [[NSUserDefaults standardUserDefaults] objectForKey:kUSER_INFO];
//    if (!dic_user) {
//        [AllMethods showAltMsg:@"为了使用户更方便的下载到更好看的小说，特此增加用户系统，请先登录后再下载。\n特此声明:不会限制用户下载数量。"];
//        return;
//    }
    
    
    
    UIButton *btn = (UIButton *)sender;
    [btn setEnabled:NO];
    
    int index = (int)[btn tag];
    
    NSDictionary *dic = [arrList objectAtIndex:index];
    [arrDownList addObject:dic];
    
    NSString *surl = [dic valueForKey:kURL];
    surl = [NSString stringWithFormat:@"%@/%@",[dic valueForKey:kURL],[dic valueForKey:kNAME]];
    surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    MZDownloadManagerViewController *down = [MZDownloadManagerViewController sharedInstance];
    [down addDownloadTask:dic[kNAME] fileURL:surl];
    
    [self.myTable reloadData];
    
    
    NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
    self.navigationItem.leftBarButtonItem.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
    
}

- (void)getBookListInThread{
    
    
    dispatch_queue_t queue = dispatch_queue_create("get_book_list", nil);
    dispatch_async(queue, ^(void){
        
        NSString *str_url = [NSString stringWithFormat:@"%@/%@?method=2&type=%d&page=%d",kMAIN_URL,kBOOK_URL,
                             (int)self.segType.currentSelected+1,curPage];
        NSString *res = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        
        if ([res length] <= 0) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                mjFoot.hidden=YES;
                [mjFoot endRefreshing];
                [mjHead endRefreshing];
            });
        }
        else{
            NSArray *arr = [res JSONValue];
            [arrList addObjectsFromArray:arr];
            
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.myTable reloadData];
                if([arr count] < BOOK_PAGE_SIZE){
                    mjFoot.hidden = YES;
                }
                else mjFoot.hidden = NO;
                
                [mjFoot endRefreshing];
                [mjHead endRefreshing];
            });
        }
    });
}

- (IBAction)typeChange:(id)sender {
//    [self getBookListInThread];
//    [_egoHead egoRefreshScrollViewDidEndDragging:self.myTable isFirstTime:YES];
    [mjHead beginRefreshing];
}

- (void)pushToCommentView:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSDictionary *dic = [arrList objectAtIndex:btn.tag];
    BWCommentView *com = [[BWCommentView alloc] initWithBook:dic];
    [com setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:com animated:YES];
}

- (void)pushToSearchPage{
    SearchView *se = [[SearchView alloc] init];
    [se setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:se animated:YES];
}

- (void)showDownloadList{
    MZDownloadManagerViewController *download = [MZDownloadManagerViewController sharedInstance];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:download];
    [self.navigationController presentViewController:nav
                                            animated:YES completion:nil];
}

#pragma mark - MJRefreshBaseViewDelegate

- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
    if ([refreshView isEqual:mjHead]) {
        [self getDocumentBooks];
        [arrList removeAllObjects];
        curPage = 1;
        [self getBookListInThread];
    }
    else{
        if (!mjFoot.hidden) {
            curPage++;
            [self getBookListInThread];
        }
    }
    
}


#pragma mark - SYS

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MZDownloadManagerViewController sharedInstance].delegate = self;
    NSArray *af = [MZDownloadManagerViewController sharedInstance].downloadingArray;
    self.navigationItem.leftBarButtonItem.badgeValue = [NSString stringWithFormat:@"%d",(int)[af count]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.8856 5025
    
    MZDownloadManagerViewController *mzDownloadingViewObj = [MZDownloadManagerViewController sharedInstance];
    mzDownloadingViewObj.sessionManager = [mzDownloadingViewObj backgroundSession];
    [mzDownloadingViewObj populateOtherDownloadTasks];
    mzDownloadingViewObj.delegate = self;
    isFirst = YES;
    
    segType = [[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 44)
                                                      items:@[@{@"text":NSLocalizedString(@"Type_0", Nil),@"icon":@""},
                                                              @{@"text":NSLocalizedString(@"Type_1", Nil),@"icon":@""}]
                                               iconPosition:IconPositionRight
                                          andSelectionBlock:^(NSUInteger segmentInde){
                                              [self typeChange:segType];
                                          }];
    
    segType.color=[UIColor colorWithWhite:.1 alpha:.9];
    segType.borderWidth=0.5;
    segType.borderColor=[UIColor darkGrayColor];
    segType.selectedColor=NAV_COLOR ;
    segType.textAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:14],
                            NSForegroundColorAttributeName:[UIColor whiteColor]};
    segType.selectedTextAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:14],
                                    NSForegroundColorAttributeName:[UIColor whiteColor]};
    
//    [self.myTable setTableHeaderView:self.segType];
    
    curPage = 1;
    [self.myTable setBackgroundColor:NAV_SHADOW_COLOR];
    
    [self.navigationItem setTitle:NSLocalizedString(@"Nav_Download", @"")];
    
    self.navigationItem.leftBarButtonItem = [AllMethods getButtonBarItemWithImageName:@"gallerydownload"
                                                                            andSelect:@selector(showDownloadList)
                                                                            andTarget:self];
    self.navigationItem.leftBarButtonItem.shouldHideBadgeAtZero = YES;
    
    
    arrDownList = [[NSMutableArray alloc] initWithCapacity:2];
    arrBooks = [[NSMutableArray alloc] initWithCapacity:2];
    arrList = [[NSMutableArray alloc] initWithCapacity:3];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setShowsTouchWhenHighlighted:YES];
	[btn setFrame:CGRectMake(0, 0, 38, 38)];
	[btn setImage:[UIImage imageNamed:@"search1.png"] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
	[btn addTarget:self action:@selector(pushToSearchPage) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.rightBarButtonItem = item;
    
    
    mjFoot = [[MJRefreshFooterView alloc] initWithScrollView:self.myTable];
    mjFoot.delegate = self;
    
    mjHead = [[MJRefreshHeaderView alloc] initWithScrollView:self.myTable];
    mjHead.delegate = self;
    
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (isFirst) {
        isFirst = NO;
        [mjHead beginRefreshing];
    }
    
#if kFREE == 1

    if (kFREE == 1) {

        CGRect frame = self.myTable.frame;
        frame.size.height = ScreenHeight-49-48;
        self.myTable.frame = frame;
        
    }
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMyTable:nil];
    [self setSegType:nil];
    [super viewDidUnload];
}

@end
