//
//  BWUserRegView.m
//  BookWorksApp
//
//  Created by 刁志远 on 13-11-28.
//
//

#import "BWUserRegView.h"
#import "SBJson.h"

@interface BWUserRegView ()

@end

@implementation BWUserRegView

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag < 4) {
        UITextField *txt = (UITextField *)[self.view viewWithTag:textField.tag+1];
        [txt becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - MY

- (IBAction)btnClicked:(id)sender {
    [WSProgressHUD showWithStatus:@"正在提交信息..."];
    UITextField *txt = (UITextField *)[self.view viewWithTag:1];
    NSString *str_nick = txt.text;
    txt = (UITextField *)[self.view viewWithTag:2];
    NSString *str_id = txt.text;
    txt = (UITextField *)[self.view viewWithTag:3];
    NSString *str_psd1 = txt.text;
    txt = (UITextField *)[self.view viewWithTag:4];
    NSString *str_psd2 = txt.text;
    
    if (!([str_nick length]>0 && [str_id length]>0 && [str_psd1 length]>0 && [str_psd2 length]>0)) {
        [WSProgressHUD showErrorWithStatus:@"请将信息填写完整"];
        return;
    }
    
    if ([str_psd2 isEqualToString:str_psd1] == NO) {
        [WSProgressHUD showErrorWithStatus:@"两次密码输入不一致"];
        return;
    }
    
    
    dispatch_queue_t queue = dispatch_queue_create("regist_queue", Nil);
    dispatch_async(queue, ^{
        NSString *surl = [NSString stringWithFormat:@"%@/%@?method=1&psd=%@&sex=%d&nick=%@&log_id=%@",kMAIN_URL,kUSER_URL,str_psd1,segSex.currentSelected,str_nick,str_id];
        
        surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:surl];
        NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:Nil];
        NSDictionary *dic = [res JSONValue];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([[dic valueForKey:@"err_code"] intValue] == 0) {
                [WSProgressHUD showSuccessWithStatus:@"注册成功" ];
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setValue:str_id forKey:kUSER_NAME];
                [def setValue:str_psd1 forKey:kUSER_PSD];
                [self performSelector:@selector(backToUpView:)
                           withObject:Nil
                           afterDelay:1.8f];
            }
            else{
                [WSProgressHUD showErrorWithStatus:[dic valueForKey:@"err_desc"]];
            }
        });
    });
}

- (IBAction)backToUpView:(id)sender{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SYS

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *btn_reg = (UIButton *)[self.view viewWithTag:11];
    [btn_reg setBackgroundColor:NAV_COLOR];
    [self.view setBackgroundColor:MAIN_COLOR];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
	[btn setFrame:CGRectMake(0, 8, 55, 35)];
	[btn setTitle:NSLocalizedString(@"Nav_Back", @"") forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
	[btn addTarget:self action:@selector(backToUpView:) forControlEvents:UIControlEventTouchUpInside];
    
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
	
	UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.leftBarButtonItem = item;
    
    self.navigationItem.title = @"注  册";
    
    UITextField *txt = (UITextField *)[self.view viewWithTag:1];
    [txt becomeFirstResponder];
    
    txt = (UITextField *)[self.view viewWithTag:4];
    segSex = [[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(txt.frame.origin.x, txt.frame.size.height+8+txt.frame.origin.y, txt.frame.size.width, txt.frame.size.height)
                                                       items:@[@{@"text":@"男",@"icon":@""},
                                                               @{@"text":@"女",@"icon":@""},
                                                               @{@"text":@"不填写",@"icon":@""}]
                                                iconPosition:IconPositionRight
                                           andSelectionBlock:^(NSUInteger segmentInde){
                                               
                                           }];
    
    segSex.color=[UIColor colorWithWhite:.1 alpha:.9];
    segSex.borderWidth=0.5;
    segSex.borderColor=[UIColor darkGrayColor];
    segSex.selectedColor=NAV_COLOR ;
    segSex.textAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:16],
                             NSForegroundColorAttributeName:[UIColor whiteColor]};
    segSex.selectedTextAttributes=@{NSFontAttributeName:[AllMethods getFontWithSize:16],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]};
    [self.view addSubview:segSex];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
