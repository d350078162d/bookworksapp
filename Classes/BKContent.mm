//
//  BKContent.m
//  BookWorksApp
//
//  Created by 刁志远 on 15/1/10.
//
//

#import "BKContent.h"

#import "RJBookData.h"
#import "RTLabel.h"
//#import "PunchScrollView.h"

#import "SwipeView.h"
#import "SOZOChromoplast.h"

#import <PopoverView/PopoverView.h>

#import "TFHpple.h"
#import "TFHppleElement.h"

#import "BookWorksAppAppDelegate.h"
#import "BKChapterList.h"

#import "BKChangeBookEnc.h"


#define TAG_FROM 100000
#define NUM_BK 7


#if kFREE

@interface BKContent ()<SwipeViewDataSource,SwipeViewDelegate, UIWebViewDelegate,BKChangeBookEncDelegate> {
    
    UIView *preAdView;
#else 
    
@interface BKContent ()<SwipeViewDataSource,SwipeViewDelegate, UIWebViewDelegate,BKChangeBookEncDelegate>{
    
#endif
    
    NSMutableArray *arrPageDic;//分线程加载页面
    NSMutableDictionary *dicList;//每一页数据
    NSString *allContent;//书籍内容
    int curIndex;//书籍index
    
    
    long curLength;//当前已解析长度
    
    
    int numPerPage;//一页的理想字符数
    int widPerLine;//一行的理想字符数
    int heiPerPage;//一页的高度字符数
    long lazyPage;//理想页面数量
    
    int curLoadPage;//当前正在加载的页码
    
    BOOL conLoad;//如果书籍页码正在加载，判断返回上一层时，是否需要继续加载。
    
    NSString *curBkImgName;//阅读的背景图片名称
    
    UILabel *labContent;//用于计算每页内容的label
    
    float lineSpace; //行间距
    
    UIColor *colorTitle;//书名字体颜色
    UIColor *colorTxt;//书籍内容字体颜色
    
    BOOL isDone;//是否加载完成（yes为完成，no未完成）
    
    UIView *viewLoad;//加载进度提示
    
    long gb30;//编码方式
    long gbKK;//编码方式
    long gb18030;
    
    UIScrollView *scrReadBk;//阅读背景选择
    
    PopoverView *popView;//弹出视图
    float fontSize; //字体大小
    
    float adHeight;
    
    BOOL shouldAuto;
    
    NSMutableArray *arrChapters;
    BKChapterList *chapterView;
}

@end

@implementation BKContent
    
    
#pragma mark - BKChangeBookEncDelegate

    - (void)didChangeEncode {
        
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
        NSString *file_path = [AllMethods applicationDocumentsDirectory];
        file_path = [NSString stringWithFormat:@"%@/%@.r",file_path,book.name];
        
        NSString *chapter_path = [NSString stringWithFormat:@"%@/%@.chapter",
                                  [AllMethods applicationDocumentsDirectory],
                                  book.name];
        if ([[NSFileManager defaultManager] fileExistsAtPath:chapter_path]) {
            [[NSFileManager defaultManager] removeItemAtPath:chapter_path
                                                       error:nil];
        }
        
        if (fontSize != 18.5) {
            file_path = [NSString stringWithFormat:@"%@/%@-%.1f.r",
                         [AllMethods applicationDocumentsDirectory],
                         book.name,fontSize];
        }
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:file_path]) {
            [[NSFileManager defaultManager] removeItemAtPath:file_path
                                                       error:nil];
        }
        
        viewLoad.hidden = NO;
        viewLoad.alpha = 1.0f;
        conLoad = YES;
        UILabel *lab = (UILabel *)[viewLoad viewWithTag:2];
        lab.hidden = NO;
        lab.alpha = 1.0f;
        
        [self.scrollView scrollToItemAtIndex:0 duration:0];
        
        curLoadPage = 1;
        curLength = 0;
        allContent = nil;
        [dicList removeAllObjects];
        dicList = nil;
        
    }
#pragma mark - SwipeViewDelegate


- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{
    [self toggleShowOrHideToolbar];
}

#pragma mark - SwipeViewDataSource

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    return lazyPage;
}



- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
//    UIView *view = (UIView *)[scrollView dequeueRecycledPage];
    if (!view) {
        
        view = [[UIView alloc] initWithFrame:self.scrollView.bounds];
        [view setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        RTLabel *lab = [[RTLabel alloc] initWithFrame:CGRectMake(10, 45, labContent.frame.size.width,
                                                        ScreenHeight-45)];
        lab.font = [AllMethods getFontWithSize:fontSize];
        lab.tag = TAG_FROM+1;
        lab.lineSpacing = lineSpace;
        [view addSubview:lab];
//        lab.layer.borderColor = colorTitle.CGColor;
//        lab.layer.borderWidth = .5f;
        
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
        
        UILabel *lab_title = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, view.frame.size.width, 20)];
        lab_title.font = [AllMethods getFontWithSize:12];
        lab_title.text = book.name;
        lab_title.textColor = colorTitle;
        [view addSubview:lab_title];
        lab_title.tag = TAG_FROM+11;

        UILabel *lab_page = [[UILabel alloc] initWithFrame:CGRectMake(20, view.frame.size.height-25,
                                                                      view.frame.size.width-40, 20)];
        lab_page.textAlignment = NSTextAlignmentRight;
        lab_page.textColor = colorTitle;
        lab_page.font = [AllMethods getFontWithSize:14];
        lab_page.tag = TAG_FROM+2;
        [view addSubview:lab_page];
        
    }
    
    
    UILabel *lab_tit = (UILabel *)[view viewWithTag:TAG_FROM+11];
    lab_tit.textColor = colorTitle;
    
    RTLabel *lab = (RTLabel *)[view viewWithTag:TAG_FROM+1];
    lab.textColor = colorTxt;
    
    NSValue *val = [dicList objectForKey:[NSString stringWithFormat:@"%ld",index+1]];
    
    if (val) {
        NSRange ran = [val rangeValue];
        if (ran.location+ran.length >= allContent.length) {
            if (allContent.length > ran.location)
                lab.text = [allContent substringFromIndex:ran.location];
            else lab.text = @"全书完";
        }
        else {
            NSString *content = [allContent substringWithRange:ran];
            content = [content stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
            lab.text = content;
        }
    }
    else {
        lab.text = @"加载中...";
    }
    
    UILabel *lab_page = (UILabel *)[view viewWithTag:TAG_FROM+2];
    lab_page.text = [NSString stringWithFormat:@"%ld / %ld",index+1,[[dicList allKeys] count]];
    lab_page.textColor = colorTitle;
    
    
    return view;
}

#pragma mark - My

- (void)toggleShowOrHideToolbar{
    
    if (!self.viewProgress.hidden) {
        self.viewProgress.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.viewProgress.alpha = 0;
                         }completion:^(BOOL finished){
                             self.viewProgress.hidden = YES;
                         }];
        
        return;
    }
    
    if (self.toolBar.hidden) {
        self.toolBar.alpha = 0;
        self.toolBar.hidden = NO;
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.toolBar.alpha = 1;
                         }completion:^(BOOL finished){
                             self.toolBar.hidden = NO;
                         }];
    }
    else {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
        self.toolBar.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.toolBar.alpha = 0;
                         }completion:^(BOOL finished){
                             self.toolBar.hidden = YES;
                         }];
    }
    
}
    - (void)scrollToChapter:(NSString *)chapter {
        
        NSRange c_ran = [allContent rangeOfString:chapter];
        for (NSString *key in [dicList allKeys]) {
            NSValue *val = [dicList objectForKey:key];
            NSRange ran = [val rangeValue];
            if (ran.location < c_ran.location && ran.location+ran.length>c_ran.location) {
                NSInteger page = [key intValue]-1 ;
                [_scrollView scrollToPage:page duration:0.f];
                break;
            }
        }
        
    }
    

- (void)toggleShowOrHideProgressView{
    
    if (!chapterView) {
        chapterView = [[BKChapterList alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) andChapterList:arrChapters];
        chapterView.hidden = YES;
        [self.view addSubview:chapterView];
        
        __weak BKContent *weakSelf = self;
        [chapterView setSelectTitle:^(NSString *title, NSInteger index) {
            [weakSelf scrollToChapter:title];
        }];
    }
    
    if (chapterView.hidden) {
        chapterView.alpha = 0;
        chapterView.hidden = NO;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             chapterView.alpha = 1;
                             if (!self.toolBar.hidden) {
                                 self.toolBar.alpha = 0;
                             }
                         }completion:^(BOOL finished){
                             chapterView.hidden = NO;
                             self.toolBar.hidden = YES;
                         }];
    }
    else {
        chapterView.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             chapterView.alpha = 0;
                         }completion:^(BOOL finished){
                             chapterView.hidden = YES;
                         }];
    }

    
    
    /*
    self.labPage.text = [NSString stringWithFormat:@"%ld/%d",[self.scrollView currentItemIndex]+1,
                         [[dicList allKeys] count]];
    self.slideProgress.value = ([self.scrollView currentItemIndex]+1)*1.0/[[dicList allKeys] count];
    
    if (self.viewProgress.hidden) {
        self.viewProgress.alpha = 0;
        self.viewProgress.hidden = NO;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.viewProgress.alpha = 1;
                             if (!self.toolBar.hidden) {
                                 self.toolBar.alpha = 0;
                             }
                         }completion:^(BOOL finished){
                             self.viewProgress.hidden = NO;
                             self.toolBar.hidden = YES;
                         }];
    }
    else {
        self.viewProgress.alpha = 1;
        [UIView animateWithDuration:.2f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.viewProgress.alpha = 0;
                         }completion:^(BOOL finished){
                             self.viewProgress.hidden = YES;
                         }];
    }
     */
}


//二分查找算法就算每页显示字数

- (BOOL)loadContentForPage:(int)page{
    
    if (!dicList) {
        dicList = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
//    NSString *str_punct = @"，。？！：“”";
    
    NSString *all_content = allContent;//[NSString stringWithString:allContent];
    
    
    long curPage = page;
    long offset = 0;
    if (page > 1) {
        NSValue *val = [dicList objectForKey:[NSString stringWithFormat:@"%d",page-1]];
        NSRange ran = [val rangeValue];
        offset = ran.length+ran.location;
    }
    
    if (offset >= [all_content length]) {
        return NO;
    }
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpace];
    NSDictionary *dic_att = @{NSFontAttributeName:[AllMethods getFontWithSize:fontSize],
                              NSParagraphStyleAttributeName:paragraphStyle};
    
    long len = numPerPage;
    NSRange ran_long = NSMakeRange(0, 0);
    NSRange ran_short = NSMakeRange(0, 0);
    
    if (len+offset <= [all_content length]) {
        
        while (true) {
            NSRange ran = NSMakeRange(offset, len);
            
            NSString *sc = [all_content substringWithRange:ran];
            
            CGSize sz;
            @autoreleasepool {
                sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:dic_att
                                      context:nil].size;
            }
            
            
            if (sz.height > labContent.frame.size.height) {
                ran_long = ran;
                if (ran_short.length+ran_short.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len-=widPerLine;
                }
            }
            else {
                ran_short = ran;
                if (ran_long.length+ran_long.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len+=widPerLine;
                    if (len+offset>=[allContent length]) {
                        if (ran_short.length == [allContent length]-offset) {
                            NSValue *val = [NSValue valueWithRange:ran_short];
                            [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                            curLength+=ran_short.length;
                            break;
                        }
                        else {
                            len = [allContent length]-offset;
                        }
                    }
                }
            }
            
            if (fabs(ran_long.length-ran_short.length) <= 1) {
                NSValue *val = [NSValue valueWithRange:ran_short];
                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                curLength+=ran_short.length;
                break;
            }
        }
        
        return YES;
    }
    else {
        len = [all_content length]-offset;
        while (true) {
            NSRange ran = NSMakeRange(offset, len);
            NSString *sc = [all_content substringWithRange:ran];
            
            CGSize sz;
            @autoreleasepool {
                sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:dic_att
                                      context:nil].size;
            }
            
            if (sz.height > labContent.frame.size.height) {
                ran_long = ran;
                if (ran_short.length+ran_short.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len-=widPerLine;
                }
            }
            else {
                ran_short = ran;
                if (ran_long.length+ran_long.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    NSValue *val = [NSValue valueWithRange:ran_short];
                    [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                    curLength+=ran_short.length;
                    break;
                }
            }
            if (fabs(ran_long.length-ran_short.length) <= 1) {
                NSValue *val = [NSValue valueWithRange:ran_short];
                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                curLength+=ran_short.length;
                break;
            }
        }
        return YES;
    }
    
    
    if (offset >= [all_content length]) {
        return NO;
    }
    
    NSRange ran = NSMakeRange(offset, [all_content length]-offset);
    
    
    
    NSString *sc = [all_content substringWithRange:ran];
    
    CGSize sz;
    @autoreleasepool {
        sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                              options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:dic_att
                              context:nil].size;
    }
    
    if (sz.height < labContent.frame.size.height) {
        ran_short = ran;
        if (ran_long.length+ran_long.length > 0) {
            len = (ran_long.length+ran_short.length)/2;
        }
        else {
            NSValue *val = [NSValue valueWithRange:ran_short];
            [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
            curLength+=ran_short.length;
        }
        return YES;
        
    }
    else {
        len = [all_content length]-offset-widPerLine;
        while (true) {
            NSRange ran = NSMakeRange(offset, len);
            NSString *sc = [all_content substringWithRange:ran];
            
            CGSize sz;
            @autoreleasepool {
                sz = [sc boundingRectWithSize:CGSizeMake(labContent.frame.size.width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:dic_att
                                      context:nil].size;
            }
            
            if (sz.height > labContent.frame.size.height) {
                ran_long = ran;
                if (ran_short.length+ran_short.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len-=widPerLine;
                }
            }
            else {
                ran_short = ran;
                if (ran_long.length+ran_long.length > 0) {
                    len = (ran_long.length+ran_short.length)/2;
                }
                else {
                    len+=widPerLine;
                }
            }
            if (fabs(ran_long.length-ran_short.length) <= 1) {
                NSValue *val = [NSValue valueWithRange:ran_short];
                [dicList setValue:val forKey:[NSString stringWithFormat:@"%ld",curPage]];
                curLength+=ran_short.length;
                break;
            }
        }
        return YES;
    }
}

- (void)loadPageInQueue{
    
    dispatch_queue_t queue = dispatch_queue_create("get_page_list", nil);
    dispatch_async(queue, ^{
        
        BOOL bol = [self loadContentForPage:curLoadPage++];
        dispatch_sync(dispatch_get_main_queue(), ^{
            UILabel *lab = (UILabel *)[viewLoad viewWithTag:2];
            
            if(bol && conLoad) {
                lab.text = [NSString stringWithFormat:@"已加载%.0f%%",(curLength*100.0)/(1.0*[allContent length])];
                [self loadPageInQueue];
            }
            else if(conLoad){
                
                RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
                NSString *file_path = [AllMethods applicationDocumentsDirectory];
                file_path = [NSString stringWithFormat:@"%@/%@.r",file_path,book.name];
                
                if (fontSize != 18.5) {
                    file_path = [NSString stringWithFormat:@"%@/%@-%.1f.r",
                                 [AllMethods applicationDocumentsDirectory],book.name,fontSize];
                }
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dicList];
                if(![data writeToFile:file_path atomically:YES])
                    NSLog(@"save failed");
                
                UIActivityIndicatorView *act = (UIActivityIndicatorView *)[viewLoad viewWithTag:1];
                [act stopAnimating];
                lazyPage = [[dicList allKeys] count];
                [self.scrollView reloadData];
                
                [UIView animateWithDuration:.2f
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     viewLoad.alpha = .0f;
                                 }completion:^(BOOL finished){
                                     viewLoad.hidden = YES;
                                     
                                     [self firstShowContent];
                                 }];
            }
        });
        
    });
}

- (IBAction)itemClicked:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                                 [[BookWorksAppAppDelegate shareInstance].tabCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
                                 
                             }];
}

- (void)loadBook{
    
    [WSProgressHUD showWithStatus:@"Loading..." maskType:WSProgressHUDMaskTypeClear];
    dispatch_queue_t queue = dispatch_queue_create("load_book_initinal", nil);
    dispatch_async(queue, ^{
        BOOL isFirst;//是否首次打开书籍
        NSString *str = @"阅";
        CGSize size = [str sizeWithAttributes:@{NSFontAttributeName: [AllMethods getFontWithSize:fontSize]}];
        
        if (!allContent) {
            
            RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
            
            NSString *file_enc = [[AllMethods applicationDocumentsDirectory] stringByAppendingPathComponent:@"file_encode.enc"];
            NSMutableDictionary *dic_enc ;
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:file_enc]) {
                dic_enc = [NSMutableDictionary dictionaryWithContentsOfFile:file_enc];
            }
            else {
                dic_enc = [NSMutableDictionary dictionaryWithCapacity:3];
            }
            
            if (dic_enc[book.bookFile]) {
                
                long enc = [dic_enc[book.bookFile] longValue];
                allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                       encoding:enc
                                                          error:nil];
                
                
            }
            else {
                NSStringEncoding encode = NSUTF8StringEncoding;
                allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
                
                if (allContent && [allContent rangeOfString:@"HTTP Error"].location != NSNotFound) {
                    [WSProgressHUD dismiss];
                    [AllMethods showAltMsg:@"由于版权问题书籍暂时被删除，请稍后重试"];
                    return ;
                }
                
                if (!allContent) {
                    encode = gb30;
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:gb30
                                                              error:nil];
                }
                if (!allContent) {
                    encode = gbKK;
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:gbKK
                                                              error:nil];
                }
                
                if (!allContent) {
                    encode = gb18030;
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:gb18030
                                                              error:nil];
                }
                
                if (!allContent) {
                    
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
                    
                    
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                
                if (!allContent) {
                    
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_2312_80);
                    
                    
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                
                if (!allContent) {
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGBK_95);
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                if (!allContent) {
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingISO_2022_CN);
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                if (!allContent) {
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingISO_2022_CN_EXT);
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                if (!allContent) {
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingEUC_CN);
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                if (!allContent) {
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingHZ_GB_2312);
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                if (!allContent) {
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5);
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                if (!allContent) {
                    encode = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingMacChineseSimp);
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:encode
                                                              error:nil];
                }
                if (!allContent) {
                    encode = NSUTF16LittleEndianStringEncoding;
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:NSUTF16LittleEndianStringEncoding
                                                              error:nil];
                }
                if (!allContent) {
                    encode = NSUTF16BigEndianStringEncoding;
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:NSUTF16BigEndianStringEncoding
                                                              error:nil];
                }
                if (!allContent) {
                    encode = NSUnicodeStringEncoding;
                    allContent = [NSString stringWithContentsOfFile:book.bookFile
                                                           encoding:NSUnicodeStringEncoding
                                                              error:nil];
                }
                
                
                if (!allContent) {
                    
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [WSProgressHUD dismiss];
                        [AllMethods showAltMsg:@"暂不支持此编码类型的文件"];
                        [self toggleShowOrHideToolbar];
//                        [self.navigationController popViewControllerAnimated:YES];
                        [self dismissViewControllerAnimated:YES completion:^{
                            [[BookWorksAppAppDelegate shareInstance].tabCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
                        }];
                    });
                    
                    return;
                }
                dic_enc[book.bookFile] = @(encode);
                [dic_enc writeToFile:file_enc atomically:YES];
            }
            
            
            
            
            
            NSArray *arr_para = [allContent componentsSeparatedByString:@"\n"];
            allContent = [arr_para componentsJoinedByString:@"\n  "];
            
            int t_w =  (labContent.frame.size.width/(size.width+5));
            int t_h = (labContent.frame.size.height/(size.height+5));
            lazyPage = [allContent length]/(t_h*t_w);
            if (lazyPage == 0) {
                lazyPage = 1;
            }
        }
        
        NSComparator cmptr = ^(id obj1, id obj2){
            if ([obj1 integerValue] > [obj2 integerValue]) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            
            if ([obj1 integerValue] < [obj2 integerValue]) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        };
        
        if (!arrChapters) {
            arrChapters = [NSMutableArray arrayWithCapacity:3];
        }
        [arrChapters removeAllObjects];
        
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
        NSString *file_path = [AllMethods applicationDocumentsDirectory];
        file_path = [NSString stringWithFormat:@"%@/%@.r",file_path,book.name];
        
        NSString *chapter_path = [NSString stringWithFormat:@"%@/%@.chapter",
                                  [AllMethods applicationDocumentsDirectory],
                                  book.name];
        if ([[NSFileManager defaultManager] fileExistsAtPath:chapter_path]) {
            NSArray *arr = [NSArray arrayWithContentsOfFile:chapter_path];
            [arrChapters addObjectsFromArray:arr];
        }
        else {
            [AllMethods separateChapter:arrChapters
                                content:allContent
                        andContentFrame:labContent.frame
                            andFontSize:fontSize
                           andLineSpace:lineSpace];
            [arrChapters writeToFile:chapter_path atomically:YES];
        }
        
        if (fontSize != 18.5) {
            file_path = [NSString stringWithFormat:@"%@/%@-%.1f.r",
                         [AllMethods applicationDocumentsDirectory],
                         book.name,fontSize];
        }
        
        
//#warning 上架之前要去掉
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:file_path]) {
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithFile:file_path];
            dicList = [NSMutableDictionary dictionaryWithDictionary:dic];
            NSArray *arr = [[dicList allKeys] sortedArrayUsingComparator:cmptr];
            curLoadPage = [[arr lastObject] intValue];
            NSRange ran = [[dicList objectForKey:[arr lastObject]] rangeValue];
            curLength = ran.location + ran.length;
            isFirst = NO;
            
            if (ran.location+ran.length == [allContent length]) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [WSProgressHUD dismiss];
                    lazyPage = [[dicList allKeys] count];
                    [self.view bringSubviewToFront:viewLoad];
                    [self.view bringSubviewToFront:self.toolBar];
                    if (!self.scrollView.delegate) {
                        self.scrollView.dataSource = self;
                        self.scrollView.delegate = self;
                    }
                    [self.scrollView reloadData];
                    [self firstShowContent];
//                    [self performSelector:@selector(firstShowContent) withObject:nil afterDelay:.2f];
                });
                
                return ;
            }
        }
        else {
            curLoadPage = 1;
            isFirst = YES;
        }
        
        
        widPerLine = (labContent.frame.size.width/(size.width+2));
        heiPerPage = (labContent.frame.size.height/(size.height+2));
        numPerPage = widPerLine*heiPerPage;
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [self.view bringSubviewToFront:viewLoad];
            [self.view bringSubviewToFront:self.toolBar];
            UIActivityIndicatorView *act = (UIActivityIndicatorView *)[viewLoad viewWithTag:1];
            [act startAnimating];
            [self loadPageInQueue];
            
            if (!self.scrollView.delegate) {
                self.scrollView.dataSource = self;
                self.scrollView.delegate = self;
            }
            if(!isFirst){
                [WSProgressHUD dismiss];
                [self firstShowContent];
            }
            else [self performSelector:@selector(firstShowContent) withObject:nil afterDelay:.5f];
        });
        
    });
}

- (void)firstShowContent{
    
    [self.scrollView reloadData];
    
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
    
    NSString *str = [AllMethods getReadProgressWithKey:book.name];
    
    if (!str) {
        str = [[NSUserDefaults standardUserDefaults] valueForKey:book.name];
    }
    
    if (str) {
        NSArray *arr = [str componentsSeparatedByString:@"-"];
        long page = [[arr firstObject] longLongValue]-1;
        [self.scrollView scrollToItemAtIndex:page duration:0];
    }
    [WSProgressHUD dismiss];
}

- (void)buildLoadView{
    viewLoad = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth-100, ScreenHeight-22, 95, 20)];
    
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    act.tag = 1;
    act.center = CGPointMake(13, viewLoad.frame.size.height/2);
    [act stopAnimating];
    [viewLoad addSubview:act];
    
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, viewLoad.frame.size.width-15,
                                                             viewLoad.frame.size.height)];
    lab.textAlignment = NSTextAlignmentRight;
    lab.textColor = colorTitle;//[UIColor whiteColor];
    lab.font = [AllMethods getFontWithSize:11];
    lab.tag = 2;
    [viewLoad addSubview:lab];
    lab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:viewLoad];
    
//    viewLoad.backgroundColor = [UIColor colorWithWhite:0 alpha:.6f];
    
    viewLoad.center = CGPointMake(ScreenWidth/2, viewLoad.center.y);
    
    viewLoad.layer.cornerRadius = 4.6f;
    
    [self.view bringSubviewToFront:self.toolBar];
}

- (void)selBkBtnClicked:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSString *sname = [NSString stringWithFormat:@"reading_bg%d",(int)btn.tag];
    curBkImgName = [NSString stringWithString:sname];
    
    UIImage *img = [UIImage imageNamed:curBkImgName];
    [self.scrollView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    [self.btnReadBk setBackgroundImage:img forState:UIControlStateNormal];
    
    [popView dismiss];
    
    SOZOChromoplast *cas = [[SOZOChromoplast alloc] initWithImage:img];
    colorTitle = [cas secondHighlight];
    if(btn.tag == NUM_BK) colorTxt = [cas secondHighlight];
    else colorTxt = [cas firstHighlight];
    cas = nil;
    
    [[NSUserDefaults standardUserDefaults] setValue:curBkImgName forKey:@"novel_back"];
    [self.scrollView reloadData];
    
//    UIGraphicsBeginImageContext(self.view.bounds.size);
//    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    UIImageView *iv = [[UIImageView alloc] initWithFrame:self.view.bounds];
//    iv.image = image;
//    [self.view addSubview:iv];
//    
//    int index = [self.scrollView currentItemIndex];
//    
//    [self.scrollView reloadData];
//    [self.scrollView scrollToItemAtIndex:index duration:0];
//    
//    [UIView animateWithDuration:.25f
//                          delay:.15f
//                        options:UIViewAnimationOptionCurveEaseOut
//                     animations:^{
//                         iv.alpha = 0;
//                     }completion:^(BOOL finished){
//                         [iv removeFromSuperview];
//                     }];
    
}

- (void)iniitialBkSelectView{
    if (scrReadBk) {
        return;
    }
    
    scrReadBk = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 180, 160)];
    
    float wid = 50;
    float hei = 50;
    float jian = (scrReadBk.frame.size.width-wid*3)/4.0;
    float size_h = 0;
    for (int i=0; i<NUM_BK; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = i+1;
        btn.frame = CGRectMake(((i%3)+1)*jian+(i%3)*wid, (i/3+1)*jian+(i/3)*hei, wid, hei);
        NSString *sname = [NSString stringWithFormat:@"reading_bg%d",i+1];
        [btn setBackgroundImage:[UIImage imageNamed:sname] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(selBkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [scrReadBk addSubview:btn];
        if (i==NUM_BK-1) {
            size_h = btn.frame.origin.y+btn.frame.size.height+10;
        }
    }
    
    [scrReadBk setContentSize:CGSizeMake(1, size_h)];
}

- (IBAction)toolBtnClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 0) {

//        [self.navigationController popViewControllerAnimated:YES];
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            [[BookWorksAppAppDelegate shareInstance].tabCtrl.view addSubview:[[BookWorksAppAppDelegate shareInstance] getADView]];
        }];
        
    }
    else if(btn.tag == 1){//进度
        [self toggleShowOrHideProgressView];
    }
    else if (btn.tag == 2){//阅读背景
        [self iniitialBkSelectView];
        
        
        CGPoint pot = CGPointMake(btn.center.x, self.toolBar.center.y-btn.frame.size.height/2);
        
        popView = [PopoverView showPopoverAtPoint:pot//self.toolBar.center
                                           inView:self.view
                                  withContentView:scrReadBk
                                         delegate:nil];
    }
    else if (btn.tag == 5) {//切换编码
        conLoad = NO;
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
        BKChangeBookEnc *bk = [[BKChangeBookEnc alloc] initWithBook:book andDelegate:self];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:bk];
        
        [self presentViewController:nav animated:YES completion:nil];
        
//        [self.navigationController pushViewController:bk animated:YES];
    }
}

- (IBAction)slideValChanged:(id)sender {
    long page = self.slideProgress.value*[[dicList allKeys] count];
    self.labPage.text = [NSString stringWithFormat:@"%ld/%ld",page,[[dicList allKeys] count]];
}

- (IBAction)slidePageChanged:(id)sender {
    int page = self.slideProgress.value*[[dicList allKeys] count];
    if (page<1) {
        page = 1;
    }
//    [self.scrollView scrollToIndexPath:[NSIndexPath indexPathForRow:page-1 inSection:0]
//                              animated:NO];
    [self.scrollView scrollToItemAtIndex:page-1 duration:0];
}

- (void)saveProgress{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
    
    long index = [self.scrollView currentItemIndex];
    
    float per = 100*(index*1.0+1.0)/[[dicList allKeys] count];
    
    NSString *str = [NSString stringWithFormat:@"%ld-%.2f",index+1,per];
    [def setValue:str forKey:book.name];
    
    NSDictionary *dic = @{@"id":book.name,@"name":book.name,@"progress":str};
    [AllMethods saveReadProgressInfo:dic];
}
    



#pragma mark - SYS

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if ([self isBeingDismissed]) {
        conLoad = NO;
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
        [USER_DEFAULT setValue:book.name forKey:LAST_READ];
        NSString *file_path = [AllMethods applicationDocumentsDirectory];
        file_path = [NSString stringWithFormat:@"%@/%@.r",file_path,book.name];
        
        if (fontSize != 18.5) {
            file_path = [NSString stringWithFormat:@"%@/%@-%.1f.r",[AllMethods applicationDocumentsDirectory],book.name,fontSize];
        }
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dicList];
        if(![data writeToFile:file_path atomically:YES])
            NSLog(@"save failed");
        
        [self saveProgress];
#if kFREE
        //[NSObject cancelPreviousPerformRequestsWithTarget:adView];
#endif
    }
    
}
    
    - (void)dealloc {
        
        conLoad = NO;
        RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
        NSString *file_path = [AllMethods applicationDocumentsDirectory];
        file_path = [NSString stringWithFormat:@"%@/%@.r",file_path,book.name];
        
        if (fontSize != 18.5) {
            file_path = [NSString stringWithFormat:@"%@/%@-%.1f.r",[AllMethods applicationDocumentsDirectory],book.name,fontSize];
        }
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dicList];
        if(![data writeToFile:file_path atomically:YES])
            NSLog(@"save failed");
        
        [self saveProgress];
        
        
    }

- (id)initWithBookIndex:(int)index{
    self = [super init];
    if (self) {
        curIndex = index;
    }
    
    return self;
}
    
    - (void)viewWillAppear:(BOOL)animated {
        
        [super viewWillAppear:animated];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    float ipad_h = 0;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ipad_h = 20;
    }
    
    if (!labContent) {
        labContent = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-20, ScreenHeight-85-adHeight-ipad_h)];
        CGRect frame = self.scrollView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = ScreenWidth;
        frame.size.height = ScreenHeight;
        self.scrollView.frame = frame;
    }
    
    conLoad = YES;
    [self loadBook];
    
    [self performSelector:@selector(toggleShowOrHideToolbar)
               withObject:nil
               afterDelay:1.0f];
#if kFREE
//    [NSObject cancelPreviousPerformRequestsWithTarget:adView];
//    [adView performSelector:@selector(load) withObject:nil afterDelay:AD_SEP];
#endif
   

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    shouldAuto = YES;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_auto"];
    
    gb30= 0x80000632;
    gbKK= 0x80000631;
    gb18030 = 0x80000630;
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    isDone = NO;
    conLoad = YES;
    lazyPage = 0;
    lineSpace = 6.6;
    fontSize = [[[NSUserDefaults standardUserDefaults] valueForKey:kFONT_SIZE] floatValue];//18.5;
    
    
    if (kFREE == 1) {
        adHeight = 50+fontSize;
    }
    else {
        adHeight = 0;
    }
    
    dicList = [NSMutableDictionary dictionaryWithCapacity:3];
    
    curBkImgName = [[NSUserDefaults standardUserDefaults] valueForKey:@"novel_back"];
    if(!curBkImgName) curBkImgName = @"reading_bg1";
    int index = [[curBkImgName substringFromIndex:[@"reading_bg" length]] intValue];
    UIImage *img = [UIImage imageNamed:curBkImgName];
    [self.scrollView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    [self.btnReadBk setBackgroundImage:img forState:UIControlStateNormal];
    
    self.btnReadBk.layer.cornerRadius = self.btnBack.frame.size.width/2;
    self.btnReadBk.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnReadBk.layer.borderWidth = .5f;
    self.btnReadBk.clipsToBounds = YES;
    
    SOZOChromoplast *cas = [[SOZOChromoplast alloc] initWithImage:img];
    colorTitle = [cas secondHighlight];
    if(index < NUM_BK) colorTxt = [cas firstHighlight];
    else colorTxt = [cas secondHighlight];
    cas = nil;
    [self buildLoadView];
    
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"v3_reading_menu_back_normal"]
                            forState:UIControlStateNormal];
    self.btnBack.layer.cornerRadius = self.btnBack.frame.size.width/2;
    self.btnBack.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnBack.layer.borderWidth = .5f;
    
    
//    [self.btnProgress setBackgroundImage:[UIImage imageNamed:@"v3_reading_menu_progress"] forState:UIControlStateNormal];
    
    self.viewProgress.frame = CGRectMake(0, ScreenHeight-self.viewProgress.frame.size.height,
                                         ScreenWidth, self.viewProgress.frame.size.height);
    self.viewProgress.backgroundColor = [UIColor colorWithWhite:0 alpha:.75f];
    
    self.viewProgress.hidden = YES;
    self.labPage.font = [AllMethods getFontWithSize:15];
    self.labPage.textColor = [UIColor whiteColor];
    [self.view addSubview:self.viewProgress];
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    [del changeADCenter:CGPointMake(ScreenWidth/2, ScreenHeight-48-49+24+10)];
    
    
//    RJSingleBook *book = [[RJBookData sharedRJBookData].books objectAtIndex:curIndex];
//    myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(15, 80, 190, 190)];
//    
//    NSURL *url = [NSURL fileURLWithPath:book.bookFile];
//    if (!url) {
//        url = [NSURL fileURLWithPath:[book.bookFile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    }
//    
//    NSURLRequest *req = [NSURLRequest requestWithURL:url];
//    [myWeb loadRequest:req];
//    
//    [self.view addSubview:myWeb];
//    
//    NSError *error = nil;
//    NSStringEncoding enc=CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
//    
//    NSString * contentStr=[NSString stringWithContentsOfFile:book.bookFile
//                                                    encoding:enc error:&error];
//    
//    if (error) {
//        NSLog(@"%@",error.description);
//    }
//    else {
//        NSLog(@"%@",contentStr);
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
