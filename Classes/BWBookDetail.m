//
//  BWBookDetail.m
//  BookWorksApp
//
//  Created by 刁志远 on 14-1-13.
//
//

#import "BWBookDetail.h"

#import "BWWebView.h"

#import "UIImageView+WebCache.h"

@interface BWBookDetail ()

@end

@implementation BWBookDetail

#pragma mark - UITableViewDelegate UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setBackgroundColor:NAV_SHADOW_COLOR];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *str_id = @"book_list_cell_id";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str_id];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str_id];
        
    }
    cell.textLabel.text = [dicDetail valueForKey:@"longIntro"];
    cell.textLabel.numberOfLines = 100;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [AllMethods getFontWithSize:16];
    
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGSize size = CGSizeMake(ScreenWidth-30, CGFLOAT_MAX);
    NSString *str = [dicDetail valueForKey:@"longIntro"];
    CGSize res = [str sizeWithFont:[AllMethods getFontWithSize:16] constrainedToSize:size];
    return res.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  self.headView.frame.size.height;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenHeight, 44)];
    [view setBackgroundColor:NAV_SHADOW_COLOR];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, ScreenWidth/2, 44);
    [btn setShowsTouchWhenHighlighted:YES];
    if ([arrLocal containsObject:[dicDetail valueForKey:@"_id"]]) {
        [btn setTitle:@"已添加" forState:UIControlStateNormal];
        [btn setEnabled:NO];
    }
    else [btn setTitle:@"添加图书" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(addBook:) forControlEvents:UIControlEventTouchUpInside];
    
    [btn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn.layer setBorderWidth:.5f];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(ScreenWidth/2, 0, ScreenWidth/2, 44);
    [btn2 setShowsTouchWhenHighlighted:YES];
    [btn2 setTitle:@"阅读图书" forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(readBook:) forControlEvents:UIControlEventTouchUpInside];
    [btn2.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn2.layer setBorderWidth:.5f];
    
    [view addSubview:btn];
    [view addSubview:btn2];
    [view.layer setBorderColor:[UIColor colorWithWhite:1.0 alpha:.8].CGColor];
    [view.layer setBorderWidth:2.3f];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 44;
}

#pragma mark - MY

- (void)readBook:(id)sender{
    BWWebView *web = [[BWWebView alloc] init];
    [web setHidesBottomBarWhenPushed:YES];
    [web setCurBook:dicBook];
    [self.navigationController pushViewController:web animated:YES];
}

- (void)addBook:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSString *sql = [NSString stringWithFormat:@"insert into 'main'.'bookbag' (id,book_name,book_id,book_author,book_img,book_other)"\
                     " values (NULL,'%@','%@','%@','%@',NULL)",[dicDetail valueForKey:@"title"],[dicDetail valueForKey:@"_id"],
                     [dicDetail valueForKey:@"author"],[dicDetail valueForKey:@"cover"]];
    [AllMethods insertBookWithSQL:sql];
    [btn setTitle:@"已添加" forState:UIControlStateNormal];
    btn.enabled = NO;
    [self getLocalBook];
}

- (void)getBookDetail{
    if (!dicUrl) {
        dicUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"book_url"];
    }
    [WSProgressHUD showWithStatus:@"Loading..."];
    
    
    dispatch_queue_t queue = dispatch_queue_create("get_book_type_list", nil);
    dispatch_async(queue, ^(void){
        
        NSString *str_url = [NSString stringWithFormat:[dicUrl valueForKey:@"book_detail"],[dicBook valueForKey:@"_id"]];
        NSString *res = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:str_url]
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
        
        
        if ([res length] <= 0) {
        }
        else{
            NSDictionary *dic = [res JSONValue];
            dicDetail = [[NSDictionary alloc] initWithDictionary:dic];
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.labAuthor.text = [dicDetail valueForKey:@"author"];
                self.labTtile.text = [dicDetail valueForKey:@"title"];
                if ([dicDetail objectForKey:@"isSerial"]) {
                    self.labState.text = @"连载中...";
                }
                else self.labState.text = @"已完结";
                
                NSString *surl = [NSString stringWithFormat:@"%@%@",[dicUrl valueForKey:@"book_img"],[dicDetail valueForKey:@"cover"]];
                
                [self.urlImg sd_setImageWithURL:[NSURL URLWithString:surl]];
                
//                NSLog(@"%@",[dicDetail valueForKey:@"longIntro"]);
                
                [self.myTable reloadData];
            });
            [WSProgressHUD dismiss];
        }
    });
    
    
}

- (void)setCurBook:(NSDictionary *)dic{
    dicBook = [[NSDictionary alloc] initWithDictionary:dic];
}

- (void)getLocalBook{
    NSArray *arr = [AllMethods getLocalBookID];
    if (!arrLocal) {
        arrLocal = [[NSMutableArray alloc] initWithCapacity:3];
    }
    [arrLocal addObjectsFromArray:arr];
    [self.myTable reloadData];
}


- (void)backUp{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SYS

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getBookDetail];
    self.labTtile.text = [dicBook valueForKey:@"title"];
    
    [self getLocalBook];
    [self.myTable setBackgroundColor:NAV_SHADOW_COLOR];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 32, 32);
    [btn setBackgroundImage:[UIImage imageNamed:@"back_up.png"] forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn addTarget:self action:@selector(backUp) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.title = @"书籍详情";
    
    [self.headView setBackgroundColor:NAV_COLOR];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
