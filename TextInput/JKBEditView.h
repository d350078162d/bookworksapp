//
//  JKBEditView.h
//  JKBDoctor
//
//  Created by 刁志远 on 14-8-22.
//  Copyright (c) 2014年 DHCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaceholderTextView.h"

typedef enum {
    EDIT_PERSONAL_INFO=0,
    EDIT_GOOD,
    EDIT_NOTICE,
    EDIT_REJECT_VIDEO,
    EDIT_QUESTION,
    EDIT_REPLY,
}EditType;

@protocol JKBEditViewDelegate;

@interface JKBEditView : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet PlaceholderTextView *txtDesc;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activeView;
@property (nonatomic,strong) id<JKBEditViewDelegate> delegate;
@property (nonatomic,strong) UIViewController *tmpCtrl;


- (id)initWithTitle:(NSString *)tit andDesc:(NSString *)desc;
- (id)initWithTitle:(NSString *)tit andDesc:(NSString *)desc withEditType:(EditType)type;
- (id)initWithTitle:(NSString *)tit andDesc:(NSString *)desc andItem:(NSDictionary *)dic;


- (IBAction)btnClicked:(id)sender;
- (void)setShouldReset:(BOOL)bol;

- (void)showEdit;
- (void)dismissEdit;
- (void)makeEditScale:(float)scale;
- (void)setActAnimate:(BOOL)ani;

@end


@protocol JKBEditViewDelegate <NSObject>

@optional
- (void)editView:(JKBEditView *)edit didClickedOK:(NSString *)str;
- (void)editView:(JKBEditView *)edit didSendReply:(NSString *)str;
- (void)editViewDidDismiss;
- (void)editView:(JKBEditView *)edit didUpdateDesc:(NSString *)str;
- (void)editView:(JKBEditView *)edit didUpdateGoodDisease:(NSString *)str;
- (void)editView:(JKBEditView *)edit inputReason:(NSString *)str;

@end


