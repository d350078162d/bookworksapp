//
//  JKBEditView.m
//  JKBDoctor
//
//  Created by 刁志远 on 14-8-22.
//  Copyright (c) 2014年 DHCC. All rights reserved.
//

#import "JKBEditView.h"
#import "BookWorksAppAppDelegate.h"

@interface JKBEditView (){
    
    NSString *strTitle,*strDesc;
    EditType eType;
    NSDictionary *dicItem;
    
    BOOL shouldReset;
    
    UIView *maskView;
}

@end

@implementation JKBEditView

#pragma mark - My

- (void)sendReply{//发表回复
    
    [self.txtDesc resignFirstResponder];
    [WSProgressHUD showWithStatus:@"Sending..." maskType:WSProgressHUDMaskTypeBlack];
}

- (void)upLoadData{//更新个人记录
    
    [self.activeView startAnimating];
    
    
}

- (void)updateGoodDisease{
    [self.activeView startAnimating];
    
    
}

- (IBAction)btnClicked:(id)sender {
    [self.txtDesc resignFirstResponder];
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 0) {
        [self dismissEdit];
    }
    else {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(editView:didClickedOK:)]) {
            [self.delegate editView:self didClickedOK:self.txtDesc.text];
        }
    }
    
    //[[AllMethods getMainNavCtrl] dismissPopupViewControllerAnimated:YES completion:nil];
}

- (void)setShouldReset:(BOOL)bol{
    shouldReset = bol;
}

#pragma mark - Public 

- (void)setActAnimate:(BOOL)ani {
    
    if(ani) [self.activeView startAnimating];
    else [self.activeView stopAnimating];
    
}

- (void)showEdit {
    
    BookWorksAppAppDelegate *del = (BookWorksAppAppDelegate *)[UIApplication sharedApplication].delegate;
    maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    maskView.backgroundColor = RGBACOLOR(0, 0, 0, 0.f);
    [maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                           action:@selector(dismissEdit)]];
    
    [del.window.rootViewController.view addSubview:maskView];
    
    CGRect frame = self.view.frame;
    frame.origin.y = -frame.size.height;
    frame.origin.x = 0;
    frame.size.width = ScreenWidth;
    self.view.frame = frame;
    self.view.center = CGPointMake(ScreenWidth/2, self.view.center.y);
    [del.window.rootViewController.view addSubview:self.view];
    
    [UIView animateWithDuration:0.2f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         maskView.backgroundColor = RGBACOLOR(0, 0, 0, 0.65f);
                         self.view.center = CGPointMake(ScreenWidth/2, ScreenHeight/2+15);
                     }
                     completion:^(BOOL finished) {
                         
                         if (finished) {
                             [UIView animateWithDuration:0.1
                                              animations:^{
                                                  self.view.center = CGPointMake(ScreenWidth/2, ScreenHeight/2);
                                              }];
                         }
                         
                     }];
}

- (void)dismissEdit {
    
    [UIView animateWithDuration:0.15
                     animations:^{
                         self.view.center = CGPointMake(ScreenWidth/2, ScreenHeight/2+15);
                     }
     completion:^(BOOL finished) {
         
         if (finished) {
             [UIView animateWithDuration:0.2f
                                   delay:0
                                 options:UIViewAnimationOptionCurveEaseIn
                              animations:^{
                                  maskView.backgroundColor = RGBACOLOR(0, 0, 0, 0.f);
                                  self.view.center = CGPointMake(ScreenWidth/2, -maskView.frame.size.height);
                              }
                              completion:^(BOOL finished) {
                                  
                                  if (finished) {
                                      [maskView removeFromSuperview];
                                      [self.view removeFromSuperview];
                                  }
                                  
                              }];
         }
     }];
    
    
    
}

- (void)makeEditScale:(float)scale {
    
    [UIView animateWithDuration:0.15f
                     animations:^{
                         self.view.transform = CGAffineTransformMakeScale(scale, scale);
                     }];
}

#pragma mark - Keyboard Notification

- (void)keyboardFrameChange:(NSNotification *)notification {
    
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    [UIView animateWithDuration:0.27f
                     animations:^{
                         self.view.center = CGPointMake(self.view.center.x, ScreenHeight/2-44);
                     }];
    
}

- (void)keyboardDidHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.27f
                     animations:^{
                         self.view.center = CGPointMake(self.view.center.x, ScreenHeight/2);
                     }];
}


#pragma mark - SYS

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.txtDesc setContentInset:UIEdgeInsetsMake(-1, 0, 0, 0)];
}

- (id)initWithTitle:(NSString *)tit andDesc:(NSString *)desc withEditType:(EditType)type {
    self = [super init];
    if (self) {
        // Custom initialization
        strTitle = [NSString stringWithString:tit];
        strDesc = [NSString stringWithString:desc];
        eType = type;
    }
    return self;
}

- (id)initWithTitle:(NSString *)tit andDesc:(NSString *)desc andItem:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        // Custom initialization
        strTitle = [NSString stringWithString:tit];
        strDesc = [NSString stringWithString:desc];
        dicItem = [NSDictionary dictionaryWithDictionary:dic];
    }
    return self;
}

- (id)initWithTitle:(NSString *)tit andDesc:(NSString *)desc{
    self = [super init];
    if (self) {
        // Custom initialization
        strTitle = [NSString stringWithString:tit];
        strDesc = [NSString stringWithString:desc];
        eType = EDIT_PERSONAL_INFO;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.activeView setTintColor:NAV_COLOR];
    
    self.txtDesc.layer.borderColor = RGBACOLOR(150, 150, 150, 1.0f).CGColor;
    self.txtDesc.layer.borderWidth = .7f;
    
    self.txtDesc.text = strDesc;
    self.labTitle.text = strTitle;
    self.labTitle.font = [AllMethods getFontWithSize:17];
    self.txtDesc.font = [AllMethods getFontWithSize:16];
    
    self.view.backgroundColor = RGBACOLOR(246, 246, 246, 1.0f);
    
    [self.activeView stopAnimating];
    
    shouldReset = YES;
    
    self.txtDesc.placeholder = @"输入问题描述";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardFrameChange:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidChangeFrameNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
