//
//  QLLoginView.m
//  QLKaHui
//
//  Created by 刁志远 on 13-10-11.
//  Copyright (c) 2013年 DZY. All rights reserved.
//

#import "QLLoginView.h"

#import "BWUserRegView.h"

#import "ASIFormDataRequest.h"

@interface QLLoginView ()

@end

@implementation QLLoginView


#pragma mark - MY

- (void)resignKeyboard{
    [self.txtUserName resignFirstResponder];
    [self.txtUserPsd resignFirstResponder];
}

- (void)goUpView{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 2:{//注册
            BWUserRegView *reg = [[BWUserRegView alloc] init];
            [reg setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:reg animated:YES];
            break;
        }
        case 3:{//登录
            if ([self.txtUserName.text length] <=0 || [self.txtUserPsd.text length]<=0) {
                [WSProgressHUD showErrorWithStatus:@"请将信息填写完整..."];
                return;
            }
            [WSProgressHUD showWithStatus:@"正在登录..."];
            dispatch_queue_t queue = dispatch_queue_create("regist_queue", Nil);
            dispatch_async(queue, ^{
                NSString *surl = [NSString stringWithFormat:@"%@/%@?method=2&psd=%@&log_id=%@",kMAIN_URL,kUSER_URL,self.txtUserPsd.text,self.txtUserName.text];
                
                surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:surl];
                NSString *res = [[NSString alloc] initWithContentsOfURL:url
                                                               encoding:NSUTF8StringEncoding
                                                                  error:Nil];
                
                NSDictionary *dic = [res JSONValue];
//                NSLog(@"%@",dic);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    if ([[dic valueForKey:@"err_code"] intValue] == 0) {
                        [WSProgressHUD showSuccessWithStatus:@"登录成功"];
                        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                        [def setValue:self.txtUserName.text forKey:kUSER_NAME];
                        [def setValue:self.txtUserPsd.text forKey:kUSER_PSD];
                        [def setObject:dic forKey:kUSER_INFO];
                        [self performSelector:@selector(backToUpView:)
                                   withObject:Nil
                                   afterDelay:1.8f];
                    }
                    else{
                        [WSProgressHUD showErrorWithStatus:[dic valueForKey:@"err_desc"]];
                    }
                });
            });
            
            break;
        }
        default:
            break;
    }
}

- (IBAction)backToUpView:(id)sender{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SYS


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.名字不写东华  院感评价系统
    
    UIButton *btn1 = (UIButton *)[self.view viewWithTag:2];
    [btn1 setBackgroundColor:NAV_COLOR];
    
    UIButton *btn2 = (UIButton *)[self.view viewWithTag:3];
    [btn2 setBackgroundColor:NAV_COLOR];
    
    [self.txtUserName becomeFirstResponder];
    
    UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard)];
    [self.view addGestureRecognizer:reg];
    
    [self.view setBackgroundColor:MAIN_COLOR];
    
    self.navigationItem.title = @"登  录";
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
	[btn setFrame:CGRectMake(0, 8, 55, 35)];
	[btn setTitle:NSLocalizedString(@"Nav_Back", @"") forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:YES];
    [btn.titleLabel setFont:[UIFont fontWithName:[[NSUserDefaults standardUserDefaults] valueForKey:kFONT]
                                            size:15]];
	[btn addTarget:self action:@selector(backToUpView:) forControlEvents:UIControlEventTouchUpInside];
    
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
	
	UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.leftBarButtonItem = item;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
