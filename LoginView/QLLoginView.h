//
//  QLLoginView.h
//  QLKaHui
//
//  Created by 刁志远 on 13-10-11.
//  Copyright (c) 2013年 DZY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QLLoginView : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtUserPsd;
- (IBAction)btnClicked:(id)sender;
@end
