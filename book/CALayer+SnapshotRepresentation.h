#import <QuartzCore/QuartzCore.h>

@interface CALayer (SnapshotRepresentation)

- (NSDictionary *)snapshotRepresentation;
- (UIImage *)snapshotImage;

@end
