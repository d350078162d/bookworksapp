//
//  AllMethods.h
//  MyOwnApp
//
//  Created by diaozhiyuan on 11-11-2.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FMDatabase.h"
#import "FMResultSet.h"
#import "FMDatabaseAdditions.h"
#import "SBJson.h"

#define Key_ShuZhai @"shuzhai.plist"
#define kScreen @"Always"

#define Key_Color @"fontcolor"
#define Key_Font @"fontname"
#define Key_Size @"fontsize"
#define Key_Chap @"allchap"

#define kMAIN_URL @"http://dydog.sinaapp.com"
#define kBOOK_URL @"books.php"

#define kNAME @"name"
#define kURL @"url"
#define kDATE @"date"
#define kID @"id"

/*
 Family name: DFSongW3-GB
 2013-05-30 11:24:21.466 BookWorksApp[1766:c07]     Font name: DFSongW3
 
 Family name: DFKaiW5-GB
 2013-05-30 11:24:21.482 BookWorksApp[1766:c07]     Font name: DFKaiW5
 
 Family name: DFWaWaW5-GB
 2013-05-30 11:24:21.512 BookWorksApp[1766:c07]     Font name: DFWaWaW5
 
 Family name: DFYuanW3-GB
 2013-05-30 11:24:21.545 BookWorksApp[1766:c07]     Font name: DFYuanW3
 
 Family name: DFPOP1W5-GB
 2013-05-30 11:24:21.349 BookWorksApp[1766:c07]     Font name: DFPOP1W5
 
 Family name: DFShaoNvW5-GB
 2013-05-30 11:24:21.417 BookWorksApp[1766:c07]     Font name: DFShaoNvW5
 */

#define kFONT @"cur_use_font"

#define FONT_ARR [NSArray arrayWithObjects:@"DFSongW3-GB",@"DFKaiW5-GB",@"DFWaWaW5-GB",\
                    @"DFYuanW3-GB",@"DFPOP1W5-GB",@"DFShaoNvW5-GB", nil]
#define FONT_ARR_NAME [NSArray arrayWithObjects:@"宋体",@"楷体",@"娃娃体",\
                    @"圆体",@"POP体",@"少女体", nil]

typedef enum{
	TableViewConfirmOne=0,//table提示类型1
	TableViewConfirmTwo=1,//table提示类型2
}TableViewConfirm;

typedef enum {
    TableViewHeadRefresh=0,
    TableViewFootRefresh=1
}TableViewRefreshType;

typedef enum{
	PageLastRefreshYourCare=0,
	PageLastRefreshYourFans=1,
	PageLastRefreshFriendReviews=2,//好友点评
	PageLastRefreshFriendFrowordReviews=3,//好友转发页面
	PageLastRefreshRandomReviews=4,//随便看看页面
	PageLastRefreshMyReviews=5,//我的点评页面
	PageLastRefreshContrctMyReviews=6,//与我相关的评论
	PageLastRefreshSheReviews=7,//他的点评
	PageLastRefreshSpendingList=8,//我的消费记录
}PageLastRefreshTime;

@interface AllMethods : UIView {
    
}

+ (NSString *)applicationDocumentsDirectory;

+(UIColor *)getColorWithIndex:(NSInteger )index;
+(NSString *)getFilePath:(NSString *)fileName;
+(NSDictionary *)getShuzhai;
+(NSString *)getUserDef:(NSString *)key;
+(FMDatabase *)openDatabase;
+(NSMutableArray *)getShuzhaiTitle;
+(NSMutableArray *)getShuzhaiContent:(NSString *)key;

+(int)getCountOfRecord:(NSString *)rec inTable:(NSString *)table;

+(void)insertObjectInTitle:(NSString *)title withObject:(id)obj;
+(NSDictionary *)getRangeWithTitle:(NSString *)title;
+(void)insertChaptersWithTitle:(NSString *)title andChapter:(int)chap;

+(void)updatePages:(NSString *)json andTitle:(NSString *)title;
+(NSString *)getSaveRangeString:(NSString *)title;
+(CGSize)getNovelSizeWithTitle:(NSString *)title;
+(NSString *)getEncodeWithTitle:(NSString *)title;
+(void)insertNovelSizeWithTitle:(NSString *)title andSize:(CGSize)size andCode:(NSString *)code;

+(void)delRecordWith:(NSString *)title;
+(void)delShuzhaiWithContent:(NSString *)con;

+(void)dbWithSQL:(NSString *)sql;

@end
