//
//  AllMethods.m
//  MyOwnApp
//
//  Created by diaozhiyuan on 11-11-2.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AllMethods.h"


@implementation AllMethods

+(FMDatabase *)openDatabase{
	BOOL success;
	NSError *error;
	NSFileManager *fm = [NSFileManager defaultManager];
	NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"novel.sqlite"];
	success = [fm fileExistsAtPath:writableDBPath];
	if(!success){
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"novel.sqlite"];
		success = [fm copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
		if(!success){
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
															message:@"无法打开数据库"
														   delegate:nil
												  cancelButtonTitle:@"确定" otherButtonTitles:nil];
			[alert show];
			return nil;
		}
	}
	
	FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
	return db;
}

+ (NSString *)applicationDocumentsDirectory {
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
	//初始化临时文件路径
	NSString *folderPath = [path stringByAppendingPathComponent:@"data"];
	//创建文件管理
	NSFileManager *fileManager = [NSFileManager defaultManager];
	//判断temp文件夹是否存在
	BOOL fileExists = [fileManager fileExistsAtPath:folderPath];
	if (!fileExists) {//如果不存在说创建,因为下载时,不会自动创建文件夹
		[fileManager createDirectoryAtPath:folderPath
			   withIntermediateDirectories:YES
								attributes:nil
									 error:nil];
	}
	
	return folderPath;
}

+(CGSize)getNovelSizeWithTitle:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT content, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		NSString *str = [db stringForQuery:strSql];
		[db close];
		
		if (str==nil) {
			return CGSizeMake(0, 0);
		}
		NSArray *arr = [str componentsSeparatedByString:@","];
		CGSize size = CGSizeMake([[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue]);
		
		return size;
	}
	
	return CGSizeMake(0, 0);
}

+(NSString *)getEncodeWithTitle:(NSString *)title{
    FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT code, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		NSString *str = [db stringForQuery:strSql];
		[db close];
        
        return str;
	}
	
	return nil;
}

+(void)insertNovelSizeWithTitle:(NSString *)title andSize:(CGSize)size andCode:(NSString *)code{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *str = [NSString stringWithFormat:@"%f,%f",size.width,size.height];
		NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'record' ('title','content','code') VALUES ('%@','%@','%@')",title,str,code];
		[db executeUpdate:strSQL];
		[db close];
	}
}

+(void)delRecordWith:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"DELETE FROM 'main'.'record' WHERE title='%@'",title];
		[db executeUpdate:strSQL];
		[db close];
	}
}

+(void)dbWithSQL:(NSString *)sql{
    FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		[db executeUpdate:sql];
		[db close];
	}
}

+(void)delShuzhaiWithContent:(NSString *)con{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"DELETE FROM 'main'.'shuzhai' WHERE content='%@'",con];
		[db executeUpdate:strSQL];
		[db close];
	}
}

+(void)updatePages:(NSString *)json andTitle:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		int num = [db intForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) FROM 'record' WHERE title='%@'",title]];
		//NSLog(@"%d",num);
		NSString *strSQL = nil;
		if(num==0) strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'record' (title,content) VALUES ('%@','%@')",title,json];
		else strSQL = [NSString stringWithFormat:@"UPDATE 'main'.'record' SET content='%@' WHERE title='%@'",json,title];
		[db executeUpdate:strSQL];
		//if([db executeUpdate:strSQL])
//			NSLog(@"success");
//		else NSLog(@"failed");
		[db close];
	}
}

+(void)insertObjectInTitle:(NSString *)title withObject:(id)obj{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'record' ('title','content') VALUES ('%@','%@')",title,obj];
		[db executeUpdate:strSQL];
		//if([db executeUpdate:strSQL])
//			NSLog(@"success");
//		else NSLog(@"failed");
		[db close];
	}
}

+(void)insertChaptersWithTitle:(NSString *)title andChapter:(int)chap{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO 'main'.'chapters' ('title','chapter') VALUES ('%@','%d')",title,chap];
		[db executeUpdate:strSQL];
		//if([db executeUpdate:strSQL])
//			NSLog(@"success");
//		else NSLog(@"failed");
		[db close];
	}
}

+(NSString *)getSaveRangeString:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT content, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		NSString *str = [db stringForQuery:strSql];
		
		[db close];
		
		return str;
	}
	
	return nil;
}

+(NSDictionary *)getRangeWithTitle:(NSString *)title{
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSDictionary *dic = nil;
		NSString *strSql = [NSString stringWithFormat:@"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'record' WHERE title='%@'",title];
		FMResultSet *rs = [db executeQuery:strSql];
		while ([rs next]) {
			NSString *str = [rs objectForColumnName:@"content"];
			//NSLog(@"%@",str);
			str = [str stringByReplacingOccurrencesOfString:@"#" withString:@"\""];
			NSArray *arr = [str JSONValue];
			dic = [arr objectAtIndex:0];
		}
		[rs close];
		[db close];
		
		return dic;
	}
	
	return nil;
}

+(int)getCountOfRecord:(NSString *)rec inTable:(NSString *)table{//Select Name,Count(*) From A Group By Name Having Count(*) > 1

	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*), _ROWID_ 'NAVICAT_ROWID' FROM '%@' WHERE title='%@'",table,rec];
		int count = [db intForQuery:sql];
		[db close];
		return count;
	}
	
	return 0;
}

+(NSMutableArray *)getShuzhaiTitle{
	NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = @"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'shuzhai' GROUP BY title";
		FMResultSet *rs = [db executeQuery:strSql];
		while ([rs next]) {
			//NSLog(@"%@",[rs stringForColumn:@"title"]);
			if([rs stringForColumn:@"title"]) [arr addObject:[rs stringForColumn:@"title"]];
		}
		[rs close];
		[db close];
	}
	
	return arr;
}

+(NSMutableArray *)getShuzhaiContent:(NSString *)key{
	NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
	FMDatabase *db = [AllMethods openDatabase];
	if ([db open]) {
		NSString *strSql = [NSString stringWithFormat:@"SELECT *, _ROWID_ 'NAVICAT_ROWID' FROM 'shuzhai' WHERE title='%@'",key];
		FMResultSet *rs = [db executeQuery:strSql];
		//NSLog(@"%d\n%@",[rs columnCount],strSql);
		while ([rs next]) {
			if([rs stringForColumn:@"content"])[arr addObject:[rs stringForColumn:@"content"]];
		}
		[rs close];
		[db close];
	}
	
	return arr;
}

+(NSString *)getUserDef:(NSString *)key{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	return [def valueForKey:key];
}

+(NSDictionary *)getShuzhai{
	NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *str = [path objectAtIndex:0];
	str = [str stringByAppendingPathComponent:Key_ShuZhai];
	if ([[NSFileManager defaultManager] fileExistsAtPath:str]) {
		NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:str];
		return dic;
	}
	
	return nil;
}

+(NSString *)getFilePath:(NSString *)fileName{
	NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *str = [path objectAtIndex:0];
	return [str stringByAppendingPathComponent:fileName];
}

+(UIColor*)getColorWithIndex:(NSInteger)index{
	switch (index) {
		case 0:
			return [UIColor blackColor];
		case 1:
			return [UIColor blueColor];
		case 2:
			return [UIColor brownColor];
		case 3:
			return [UIColor cyanColor];
		case 4:
			return [UIColor darkGrayColor];
		case 5:
			return [UIColor grayColor];
		case 6:
			return [UIColor greenColor];
		case 7:
			return [UIColor lightGrayColor];
		case 8:
			return [UIColor magentaColor];
		case 9:
			return [UIColor orangeColor];
		case 10:
			return [UIColor purpleColor];
		case 11:
			return [UIColor redColor];
		case 12:
			return [UIColor whiteColor];
		case 13:
			return [UIColor yellowColor];
		
		default:
			break;
	}
	return nil;
}


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/



@end
