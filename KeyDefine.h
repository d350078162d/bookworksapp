//
//  KeyDefine.h
//  BookWorksApp
//
//  Created by 刁志远 on 2017/4/17.
//
//

#ifndef KeyDefine_h
#define KeyDefine_h


#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define ImageNamed(_pointer) [UIImage imageNamed:_pointer]

#define ORIGINAL_Y(obj) (obj.frame.size.height+obj.frame.origin.y)
#define ORIGINAL_X(obj) (obj.frame.size.width +obj.frame.origin.x)

#define SIZE_H(obj) (obj.frame.size.height)
#define SIZE_W(obj) (obj.frame.size.width)
#define USER_DEFAULT [NSUserDefaults standardUserDefaults]

#define LAST_READ @"last_read_book_name"


#define kOPEN_GID @"233471349752857016"

#define kMOB_ID @"687b38e5ae3047aeaeceddfc7183d049"
#define kADWO_ID @"a4b24c13f03d4fc09d0912923f545c88"
#define kBAIDU_ID @"fdc9d1e2"

#define kGDT_APP_ID @"1105888344"
#define kGDT_BANNER_ID @"4090610797815517"
#define kGDT_SPL_ID @"5080519797819660"
#define kGDT_INTER_ID @"3020613767717569"

#define kCHAT_NOTI_KEY @"receive_chat_noti"

#define kHAD_LAUNCH @"app_first_launch"

#define kUSER_HAD_LOGIN @"uesr_had_login"
#define kUSER_NAME @"saved_user_name"
#define kUSER_PSD @"saved_user_psd"
#define kUSER_INFO @"saved_user_info"

#define Key_ShuZhai @"shuzhai.plist"
#define kScreen @"Always"

#define Key_Color @"fontcolor"
#define Key_Font @"fontname"
#define Key_Chap @"allchap"

#define kBACK_COLOR @"view_back_color"
#define kTEXT_COLOR @"text_color"

#define kFONT_SIZE @"font_size"
#define kEXIT @"should_exit_app"

#define kMAIN_URL @"http://dydog.sinaapp.com"
#define kBOOK_URL @"books.php"
#define kUSER_URL @"user_info.php"
#define kAUTO_SAVE @"auto_save_progress"
#define kKEEP_ON @"keep_screen_always_on"

#define kZHUI_URL @"http://dydog.sinaapp.com"

#define kNAME @"name"
#define kURL @"url"
#define kDATE @"date"
#define kID @"id"
#define kSIZE @"size"

#pragma mark - YTX

#define CellMessageReadCount @"CellMessageReadCount"
#define CellMessageUnReadCount @"CellMessageUnReadCount"
#define Notification_ChangeMainDisplay @"Notification_ChangeMainDisplay"
#define NavAndBarHeight 64.0f

#define DefaultAppKey @"8aaf0708586c4340015885d3146f0b9f"
#define DefaultAppToke @"6787f16cf7d561a0499fe1b321751d77"

#pragma mark -----------------


#define kFONT @"cur_use_font"

#define FONT_ARR [NSArray arrayWithObjects:@"LEXUS-HeiS-Light-U",@"Arial",@"DFSongW3-GB",@"DFKaiW5-GB",@"DFWaWaW5-GB",\
@"DFYuanW3-GB",@"DFPOP1W5-GB",@"DFShaoNvW5-GB", nil]
#define FONT_ARR_NAME [NSArray arrayWithObjects:@"汉体",@"Arial",@"宋体",@"楷体",@"娃娃体",\
@"圆体",@"POP体",@"少女体", nil]

#define fileDest [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/"]


#define ScreenRect                          [[UIScreen mainScreen] bounds]
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height


#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

//Ext keyWord
#define kMesssageExtWeChat @"weichat"
#define kMesssageExtWeChat_ctrlType @"ctrlType"
#define kMesssageExtWeChat_ctrlType_enquiry @"enquiry"
#define kMesssageExtWeChat_ctrlType_inviteEnquiry @"inviteEnquiry"
#define kMesssageExtWeChat_ctrlType_transferToKfHint  @"TransferToKfHint"
#define kMesssageExtWeChat_ctrlType_transferToKf_HasTransfer @"hasTransfer"
#define kMesssageExtWeChat_ctrlArgs @"ctrlArgs"
#define kMesssageExtWeChat_ctrlArgs_inviteId @"inviteId"
#define kMesssageExtWeChat_ctrlArgs_serviceSessionId @"serviceSessionId"
#define kMesssageExtWeChat_ctrlArgs_detail @"detail"
#define kMesssageExtWeChat_ctrlArgs_summary @"summary"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define KNOTIFICATION_LOGINCHANGE @"loginStateChange"
#define KNOTIFICATION_CHAT @"chat"
#define KNOTIFICATION_SETTINGCHANGE @"settingChange"

#define CHATVIEWBACKGROUNDCOLOR [UIColor colorWithRed:0.936 green:0.932 blue:0.907 alpha:1]

#define kDefaultAppKey @"sipsoft#sandbox"
#define kDefaultCustomerName @"customer0"
//#define kDefaultCustomerName @"130121"
//#define kDefaultAppKey @"culiukeji#99baoyou"
//#define kDefaultCustomerName @"culiutest3"
#define kAppKey @"CSEM_appkey"
#define kCustomerName @"CSEM_name"
#define kCustomerNickname @"CSEM_nickname"

#define kPAGE_CHANGE_NOTI @"page_change_notification"
#define kPAGE_CLICK_NOTI @"page_click_notification"


//end

#define NAV_COLOR [UIColor colorWithRed:32.0/255.0 green:141.0/255.0 blue:219.0/255.0 alpha:1.0f]
#define NAV_TITLE_COLOR [UIColor colorWithRed:228.0/255.0 green:238.0/255.0 blue:273.0/255.0 alpha:1.0f]
#define NAV_SHADOW_COLOR [UIColor colorWithRed:60.0/255.0 green:70.0/255.0 blue:84.0/255.0 alpha:1.0f]
#define CELL_COLOR [UIColor colorWithRed:35.0/255.0 green:148.0/255.0 blue:232.0/255.0 alpha:.9f]

#define MAIN_COLOR [UIColor colorWithRed:88.0f/255.0 green:88.0f/255.0 blue:88.0f/255.0 alpha:.9]

#define kREAD_CONFIG @"ReadConfig"

#endif /* KeyDefine_h */
