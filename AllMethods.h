//
//  AllMethods.h
//  MyOwnApp
//
//  Created by diaozhiyuan on 11-11-2.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <FMDB/FMDB.h>

#import <WSProgressHUD/WSProgressHUD.h>
#import "BookWorksAppAppDelegate.h"
#import "UIButton+Extension.h"
#import <objc/runtime.h>
#import <MJRefresh/MJRefresh.h>

#import "KeyDefine.h"

//#define kFREE 1

#if kFREE

#import "GDTMobBannerView.h"
#import "GDTMobInterstitial.h"
#import "GDTSplashAd.h"

#endif


#import <UIImageView+WebCache.h>

typedef enum {
    
    REQUEST_GET=0,
    REQUEST_POST,
    
}RequestType;

typedef void(^HandleRequestDataEvent)(id);

@interface AllMethods : UIView {
    
}

+ (void)clearDocumentTrash;

+ (NSString *)applicationDocumentsDirectory;
+ (NSString *)tempDirectory;

+ (void)createTableBuddy;
+ (void)saveUserInfoWithUID:(NSString *)uid nick:(NSString *)nick andHead:(NSString *)head;
+ (NSDictionary *)getUserInfoByUID:(NSString *)uid;
+ (NSDictionary *)getAllUserInfo;

+(UIColor *)getColorWithIndex:(NSInteger )index;
+(NSString *)getFilePath:(NSString *)fileName;
+(NSDictionary *)getShuzhai;
+(NSString *)getUserDef:(NSString *)key;
+(FMDatabase *)openDatabase;
+(NSMutableArray *)getShuzhaiTitle;
+(NSMutableArray *)getShuzhaiContent:(NSString *)key;

+ (BOOL)createReadProgressTable;
+ (BOOL)saveReadProgressInfo:(NSDictionary *)dic;
+ (NSString *)getReadProgressWithKey:(NSString *)key;
+ (BOOL)deleteReadProgressWithKey:(NSString *)key;


+ (void)clearOldReadInfoWithBookIndex:(NSInteger)bookIndex;
+ (void)deleteBookWithIndex:(NSInteger)delIndex;

+ (BOOL)createDownloadListTable;
+ (BOOL)saveDownloadInfo:(NSDictionary *)dic;
+ (NSDictionary *)getBookDownloadList;

+ (BOOL)createNewUpdateTable;
+ (BOOL)saveNewUpdateInfo:(NSDictionary *)dic;
+ (BOOL)getCntOfBookIdInNewUpdateTable:(NSString *)href;
+ (BOOL)delBookInNewUpdateTable:(NSString *)href;
+ (NSArray *)getBookListInNewUpdateTable;

+(int)getCountOfRecord:(NSString *)rec inTable:(NSString *)table;

+(void)insertObjectInTitle:(NSString *)title withObject:(id)obj;
+(NSDictionary *)getRangeWithTitle:(NSString *)title;
+(void)insertChaptersWithTitle:(NSString *)title andChapter:(int)chap;

+(void)updatePages:(NSString *)json andTitle:(NSString *)title;
+(NSString *)getSaveRangeString:(NSString *)title;
+(CGSize)getNovelSizeWithTitle:(NSString *)title;
+(NSString *)getEncodeWithTitle:(NSString *)title;
+(void)insertNovelSizeWithTitle:(NSString *)title andSize:(CGSize)size andCode:(NSString *)code;

+(void)delRecordWith:(NSString *)title;
+(void)delShuzhaiWithContent:(NSString *)con;

+(void)dbWithSQL:(NSString *)sql;

+(NSDate *)getPriousDateFromDate:(NSDate *)date withHour:(NSInteger)hour;

+ (void)showAltMsg:(NSString *)msg;

+ (float)sysversion;

+ (UIImage *)getImageWithColor:(UIColor *)color andRect:(CGRect)rect;
+ (float)getEdge;
+ (float)getContentInset;

+ (UIFont *)getFontWithSize:(float)size;
+ (UIFont *)getChapterFontWithSize:(float)size;
+ (NSArray *)getBookBagIDList ;
+ (NSArray *)getBookBagList;
+ (NSDictionary *)getBookWithBookID:(NSString *)bid;

+ (void)insertBookWithSQL:(NSString *)sql;
+ (NSArray *)getLocalBookID;
+ (void)delUpdateBookWithID:(NSDictionary *)bid;


+ (void)clearCacheWithExtention:(NSString *)ext;
+ (UIBarButtonItem *)getButtonBarItemWithImageName:(NSString *)str andSelect:(SEL)select andTarget:(id)obj;
+ (UIBarButtonItem *)getButtonBarItemWithTitle:(NSString *)str andSelect:(SEL)select andTarget:(id)obj;

+ (BOOL)isRarFile:(NSData *)data;

+ (void)clearDocumentTrash;

+ (NSString *)updateURL;

+ (void)requestWithType:(RequestType )type andPara:(id)obj andUrl:(NSString *)surl andGetDataBlock:(HandleRequestDataEvent)block;

+(void)separateChapter:(NSMutableArray *)chapters content:(NSString *)content andContentFrame:(CGRect) frame andFontSize:(float)font_size andLineSpace:(float)space;

@end








@interface UIButton (IndexPath)

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSString *submitKey;
@property (nonatomic, strong) NSString *extFlag;
@property (nonatomic, strong) UITextView *txtView;

@end

@interface NSData (gb18030)

- (NSData *)dataByHealingGB18030Stream;

@end

@interface UIColor (UIColorToNSString)

+ (UIColor *)colorFromeString:(NSString *)str;

+ (UIColor *)colorFromeString:(NSString *)str withAlpha:(float) alpha;

- (NSString *)colorToString;

+ (CGFloat )colorSumFromString:(NSString *)str;

- (UIColor *) mix: (UIColor *) color;
- (UIColor *) darken;
- (UIColor *) lighten ;

@end


@interface UIImage (Tint)

- (UIImage *) imageWithTintColor:(UIColor *)tintColor;
- (UIImage *) imageWithGradientTintColor:(UIColor *)tintColor;

@end



@interface UIView (Boom)
-(void)boom;
@end

@interface UIViewController (StatusBar)

- (UIStatusBarStyle )preferredStatusBarStyle;
- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation ;

@end



