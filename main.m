//
//  main.m
//  BookWorksApp
//
//  Created by diaozhiyuan on 11-11-3.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BookWorksAppAppDelegate.h"

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BookWorksAppAppDelegate class]));
    }
    
//    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
//    int retVal = UIApplicationMain(argc, argv, nil, nil);
//    [pool release];
//    return retVal;
}
